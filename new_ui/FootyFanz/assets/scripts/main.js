"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var clubs = [];

clubs = [[{
	clubName: "New castle",
	clubLogo: "./assets/img/clubs/newcastle.png",
	clubColor: "#ffffff",
	clubLink: "#"
}, {
	clubName: "Arsenal",
	clubLogo: "./assets/img/clubs/arsenal.png",
	clubColor: "#cc0000",
	clubLink: "#arsenal"
}, {
	clubName: "Chelsea",
	clubLogo: "./assets/img/clubs/arsenal.png",
	clubColor: "#00458a",
	clubLink: "#"
}, {
	clubName: "Manchester Utd",
	clubLogo: "./assets/img/clubs/manu.png",
	clubColor: "#ce0000",
	clubLink: "#"
}, {
	clubName: "Totten Ham",
	clubLogo: "./assets/img/clubs/tottenham.png",
	clubColor: "#ffffff",
	clubLink: "#"
}, {
	clubName: "Everton",
	clubLogo: "./assets/img/clubs/everton.png",
	clubColor: "#0647af",
	clubLink: "#"
}, {
	clubName: "Manchester City",
	clubLogo: "./assets/img/clubs/mancester city.png",
	clubColor: "#0647af",
	clubLink: "#"
}, {
	clubName: "LiverPool",
	clubLogo: "./assets/img/clubs/liverpool.png",
	clubColor: "#b70000",
	clubLink: "#"
}, {
	clubName: "Manchester Utd",
	clubLogo: "./assets/img/clubs/manu.png",
	clubColor: "#ce0000",
	clubLink: "#"
}], [{
	clubName: "Arsenal",
	clubLogo: "./assets/img/clubs/arsenal.png",
	clubColor: "#cc0000",
	clubLink: "#arsenal"
}, {
	clubName: "Chelsea",
	clubLogo: "./assets/img/clubs/arsenal.png",
	clubColor: "#00458a",
	clubLink: "#"
}, {
	clubName: "Manchester Utd",
	clubLogo: "./assets/img/clubs/manu.png",
	clubColor: "#ce0000",
	clubLink: "#"
}, {
	clubName: "Totten Ham",
	clubLogo: "./assets/img/clubs/tottenham.png",
	clubColor: "#ffffff",
	clubLink: "#"
}, {
	clubName: "Everton",
	clubLogo: "./assets/img/clubs/everton.png",
	clubColor: "#0647af",
	clubLink: "#"
}, {
	clubName: "Manchester City",
	clubLogo: "./assets/img/clubs/mancester city.png",
	clubColor: "#0647af",
	clubLink: "#"
}, {
	clubName: "LiverPool",
	clubLogo: "./assets/img/clubs/liverpool.png",
	clubColor: "#b70000",
	clubLink: "#"
}, {
	clubName: "New castle",
	clubLogo: "./assets/img/clubs/newcastle.png",
	clubColor: "#ffffff",
	clubLink: "#"
}]];

var FootyFanz = function () {
	function FootyFanz() {
		_classCallCheck(this, FootyFanz);

		// code
		this.clubs1 = clubs[0];
		this.clubs2 = clubs[1];
	}

	// methods


	_createClass(FootyFanz, [{
		key: "loadClubs",
		value: function loadClubs() {
			this.clubs1.map(function (element, index) {
				var club = "\n\t\t\t\t<div class=\"footy-carusel__item\">\n                    <div class=\"footy-club__items\" style=\"background-color: " + element.clubColor + "\">\n                        <img src=\"" + element.clubLogo + "\" alt=\"\">\n                    </div>\n                    <a href=\"" + element.clubLink + "\">" + element.clubName + "</a>\n                </div>";

				if (index <= 7) {
					$("#footy-carousel__content").append(club);
				};
			});
		}
	}, {
		key: "carouselControls",
		value: function carouselControls() {
			var _this = this;

			$("#footy-control__next").on("click", function (e) {
				e.preventDefault();
				var content = document.getElementById('footy-carousel__content');
				content.innerHTML = "";
				// $("#footy-carousel__content").html(clea
				_this.clubs2.map(function (element, index) {
					var club = "\n\t\t\t\t\t<div class=\"footy-carusel__item\">\n\t                    <div class=\"footy-club__items\" style=\"background-color: " + element.clubColor + "\">\n\t                        <img src=\"" + element.clubLogo + "\" alt=\"\">\n\t                    </div>\n\t                    <a href=\"" + element.clubLink + "\">" + element.clubName + "</a>\n\t                </div>";
					$("#footy-carousel__content").append(club);
				});
			});

			$("#footy-control__prev").on("click", function (e) {
				e.preventDefault();
				var content = document.getElementById('footy-carousel__content');
				content.innerHTML = "";
				_this.loadClubs();
			});
		}
	}]);

	return FootyFanz;
}();