let clubs = [];

clubs = [[
	
	{
		clubName: "New castle",
		clubLogo: "./assets/img/clubs/newcastle.png",
		clubColor: "#ffffff",
		clubLink: "#"
	},
	{
		clubName: "Arsenal",
		clubLogo: "./assets/img/clubs/arsenal.png",
		clubColor: "#cc0000",
		clubLink: "#arsenal"
	},
	{
		clubName: "Chelsea",
		clubLogo: "./assets/img/clubs/arsenal.png",
		clubColor: "#00458a",
		clubLink: "#"
	},
	{
		clubName: "Manchester Utd",
		clubLogo: "./assets/img/clubs/manu.png",
		clubColor: "#ce0000",
		clubLink: "#"
	},
	{
		clubName: "Totten Ham",
		clubLogo: "./assets/img/clubs/tottenham.png",
		clubColor: "#ffffff",
		clubLink: "#"
	},
	{
		clubName: "Everton",
		clubLogo: "./assets/img/clubs/everton.png",
		clubColor: "#0647af",
		clubLink: "#"
	},
	{
		clubName: "Manchester City",
		clubLogo: "./assets/img/clubs/mancester city.png",
		clubColor: "#0647af",
		clubLink: "#"
	},
	{
		clubName: "LiverPool",
		clubLogo: "./assets/img/clubs/liverpool.png",
		clubColor: "#b70000",
		clubLink: "#"
	},
	{
		clubName: "Manchester Utd",
		clubLogo: "./assets/img/clubs/manu.png",
		clubColor: "#ce0000",
		clubLink: "#"
	},

],
[
	{
		clubName: "Arsenal",
		clubLogo: "./assets/img/clubs/arsenal.png",
		clubColor: "#cc0000",
		clubLink: "#arsenal"
	},
	{
		clubName: "Chelsea",
		clubLogo: "./assets/img/clubs/arsenal.png",
		clubColor: "#00458a",
		clubLink: "#"
	},
	{
		clubName: "Manchester Utd",
		clubLogo: "./assets/img/clubs/manu.png",
		clubColor: "#ce0000",
		clubLink: "#"
	},
	{
		clubName: "Totten Ham",
		clubLogo: "./assets/img/clubs/tottenham.png",
		clubColor: "#ffffff",
		clubLink: "#"
	},
	{
		clubName: "Everton",
		clubLogo: "./assets/img/clubs/everton.png",
		clubColor: "#0647af",
		clubLink: "#"
	},
	{
		clubName: "Manchester City",
		clubLogo: "./assets/img/clubs/mancester city.png",
		clubColor: "#0647af",
		clubLink: "#"
	},
	{
		clubName: "LiverPool",
		clubLogo: "./assets/img/clubs/liverpool.png",
		clubColor: "#b70000",
		clubLink: "#"
	},
	{
		clubName: "New castle",
		clubLogo: "./assets/img/clubs/newcastle.png",
		clubColor: "#ffffff",
		clubLink: "#"
	},
]
]

class FootyFanz {
	constructor() {
		// code
		this.clubs1 = clubs[0];
		this.clubs2 = clubs[1];
	}

	// methods
	loadClubs() {
		this.clubs1.map((element, index) => {
			let club = `
				<div class="footy-carusel__item">
                    <div class="footy-club__items" style="background-color: ${element.clubColor}">
                        <img src="${element.clubLogo}" alt="">
                    </div>
                    <a href="${element.clubLink}">${element.clubName}</a>
                </div>`;

			if (index <= 7) {
				 $("#footy-carousel__content").append(club);
			};
			
		});
	}
	carouselControls() {
		$("#footy-control__next").on("click", (e)=> {
			e.preventDefault();
			let content = document.getElementById('footy-carousel__content');
			content.innerHTML = "";
			// $("#footy-carousel__content").html(clea
			this.clubs2.map((element, index) => {
				let club = `
					<div class="footy-carusel__item">
	                    <div class="footy-club__items" style="background-color: ${element.clubColor}">
	                        <img src="${element.clubLogo}" alt="">
	                    </div>
	                    <a href="${element.clubLink}">${element.clubName}</a>
	                </div>`;
				$("#footy-carousel__content").append(club);
			});
				
		});

		$("#footy-control__prev").on("click", (e)=> {
			e.preventDefault();
			let content = document.getElementById('footy-carousel__content');
			content.innerHTML = "";
			this.loadClubs();
		});

	}
}
