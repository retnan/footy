var shareImageButton = document.querySelector('#share-image-button');
var createPostArea = document.querySelector('#create-post');
var closeCreatePostModalButton = document.querySelector('#close-create-post-modal-btn');

function openCreatePostModal() {
	createPostArea.style.display = 'block';

	/*if (defferedPrompt) {
		defferedPrompt.prompt();

		defferedPrompt.userChoice.then(function(choiceResult){
			console.log(choiceResult.outcome);
			if (choiceResult.outcome === 'dismissed') {
				console.log('User canceled installation');
			}else{
				console.log('Installation added to home screen');
			}
		});

		defferedPrompt = null;
	}*/

}

function closeCreatePostModal() {
	createPostArea.style.display = 'none';
}

if (null != shareImageButton) {

	shareImageButton.addEventListener('click', openCreatePostModal);

}



if (null != closeCreatePostModalButton) {
	closeCreatePostModalButton.addEventListener('click', closeCreatePostModal);
}

document.addEventListener("DOMContentLoaded", function() { 
	if (defferedPrompt) {
		defferedPrompt.prompt();
		defferedPrompt.userChoice.then(function(choiceResult){
			console.log(choiceResult.outcome);
			if (choiceResult.outcome === 'dismissed') {
				console.log('User canceled installation');
			}else{
				console.log('Installation added to home screen');
			}
		});

		defferedPrompt = null;
	};


	console.log(usrExist);
});
