var defferedPrompt;
var usrExist;
var loginBtn = document.querySelector('#userLoginBtn');
var crtAccBtn = document.querySelector('#userCreateAcc');

if (!window.Promise) {
	window.Promise = Promise;
};

if ('serviceWorker' in navigator) {

	navigator.serviceWorker.register('https://footyfanz.com/public/sw.js')
	.then(function(){
		console.log('Serviccce Worker Registered');
	});
}

window.addEventListener('beforeinstallprompt', function(event){
	console.log(event);
	//event.preventDefault();
	defferedPrompt = event;
	return false;
});


document.addEventListener("DOMContentLoaded", function() { 
	if (defferedPrompt) {
		defferedPrompt.prompt();
		defferedPrompt.userChoice.then(function(choiceResult){
			console.log(choiceResult.outcome);
			if (choiceResult.outcome === 'dismissed') {
				console.log('User canceled installation');
			}else{
				console.log('Installation added to home screen');
			}
		});

		defferedPrompt = null;
	};

});


if (null != loginBtn) {
	loginBtn.addEventListener('click', function(event){
		event.preventDefault();

		var usremail = document.getElementById('username').value,
		usrpassword = document.getElementById('pwd').value;
	//alert(email);

	fetch('https://www.footyfanz.com/user/pwa', {
		method: "POST",
		headers: {
			"Accept": "application/json"
		},
		body: JSON.stringify({
			email: usremail,
			password: usrpassword
		})

	}).then(function(response){
		console.log(response);
		return response.json();
		/*if (usrExist.error_msg != null) {
			return window.location.href = "/home";
		};*/

	}).then(function(data){
		console.log(data);
		usrExist = data;

		if (data.error_msg === null) {
			return window.location.href = "/home?usr="+data.user_id+"&c_logo="+data.club_image+"&usr_img="+data.image+"&usr_em="+data.email;
		};
		

	}).catch(function(err){
		console.log(err);
	});
	
});
}

if (null != crtAccBtn) {
	crtAccBtn.addEventListener('click', function(event){
		event.preventDefault();

		fetch('https://www.footyfanz.com/user/pwaRegister', {
			method: "POST",
			headers: {
				"Accept": "application/json"
			},
			body: JSON.stringify({
				names: document.getElementById('name').value,
				email: document.getElementById('email').value,
				username: document.getElementById('usrname').value,
				password: document.getElementById('pwd').value,
				club: document.getElementById('club').value
			})

		}).then(function(response){
			console.log(response);
			return response.json();

		}).then(function(data){
			console.log(data);
			if (data.error_msg === null) {
				alert(data.message);
			}else{
				alert(data.error_msg);
			}


		}).catch(function(err){
			console.log(err);
		});

	});
}

/*
window.addEventListener('DOMContentLoaded', function(event){
	fetch('https://www.footyfanz.com/pwanews/').then(function(response){
		console.log(response);
		return response;
	}).then(function(data){
		console.log(data);
		document.getElementById('mainContent').load(data.url);
		
	}).catch(function(err){
		console.log(err);
	});
});
*/

