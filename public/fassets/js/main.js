$(document).on('click', ".ajax[data-toggle='modal']", function () {
    var modal = $(this).data("target");
    var url = $(this).data("url");
    $(".modal-content", modal).load(url);
});
$(document).on('click', ".btn_message:not(.disabled)", function () {

    if ($("#messageForm").valid()) {
        $(this).addClass("disable");
        var post_data = $("#messageForm").serialize();
        $.post(base_url + "profile/send_message", post_data, function (res) {
            $(".msg_success").hide().fadeIn();
            $("#messageForm")[0].reset();
            $(".btn_message").removeClass(".disabled");
        });
    }
});
var handleValidationDefault = function () {
    if ($().validate) {
        $.validator.setDefaults({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            invalidHandler: function (event, validator) { //display error alert on form submit
                $('.alert-success', $(this)).hide();
                $('.alert-error', $(this)).fadeIn();
            },
            highlight: function (element) { // hightlight error inputs

                $(element)
                        .closest('.help-block').removeClass('ok'); // display OK icon
                $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                        .addClass('valid').addClass('help-block') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group

            },
        });
    }

}


$(document).ready(function (e) {
    handleValidationDefault();
    $(".loaddata").each(function () {
        var ac_con = $(this);
        loadData(ac_con);
    });
});
function loadData(ac_con) {
    ac_con.append(article_loader);
    var post_data = ac_con.data();
    $.post(ac_con.data('url'), post_data, function (res) {
        ac_con.find(".article_loader").remove();
        if (ac_con.data("ltype") == "append") {
            ac_con.append(res);
        } else {
            ac_con.html(res);
        }
    });
}

function popupwindow(url, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

$(document).on("click", '.btnfbShare', function (e) {
    e.preventDefault();
    $title = $(this).data('title');
    if ($title = "") {
        $title = "FootyFanz.com"
    }
    var fbs = popupwindow("https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent($(this).data('href')), $title, "600", "400");
    return false;
});
$(document).on("click", '.btnttShare', function (e) {
    e.preventDefault();
    $title = $(this).data('title');
    if ($title = "") {
        $title = "FootyFanz.com"
    }
    var ttr = popupwindow("http://twitter.com/share?text=" + $title + "&url=" + encodeURIComponent($(this).data('href')), $title, "600", "400");
    return false;
});
$(document).on("click", '.btngglShare', function (e) {
    e.preventDefault();
    $title = $(this).data('title');
    if ($title = "") {
        $title = "FootyFanz.com"
    }
    var ggl = popupwindow("https://plus.google.com/share?url=" + encodeURIComponent($(this).data('href')), $title, "600", "400");
    return false;
});
$(document).on("click", '.btnlnkShare', function (e) {
    e.preventDefault();
    $title = $(this).data('title');
    if ($title = "") {
        $title = "FootyFanz.com"
    }
    $desc = $(this).data('desc');
    var lnd = popupwindow("http://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent($(this).data('href')) + "&title=" + $title + "&summary=" + encodeURIComponent($desc), $title, "600", "400");
    return false;
});
$(document).on("click", '.btnstuShare', function (e) {
    e.preventDefault();
    $title = $(this).data('title');
    if ($title = "") {
        $title = "FootyFanz.com"
    }
    $desc = $(this).data('desc');

    var lnd = popupwindow("https://www.stumbleupon.com/submit?url=" + encodeURIComponent($(this).data('href')) + "&title=" + $title + "&summary=" + encodeURIComponent($desc), $title, "600", "400");
    return false;
});
$(document).on("click", '.btnpinShare', function (e) {
    e.preventDefault();
    $title = $(this).data('title');
    if ($title = "") {
        $title = "FootyFanz.com"
    }
    $desc = $(this).data('desc');
    $image = $(this).data('image');
    var lnd = popupwindow("https://pinterest.com/pin/create/button/?url=" + encodeURIComponent($(this).data('href')) + "&media=" + $image + "&description=" + encodeURIComponent($desc), $title, "600", "400");
    return false;
});

$(document).on("click", ".btn_follow", function () {
    var follow_btn = $(this);
    var post_data = {following_id: $(this).data("id")};
    $.post(base_url + "profile/follow", post_data, function (res) {
        if(res.status=="1"){
            follow_btn.hide();
            $(".followers").html(parseInt($(".followers").html()) + 1);
            $(".btn_unfollow").fadeIn();
        }else{
            window.location.href=site_url+"login";
        }
        
    },"json");
});
$(document).on("click", ".btn_unfollow", function () {
    var unfollow_btn = $(this);
    var uname = $(".userprofiledata").data("uname");
    var post_data = {following_id: $(this).data("id")};
    $.confirm({
        title: 'Unfollow',
        content: 'Are you sure to unfollow ' + uname,
        confirmButton: 'Unfollow',
        confirmButtonClass: 'btn-danger',
//                                    icon: 'fa fa-question-circle',
        animation: 'scale',
        animationClose: 'top',
        opacity: 0.5,
        confirm: function () {
            $.post(base_url + "profile/unfollow", post_data, function (res) {
                if(res.status=="1"){
                unfollow_btn.hide();
                $(".followers").html(parseInt($(".followers").html()) - 1);
                $(".btn_follow").fadeIn();
            }else{
                window.location.href=site_url+"login";
            }
            },"json");

        }
    });
});

$(document).on("keypress", ".autoline", function () {
    $(this).height("1px");
    $(this).height(($(this).prop('scrollHeight')-10) + "px");
});