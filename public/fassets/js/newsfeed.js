/* global $ */
/* global base_url */
/* global site_url */
/* global EmojiPicker */
var page = 1;
function getFeeds() {
    var post_data = {page: page, user_id: $(".feedcontainer").data("user_id"), ref_id: $(".feedcontainer").data("ref_id")};
    $('.commentloader').addClass('loading');
    $.post(base_url + "profile/getfeeds", post_data, function(res) {
        $('.commentloader').removeClass('loading');
        $(".feedcontainer").append(res);
        window.emojiPicker.discover();
    });
}
function getLatestFeeds() {
    var post_data = {last_id: $(".feedsingle:first").data("last_feed"), user_id: $(".feedcontainer").data("user_id"), ref_id: 0};
    $.post(base_url + "profile/getlatestfeeds", post_data, function(res) {
        
        $(".feedcontainer").prepend(res);
        window.emojiPicker.discover();
    });
}
$(document).on('click', ".loadmore", function(e) {
    page++;
    getFeeds();
});
$(document).on('click', ".more_comments", function(e) {
    var com_ccontainer = $(this).closest(".feedfooter").find(".feedcomments");
    var btn_more = $(this);
    var post_data = $(this).closest(".feedfooter").find(".commentsingle:first").data();
    $.post(base_url + "profile/getoldcomments", post_data, function(res) {
        com_ccontainer.prepend(res);
        $(".commentsingle", com_ccontainer).slideDown();
        btn_more.fadeOut();
    });
});
$(document).on('click', ".btn_remove_comment", function(e) {
    var com_container = $(this).closest(".commentsingle");
    var post_data = $(this).data();
    var com_count = $(this).closest(".feedfooter").find(".com_count");
    $.confirm({
        title: 'Remove Comment',
        content: 'Are you sure to remove this comment',
        confirmButton: 'Remove',
        confirmButtonClass: 'btn-danger',
        animation: 'scale',
        animationClose: 'top',
        opacity: 0.5,
        confirm: function() {
            $.post(base_url + "profile/deletecomment", post_data, function(res) {
                com_container.fadeOut(function() {
                    com_container.remove();
                    com_count.html(parseInt(com_count.html()) - 1);
                });
            });
        }
    });
});
$(document).on('click', ".poll_answer", function(e) {

    var post_data = $(this).data();
    $.confirm({
        title: 'Poll Voting',
        content: 'Are you sure to Vote your Option',
        confirmButton: 'Sure',
        confirmButtonClass: 'btn-success',
        animation: 'scale',
        animationClose: 'top',
        opacity: 0.5,
        confirm: function() {
            $.post(base_url + "home/save_poll", post_data, function(res) {
                alert("Your Vote has been Saved");
            });
        }
    });
});
$(document).on('click touchend', ".btn_like", function(e) {
    //alert("Hello");
    e.stopPropagation();
    e.preventDefault();
    var $this = $(this);
    var status = $(this).data("like_status");
    var post_data = $(this).data();
    var like_count = $(this).closest(".feedfooter").find(".like_count");
    $.post(base_url + "profile/managelike", post_data, function(res) {
        if (status == 0) {
            like_count.html(parseInt(like_count.html()) + 1);
            $this.html('<i class="fa fa-thumbs-up"></i> Liked');
            $this.data("like_status", "1");
        } else {
            like_count.html(parseInt(like_count.html()) - 1);
            $this.html('<i class="fa fa-thumbs-up"></i> Like');
            $this.data("like_status", "0");
        }

    });
});
$(document).on('click touchend', ".btn_share", function(e) {
    e.stopPropagation();
    e.preventDefault();
    var $this = $(this);
    var post_data = $(this).data();
    $.confirm({
        title: 'Share Newsfeed',
        content: 'Are you sure to Share this News',
        confirmButton: 'Share',
        confirmButtonClass: 'btn-success',
        animation: 'scale',
        animationClose: 'top',
        opacity: 0.5,
        confirm: function() {
            $.post(base_url + "profile/sharefeed", post_data, function(res) {
                getLatestFeeds();
            });
        }
    });
});
$(document).on('click', ".delete_feed", function(e) {
    var $this = $(this);
    var post_data = $(this).data();
    var feed_block = $(this).closest(".feedsingle");
    $.confirm({
        title: 'Remove Feed',
        content: 'Are you sure to remove this feed and all its contents',
        confirmButton: 'Remove Feed',
        confirmButtonClass: 'btn-danger',
        animation: 'scale',
        animationClose: 'top',
        opacity: 0.5,
        confirm: function() {
            $.post(base_url + "profile/delete_feed", post_data, function(res) {
                feed_block.fadeOut(function() {
                    feed_block.remove();
                });
            });
        }
    });
});

$(document).on('click', ".btnpostcomment", function(e) {
    var form = $(this).closest(".feedcommentspost").find("form");
    var com_block = $(this).closest(".feedfooter").find(".feedcomments");
    var com_count = $(this).closest(".feedfooter").find(".com_count");
    var post_data = form.serialize();
    form.find(".com_text").attr("disabled", "disabled");
    $.post(base_url + "profile/savecomment", post_data, function(res) {
        com_block.append(res.data);
        com_count.html(parseInt(com_count.html()) + parseInt(res.count));
        $(".commentsingle", com_block).slideDown();
        form.find('[contenteditable=true]').empty();
        form.find(".com_text").removeAttr("disabled");
        form.find(".com_text").val("");
        form.find(".com_last_id").val(res.last_id);
    }, 'json');
});

$(document).ready(function(e) {
    getFeeds();
    $(".fancybox-effects-a").fancybox();
    $('.fancybox-media')
        .attr('rel', 'media-gallery')
        .fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            prevEffect: 'none',
            nextEffect: 'none',
            arrows: false,
            helpers: {
                media: {},
                buttons: {}
            }
        });
    $(".btnactionfeed").on('click', function() {
        if ($.trim($("#posttext").val()) != "" || $("#filetype").val() != "N") {
            var cntval = $("#posttext").val().length;
            if (cntval > 160) {
                $("#posttext").trigger("keyup");
            } else {
                $('#postform').submit();
            }

        }
    });
    $(document).on("change", "#fileimage", function() {
        if ($("#filetype").val() == "I" && $(this).val() == "") {
            $(".feedfilename").html("");
            $("#filetype").val("N");
        }
        if ($(this).val() != "") {
            $("#filetype").val("I");
            $(".feedfilename").html(getFileName($(this).val()));
        }
    });
    $(document).on("change", "#filevideo", function() {
        if ($("#filetype").val() == "V" && $(this).val() == "") {
            $(".feedfilename").html("");
            $("#filetype").val("N");
        }
        if ($(this).val() != "") {
            $("#filetype").val("V");
            $(".feedfilename").html(getFileName($(this).val()));
        }
    });
});
function getFileName(mainname) {
    var path = mainname;
    var fileName = path.match(/[^\/\\]+$/);
    return fileName;
}

$(document).ready(function(e) {
    $('#postform').ajaxForm({
        //dataType: 'json',
        beforeSubmit: function() {
            $(".feedposter").addClass("loading");
            $(".uploadpouter").show();
            $(".uploadpinner").css("width", "0%");
        },
        uploadProgress: function(event, position, total, percentComplete) {
            if (percentComplete == 100) {
                $(".uploadpouter").hide();
            } else {
                $(".uploadpinner").css("width", percentComplete + '%');
            }
        },
        success: function(res) {
            $(".uploadpouter").hide();
            $(".feedposter").removeClass("loading");
            $(".feedfilename").html("");
            $("#filetype").val("N");
            $('#postform')[0].reset();
            // $('#postform').find("input[type=text], textarea").val("");
            $('#postform').find('[contenteditable=true]').empty();
            getLatestFeeds();
        },
        failed: function() {
            $(".uploadpouter").hide();
            $(".feedposter").removeClass("loading");
        }
    });
});
$(document).on('keydown', ".txtcommentbox", function(e) {
    if (e.ctrlKey && e.keyCode == 13) {
        e.preventDefault();
        $(this).closest(".feedcommentspost").find(".btnpostcomment").click();
        return false;
    }
});
$(document).on('keyup', "#posttext", function(e) {
    var cntval = $(this).val().length;
    if (cntval > 160) {
        $.confirm({
            title: 'Newsfeed limit exceed',
            content: 'Newsfeed max size is 160 character. Please write article for large content.',
            confirmButton: 'Write Article',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                var post_data = {"data": $("#posttext").val()};
                $.post(site_url + "articles/prepost", post_data, function(res) {
                    window.location.href = site_url + "articles/post";
                });
            }
        });
        return false;
    }
});
$(document).on('click', ".btn_edit_comment", function(e) {

    var $this = $(this);
    var parent = $this.closest(".commentsingle");
    parent.find(".com_data_block").hide();
    $(".com_edit_temp .com_text").html(parent.find(".com_text_temp").html());
    $(".com_edit_temp .com_id").val($this.data("id"));
    parent.find(".com_edit_block").html($(".com_edit_temp").html());
});
$(document).on('click', ".btncancelcomment", function(e) {
    var $this = $(this);
    var parent = $this.closest(".commentsingle");
    parent.find(".com_edit_block").html("");
    parent.find(".com_data_block").show();
});
$(document).on('click', ".btneditcomment", function(e) {
    var $this = $(this);
    var parent = $this.closest(".commentsingle");
    parent.find(".com_text_block").html(parent.find(".com_edit_block .com_text").val());
    parent.find(".com_text_temp").html(parent.find(".com_edit_block .com_text").val());
    var post_data = parent.find(".com_edit_block form").serialize();
    $.post(site_url + "profile/commentupdate", post_data, function(res) {
        $(".btncancelcomment").click();
    });
});

var point_start = 0;
$(document).ready(function(e) {
    $(window).scroll(function() {
        var q_con_height = $(".feedcontainer").height() + $('.feedcontainer').offset().top - 600;
        //alert(q_con_height + "----" + $(window).scrollTop());
        if ($(window).scrollTop() > q_con_height) {
            if ($(".loadmore").css("display") != "none") {
                $(".loadmore").click();
            }
        }


        var static_height = $(".staticcheck").offset().top;
        var window_scroll = $(window).scrollTop();
        var window_width = $("body").width();
        var devider_offset = $(".staticdevider").offset().top - 60;
        var staticstarter = $(".staticstarter").offset().top;
        //console.log(devider_offset+"----"+window_scroll);
        if (devider_offset < window_scroll) {
            if (point_start == 0) {
                point_start = devider_offset;
            }
            if (window_width > 1170) {
                if (window_scroll > devider_offset) {
                    if ($(".fixerinner").hasClass("fixedNow") == false) {
                        $(".fixerinner").addClass("fixedNow");
                        $(".fixerinner").css("position", "fixed");
                        $(".fixerinner").css("width", 347);
                        $(".fixerinner").css("top", ((devider_offset - staticstarter) * (-1)) + 15);
                    }
                } else {
                    setWidthAuto();
                }
            } else {
                setWidthAuto();
            }
        } else {
            if (point_start > window_scroll) {
                setWidthAuto();
            }
        }
    });
    $(window).scroll(function() {
        var static_height = $(".staticcheck").offset().top;
        var window_scroll = $(window).scrollTop();
        var window_width = $("body").width();
        var devider_offset = $(".staticdevider2").offset().top + -60;
        var staticstarter = $(".staticstarter2").offset().top;
        // console.log(devider_offset + "----" + window_scroll);
        if (devider_offset < window_scroll) {
            if (point_start == 0) {
                point_start = devider_offset;
            }
            if (window_width > 1170) {
                if (window_scroll > devider_offset) {
                    if ($(".fixerinner2").hasClass("fixedNow2") == false) {
                        $(".fixerinner2").addClass("fixedNow2");
                        $(".fixerinner2").css("position", "fixed");
                        $(".fixerinner2").css("width", 253);
                        $(".fixerinner2").css("top", ((devider_offset - staticstarter) * (-1)) + 15);
                    }
                } else {
                    setWidthAuto2();
                }
            } else {
                setWidthAuto2();
            }
        } else {
            if (point_start > window_scroll) {
                setWidthAuto2();
            }
        }
    });
});
function setWidthAuto() {
    if ($(".fixerinner").hasClass("fixedNow")) {
        $(".fixerinner").removeClass("fixedNow");
        $(".fixerinner").css("position", "relative");
        $(".fixerinner").css("width", "auto");
        $(".fixerinner").css("top", "0");
    }
}

function setWidthAuto2() {
    if ($(".fixerinner2").hasClass("fixedNow2")) {
        $(".fixerinner2").removeClass("fixedNow2");
        $(".fixerinner2").css("position", "relative");
        $(".fixerinner2").css("width", "auto");
        $(".fixerinner2").css("top", "0");
    }
}

