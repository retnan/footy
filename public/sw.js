self.addEventListener('install', function(event){
	console.log('[Service Worker] Installing service worker...', event);
	event.waitUntil(caches.open('footy').then(function(cache){
		console.log('Caching for offline');
		cache.addAll([
		'/',
		'fassets/images/loginbg.jpg',
		'/home/login/index',
		'fassets/pwa/app.js',
		'fassets/css/bootstrap.min.css',
		'fassets/css/font-awesome.min.css',
		'fassets/css/animate.min.css',
		'fassets/css/main.css',
		'fassets/css/responsive.css',
		'fassets/js/jquery.js',
		'fassets/js/bootstrap.min.js',
		'fassets/js/jquery.prettyPhoto.js',
		'fassets/js/jquery.isotope.min.js',
		'fassets/js/jquery.validate.min.js',
		'fassets/js/additional-methods.js',
		'fassets/js/jquery.form.js',
		'fassets/js/main.js',
		'fassets/js/app.js',
		'fassets/js/wow.min.js'
		]);
	}));
});

self.addEventListener('activate', function(event){
	//console.log('[Service Worker] Activating service worker...', event);
	return self.clients.claim();
});

self.addEventListener('fetch', function(event) {
	//console.log('[Service Worker] Fetching something ....', event);
	//event.respondWith(fetch(event.request));
	event.respondWith(caches.match(event.request).then(function(response){
		if(response){
		  return response;
		}else{
		  return fetch(event.request)
		  .then(function(res){
		    return caches.open('dynamic').then(function(cache){
		      cache.put(event.rquest.url, res.clone());
		      return res;
		    })
		  })
		  .catch(function(){
		  
		  })
		}
	})
	
	);
});