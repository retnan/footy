$(document).ready(function(e) {
    $(".view_fest_list").live('click', function() {
        $(".fest_form_container").hide().html('');
        $(".fest_list_container").show();
    });
    $(".add_schedule").live('click', function() {
        $(".fest_form_container").data("id", "");
        $(".fest_list_container").hide();
        $(".fest_form_container").show();
        App.loadData($(".fest_form_container"));

    });
    $(".btn_cancel_fest_master").live('click', function() {
        $(".cat_pop").popover('hide');
    });

    $("#fest_category").live('change', function() {
        updateCategoryFest();
    });

    $(".btn_add_fest_master").live('click', function() {
        var ele_file_form = $("#form_fest_schedule");
        $txtCat = $.trim($(".txtAddFest").val());
        $noExist = true;
        $("#fest_select option", ele_file_form).each(function() {
            if ($.trim($(this).html()) == $txtCat) {
                $noExist = false;
            }
        });
        if ($txtCat != '' && $noExist) {
            App.blockUI(ele_file_form);
            $.post(base_url + "colleges/fest/addfestmaster", {fest_cat: $("#fest_category").val(), fest_text: $txtCat}, function(data) {
                jdata = $.parseJSON(data);
                if (jdata.status == "success") {
                    $.gritter.add({title: 'Fest Master', text: 'Fest added successfully'});
                    updateCategoryFest();
                } else {
                    alert(jdata.msg);
                }
                App.unblockUI(ele_file_form);
            });
        }
        $(".cat_pop").popover('hide');
    });


    $("#fest_state").live('change', function() {
        App.blockUI($(".fest_city"));
        var post_data = {state_id: $(this).val()};
        $.post(base_url + "colleges/fest/getcity", post_data, function(res) {
            $("#fest_city").html(res).change();
            App.unblockUI($(".fest_city"));
        });
    });

    $(".contact_persons .btn_add").live('click', function() {
        addContactPerson();
    });

    $(".contact_persons .btn_remove").live('click', function() {
        $(this).closest(".contact_row").remove();
        resetContact();
    });

    $(".events .btn_add").live('click', function() {
        addEvents();
    });

    $(".events .btn_remove").live('click', function() {
        $(this).closest(".event_row").remove();
        resetEvent();
    });

    $(".fest_schedule_delete").live('click', function() {
        App.blockUI($("#fest_list"));
        var post_data = $(this).data();
        $.post($(this).data("url"), post_data, function(res) {
            $(".searchBtn").click();
            App.unblockUI($(".fest_list"));
        });
    });

    $(".approve_fest").live('click', function() {
        App.blockUI($("#fest_list"));
        var post_data = $(this).data();
        $.post($(this).data("url"), post_data, function(res) {
            $(".searchBtn").click();
            App.unblockUI($(".fest_list"));
        });
    });

});

function updateCategoryFest() {
    $category = $("#fest_category").val();
    var post_data = {cat_id: $category};
    $.post(base_url + "colleges/fest/festbycategory", post_data, function(res) {
        $("#fest_select").html(res);
    });
}

function addContactPerson() {
    $(".contact_persons").append($(".template_contact").html());
    resetContact();
}

function addEvents() {
    $(".events").append($(".template_event").html());
    resetEvent();
}

function resetContact() {
    $i = 1;
    $length = $(".contact_persons .contact_row").length;
    $(".contact_persons .contact_row").each(function() {
        $(this).find(".num_id").html($i);
        if ($i < $length) {
            $(this).find(".btn_add").hide();
        } else {
            $(this).find(".btn_add").show();
        }
        if ($length == 1) {
            $(this).find(".btn_remove").hide();
        } else {
            $(this).find(".btn_remove").show();
        }
        $i++;
    });
}

function resetEvent() {
    $i = 1;
    $length = $(".events .event_row").length;
    $(".events .event_row").each(function() {
        $(this).find(".num_id").html($i);
        if ($i < $length) {
            $(this).find(".btn_add").hide();
        } else {
            $(this).find(".btn_add").show();
        }
        if ($length == 1) {
            $(this).find(".btn_remove").hide();
        } else {
            $(this).find(".btn_remove").show();
        }
        $i++;
    });
}

function saveFestSchedule() {
    App.blockUI($(".form_fest_schedule"));
    var formData = new FormData($("form#form_fest_schedule")[0]);
    $.ajax({
        url: base_url + "colleges/fest/save_schedule",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            App.unblockUI($(".form_fest_schedule"));
            $(".fest_form_container").hide();
            $(".fest_list_container").show();
            $(".tabdata").first().click();
        },
        error: function(data) {
            App.unblockUI($(".form_fest_schedule"));
            $.gritter.add({title: 'Fest Error', text: 'Error in Saving Fest Schedule'});
        },
        cache: false,
        contentType: false,
        processData: false
    });
}