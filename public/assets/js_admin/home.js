
var Index = function() {
    return {
        //main function

        initCalendar: function() {
            if (!jQuery().fullCalendar) {
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};

            if ($('#calendar').width() <= 400) {
                $('#calendar').addClass("mobile");
                h = {
                    left: 'title, prev, next',
                    center: '',
                    right: 'today,month,agendaWeek,agendaDay'
                };
            } else {
                $('#calendar').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: 'title',
                        center: '',
                        left: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                } else {
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }
        },
        initDashboardDaterange: function() {
            $('#dashboard-report-range').daterangepicker({
                ranges: {
                    'Last 30 Days': [Date.today().add({days: -30}), 'today'],
                    'Last 3 Months': [Date.today().add({months: -3}), Date.today()],
                    'Last 6 Months': [Date.today().add({months: -6}), Date.today()],
                    'Last 9 Months': [Date.today().add({months: -9}), Date.today()],
                },
                opens: (App.isRTL() ? 'right' : 'left'),
                format: 'MM/dd/yyyy',
                separator: ' to ',
                startDate: Date.today().add({
                    days: -29
                }),
                endDate: Date.today(),
                minDate: '01/01/2012',
                maxDate: Date.today(),
                locale: {
                    applyLabel: 'Submit',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom Range',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                },
                showWeekNumbers: true,
                buttonClasses: ['btn-danger']
            },
            function(start, end) {
                App.blockUI(jQuery(".data_block"));
                get_graph_Data(start.toString('yyyy-MM-dd'), end.toString('yyyy-MM-dd'), $('#dashboard-report-range').data("page"), true);
                App.unblockUI(jQuery(".data_block"));
                var dt_range_txt = start.toString('MMM dd, yyyy') + ' - ' + end.toString('MMM dd, yyyy');

                $('#dashboard-report-range span').html(dt_range_txt);
            });
            $('#dashboard-report-range').show();
            $('#dashboard-report-range span').html(Date.today().add({
                days: -30
            }).toString('MMM dd, yyyy') + ' - ' + Date.today().toString('MMM dd, yyyy'));
        }
    };
}();
function phpDateToUTC($date) {
    var sp_date = $date.split("-");
    var new_date = Date.UTC(sp_date[0], sp_date[1] - 1, sp_date[2]);
    return new_date;
}
function get_graph_Data(st, end, page, show_all) {
    show_all = (typeof (show_all) == 'undefined') ? false : true;
    var post_data = {action_type: 'get_graph_data', dt_from: st, dt_to: end, show_all: show_all};
    $.post(page, post_data, function(res) {
        $("#load_script").html(res);
        $(".data_block .grph_text").html(" (" + $('#dashboard-report-range span').html() + ")");
    });
}

function load_active_tab() {
    var page = $(".left_menu_bm li.active a").data("bm_type");
    $(".data-loader[data-bm_type='" + page + "']").click();
}

$(document).ready(function(e) {
    Index.initDashboardDaterange();
    get_graph_Data(Date.today().add({days: -30}).toString('yyyy-MM-dd'), Date.today().toString('yyyy-MM-dd'), $('#dashboard-report-range').data("page"));
});

function print_chart(chart, chart_data) {
    var plot_lines = false;
    goal_line = false;
    bm_chart = new Highcharts.Chart({
        chart: {
            renderTo: chart.container,
            type: 'spline'
        },
        title: {
            text: chart.title
        },
        subtitle: {
            text: chart.subtitle
        }, plotOptions: {
            series: {
                connectNulls: true // by default
            }
        },
        xAxis: {
            type: 'datetime',
            tickInterval: chart.gap * 24 * 3600 * 1000,
            endOnTick: true,
            startOnTick: true
                    //showLastLabel:true,
//            min: Date.UTC(chart_min_date[0], chart_min_date[1], chart_min_date[2]),
//            max: Date.UTC(chart_max_date[0], chart_max_date[1], chart_max_date[2]),
        },
        yAxis: {
            title: {
                text: chart.title
            },
            min: chart.min_val,
            max: chart.max_val,
            plotLines: plot_lines
        },
        tooltip: {
            formatter: function() {
                return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%e. %b', this.x) + ': ' + this.y + ' ' + chart.unit;
            }
        },
        series: chart_data
    });

}
