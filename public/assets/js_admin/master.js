function add_country() {
    var post_data = $("#country_add").serialize();
    $.post(base_url + "admin/master/country_save", post_data, function(res) {
        if (res.status == 'OK') {
            $(".error_block").hide();
            $(".searchBtn").click();
            $.gritter.add({
                title: 'Country',
                text: 'New Country has been added'
            });
            $('#modal_country_add .close').click();
        } else {
            $(".error_block").html(res.msg).hide().fadeIn('fast');
        }
    }, 'json');
}
function update_country() {
    var post_data = $("#country_edit").serialize();
    $.post(base_url + "admin/master/country_save", post_data, function(res) {
        if (res.status == 'OK') {
            $(".error_block").hide();
            $(".searchBtn").click();
            $.gritter.add({
                title: 'Country',
                text: 'Country has been Updated'
            });
        } else {
            $(".error_block").html(res.msg).hide().fadeIn('fast');
        }
    }, 'json');
}
function add_state() {
    var post_data = $("#state_add").serialize();
    $.post(base_url + "admin/master/state_save", post_data, function(res) {
        if (res.status == 'OK') {
            $(".error_block").hide();
            $(".searchBtn").click();
            $.gritter.add({
                title: 'State',
                text: 'New State has been added'
            });
            $('#modal_state_add .close').click();
        } else {
            $(".error_block").html(res.msg).hide().fadeIn('fast');
        }
    }, 'json');
}
function update_state() {
    var post_data = $("#state_edit").serialize();
    $.post(base_url + "admin/master/state_save", post_data, function(res) {
        if (res.status == 'OK') {
            $(".error_block").hide();
            $(".searchBtn").click();
            $.gritter.add({
                title: 'State',
                text: 'State has been Updated'
            });

        } else {
            $(".error_block").html(res.msg).hide().fadeIn('fast');
        }
    }, 'json');
}
function add_city() {
    var post_data = $("#city_add").serialize();
    $.post(base_url + "admin/master/city_save", post_data, function(res) {
        if (res.status == 'OK') {
            $(".error_block").hide();
            $(".searchBtn").click();
            $.gritter.add({
                title: 'City',
                text: 'New City has been added'
            });
            $('#modal_city_add .close').click();
        } else {
            $(".error_block").html(res.msg).hide().fadeIn('fast');
        }
    }, 'json');
}
function update_city() {
    var post_data = $("#city_edit").serialize();
    $.post(base_url + "admin/master/city_save", post_data, function(res) {
        if (res.status == 'OK') {
            $(".error_block").hide();
            $(".searchBtn").click();
            $.gritter.add({
                title: 'City',
                text: 'City has been Updated'
            });

        } else {
            $(".error_block").html(res.msg).hide().fadeIn('fast');
        }
    }, 'json');
}

function add_category() {
    var formData = new FormData($("form#category_add")[0]);
    $.ajax({
        url: base_url + "admin/master/category_save",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            var res = $.parseJSON(data)
            if (res.status == 'OK') {
                $(".error_block").hide();
                $(".searchBtn").click();
                $.gritter.add({
                    title: 'Category',
                    text: 'Category has been Added'
                });
                $('#modal_category_add .close').click();
            } else {
                $(".error_block").html(res.msg).hide().fadeIn('fast');
            }
        },
        error: function(data) {

        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
}
function add_league() {
    var formData = new FormData($("form#league_add")[0]);
    $.ajax({
        url: base_url + "admin/master/league_save",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
//            alert(data);
            var res = $.parseJSON(data)
            if (res.status == 'OK') {
                $(".error_block").hide();
                $(".searchBtn").click();
                $.gritter.add({
                    title: 'League',
                    text: 'League has been Added'
                });
                $('#modal_league_add .close').click();
            } else {
                $(".error_block").html(res.msg).hide().fadeIn('fast');
            }
        },
        error: function(data) {

        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
}

function update_league() {
    var formData = new FormData($("form#league_edit")[0]);
    $.ajax({
        url: base_url + "admin/master/league_save",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
//            alert(data);
            var res = $.parseJSON(data)
            if (res.status == 'OK') {
                $(".cpageval").removeClass("active").addClass("inactive").click();
                $.gritter.add({
                    title: 'League',
                    text: 'League has been Updated'
                });
                 $(".searchBtn").click();
                $('#modal_league_edit .close').click();
            } else {
                $(".error_block").html(res.msg).hide().fadeIn('fast');
            }
        },
        error: function(data) {

        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
}
function add_club() {
    var formData = new FormData($("form#club_add")[0]);
    $.ajax({
        url: base_url + "admin/master/club_save",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
//            alert(data);
            var res = $.parseJSON(data)
            if (res.status == 'OK') {
                $(".error_block").hide();
                $(".searchBtn").click();
                $.gritter.add({
                    title: 'Club',
                    text: 'Club has been Added'
                });
                $('#modal_club_add .close').click();
            } else {
                $(".error_block").html(res.msg).hide().fadeIn('fast');
            }
        },
        error: function(data) {

        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
}

function update_club() {
    var formData = new FormData($("form#club_edit")[0]);
    $.ajax({
        url: base_url + "admin/master/club_save",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
//            alert(data);
            var res = $.parseJSON(data)
            if (res.status == 'OK') {
                $(".cpageval").removeClass("active").addClass("inactive").click();
                $.gritter.add({
                    title: 'Club',
                    text: 'Club has been Updated'
                });
                 $(".searchBtn").click();
                $('#modal_club_edit .close').click();
            } else {
                $(".error_block").html(res.msg).hide().fadeIn('fast');
            }
        },
        error: function(data) {

        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
}

function add_brand() {
    var post_data = $("#brand_add").serialize();
    $.post(base_url + "admin/master/brand_save", post_data, function(res) {
        if (res.status == 'OK') {
            $(".error_block").hide();
            $(".searchBtn").click();
            $.gritter.add({
                title: 'Brand',
                text: 'New Brand has been added'
            });
            $('#modal_brand_add .close').click();
        } else {
            $(".error_block").html(res.msg).hide().fadeIn('fast');
        }
    }, 'json');
}

function update_brand() {
    var post_data = $("#brand_edit").serialize();
    $.post(base_url + "admin/master/brand_save", post_data, function(res) {
        if (res.status == 'OK') {
            $(".error_block").hide();
            $('#modal_brand_edit .close').click();
            $(".cpageval").removeClass("active").addClass("inactive").click();
            $.gritter.add({
                title: 'Brand',
                text: 'Brand has been Updated'
            });

        } else {
            $(".error_block").html(res.msg).hide().fadeIn('fast');
        }
    }, 'json');
}

$(document).ready(function(e) {
    $(".master_status").live('click', function(e) {
        e.preventDefault();
        var post_data = $(this).data();
        var url = $(this).data('url');
        $.post(url, post_data, function(res) {
            $(".cpageval").removeClass("active").addClass("inactive").click();
            $.gritter.add({
                title: res.title,
                text: res.text
            });
        }, 'json');

    });
    $(".master_delete").live('click', function(e) {
        e.preventDefault();
        var post_data = $(this).data();
        var url = $(this).data('url');
        $.post(url, post_data, function(res) {
            $(".cpageval").removeClass("active").addClass("inactive").click();
            $.gritter.add({
                title: res.title,
                text: res.text
            });
        }, 'json');

    });
    $("#country.dynamic").live('change', function() {
        var post_data = {id: $(this).val()};
        var state = $(this).closest("form").find("#state");
        var outer_block = $("#state").parents(".control-group");
        App.blockUI(outer_block);
        state.attr("disabled", true);
        $.post(base_url + "admin/master/state_by_country", post_data, function(res) {
            state.attr("disabled", false);
            state.html(res);
            $("#state.dynamic").change();
            App.unblockUI(outer_block);
        });
    });

    $("#state.dynamic").live('change', function() {
        var post_data = {id: $(this).val()};
        var city = $(this).closest("form").find("#city");
        var outer_block = $("#city").parents(".control-group");
        App.blockUI(outer_block);
        city.attr("disabled", true);
        $.post(base_url + "admin/master/city_by_state", post_data, function(res) {
            city.attr("disabled", false);
            city.html(res);
            App.unblockUI(outer_block);
        });
    });
});
