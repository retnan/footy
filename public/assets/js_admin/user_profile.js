var map_profile_edit;
var map_profile_view;
var map_emp_view = '';
var map_emp_edit;
$(document).ready(function(e) {
    $(".left_menu li.active a").click();
    
   
});

function afterProfileLoad() {
    map_profile_edit = '';
    map_profile_view = '';
    map_emp_view = '';
    map_emp_edit = '';
    map_profile_view = $("#profile_map_display").superMap();
    map_profile_view = $("#emp_map_display").superMap();
    FormValidationBasic.init();
}

function save_profile_basic() {
    $basic_container = $("#user_profile_basic_form");
    App.blockUI($basic_container);
    $.post("ajax/profile.php?action_type=update_basic", $basic_container.serialize(), function(data) {
        $(".profile_basic").data('activetab', 'tab_1_1').click();
        $.gritter.add({
            title: 'Profile Information Update',
            text: 'Profile Overview Information has been updated.'
        });
        App.unblockUI($basic_container);
    });
}

var FormValidationBasic = function() {
    var handleValidationBasic = function() {
        $('#user_profile_basic_form').validate({
            submitHandler: function(form) {
                save_profile_basic();
                return false;
            }
        });
    };
    return {
        init: function() {
            handleValidationBasic();
        }
    };
}();

//************************** Employer Section ****************************//
function countJSON($data){
    var i=0;
    $.each($data,function($k,v){
        i++;
    });
    return i;
}

//*************************** Image Avatar **************************
//************************* Avatar Scripts **********************************
$(document).ready(function() {
    $('.save_avatar').live("click", function() {
        if ($(this).hasClass("disabled")) {
            return;
        }
        if (parseInt($('#w').val())) {
            save_image_cropper();
        } else {
            alert('Please select a crop region then press save.');
        }
    });
    $(".webcam_capture_btn").live("click", function() {
        $(".avatar_container").hide();
        $(".avatar_container_camera").show();
        FormWebcamAvatar.init();
    });
    $(".image_upload_btn").live("click", function() {
        $(".avatar_container_camera").hide();
        $(".avatar_container").show();
    });
    $(".btn_capture").live("click", function() {
        FormWebcamAvatar.capture();
    });
    $(".btn_configure").live("click", function() {
        FormWebcamAvatar.configure();
    });
});
//***************************************************************************

//***************************************************************************
function after_upload_image() {
    $.post(base_url+"user/profile/get_large_image", {}, function(data) {
        if ($.trim(data) != '') {
            $("#avatar_image").attr("src", $.trim(data));
            FormImageCropAvatar.init();
            $(".avatar_container,.avatar_container_camera").hide();
            $("#avatar_model .modal-header h3").html('<i class="icon-file"></i> Crop Your Image');
            $(".avatar_cropper").show();
        }
    });
}
//***************************************************************************
var jcrop_api;
function save_image_cropper() {
    $.post(base_url+"user/profile/crop_image_save", $("#avatar_form").serializeArray(), function(data) {
        $("#avatar_model").modal('hide');
        $("#avatar_image_view").attr("src", $.trim(data));
        $("#avatar_image_view_small").attr("src", $.trim(data));
        $(".avatar_container").fadeIn();
        $("#avatar_model .modal-header h3").html('<i class="icon-file"></i> Add Your Image');
        $(".avatar_cropper").fadeOut();
        $('.save_avatar').addClass("disabled");
        jcrop_api.destroy();
        $("#avatar_image").removeAttr(" ");
    });
}
//***************************************************************************
var FormImageCropAvatar = function() {
    var avatar_cropper = function() {
        //$('#avatar_image').css('width','100%');
        $cropper = $('#avatar_image').Jcrop({
            aspectRatio: 1,
            onSelect: updateCoords,
            setSelect: [0, 0, 50, 50],
        }, function() {
            jcrop_api = this;
            $("#imgw").val($(".jcrop-holder").width());
        });
        function updateCoords(c) {
            $('#x1').val(c.x);
            $('#y1').val(c.y);
            $('#x2').val(c.x2);
            $('#y2').val(c.y2);
            $('#w').val(c.w);
            $('#h').val(c.h);           
            if (parseInt(c.w) >= 50 && parseInt(c.h) >= 50) {
                $('.save_avatar').removeClass("disabled");
            } else {
                $('.save_avatar').addClass("disabled");
            }
        }
        ;
    }
    return {
        init: function() {
            if (!jQuery().Jcrop) {
                ;
                return;
            }
            avatar_cropper();
        },
        distroy: function() {
            $(".jcrop-holder").remove();
        }
    };
}();
//***************************************************************************
function after_completion_handler(msg) {
    if (msg.match(/(http\:\/\/\S+)/)) {
        var image_url = RegExp.$1;
        after_upload_image();       
        webcam.reset();
    }
    else
        alert("Server Error: " + msg);
}
//***************************************************************************
var FormWebcamAvatar = function() {
    var avatar_capture = function() {
        webcam.set_api_url(base_url+'user/profile/camera_save');
        webcam.set_quality(90); // JPEG quality (1 - 100)
        webcam.set_shutter_sound(true, public_url+"assets/plugins/photobooth/shutter.mp3"); // play shutter click sound
        webcam.set_swf_url( public_url+"assets/plugins/photobooth/webcam.swf");
        
       
        var webcam_html = webcam.get_html(420, 300, 620, 400);
        $("#cam_container").html(webcam_html);
        webcam.set_hook('onComplete', 'after_completion_handler');
    }
    function capture_image() {
        webcam.snap();
    }
    function configure_cam() {
        webcam.configure();
    }
    return {
        //main function to initiate the module
        init: function() {
            avatar_capture();
        },
        capture: function() {
            capture_image();
        },
        configure: function() {
            configure_cam();
        }
    };
}();
//***************************************************************************
$(document).ready(function(e) {
    $("#avatar_model").live("shown", function() {
        $(".save_avatar")
    });
});

 