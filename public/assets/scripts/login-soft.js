function user_login(){ 
    var post_data = $('#login-form').serialize();
    $.post(base_url+"main/verify", post_data, function(res) {
       
     if(res.status=="success"){
      window.location=base_url+res.page;
     }else{
    $('.login_error').html(res.msg).hide().fadeIn(300);
     }     
    },'json');
    
}
function user_signup(){ 
    var post_data = $('#signup-form').serialize();
    $.post(base_url+"main/signup", post_data, function(res) {
     if(res.status=="true"){
         $("#modal_signup .uname").html(res.name);
         $("#modal_signup .uemail").html(res.email);
         $("#modal_signup .reg_success").show();
         $("#signup-form,.btn_signup").hide();
     }
     
    },'json');
    
}
function user_forgot(){ 
//    alert("log");
//    return;
    var post_data = $('#forget-form').serialize();
    $.post(base_url+"main/forgot", post_data, function(res) {       
     if(res.status=='invalid'){
         var parent=$("#forget-form #txt_email").closest(".control-group");
         parent.removeClass("success");
         parent.addClass("error");
         parent.find("span.help-inline").html("Email is not registered");
     }else{
         $('.view_forgot').hide();
         $('.view_login').fadeIn();
         $("#forget-form #txt_email").val("");
         $.gritter.add({
            'title':'Forgot Password',
            'text':'A reset password link has been sent on your Email. Kindly check your Email.'
         });
         
     }
    },'json');
    
}
$(document).ready(function(e) {
   $(".email_check").live('blur',function() {
        var input = $(this);
        var email = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (input.val() === "" || !email.test(input.val())) {
            input.popover('destroy');
            return;
        }
        //$('#reg_client  .alert-success b').html(input.val());
        input.attr("readonly", true).attr("disabled", true).addClass("spinner");

        $.post(base_url+"main/check_email", {email: input.val()}, function(res) {

            input.attr("readonly", false).attr("disabled", false).removeClass("spinner");
            input.popover('destroy');
            input.popover({
                'html': true,
                'placement': App.isRTL() ? 'left' : 'right',
                'title': 'Email/Username Availability',
                'content': res.msg,
            });

            // change popover font color based on the result
            if (res.status == 'success') {
                input.closest('.control-group').removeClass('error').addClass('success');
                //input.after('<span class="help-inline ok"></span>');                    
                input.popover('destroy');
            } else {
                input.val("");
                input.closest('.control-group').removeClass('success').addClass('error');
                $('.help-inline.ok', input.closest('.control-group')).remove();
                input.popover('show');
                input.focus();
            }

        }, 'json');

    });
    $('#profession').live('change',function(){
        $(".prof_doc").hide();
        $(".prof_doc #doc_reg_no").removeClass('required');
       if($(this).val()=='2'){
           $(".prof_doc #doc_reg_no").addClass('required');
           $(".prof_doc").show();
       } 
    });
        
        
    
    
});


var Login = function () {
var handleLogin = function() {
		$('#login-form').validate({	          
	            submitHandler: function (form) {
	                user_login();
                        return false;
	            }
	        });              
	        $('#login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('#login-form').validate().form()) {
	                     user_login();
	                }
	                return false;
	            }
	        });
	}
        var handleSignup = function() {		
                $('#signup-form').validate({	          
	            submitHandler: function (form) {
	                user_signup();
                        return false;
	            }
	        });	        
	}

        var handleReset=function(){
            $("#reset-form").validate({	          
                submitHandler: function (form) {
                    reset_password();
                    return false;
                }
            });
        }
	var handleForgetPassword = function () {
		$('#forget-form').validate({	            
	            submitHandler: function (form) {
	                user_forgot();
                        return false;
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}
    
    return {
        //main function to initiate the module
        init: function () {
        	
            handleLogin();
            handleReset();
//            handleSignup();
            handleForgetPassword();
//            $.backstretch([
//		        "public/assets/img/bg/1.jpg",
//		        "public/assets/img/bg/2.jpg",
//		        "public/assets/img/bg/3.jpg",
//		        "public/assets/img/bg/4.jpg"
//		        ], {
//		          fade: 1000,
//		          duration: 8000
//		    });
	       
        }
    };

}();