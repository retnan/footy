$(document).ready(function() {
    window.emojiPicker = new EmojiPicker({
        emojiable_selector: '[data-emojiable=true]',
        assetsPath: '/public/assets/plugins/emoji-picker/lib/img/',
        popupButtonClasses: 'fa fa-smile-o'
    });
    window.emojiPicker.discover();
})