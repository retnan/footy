/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function(config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.extraPlugins = 'uploadimage';
    config.extraPlugins = 'image';
    config.uploadUrl = base_url + 'home/upload';
    // config.filebrowserImageBrowseLinkUrl = base_url + 'public/ck/ckupload.php';
    config.filebrowserUploadUrl = base_url + 'home/ckupload';
};
