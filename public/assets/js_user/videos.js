$(document).ready(function(e) {
    $(".master_delete").live("click", function() {
        parent_block = $(this).closest(".portlet");
        portfolio_block = $(this).closest(".portfolio-block");
        App.blockUI(parent_block);
        var post_data = $(this).data();
        $.post($(this).data("url"), post_data, function(res) {
            portfolio_block.slideUp(function() {
                $(this).remove();
                App.unblockUI(parent_block);
            });
        });
    });
    $(".master_status").live("click", function() {
        parent_block = $(this).closest(".portlet");
        portfolio_block = $(this).closest(".portfolio-block");
        App.blockUI(parent_block);
        var post_data = $(this).data();
        $.post($(this).data("url"), post_data, function(res) {
            App.unblockUI(parent_block);
            $(".searchBtn").click();
        });
    });
});

function update_video() {
    parent_block = $("#video_edit").closest(".portlet");
    portfolio_block = $(this).closest(".portfolio-block");
    App.blockUI(parent_block);
    var post_data = $("#video_edit").serialize();
    $.post($("#video_edit").prop("action"), post_data, function(res) {
        App.unblockUI(parent_block);
        res = $.parseJSON(res);
        $.gritter.add({
            title: res.title,
            text: res.text
        });
    });
}