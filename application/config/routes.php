<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */


$route['default_controller'] = "home";
$route['404_override'] = 'general/page_404';
$route['admin'] = 'admin/home';
$route['login'] = 'home/login';
$route['signup'] = 'home/signup';
$route['contact'] = 'home/contact';
$route['dashboard'] = 'profile/newsfeed/0/0';
$route['newsfeed'] = 'profile/newsfeed/0/0';
$route['oyo5aside'] = 'Features/index';
$route['matchroom'] = 'Matchroom/index';
$route['matchroom/load_banter/(:num)'] = 'Matchroom/load_banter/$1';
$route['matchroom/banter'] = 'Matchroom/load_banter/0/$1';
$route['plus'] = 'footyfanz/index';
$route['dashboard' . '/([0-9a-zA-Z_-]+)'] = 'profile/newsfeed/0/$1';
$route['newsfeed' . '/([0-9a-zA-Z_-]+)'] = 'profile/newsfeed/$1';
$route['pwanews' . '/([0-9a-zA-Z_-]+)'] = 'profile/pwa_newsfeed/$1/$em';
$route['followers' . '/([0-9a-zA-Z_-]+)'] = 'profile/followers/$1';
$route[NEWS_TAG . '/([0-9a-zA-Z_-]+)'] = 'articles/articleview/$1';
$route[ARTICLE_TAG . '/([0-9a-zA-Z_-]+)'] = 'articles/articleview/$1';
$route[PAGES_TAG . '/([0-9a-zA-Z_-]+)'] = 'pages/pagecontent/$1';
$route['([0-9a-zA-Z_-]+)'] = "articles/listing/$1";
$route['search'] = 'articles/search';
$route['user'] = 'user/home';
$route['club' . '/([0-9a-zA-Z_-]+)'] = 'club/home/$1';
$route['terms'] = 'general/terms';
$route['legal'] = 'general/legal';
$route['privacy'] = 'general/privacy';
$route['feedback'] = 'general/feedback';
$route['faq'] = 'general/faq';
$route['sitemap\.xml'] = "admin/seo/sitemapnews";
$route['sitemapnews\.xml/([0-9a-zA-Z_-]+)'] = "admin/seo/sitemapnews/$1";
$route['sitemaparticle\.xml/([0-9a-zA-Z_-]+)'] = "admin/seo/sitemaparticle/$1";
$route['sitemapuser\.xml/([0-9a-zA-Z_-]+)'] = "admin/seo/sitemapuser/$1";
$route['sitemapclub\.xml/([0-9a-zA-Z_-]+)'] = "admin/seo/sitemapclub/$1";

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
$route['api/user/login'] = 'user/pwa';
$route['api/user/signup'] = 'user/registration/1';
$route['api/user/clubs'] = 'user/fetch_clubs';
$route['api/user/face'] = 'user/photo/1';
$route['api/user/matchroom'] = 'Matchroom/index/1';
$route['api/user/banters/(:num)'] = 'Matchroom/load_banter/$1/1';
$route['api/user/banters'] = 'profile/getfeeds/1';
$route['api/user/newsfeed'] = 'profile/getfeeds/1';
$route['api/user/articles/([0-9a-zA-Z_-]+)'] = 'articles/listing/$1/1';
$route['api/user/article/([0-9a-zA-Z_-]+)'] = 'articles/articleview/$1/1';

$route['api/user/follows'] = 'profile/follow/1';
$route['api/user/unfollows'] = 'profile/unfollow/1';
$route['api/user/footyfanzplus'] = 'footyfanz/getlist/1';
$route['api/user/followers/([0-9a-zA-Z_-]+)'] = 'profile/followers/$1/1';

$route['api/user/postfeed'] = 'profile/feedsave/1';
$route['api/user/postbanter/(:num)'] = 'Matchroom/add_banter/$1/1/1';
/* End of file routes.php */
/* Location: ./application/config/routes.php */