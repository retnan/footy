<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

define('ADMIN_DIR', "admin/");
define('USER_DIR', "user/");

define('PROFILE_IMG_DIR', "images/profile/");
define('CLUB_IMG_DIR', "public/images/club/");
define('LEAGUE_IMG_DIR', "public/images/league/");
define('LOGO_DIR', "images/logo/");


define('CONTACT_EMAIL', "info@footyfanz.com");// ???
define('ADMIN_EMAIL', "noreply@footyfanz.com");

define('MAIL_HOST', "50.87.190.41");
define('MAIL_USER', "noreply@footyfanz.com");
define('MAIL_PASSWORD', "Footy@2016");
define('MAIL_AGENT', "FootyFanz");
define('SITE_NAME', "FootyFanz");
define('META_DESC', "");

define('PUBLIC_DIR', "public/");
define('PAGING_MED', "10");
define('NO_IMAGE', "public/assets/img/noimage.jpg");
define('NO_USER', "public/assets/img/user.jpg");
define('DEFAULT_USER', "public/assets/img/default-user.jpg");
define('IMG_ARTICLE', "public/images/article/");
define('IMG_NEWSFEED', "public/images/news_feed/");
define('IMG_GALLERY', "public/images/gallery/");
define('VID_NEWSFEED', "public/videos/news_feed/");
define('VID_GALLERY', "public/videos/gallery/");
define('MEDIA_DIR', "public/media/");
define('ADS_DIR', "public/images/ads/");
define('IMG_PROFILE', "public/images/profile/");
define('POLL_DIR', "public/images/polls/");
define('USER_GALLERY', "public/uploads/user/");
define("ARTICLE_TAG", "article");
define("NEWS_TAG", "news");
define("NEWS_FEED", "profile/newsfeed/");
define("PAGES_TAG", "page");
define("FB_APP", "246748612376671");
define("FB_SECRET", "a2f0b5263ec8969c0dc20afb70dcbe2d");

/* End of file constants.php */
/* Location: ./application/config/constants.php */
