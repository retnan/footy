<div class="row">
    <div class="col-md-12">
        <?php $ads = getTopAds(); ?>
        <div id="myCarousel" class="carousel slide carousel-fade" style="margin-bottom: 15px;">
            <div class="carousel-inner">
                <?php
                $i = 1;
                foreach ($ads as $ad) {
                    ?>
                    <div class="item <?php
                    echo ($i == 1) ? "active" : "";
                    $i++;
                    ?>">
                        <a href="<?php echo $ad['link']; ?>">
                            <img src="<?php echo $ad['image'] ?>" style="max-width: 100%;" />
                        </a>
                    </div>
                    <?php
                }
                ?>

            </div>
            <?php if (count($ads) > 0) { ?>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
            <?php } ?>
        </div><!-- /.carousel -->

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php echo getCMSContent("ad_ggl_top"); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3 home_first" >
        <div class="header_title">
            हिट स्पॉट
        </div>
        <div class="article_section_1 loaddata" data-url="<?php echo site_url(); ?>articles/catsection1" data-page="1" data-category="<?php echo $cat_id ?>">

        </div>
    </div>
    <div class="col-md-4" >
        <div class="header_title">
            राइजिंग स्पॉट 
        </div>
        <div class="article_section_2 loaddata" data-url="<?php echo site_url(); ?>articles/catsection2" data-page="1" data-category="<?php echo $cat_id ?>">

        </div>        
    </div>
    <div class="col-md-5 home_third">
        <?php $this->load->view("front/newsletter/newsbox.php", array("categories" => $categories)); ?>
        <div class="header_title">
            हॉट स्पॉट 
        </div>
        <div class="article_section_3 loaddata" data-url="<?php echo site_url(); ?>articles/catsection3" data-page="1" data-category="<?php echo $cat_id ?>">

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on("click", ".submitnewsletter", function(e) {
        e.preventDefault();
        if ($("#newsletterform").valid()) {
            $(".news_success,.news_error").hide();
            var post_data = $("#newsletterform").serialize();
            $.post(base_url + "newsletter/saveNewsletter", post_data, function(res) {
                var jdata = $.parseJSON(res);
                if (jdata.status == "1") {
                    $(".news_success").show().html(jdata.msg);
                    $("#newsletterform")[0].reset();
                    $("#newsletterform").find(".refresh_captcha").click();
                } else {
                    $(".news_error").show().html(jdata.msg);
                }
            });
        }
    });

    $(document).ready(function(e) {
        $(window).on("scroll", function() {
            var top_pos = $(window).scrollTop();
            var section3_offset = $(".article_section_3").height() + $(".newsletter_box").height() - $(window).height();
            if (top_pos > section3_offset) {
                loadMoreSection3();
            }
            var section2_offset = $(".article_section_2").height() - $(window).height();
            if (top_pos > section2_offset) {
                loadMoreSection2();
            }
            var section1_offset = $(".article_section_1").height() - $(window).height();
            if (top_pos > section1_offset) {
                loadMoreSection1();
            }
        });
    });
    function loadMoreSection3() {
        if ($(".article_section_3 .nomore").length == 0 && $(".article_section_3 .article_loader").length == 0) {
            $(".article_section_3").data("page", $(".article_section_3").data("page") + 1);
            $(".article_section_3").data("ltype", "append");
            loadData($(".article_section_3"));
        }
    }
    function loadMoreSection2() {
        if ($(".article_section_2 .nomore").length == 0 && $(".article_section_2 .article_loader").length == 0) {
            $(".article_section_2").data("page", $(".article_section_2").data("page") + 1);
            $(".article_section_2").data("ltype", "append");
            loadData($(".article_section_2"));
        }
    }
    function loadMoreSection1() {
        if ($(".article_section_1 .nomore").length == 0 && $(".article_section_1 .article_loader").length == 0) {
            $(".article_section_1").data("page", $(".article_section_1").data("page") + 1);
            $(".article_section_1").data("ltype", "append");
            loadData($(".article_section_1"));
        }
    }
</script>
