<?php
$base = base_url()."/public/";
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?>

<section class="newsfeed">
    <div class="col-md-12">
        <div class="blacklabel" style="text-align: left; padding-left: 15px;">
        </div>
    </div>
<!--This section is to display ,live matches-->
    <div class="col-md-12">
      <div class="row strip">
          <div class="col-md-12">
              <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                  Live Matches
              </div>
          </div>
      </div>
    <?php
    foreach ($live_matches as $value)
    {
      echo "<a href = 'matchroom/load_banter/".$value['match_room_id']."'><img src='".$value['club_one']['logo']."' height= '64px' width = '64px'>".$value['club_one']['name'] .' VS '. $value['club_two']['name']."<img src = '".$value['club_two']['logo']." ' height= '64px' width = '64px'> | </a>";
    }
    ?>
  </div>
  <!--This section is to display ,upcoming matches-->
      <div class="col-md-12">
        <div class="row strip">
            <div class="col-md-12">
                <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                    Upcoming Matches
                </div>
            </div>
        </div>
      <?php
      foreach ($upcoming_matches as $value)
      {
        echo "<a href = 'matchroom/load_banter/".$value['match_room_id']."'><img src = '".$value['club_one']['logo']."' height= '64px' width = '64px'>".$value['club_one']['name'] .' VS '. $value['club_two']['name']."<img src = '".$value['club_two']['logo']."' height= '64px' width = '64px'> | </a>";
      }
      ?>
    </div>
</section>