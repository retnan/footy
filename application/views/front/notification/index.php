<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?> 
<section class="articlecontent">
    <div class="row">
        <div class="col-md-8">
            <div class="row strip">
                <div class="col-md-12">
                    <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                        Notifications
                    </div>
                </div>
            </div>




            <div class="bd-example" data-example-id="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="commentcontainer notificationsview">



                        </div>
                        <div class="commentloader">
                            <div class="loadmore" onclick="">View more Notification</div>
                            <div class="loaderview"><img src="<?php echo $base; ?>images/loadingBar.gif" /></div>
                        </div>



                    </div>
                </div>         
            </div>

        </div>
        <div class="col-md-4">
            <?php
            $this->load->view("front/includes/right_panel_news.php", array());
            ?>
        </div>
    </div>


</section>
<script type="text/javascript">
    var page = 1;
    function loadnotification() {
        $('.commentloader').addClass('loading');
        var post_data = {post_id: $("#post_id").val(), ptype: 'A', page: page};
        $.post(base_url + "notification/loadnotification", post_data, function(res) {
            $('.commentloader').removeClass('loading');
            $('.notificationsview').append(res);
        });
    }
    $(".loadmore").click(function() {
        page++;
        loadnotification();
    });
    $(document).ready(function(e) {
        loadnotification();
        $('.notificationsview').on('click', '.msg_delete', function(e) {
            var container = $(this).closest(".commetsingle");
            var msg_id = $(this).data("id");
            e.preventDefault();
            $.confirm({
                title: 'Delete Notification',
                content: 'Are you sure to Delete Notification',
                confirmButton: 'Delete',
                confirmButtonClass: 'btn-danger',
//                                    icon: 'fa fa-question-circle',
                animation: 'scale',
                animationClose: 'top',
                opacity: 0.5,
                confirm: function() {
                    var post_data = {msg_id: msg_id};
                    $.post(base_url + "notification/delete_notification", post_data, function(res) {
                        container.fadeOut(function() {
                            container.remove();
                        })
//                        loadnotification();
                    });

                }
            });
        });
    });
</script>