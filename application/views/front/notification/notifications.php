<?php
if ($notifications) {
    foreach ($notifications['data'] as $notification) {
        ?>
<a href="<?php echo $notification['url'] ?>" class="noti_single">
        <div class="commetsingle">
            <div class="comimage" style="border-radius:0;">
                <img src="<?php echo $notification['thumb']; ?>" />
            </div>
            <div class="comcontent" style="min-height: 45px;">
                <div ><?php echo $notification['content']; ?></div>
                 <div class="comdate"><i class="fa fa-clock"></i> <?php
                    echo timeAgo(strtotime($notification['created_at']));
                    ?></div>
            </div>
        </div>
</a>
        <?php
    }
}
if (!$notifications['load']) {
    ?>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $(".commentloader").hide();
        });
    </script>
    <?php
}
?>

