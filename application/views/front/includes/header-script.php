<?php $base = base_url() . PUBLIC_DIR . "fassets/"; ?>
<!-- core CSS -->
<meta property="fb:pages" content="257146644468945" />
<!--<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">-->
<link href="<?php echo $base; ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $base; ?>css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo $base; ?>css/animate.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="<?php echo $base; ?>css/main.css" rel="stylesheet">
<link href="<?php echo $base; ?>css/responsive.css" rel="stylesheet">
<script src="<?php echo $base; ?>js/jquery.js"></script>
<!--[if lt IE 9]>
<script src="<?php echo $base; ?>js/html5shiv.js"></script>
<script src="<?php echo $base; ?>js/respond.min.js"></script>
<![endif]-->
<link rel="shortcut icon" href="<?php echo $base; ?>images/ico/favicon.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $base; ?>images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $base; ?>images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $base; ?>images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo $base; ?>images/ico/apple-touch-icon-57-precomposed.png">
<script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
    var site_url = "<?php echo base_url(); ?>";
    var user_id = "<?php echo $this->session->userdata("user_id"); ?>";
</script>

<?php if($this->session->userdata("articlemeta")){ 
    $artdata=  unserialize($this->session->userdata("articlemeta"));
    if($artdata['image']!=""){
        ?>
            <meta property="og:image" content="<?php echo $artdata['image']; ?>"/>
            <?php
    }else{
        ?>
            <meta property="og:image" content="<?php echo $base; ?>images/logo.png"/>
            <?php
    }
    
    
    if($artdata['title']!=""){
        ?>
             <meta property="og:title" content="<?php echo str_replace('"', "'", strip_tags($artdata['title'])); ?>" />
              <title><?php echo SITE_NAME; ?>-<?php echo str_replace('"', "'", strip_tags($artdata['title'])); ?></title>
            <?php
    }else{
        ?>
           <meta property="og:title" content="<?php echo SITE_NAME; ?>" />
              <title><?php echo SITE_NAME; ?></title>
            <?php
    }
    if($artdata['content']!=""){
        ?>
             <meta property="og:description" content="<?php echo str_replace('"', "'", strip_tags($artdata['content'])); ?>" />
            <?php
    }    
    ?>

<?php }else{ ?>
     <meta property="og:image" content="<?php echo $base; ?>images/logo.png"/>
      <meta property="og:title" content="<?php echo SITE_NAME; ?>" /> 
<?php } ?>
<meta property="og:site_name" content="www.<?php echo SITE_NAME; ?>.com"/>
<meta property="fb:app_id" content="246748612376671" />

<link rel="manifest" href="<?php echo base_url() . PUBLIC_DIR . "manifest.json"; ?>">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="#ae1e23">
  <meta name="apple-mobile-web-app-title" content="FootyFanz - By the Fanz, for the Fanz">
  <link rel="apple-touch-icon" href="<?php echo $base ?>images/icons/apple-icon-144x144.png" size="144x144">
  <link rel="apple-touch-icon" href="<?php echo $base ?>images/icons/apple-icon-114x114.png" size="114x114">
  <link rel="apple-touch-icon" href="<?php echo $base ?>images/icons/apple-icon-120x120.png" size="120x120">
  <link rel="apple-touch-icon" href="<?php echo $base ?>images/icons/apple-icon-152x152.png" size="152x152">
  <link rel="apple-touch-icon" href="<?php echo $base ?>images/icons/apple-icon-180x180.png" size="180x180">
  <link rel="apple-touch-icon" href="<?php echo $base ?>images/icons/apple-icon-57x57.png" size="57x57">
  <link rel="apple-touch-icon" href="<?php echo $base ?>images/icons/apple-icon-60x60.png" size="60x60">
  <link rel="apple-touch-icon" href="<?php echo $base ?>images/icons/apple-icon-72x7276x76.png" size="72x7276x76">
  <link rel="apple-touch-icon" href="<?php echo $base ?>images/icons/apple-icon-76x76.png" size="76x76">

  <meta name="msapplication-TileImage" src="<?php echo $base ?>images/icons/app-icon-144x144.png">
  <meta name="msapplication-TileColor" content="#fff">
  <meta name="theme-color" content="#ae1e23">



