<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>
<header id="header">
    <div class="top-bar">

        <div class="row" style="margin: 0px;">
            <div class="<?php echo ($this->session->userdata("user_id") > 0) ? "col-sm-6 col-xs-8" : "col-sm-9 col-xs-9"; ?>">
                <div class="sitelogo">
                    <a href="<?php echo site_url(); ?>">
                        <img src="<?php echo $base; ?>images/logo.png" />
                    </a>

                    <div class="social">
                        <ul class="social-share">
                            <?php
                            $link = getCMSContent("fb_link");
                            if ($link) {
                                ?>
                                <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-facebook"></i></a></li>
                                <?php } ?>
                                <?php
                                $link = getCMSContent("tw_link");
                                if ($link) {
                                    ?>
                                    <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-twitter"></i></a></li>
                                    <?php } ?>
                                    <?php
                                    $link = getCMSContent("ln_link");
                                    if ($link) {
                                        ?>
                                        <li><a href="<?php echo $link; ?>"><i class="fa fa-linkedin"></i></a></li>
                                        <?php } ?>
                                        <?php
                                        $link = getCMSContent("gp_link");
                                        if ($link) {
                                            ?>
                                            <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-google-plus"></i></a></li>
                                            <?php } ?>
                                            <?php
                                            $link = getCMSContent("yt_link");
                                            if ($link) {
                                                ?>
                                                <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-youtube"></i></a></li>
                                                <?php } ?>
                                                <?php
                                                $link = getCMSContent("inst_link");
                                                if ($link) {
                                                    ?>
                                                    <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-instagram"></i></a></li>
                                                    <?php } ?>
                                                    <?php
                                                    $link = getCMSContent("fl_link");
                                                    if ($link) {
                                                        ?>
                                                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-flickr"></i></a></li>
                                                        <?php } ?>
                                                        <?php
                                                        $link = getCMSContent("vimio_link");
                                                        if ($link) {
                                                            ?>
                                                            <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-vimeo-square"></i></a></li>
                                                            <?php } ?>
                                                            <?php
                                                            $link = getCMSContent("pintrest_link");
                                                            if ($link) {
                                                                ?>
                                                                <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-pinterest"></i></a></li>
                                                                <?php } ?>
                                                                <?php
                                                                $link = getCMSContent("vine_link");
                                                                if ($link) {
                                                                    ?>
                                                                    <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-vine"></i></a></li>
                                                                    <?php } ?>
                                                                    <?php
                                                                    $link = getCMSContent("pingler_link");
                                                                    if ($link) {
                                                                        ?>
                                                                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-bullseye"></i></a></li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php if ($this->session->userdata("user_id") > 0) { ?>
                                                        <div class="col-sm-6 col-xs-4">
                                                            <div class="rightman">
                                                                <div class="manouter">
                                                                    <div class="maninner">
                                                                        <a href="<?php echo site_url(); ?>newsfeed">
                                                                            <img src="<?php echo $this->session->userdata("image"); ?>" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <?php if ($this->session->userdata("club_image") != "") { ?>
                                                                <div class="footyouter">
                                                                    <div class="footyinner">
                                                                        <a href="<?php echo site_url() . "club/" . $this->session->userdata("club_slug"); ?>">
                                                                            <img src="<?php echo $this->session->userdata("club_image"); ?>" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <?php } ?>
                                                                <div class="social socialright">
                                                                    <ul class="social-share">
                                                                        <li title="Write Article"><a href="<?php echo site_url(); ?>articles/post/"><i class="fa fa-pencil"></i></a></li>   
                                                                        <li title="Message"><a href="<?php echo site_url(); ?>message"><?php if ($counts['msg'] > 0) { ?><span class="badges"><?php echo $counts['msg'] ?></span><?php } ?><i class="fa fa-comments"></i></a>

                                                                        </li>                                
                                                                        <li title="Notifications" style="position:relative;"><a href="<?php echo site_url(); ?>notification"><?php if ($counts['noti'] > 0) { ?><span class="badges"><?php echo $counts['noti'] ?></span><?php } ?><i class="fa fa-bell"></i></a>  <ul class="pull-dropdown">
                                                                            <li class="arraow-top"></li>
                                                                            <li class="header">
                                                                                Notifications
                                                                            </li>
                                                                            <?php
                                                                            if (count($noti_header['data']) == 0) {
                                                                                ?> 
                                                                                <li>
                                                                                    <div class="noti_single">
                                                                                        <div class="pd-outer">
                                                                                            <div class="pd-content" style="min-height: 40px; margin-top: 10px;">
                                                                                                No Notifications Available
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <?php
                                                                            }
                                                                            foreach ($noti_header['data'] as $notification) {
                                                                                ?>
                                                                                <li>
                                                                                    <a href="<?php echo $notification['url'] ?>" class="noti_single">
                                                                                        <div class="pd-outer">
                                                                                            <div class="pd-image" style="border-radius:0;">
                                                                                                <img src="<?php echo $notification['thumb']; ?>" />
                                                                                            </div>
                                                                                            <div class="pd-content" style="min-height: 45px;">
                                                                                                <div ><?php echo $notification['content']; ?></div>
                                                                                                <div class="comdate"><i class="fa fa-clock"></i> <?php
                                                                                                echo timeAgo(strtotime($notification['created_at']));
                                                                                                ?></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <?php } ?>
                                                                                <li class="footer">
                                                                                    <a href="<?php echo site_url(); ?>notification">See all Notifications</a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <?php } else { ?>

                                                        <div class="col-sm-3 col-xs-3">
                                                            <div class="toplinks first"><a class="pull-right" href="<?php echo site_url() . "login"; ?>">Login</a></div>

                                                            <div class="toplinks"><a class="pull-right" href="<?php echo site_url() . "signup"; ?>">Signup</a> </div>
                                                        </div>   

                                                        <?php } ?>
                                                    </div>

                                                </div><!--/.top-bar-->

                                                <nav class="navbar navbar-inverse dynamicheader" role="banner" >

                                                    <div class="navbar-header">



                                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                                            <span class="sr-only">Toggle navigation</span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                        </button>

                                                        <div class="mobile_search show_mobile" style="float:left;">

                                                            <div class="search">
                                                                <form role="form">
                                                                    <input type="text" class="search-form post_search" autocomplete="off" placeholder="Search...">
                                                                    <i class="fa fa-search btn_post_search"></i>
                                                                </form>
                                                            </div>

                                                        </div>
                                                        <div class="mobile_dd show_mobile" style="width:90px; margin-right: 10px;">
                                                            <div class="dropdown">
                                                                <?php if ($this->session->userdata("user_id") > 0) { ?>
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="    clear: both;
                                                                display: block;
                                                                overflow: hidden;
                                                                white-space: nowrap;     text-overflow: ellipsis;"><i class="fa fa-user"></i> <?php echo $this->session->userdata("name") ?> <i class="fa fa-caret-down"></i></a>

                                                                <ul class="dropdown-menu">
                                                                    <li><a href="<?php echo site_url(); ?>articles/post/">Write Article</a></li>
                                                                    <li><a href="<?php echo site_url(); ?><?php echo $this->session->userdata("slug"); ?>">My Articles</a></li>
                                                                    <li><a href="<?php echo site_url(); ?>newsfeed">Newsfeed</a></li>
                                                                    <li><a href="<?php echo site_url(); ?>user/edit">Profile</a></li>
                                                                    <li><a href="<?php echo site_url(); ?>user/logout">Logout</a></li>
                                                                </ul>
                                                                <?php } else { ?>
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> My Account</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="<?php echo site_url(); ?>login">Login</a></li>
                                                                    <li><a href="<?php echo site_url(); ?>signup">Sign Up</a></li>
                                                                </ul>
                                                                <?php } ?>
                                                            </div>
                                                        </div>




                                                    </div>

                                                    <div class="collapse navbar-collapse ">
                                                        <ul class="nav navbar-nav">
                                                            <li class="active"><a href="<?php echo site_url(); ?>">HOME</a></li>         
                                                            <li><a href="<?php echo site_url(); ?>articles/listing">ARTICLES</a></li>
                                                            <li><a href="<?php echo site_url(); ?>newsfeed">NEWSFEED</a></li>
                                                            <li><a href="<?php echo site_url(); ?>plus">FOOTYFANZ+</a></li>
                                                            <li><a href="<?php echo site_url(); ?>oyo5aside">EVENTS</a></li>
                                                            <li><a href="<?php echo site_url(); ?>contact">CONTACT</a></li>

                                                            <li  class="hide_mobile">
                                                                <div class="search">
                                                                    <form role="form">
                                                                        <input type="text" class="search-form post_search" autocomplete="off" placeholder="Search...">
                                                                        <i class="fa fa-search btn_post_search"></i>
                                                                    </form>
                                                                </div>
                                                            </li>
                                                            <li class="dropdown  lastmenu hide_mobile">
                                                                <?php if ($this->session->userdata("user_id") > 0) { ?>
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->session->userdata("name") ?> <i class="fa fa-caret-down"></i></a>

                                                                <ul class="dropdown-menu">
                                                                    <li><a href="<?php echo site_url(); ?>articles/post/">Write Article</a></li>
                                                                    <li><a href="<?php echo site_url(); ?><?php echo $this->session->userdata("slug"); ?>">My Articles</a></li>
                                                                    <li><a href="<?php echo site_url(); ?>newsfeed">Newsfeed</a></li>
                                                                    <li><a href="<?php echo site_url(); ?>user/edit">Profile</a></li>
                                                                    <li><a href="<?php echo site_url(); ?>user/logout">Logout</a></li>
                                                                </ul>
                                                                <?php } else { ?>
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> My Account</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="<?php echo site_url(); ?>login">Login</a></li>
                                                                    <li><a href="<?php echo site_url(); ?>signup">Sign Up</a></li>
                                                                </ul>
                                                                <?php } ?>

                                                            </li>
                                                        </ul>
                                                    </div>

                                                </nav><!--/nav-->

                                                <?php
                                                $lg_id = $this->session->userdata("site_league");
                                                if ($lg_id == "" or $lg_id == "0") {
                                                    $lg_id = "1";
                                                }
                                                ?>
                                                <div class="leaguemenu">
                                                    <div class="dropdownleague">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-angle-down"></i>
                                                            <span class="leaguetitle"><?php echo $data_league[$lg_id]['name'] ?></span>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <?php foreach ($data_league as $key => $dt) {
                                                                ?>
                                                                <li><a class="leagueselect" data-keyid='<?php echo $key ?>' data-id='<?php echo $dt['id'] ?>' data-image='<?php echo $dt['image'] ?>' style="cursor:pointer;"><?php echo $dt['name'] ?></a></li>
                                                                <?php }
                                                                ?>             
                                                            </ul>
                                                        </div>
                                                        <div class="leaguelogo">
                                                            <img src="<?php echo $data_league[$lg_id]['image'] ?>" />
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="clublist ">
                                                        <div>
                                                            <ul  class="clublistinner">
                                                                <?php foreach ($club_list['clubs'] as $key => $dt) {
                                                                    ?>
                                                                    <li><a data-toggle="tooltip" data-placement="top" title="<?php echo $dt['name']; ?>" href="<?php echo site_url() . 'club/' . $dt['slug']; ?>">
                                                                        <img src="<?php echo $dt['image']; ?>" />
                                                                    </a></li>
                                                                    <?php }
                                                                    ?>                  


                                                                </ul>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <?php if (1 == 2) { ?>
                                                        <div class="clubscore">
                                                            <div>
                                                                <ul  class="clubscoreinner">
                                                                    <li><a href="#">Crystal P <span class="scoreval">1</span></a></li>
                                                                    <li><a href="#">A Villa <span class="scoreval">0</span></a></li>
                                                                    <li><a href="#">Arsenal <span class="scoreval">6</span></a></li>
                                                                    <li><a href="#">Everton <span class="scoreval">0</span></a></li>
                                                                    <li><a href="#">Spurs <span class="scoreval">0</span></a></li>
                                                                    <li><a href="#">Newcast <span class="scoreval">4</span></a></li>
                                                                    <li><a href="#">West Ham <span class="scoreval">2</span></a></li>
                                                                    <li><a href="#">Man C <span class="scoreval">5</span></a></li>
                                                                    <li><a href="#">Watford <span class="scoreval">1</span></a></li>
                                                                    <li><a href="#">Leicester <span class="scoreval">2</span></a></li>
                                                                    <li><a href="#">Swansea <span class="scoreval">1</span></a></li>
                                                                    <li><a href="#">Man U <span class="scoreval">9</span></a></li>
                                                                    <li><a href="#">Bournmo <span class="scoreval">1</span></a></li>
                                                                    <li><a href="#">Norwich <span class="scoreval">0</span></a></li>
                                                                    <li><a href="#">Wesgt Bro <span class="scoreval">4</span></a></li>
                                                                    <li><a href="#">Sundarla <span class="scoreval">4</span></a></li>
                                                                    <li><a href="#">Southmap <span class="scoreval">1</span></a></li>
                                                                    <li><a href="#">Stroke <span class="scoreval">0</span></a></li>
                                                                    <li><a href="#">Chelsea <span class="scoreval">0</span></a></li>
                                                                    <li><a href="#">Liverpool <span class="scoreval">1</span></a></li>
                                                                </ul>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                        <div class="hide_mobile">
                                                            <?php
                                                            $gc = trim(getCMSContent("ad_ggl_large"));
                                                            if ($gc == "") {
                                                                $ads = getAds("small");
                                                                $cnt = count($ads);
                                                                if ($cnt > 0) {
                                                                    $num = array_rand($ads);
                                                                    ?>
                                                                    <div class="leftadd">
                                                                        <a href="<?php echo $ads[$num]['link'] ?>">
                                                                            <img src="<?php echo $ads[$num]['image']; ?>" />
                                                                        </a>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <div class="clear clearfix" style="margin-top: 10px;"></div>
                                                                <?php
                                                                echo $gc;
                                                                ?>
                                                                <div class="clear clearfix" style="margin-top: 0px;"></div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </header><!--/header-->

                                                    <script type="text/javascript">
                                                    $(document).ready(function(e) {

                                                        $(".btn_post_search").on('click', function() {
                                                            var search_key = $(this).closest("form").find(".post_search").val();
                                                            var url = encodeURI(site_url + "search/?search_key=" + search_key);
                                                            window.location.href = url;
                                                        });
                                                        $(".post_search").on('keyup', function(e) {
                                                            if (e.keyCode == 13) {
                                                                $(this).closest("form").find(".btn_post_search").click();
                                                            }
                                                        })

                                                        <?php if ($home_page == "1") { ?>
                                                            $(".leagueselect").on('click', function() {
                                                                var league_id = $(this).data("id");
                                                                //$(this).attr('data-id');
                                                                var post_data = {};
                                                                $.post(site_url + "home/setleague/" + league_id, post_data, function(res) {
                                                                    window.location.reload(true);
                                                                });
                                                            });
                                                            $('[data-toggle="tooltip"]').tooltip();
                                                            <?php } else { ?>
                                                                $(".leagueselect").on('click', function() {
                                                                    $(".leaguetitle").html($(this).html());
                                                                    $(".leaguelogo img").attr("src", $(this).data("image"));

                                                                    var league_id = $(this).data("id");
                                                                    var post_data = {};
                                                                    $.post(site_url + "home/getclubs/" + league_id, post_data, function(res) {
                    //        if($(".siteholder").data("home_page")=="1"){
                        window.location.href = site_url;
                        return;
                    //
                    //
                    //                                   }
                    var clubs = res[league_id]['clubs'];
                    $(".clublistinner").html('').hide();
                    $.each(clubs, function(k, v) {
                        var clubhtml = '<li><a data-toggle="tooltip" data-placement="top" title="' + v.name + '" href="' + site_url + "club/" + v.slug + '"><img src="' + v.image + '" /></a></li>';
                        $(".clublistinner").append(clubhtml);
                    });
                    $('[data-toggle="tooltip"]').tooltip();
                    $(".clublistinner").fadeIn();
                }, "JSON");

                                                                });
            //$(".leagueselect[data-id=<?php echo ($this->session->userdata("site_league") > 0) ? $this->session->userdata("site_league") : "1"; ?>]").click();
            <?php } ?>

        });
</script>





