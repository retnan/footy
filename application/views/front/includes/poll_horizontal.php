<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>
<?php
if (isset($poll_data) && $poll_data != false) {
    $ans_cnt = 2;
    if (trim($poll_data['option3']) != "") {
        $ans_cnt++;
    }
    if (trim($poll_data['option4']) != "") {
        $ans_cnt++;
    }
    if (trim($poll_data['option5']) != "") {
        $ans_cnt++;
    }
    $width = round(100 / $ans_cnt);
    ?>
    <style>
        .polloptioncon{width:<?php echo $width; ?>%; float:left; }
        @media (max-width: 967px) {
            .polloptioncon{width:50% !important;}
        }
    </style>
    <div class="poll" style="background: url(<?php echo $poll_data['image']; ?>); background-size: cover;">

        <div class="polltitle">

            <?php echo $poll_data['title']; ?>
        </div>
        <div style="    text-align: center;

             color: #fff;
             background: rgba(0, 0, 0, 0.19);
             width: 100%;
             font-size: 12px;">
            <?php echo $poll_data['option1'] . ": " . $poll_data['votes']['perA']; ?>%
            &nbsp; &nbsp; <?php echo $poll_data['option2'] . ": " . $poll_data['votes']['perB']; ?>%
            <?php if (trim($poll_data['option3']) != "") { ?>
                &nbsp; &nbsp; <?php echo $poll_data['option3'] . ": " . $poll_data['votes']['perC']; ?>%
            <?php } ?>
            <?php if (trim($poll_data['option4']) != "") { ?>
                &nbsp; &nbsp; <?php echo $poll_data['option4'] . ": " . $poll_data['votes']['perD']; ?>%
            <?php } ?>
            <?php if (trim($poll_data['option5']) != "") { ?>
                &nbsp; &nbsp; <?php echo $poll_data['option5'] . ": " . $poll_data['votes']['perE']; ?>%
            <?php } ?>

        </div>
        <div class="polloptions">
            <div class="polloptioncon">
                <div class="polloption  poll_answer" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="1"><?php echo $poll_data['option1']; ?></div>
            </div>
            <div class="polloptioncon">
                <div class="polloption  poll_answer" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="2"><?php echo $poll_data['option2']; ?></div>
            </div>
            <?php if (trim($poll_data['option3']) != "") { ?>
                <div class="polloptioncon">
                    <div class="polloption  poll_answer" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="3"><?php echo $poll_data['option3']; ?></div>
                </div>
            <?php } ?>
            <?php if (trim($poll_data['option4']) != "") { ?>
                <div class="polloptioncon">
                    <div class="polloption  poll_answer" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="4"><?php echo $poll_data['option4']; ?></div>
                </div>
            <?php } ?>
            <?php if (trim($poll_data['option5']) != "") { ?>
                <div class="polloptioncon">
                    <div class="polloption  poll_answer" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="5"><?php echo $poll_data['option5']; ?></div>
                </div>
            <?php } ?>
        </div>



        <div class="clear clearfix"></div>
    </div>
    <div class="clear clearfix"></div>
<?php } else { ?>

    <div class="poll" style="background:url(<?php echo $base; ?>images/pollbg.jpg); display:none;">

        <div class="polltitle">Who should start in Goal for Spanish's Euro Squad</div>
        <div class="polloption option1">De Gea</div>
        <div class="polloption option2">Casillas</div>
    </div>
<?php } ?>

<script type="text/javascript">
    $(document).on('click', ".poll_answer", function(e) {

        var post_data = $(this).data();
        $.confirm({
            title: 'Poll Voting',
            content: 'Are you sure to Vote your Option',
            confirmButton: 'Sure',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(base_url + "home/save_poll", post_data, function(res) {
                    alert("Your Vote has been Saved");

                });
            }
        });
    });
</script>
