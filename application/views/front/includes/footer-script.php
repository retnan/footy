<!-- JS Files and Scripts Section -->
<?php $base = base_url() . PUBLIC_DIR . "fassets/" ?>





<script src="<?php echo $base; ?>js/bootstrap.min.js"></script>
<script src="<?php echo $base; ?>js/jquery.prettyPhoto.js"></script>
<script src="<?php echo $base; ?>js/jquery.isotope.min.js"></script>
<script src="<?php echo $base; ?>js/jquery.validate.min.js"></script>
<script src="<?php echo $base; ?>js/additional-methods.js"></script>
<script src="<?php echo $base; ?>js/jquery.form.js" type="text/javascript"></script>
<script src="<?php echo $base; ?>js/main.js"></script>
<script src="<?php echo $base; ?>js/app.js"></script>
<script src="<?php echo $base; ?>js/wow.min.js"></script>

<script src="<?php echo $base ?>/pwa/promise.js"></script>
<script src="<?php echo $base ?>/pwa/fetch.js"></script>
<script src="<?php echo $base ?>/pwa/app.js"></script>

<!-- End of JS Files and Scripts Section --!>