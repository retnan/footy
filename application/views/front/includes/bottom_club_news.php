<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
$club_data = getLatestClubNews();

//print_r($club_data);
?>
<div class="row strip homeclubnews">
    <?php
    if ($club_data) {
        foreach ($club_data as $row) {
            ?>
            <div class="col-md-3">
                <div class="newspanel">
                    <div class="clearfix"></div>
                    <div class="panelheader">
                        <?php echo $row['club_name']; ?>
                    </div>
                    <div class="panelcontent">
                        <?php
                        if ($row['club_news']) {
                            foreach ($row['club_news'] as $club) {
                                ?>
                                <a href="<?php echo site_url() . NEWS_TAG . "/" ?><?php echo $club['slug'] ?>">
                                    <div class="newshead">
                                        <div class="leftsection">
                                            <img src='<?php echo $club['thumb']; ?>' />
                                        </div>
                                        <div class="rightsection"><?php echo $club['title']; ?></div>
                                    </div>
                                </a>
                                <?php
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
            <?php
        }
    }
    ?>

</div>