<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php
    echo $this->load->view("front/includes/header-script.php", (isset($header_data) ? $header_data : array()));
    ?>

</head><!--/head-->
<body class="loginpage">
    <?php echo isset($content) ? $content : '' ?>
    <?php
    echo $this->load->view("front/includes/footer-script.php", array());
    ?>
</body>
</html>