<meta name="robots" content="index, follow" />
<meta name="distribution" content="Global" />
<meta name="language" content="Hindi, English" />
<meta name="author" content="Sandeep Pundir" />
<meta name="copyright" content="<?php echo SITE_NAME; ?>" />

<?php $base_fav = site_url() . "public/fassets/fevicons/" ?>
<!-- Fav and touch icons starts -->
<link rel="apple-touch-icon" sizes="192x192" href="<?php echo $base_fav; ?>apple-icon.png">
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $base_fav; ?>apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $base_fav; ?>apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $base_fav; ?>apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $base_fav; ?>apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $base_fav; ?>apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $base_fav; ?>apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $base_fav; ?>apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $base_fav; ?>apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $base_fav; ?>apple-icon-180x180.png">
<link rel="apple-touch-icon" sizes="192x192" href="<?php echo $base_fav; ?>apple-icon-precomposed.png">

<link rel="icon" type="image/png" sizes="36x36"  href="<?php echo $base_fav; ?>android-icon-36x36.png">
<link rel="icon" type="image/png" sizes="48x48"  href="<?php echo $base_fav; ?>android-icon-48x48.png">
<link rel="icon" type="image/png" sizes="72x72"  href="<?php echo $base_fav; ?>android-icon-72x72.png">
<link rel="icon" type="image/png" sizes="96x96"  href="<?php echo $base_fav; ?>android-icon-96x96.png">
<link rel="icon" type="image/png" sizes="144x144"  href="<?php echo $base_fav; ?>android-icon-144x144.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $base_fav; ?>android-icon-192x192.png">

<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $base_fav; ?>favicon.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $base_fav; ?>favicon-16x16.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $base_fav; ?>favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $base_fav; ?>favicon-96x96.png">

<meta name="msapplication-TileImage" content="<?php echo $base_fav; ?>ms-icon-70x70.png">
<meta name="msapplication-TileImage" content="<?php echo $base_fav; ?>ms-icon-144x144.png">
<meta name="msapplication-TileImage" content="<?php echo $base_fav; ?>ms-icon-150x150.png">
<meta name="msapplication-TileImage" content="<?php echo $base_fav; ?>ms-icon-310x310.png">

<link rel="manifest" href="<?php echo $base_fav; ?>manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
<!-- Fav and touch icons ends -->