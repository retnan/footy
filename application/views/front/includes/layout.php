<?php

$base = base_url() . PUBLIC_DIR . "fassets/";

?><!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">
    
    <title>FootyFanz</title>

    <?php

    echo $this->load->view("front/includes/header-script.php", (isset($header_data) ? $header_data : array()));

    ?>

    <?php

    if (isset($css)) {

        if (is_array($css)) {

            foreach ($css as $css_file) {

                ?>

                <link type="text/css" rel="stylesheet" href="<?php echo base_url() . PUBLIC_DIR . $css_file; ?>"/>

                <?php

            }

        } else if ($css != '') {

            ?>

            <link type="text/css" rel="stylesheet" href="<?php echo base_url() . PUBLIC_DIR . $css; ?>"/>

            <?php

        }

    }

    ?>



    <!-- Google Tag Manager -->

    <script>(function(w, d, s, l, i) {

        w[l] = w[l] || [];

        w[l].push({'gtm.start':

            new Date().getTime(), event: 'gtm.js'});

        var f = d.getElementsByTagName(s)[0],

        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';

        j.async = true;

        j.src =

        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;

        f.parentNode.insertBefore(j, f);

    })(window, document, 'script', 'dataLayer', 'GTM-PQKLPDF');</script>

    <!-- End Google Tag Manager -->

    <!-- Facebook Pixel Code -->

    <script>

    !function(f, b, e, v, n, t, s) {

        if (f.fbq)

            return;

        n = f.fbq = function() {

            n.callMethod ?

            n.callMethod.apply(n, arguments) : n.queue.push(arguments)

        };

        if (!f._fbq)

            f._fbq = n;

        n.push = n;

        n.loaded = !0;

        n.version = '2.0';

        n.queue = [];

        t = b.createElement(e);

        t.async = !0;

        t.src = v;

        s = b.getElementsByTagName(e)[0];

        s.parentNode.insertBefore(t, s)

    }(window,

        document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '334983033546976'); // Insert your pixel ID here.

            fbq('track', 'PageView');

            </script>



            <!-- DO NOT MODIFY -->

            <!-- End Facebook Pixel Code -->

        </head><!--/head-->



        <body class="homepage">

            <noscript><img height="1" width="1" style="display: none" src="https://www.facebook.com/tr?id=334983033546976&ev=PageView&noscript=1" /></noscript>

               <!-- Google Tag Manager (noscript) -->

               <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PQKLPDF" height="0" width="0" style="display: none; visibility: hidden;"></iframe></noscript>

                  <!-- End Google Tag Manager (noscript) -->

                  <div class="hide_mobile" style="position: absolute; left: 0; top: 0; width: 100%;">

                    <div style="position: relative">

                        <div class="container" style="margin: 0px auto; ">

                            <div class="row " >

                                <div class="col-md-12" style="margin-top: 25px;">

                                    <?php

                                    $gc = trim(getCMSContent("ad_ggl_large"));
                                    
                                    // $ads = getAds("large");


                                    $ads = getSiteAds(1, "0", "large");

                                    $cnt = count($ads);

                                    if ($cnt > 0) {

                                        $num = array_rand($ads);

                                        ?>

                                        <div class="leftadd" style=" width:70%; margin-left: 15%;  max-width: 728px; max-height: 90px;">

                                            <a href="<?php echo $ads[$num]['link'] ?>">

                                                <img src="<?php echo $ads[$num]['image']; ?>" style="width: 100%; max-width: 728px; max-height: 90px;" />

                                            </a>

                                        </div>

                                        <?php

                                    } else {

                                        echo $gc;

                                    }

                                    ?>

                                </div>

                            </div>

                        </div>

                    </div>



                </div>



                <div class="container">

                    <?php echo $this->load->view("front/includes/menu_fixed.php", array()); ?>

                    <div class="siteholder">

                        <?php

                        if (isset($home_page) and $home_page == 1) {

                            

                        } else {

                            $home_page = "0";

                        }



                        echo $this->load->view("front/includes/header-menu.php", (isset($data_league) ? array("data_league" => $data_league, "counts" => $counts, "noti_header" => $noti_header, "home_page" => $home_page, "club_list" => $club_list) : array("counts" => $counts, "noti_header" => $noti_header, "home_page" => $home_page, "club_list" => $club_list)));

                        ?>

                        <div class="testcalss"></div>

                        <?php echo isset($content) ? $content : '' ?>

                    </div>

                    <div class="clearfix"></div> 



                </div>

                <div class="footer">

                    <div class="container">

                        <div class="footerouter">

                            <div class="row">

                                <div class="col-md-1 col-sm-3 col-xs-3">

                                    <img src="<?php echo $base; ?>images/logosmall.png" />

                                </div>

                                <div class="col-md-3 col-sm-9 col-xs-9">

                                    <?php echo getCMSContent("ft_about"); ?>

                                </div>

                                <div class="col-md-2 col-sm-6 col-xs-6">



                                    <ul class="footerlink">

                                        <li><a href="<?php echo site_url(); ?>">Home</a></li>         

                                        <li><a href="<?php echo site_url(); ?>articles/listing">Articles</a></li>

                                        <li><a href="<?php echo site_url(); ?>newsfeed">Newsfeed</a></li>

                                    </ul>

                                </div>

                                <div class="col-md-2 col-sm-6 col-xs-6">



                                    <ul class="footerlink" style="padding-left: 0">                                

                                        <li><a href="<?php echo site_url(); ?>footyfanz">Footyfanz+</a></li>

                                        <li><a href="#">Featured</a></li>

                                        <li><a href="<?php echo site_url(); ?>contact">Contact Us</a></li>

                                    </ul>

                                </div>

                                <div class="col-md-3">                          

                                    <?php echo getCMSContent("ft_contact"); ?>

                                </div>

                            </div>

                            <div class="footerline"></div>

                            <div class="row">

                                <div class="col-md-12" style="text-align:center;  ">                          

                                    Copyright&copy;2016 <a href="<?php echo site_url(); ?>">FootyFanz</a>

                                </div>

                            </div>

                        </div>

                    </div>         

                </div>

                <?php

                echo $this->load->view("front/includes/footer-script.php", array());

                ?>

                <?php

                if (isset($js)) {

                    if (is_array($js)) {

                        foreach ($js as $j) {

                            ?>

                            <script type="text/javascript" src="<?php echo base_url() . PUBLIC_DIR . $j; ?>"></script>

                            <?php

                        }

                    } else if ($js != '') {

                        ?>

                        <script type="text/javascript" src="<?php echo base_url() . PUBLIC_DIR . $js; ?>"></script>

                        <?php

                    }

                }

                ?>



                <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

                    <div class="modal-dialog" role="document">

                        <div class="modal-content">



                        </div>

                    </div>

                </div>

            </body>

            </html>



