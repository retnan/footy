﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">



        <?php echo $this->load->view("front/includes/seo.php") ?>


        <?php
        $header_data = (isset($header_data) AND is_array($header_data)) ? $header_data : array("title" => "", "desc" => "", "image" => "");

        if (isset($article['heder_meta'])) {
            echo $article['heder_meta'];
        } else if (isset($cat_data['header_meta'])) {
            echo $cat_data['header_meta'];
        } else if ($_SERVER['REQUEST_URI'] == "/") {
            echo getCMSContent("homemeta");
            ?>
            <title><?php echo isset($title) ? $title : "JyotishShastra - हिंदी भाषा का विश्वस्तरीय सम्पूर्ण एवं सटीक ज्योतिष ब्लॉग"; ?></title>
            <?php
        } else {
            echo getCMSContent("innermeta");
            ?>
            <title><?php echo isset($title) ? $title : "JyotishShastra - हिंदी भाषा का विश्वस्तरीय सम्पूर्ण एवं सटीक ज्योतिष ब्लॉग"; ?></title>
            <?php
        }
        ?>
        <link rel="canonical" href="https://jyotishshastra.com<?php echo $_SERVER['REQUEST_URI']; ?>" />
        <?php
        echo $this->load->view("front/includes/header-script.php", $header_data);
        ?>

        <?php
        if (isset($css)) {
            if (is_array($css)) {
                foreach ($css as $css_file) {
                    ?>
                    <link type="text/css" rel="stylesheet" href="<?php echo base_url() . PUBLIC_DIR . "fassets/" . $css_file; ?>"/>
                    <?php
                }
            } else if ($css != '') {
                ?>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url() . PUBLIC_DIR . "fassets/" . $css; ?>"/>
                <?php
            }
        }
        ?>
        <?php
        if (isset($pagetype) && $pagetype == "articleview") {
            $slug_data = explode("-", $article['slug']);
            $slug_data2 = explode("-", $article['cslug']);
            $slugs = array_merge($slug_data, $slug_data2);
            $keywords_str = '["' . implode('","', $slugs) . '"]';
            foreach ($slugs as $key => $value) {
                $slugs[$key] = ucfirst($value);
            }
            $topics = implode(",", $slugs);
            ?>
            <script type="application/ld+json">{"@context":"http://schema.org","headline":"<?php echo str_replace('"', "", $article['title']) ?>","url":"<?php echo site_url() . ARTICLE_TAG . "/" . $article['cslug'] . "/" . $article['slug'] ?>",
                "image":{
                "@type": "ImageObject",
                "url": "<?php echo $article['thumb'] ?>",
                "width": "<?php echo $article['thumb_width'] ?>",
                "height": "<?php echo $article['thumb_height'] ?>"
                },
                "keywords":<?php echo $keywords_str ?>,"@type":"Article","dateCreated":"<?php echo date("Y-m-d", strtotime($article['created_at'])) ?>",
                "datePublished":"<?php echo date("Y-m-d", strtotime($article['created_at'])) ?>",
                "publisher":{"@type": "Organization","name":"<?php echo $article['user']['name']; ?>",
                "logo":{"@type": "ImageObject",
                "url": "<?php echo site_url() ?>public/fassets/images/logo_schema.png",
                "width": 600,
                "height": 70}
                },
                "dateModified":"<?php echo date("Y-m-d", strtotime($article['created_at'])) ?>",
                "articleSection":"<?php echo $article['categories'][0]['name'] ?>","creator":"<?php echo $article['user']['name']; ?>","author":"<?php echo $article['user']['name']; ?>","mainEntityOfPage":"True"}</script>




            <?php
        }
        if (isset($pagetype) && $pagetype == "categoryview") {


            $slug_data = explode("-", $cat_data['slug']);
            foreach ($slug_data as $key => $value) {
                $slug_data[$key] = ucwords($value);
            }

            $keywords_str = '["' . implode('","', $slug_data) . '"]';
            ?>
            <script type="application/ld+json">{"@context":"http://schema.org","headline":"<?php echo $cat_data['category']; ?>","url":"<?php echo site_url() . CATEGORY_TAG . "/" . $cat_data['slug']; ?>","image":null,"keywords":<?php echo $keywords_str ?>,"@type":"WebPage"}</script>


        <?php }
        ?>

    </head><!--/head-->
    <body>
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '1386300991479135',
              xfbml      : true,
              version    : 'v2.11'
            });
            FB.AppEvents.logPageView();
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "https://connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>
        <?php echo $this->load->view("front/includes/header-menu.php", array('menu' => $menu, 'data_hint' => $data_hint, 'data_categories' => $data_categories)) ?>
        <section class="page_outer ">
            <div class="container ">
                <?php echo isset($content) ? $content : '' ?>
            </div><!--/container-->
        </section><!--/pricing-page-->
        <?php echo $this->load->view("front/includes/footer.php", array()) ?>
        <?php echo $this->load->view("front/includes/footer-script.php"); ?>
        <?php
        if (isset($js)) {
            if (is_array($js)) {
                foreach ($js as $j) {
                    ?>
                    <script type="text/javascript" src="<?php echo base_url() . PUBLIC_DIR . "fassets/" . $j; ?>"></script>
                    <?php
                }
            } else if ($js != '') {
                ?>
                <script type="text/javascript" src="<?php echo base_url() . PUBLIC_DIR . "fassets/" . $js; ?>"></script>
                <?php
            }
        }
        ?>

        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-77370405-1']);
            _gaq.push(['_trackPageview']);
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        </script>






        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-77370405-1', 'jyotishshastra.com');
            ga('send', 'pageview');

        </script>   </body>
</html>