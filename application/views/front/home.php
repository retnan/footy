<style>
    .dashboard-date-range {
        display: none;
        padding-top: -1px;
        margin-right: 0px;
        margin-top: -8px;
        padding: 10px;
        padding-bottom: 8px;
        cursor: pointer;
        /*    color: #fff;
            background-color: #e02222;*/

        color: #333;
        background-color: #e5e5e5;
        padding-top: 8px;
        font-size:14px;
    }
    .label-grey{
        display:inline-block;
        background-color:#eee;
        color:#444;
        padding: 2px 4px;
        line-height: 15px;
        text-shadow:none;
        font-weight:600;
    }
    .dashboard-date-range:hover{
        background-color: #d8d8d8;
    }

    .pointtertop
    {
        position:absolute;
        z-index:100;
        top:3.5px;
        right:-5.9px;
        border-top:8px solid #333;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;

    }
    .pointterbottom
    {
        position:absolute;
        z-index:100;
        bottom:4.5px;
        right:-6.0px;
        border-bottom: 8px solid #333;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
    }
</style>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Statistics
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    <a href="#">Statistics</a>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->

    <!-- END PAGE CONTENT-->
    <div class="page-content-body">    <div id="dashboard">
            <!-- BEGIN DASHBOARD STATS -->
            <div class="row-fluid">
                <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                    <div class="dashboard-stat blue">
                        <div class="visual">
                            <i class="icon-play-circle"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                2000
                            </div>
                            <div class="desc">                           
                                Article
                            </div>
                        </div>
                        <a class="more" href="#">
                            View List <i class="m-icon-swapright m-icon-white"></i>
                        </a>                 
                    </div>
                </div>


                <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                    <div class="dashboard-stat red">
                        <div class="visual">
                            <i class="icon-play"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                200
                            </div>
                            <div class="desc">
                                Category
                            </div>
                        </div>
                        <a class="more" href="#">
                            View List <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>

                <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                    <div class="dashboard-stat yellow">
                        <div class="visual">
                            <i class="icon-book"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                100
                            </div>
                            <div class="desc">
                                Users
                            </div>
                        </div>
                        <a class="more" href="#">
                            View List <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>

                <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                    <div class="dashboard-stat green">
                        <div class="visual">
                            <i class="icon-bug"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                100
                            </div>
                            <div class="desc">
                                Views
                            </div>
                        </div>
                        <a class="more" href="#">
                            View List <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>




            </div>
            <div class="row-fluid">
                <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                    <div class="dashboard-stat purple">
                        <div class="visual">
                            <i class="icon-beaker"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                10
                            </div>
                            <div class="desc">
                                Ads
                            </div>
                        </div>
                        <a class="more" href="#">
                            View List <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>

                <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                    <div class="dashboard-stat blue">
                        <div class="visual">
                            <i class="icon-group"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                54
                            </div>
                            <div class="desc">
                                Messages
                            </div>
                        </div>
                        <a class="more" href="#">
                            View List <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>

                <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                    <div class="dashboard-stat yellow">
                        <div class="visual">
                            <i class="icon-group"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                574
                            </div>
                            <div class="desc">
                                Weekly Views
                            </div>
                        </div>
                        <a class="more" href="#">
                            View List <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="portlet box blue">
                    <div class="portlet-title" style="padding: 7px 0px 0px 10px;">
                        <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="th_icon"></i> Daily Article Views</div>
                        <div class="tools" style="margin-top: 1px;">
                            <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive bm_wt" data-page="<?php echo base_url() . ADMIN_DIR . "home/status_graph" ?>" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change Date Range">
                                <i class="icon-calendar"></i>
                                <span></span>
                                <i class="icon-angle-down"></i>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="graph_stat" id="graph_stat"></div>
                    </div>
                </div>               
            </div>
            <div id="load_script"></div>
        </div>
    </div>

</div>