<div class="cm_box newsletter_box">
    <div class="cm-portlet yellow fest-review">
        <div class="cm-portlet-header">
            Newsletter
        </div>
        <div class="cm-portlet-content" style="margin-top:  20px ;">
            <div class="alert alert-success news_success" style="display: none;     padding: 4px 10px;"></div>
            <div class="alert alert-danger news_error" style="display: none;     padding: 4px 10px;"></div>
            <form class="form-horizontal" method="post" id="newsletterform">
                <div class="">
                    <div class="col-md-3 text-right">
                        <label style="margin-top: 5px;">Email:</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control required email" placeholder="Email" data-msg-required="Please enter email" id="email" name="email">
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="col-md-3 text-right">
                        <label style="margin-top: 5px;">Category:</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select class="form-control" name="category" id="category">
                                <option value="0">All Categories</option>
                                <?php foreach ($categories as $key => $category) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $category['category']; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <div class="col-md-3 text-right">
                            <label style="margin-top: 5px;">Captcha:</label>
                        </div>
                        <div class="col-md-5">

                            <input type="text" class="form-control required" placeholder="Captcha" id="captcha" name="captcha" data-msg-required="Please enter captcha" >

                        </div>
                        <div class="col-md-4 captcha_container">
                            <img src="<?php echo site_url(); ?>general/captcha/newsletter" />
                            <div class="refresh_captcha" data-type="newsletter"><i class="fa fa-refresh"></i></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div style="margin: 5px 0px;">
                    <button class="btn mini green pull-right submitnewsletter">Subscribe</button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>