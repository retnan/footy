<?php
if ($messages) {
    foreach ($messages['data'] as $message) {
        ?>
        <div class="commetsingle">
            <div class="comimage">
                <img src="<?php echo $message['thumb']; ?>" />
            </div>
            <div class="comcontent">
                <div class="arraowleft"></div>
                <div class="comheader">
                    <?php echo $message['title']; ?>

                </div>
                <div class="comtext">
                    <div><?php echo nl2br(makeClickableLinks($message['content'])); ?>
                    </div>
                </div>
                <div class="articlestrip">
                    
                    <a href="#" class="viewall msg_delete" data-id="<?php echo $message['pk_message_id']; ?>">
                        Delete
                    </a>
                    <a href="#" class="viewall " >
                        Reply
                    </a>
                    

                    <div class="uname">by <?php echo $message['name']; ?> <div class="comdate"><i class="fa fa-clock"></i> <?php
                        echo timeAgo(strtotime($message['created_at']));
                        ?></div></div>                                
                </div>
            </div>
        </div>



        <?php
    }
}
if (!$messages['load']) {
    ?>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $(".commentloader").hide();

        });
    </script>

    <?php
}
?>

