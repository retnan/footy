<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?> 
<section class="articleheader">
    <div class="row hide">
        <div class="col-md-5">
            <div class="box1">
                <div class="boxheader">Ibkdreams</div>
                <div class="boxcontent">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="user">
                                <div class="manouter">
                                    <div class="maninner">
                                        <img src="<?php echo $base; ?>images/laughing.jpg" />
                                    </div>
                                </div>
                                <div class="footyouter">
                                    <div class="footyinner">
                                        <img src="<?php echo $base; ?>images/clubs/manchester_city.png" />
                                    </div>
                                </div>                                                
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="clearfix"></div>
                            <div class="victory">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <img src="<?php echo $base; ?>images/victor.png" />
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <img src="<?php echo $base; ?>images/victor.png" />
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <img src="<?php echo $base; ?>images/victor.png" />
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <img src="<?php echo $base; ?>images/victor.png" />
                                    </div>
                                </div>
                            </div>
                            <div style="margin-top: 15px;">
                                <div class="borderbutton">1st</div>
                                <div class="borderbutton">Follow</div>
                                <div class="borderbutton">Message</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="teamheader">
                <img src="<?php echo $base; ?>images/teamheader.jpg" />
                <img class="logo" src="<?php echo $base; ?>images/clubs/manchester_united.png" />
                <div class="sociallinks">
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="insta"><i class="fa fa-instagram"></i></a>
                    <a href="#" class="feed"><i class="fa fa-rss"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="articlecommentbox hide">
    <form action="" id="comment_form">                           
        <input type="hidden" id="post_type" name="post_type" value="A">
        <input type="hidden" id="post_id" name="post_id" value="19"></form>

    <div><div class ="borderbutton btn_comment">Post Comment</div></div>
</div>
<section class="articlecontent">
    <div class="row">
        <div class="col-md-8">
            <div class="row strip">
                <div class="col-md-12">
                    <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                        My Messages
                    </div>
                </div>
            </div>




            <div class="bd-example" data-example-id="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="commentcontainer messagesview">



                        </div>
                        <div class="commentloader">
                            <div class="loadmore" onclick="">View more Messages</div>
                            <div class="loaderview"><img src="<?php echo $base; ?>images/loadingBar.gif" /></div>
                        </div>



                    </div>
                </div>         
            </div>

        </div>
        <div class="col-md-4">
            <?php
            $this->load->view("front/includes/right_panel_news.php", array());
            ?>
        </div>
    </div>


</section>
<script type="text/javascript">
    var page = 1;
    function loadmessage() {
        $('.commentloader').addClass('loading');
        var post_data = {post_id: $("#post_id").val(), ptype: 'A', page: page};
        $.post(base_url + "message/loadmessage", post_data, function(res) {
            $('.commentloader').removeClass('loading');
            $('.messagesview').append(res);
        });
    }
    $(".loadmore").click(function() {
        page++;
        loadmessage();
    });
    $(document).ready(function(e) {
        loadmessage();
        $('.messagesview').on('click', '.msg_delete', function(e) {
            var container = $(this).closest(".commetsingle");
            var msg_id = $(this).data("id");
            e.preventDefault();
            $.confirm({
                title: 'Delete Message',
                content: 'Are you sure to Delete Message',
                confirmButton: 'Delete',
                confirmButtonClass: 'btn-danger',
//                                    icon: 'fa fa-question-circle',
                animation: 'scale',
                animationClose: 'top',
                opacity: 0.5,
                confirm: function() {
                    var post_data = {msg_id: msg_id};
                    $.post(base_url + "message/delete_message", post_data, function(res) {
                        container.fadeOut(function() {
                            container.remove();
                        })
//                        loadmessage();
                    });

                }
            });
        });
    });
</script>