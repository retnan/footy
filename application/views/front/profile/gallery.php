<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?> 

<section class="newsfeed">
    <?php
    if ($ud) {
        $this->load->view("front/profile/profileheader.php", array("ud" => $ud, "follow_status" => $follow_status));
    }
    ?>

    <div class="row strip">
        <div class="col-md-12">
            <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                Gallery
            </div>
        </div>
    </div>

    <div class="row">        
        <div class="col-md-8" style="padding: 0;">
            <div class="">
                <?php if ($follow_status == "NA") { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="addmedia">
                                <div class="youtubesec">
                                    <div class="btnYoutube">Add</div>
                                    <div class="txtBox">
                                        <input type="text" name="youtube_link" id="youtube_link" placeholder="Add youtube link" value="" />
                                    </div>
                                </div>
                                <div class="invalidYoutube" id="invalidYoutube">Invalid Youtube link</div>
                                <div class="orsection"><span>OR</span></div>
                                <div class="imagesection"><span>Upload Image</span></div>
                                <div class="uploadpouter" style="display: none; margin-top:-15px; margin-bottom: 10px;">
                                    <div class="uploadpinner" style="width: 0%;"></div>
                                </div>
                                <div class="clear clearfix"></div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="usergallery">
                            <div id="listing_data"></div>
                            <?php
                            paginationScriptFront(base_url() . "profile/gallery_data", "listing_data", "user_id=" . $user_id);
                            ?>

                        </div>                       

                    </div>
                </div>
            </div>       


        </div>
        <div class="col-md-4">           
            <?php
            $this->load->view("front/includes/right_panel_news.php", array('bottom' => false));
            ?>


        </div>
    </div>

</section>

<form method="post" id="postform" action="<?php echo site_url() . "profile/uploadgalleryimage" ?>" enctype="multipart/form-data">
    <input type="file" name="fileimage" id="fileimage" accept="image/x-png, image/gif, image/jpeg" style="display: none;" />
</form>
<script type="text/javascript">
    function matchYoutubeUrl(url) {
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        var matches = url.match(p);
        if (matches) {
            return matches[1];
        }
        return false;
    }
    var uploging = false;
    $(document).ready(function(e) {
        $(".fancybox-effects-a").fancybox();
        $('.fancybox-media')
                .attr('rel', 'media-gallery')
                .fancybox({
                    openEffect: 'none',
                    closeEffect: 'none',
                    prevEffect: 'none',
                    nextEffect: 'none',
                    arrows: false,
                    helpers: {
                        media: {},
                        buttons: {}
                    }
                });
        $(".btnYoutube").on("click", function() {
            $("#invalidYoutube").hide();
            var link = $("#youtube_link").val();
            if ($.trim(link) != "") {
                var yt_id = matchYoutubeUrl(link);
                if (yt_id == false) {
                    $("#invalidYoutube").show();
                } else {
                    var post_data = {id: yt_id};
                    $.post(site_url + "profile/savegalleryvideo", post_data, function(res) {
                        if (res.status == "1") {
                            $("#youtube_link").val('');
                            $(".searchBtn").click();
                        }
                    }, "json");
                }
            }
        });




        $('#postform').ajaxForm({
            //dataType: 'json',
            beforeSubmit: function() {
                uploging = true;
                $(".imagesection span").html("Uploading...");
                $(".uploadpouter").show();
                $(".uploadpinner").css("width", "0%");
            },
            uploadProgress: function(event, position, total, percentComplete) {
                if (percentComplete == 100) {
                    $(".uploadpouter").hide();
                } else {
                    $(".uploadpinner").css("width", percentComplete + '%');
                }
            },
            success: function(res) {
                uploging = false;
                $(".uploadpouter").hide();
                $(".imagesection span").html("Upload Image");
                $(".searchBtn").click();
            },
            failed: function() {
                uploging = false;
                $(".uploadpouter").hide();
                $(".imagesection span").html("Upload Image");
            }
        });
        $(".imagesection").click(function() {
            $("#fileimage").click();
        });
        $("#fileimage").change(function() {
            if ($(this).val() != "") {
                $("#postform").submit();
            }
        });
        $(document).on("click", ".deletebtn", function() {
            var thisid = $(this).data("id");
            $.confirm({
                title: 'Delete Media?',
                content: 'Are you sure to remove this media?',
                confirmButton: 'Yes',
                confirmButtonClass: 'btn-success',
                animation: 'scale',
                animationClose: 'top',
                opacity: 0.5,
                confirm: function() {
                    var post_data = {id: thisid};
                    $.post(site_url + "profile/delete_data", post_data, function(res) {
                        $(".pagination .cpageval").removeClass("active").addClass("inactive").click();
                    });
                }
            });


        });
    });
</script>



