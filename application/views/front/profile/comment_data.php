<?php
if ($data) {
    foreach ($data as $com) {
        ?> 
        <div class="commentsingle" style="display: none;" data-last_id="<?php echo $com['pk_comment_id']; ?>">
            <div class="com_data_block">
                <div class="comuser">
                    <img  src="<?php echo $com['thumb']; ?>" />
                </div>
                <div class="commenttext">
                    <div class="comuname">
                        <a href="<?php echo site_url() . "articles/listing/" . $com['slug']; ?>"><?php echo $com['name']; ?></a>
                        <?php if ($com['remove_com'] == true) { ?>
                            <div class="remove btn_remove_comment" data-id="<?php echo $com['pk_comment_id']; ?>"><i class="fa fa-times"></i></div>
                        <?php } ?>
                        <?php if ($com['fk_user_id'] == $this->session->userdata('user_id')) { ?>
                            <div class="remove btn_edit_comment" data-id="<?php echo $com['pk_comment_id']; ?>"><i class="fa fa-pencil"></i></div>


                        <?php } ?>
                        <div class="comdate"><?php echo timeAgo($com['created_at']); ?></div>
                    </div>
                    <div class="com_text_temp" style="display: none;"><?php echo trim($com['content']); ?></div>
                    <div class="com_text_block">
                        <?php echo nl2br(makeClickableLinks($com['content'])); ?> 
                    </div>
                    <div class="line"></div>
                </div>
            </div>
            <div class="com_edit_block">

            </div>
        </div>
        <?php
    }
}
?>