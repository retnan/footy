<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?> 
<?php
//print_r($data);

if ($data) {
    foreach ($data as $feed) {
        $comments = $feed['comments'];
        $shared_by = $feed['shared_by'];
        ?> 
        <div class="feedsingle" data-last_feed="<?php echo $feed['pk_feed_id']; ?>">
            <?php
            if ($feed['art_data']) {
                $fd = $feed['art_data'];
//                print_r($fd);
                ?>
                <div class="feedheader">
                    <div class="postercon">
                        <div class="feeduser">
                            <img  src="<?php echo $feed['thumb']; ?>" />                                                
                        </div>
                        <div class="feedhead">

                            <div class="username">
                                <a href="<?php echo site_url() . "articles/listing/" . $feed['slug']; ?>"><?php echo $feed['name']; ?> posted a Article</a>
                                <?php if ($shared_by) { ?>
                                    <span style="font-size: 13px;margin-top: 1px;margin-left: 2px;"> Shared a post from <a href="<?php echo site_url() . "articles/listing/" . $shared_by['slug']; ?>"><?php echo $shared_by['name']; ?></a>
                                    </span><?php } ?>
                            </div>

                            <div class="feedtime"><?php echo timeAgo(strtotime($feed['created_at'])); ?></div>
                            <?php if ($feed['remove_feed']) { ?>
                                <div class="dropdown-toggle pull-right">
                                    <div class="feedmenu" data-toggle="dropdown"><i class="fa fa-angle-down"></i></div>
                                    <ul class="dropdown-menu">
                                        <li><a style="cursor: pointer;" class="delete_feed" data-id="<?php echo $feed['pk_feed_id']; ?>">Delete</a></li>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="feedmiddle">
                    <h4 style="color: #33f1ff"><a href="<?php echo site_url() . ARTICLE_TAG . "/" . $fd['slug']; ?>"> <?php echo $fd['title']; ?></a></h4>
                    <a href="<?php echo site_url() . ARTICLE_TAG . "/" . $fd['slug']; ?>">

                        <p style="color:#777;">
                            <?php echo wordLimiter(strip_tags($fd['content']), 160); ?>
                        </p>
                        <div class="clearfix"></div>
                        <div class="feedimage">
                            <?php if ($fd['file_type'] == 'I' and $fd['image'] != "") { ?>
                                <img src="<?php echo $fd['image']; ?>" />
                            <?php } ?>
                            <?php if ($fd['file_type'] == 'V' and $fd['vid_link'] != "") { ?>

                                <iframe class="articlevideo" src="<?php echo $fd['vid_link']; ?>" frameborder="0" allowfullscreen></iframe>
                            <?php } ?>
                        </div>
                    </a>
                </div>
            <?php } else { ?>
                <div class="feedheader">
                    <div class="postercon">
                        <div class="feeduser">
                            <img  src="<?php echo $feed['thumb']; ?>" />                                                
                        </div>
                        <div class="feedhead">

                            <div class="username">
                                <a href="<?php echo site_url() . "articles/listing/" . $feed['slug']; ?>"><?php echo $feed['name']; ?></a>
                                <?php if ($shared_by) { ?>
                                    <span style="font-size: 13px;margin-top: 1px;margin-left: 2px;"> Shared a News from <a href="<?php echo site_url() . "articles/listing/" . $shared_by['slug']; ?>"><?php echo $shared_by['name']; ?></a>
                                    </span><?php } ?>
                            </div>

                            <div class="feedtime"><?php echo timeAgo(strtotime($feed['created_at'])); ?></div>
                            <?php if ($feed['remove_feed']) { ?>
                                <div class="dropdown-toggle pull-right">
                                    <div class="feedmenu" data-toggle="dropdown"><i class="fa fa-angle-down"></i></div>
                                    <ul class="dropdown-menu">
                                        <li><a style="cursor: pointer;" class="delete_feed" data-id="<?php echo $feed['pk_feed_id']; ?>">Delete</a></li>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="feedmiddle">
                    <p>
                        <?php echo $feed['content']; ?> 
                    </p>
                    <div class="clearfix"></div>
                    <div class="feedimage">
                        <?php if ($feed['file_type'] == 'I' and $feed['image'] != "") { ?>
                            <img src="<?php echo $feed['image']; ?>" />
                        <?php } ?>
                        <?php if ($feed['file_type'] == 'V' and $feed['video'] != "") { ?>

                            <video width="100%" controls>
                                <source src="<?php echo $feed['video']; ?>" type="video/mp4">


                            </video>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

            <div class="feedfooter">
                <div class="feedsummary">
                    <span class="like_count"><?php echo $feed['like_count']; ?></span> Likes  &nbsp; <span class="com_count"><?php echo $feed['com_count']; ?></span> Comments
                </div>
                <div class="line"></div>

                <div class="actions">
                    <?php if ($feed['like_status'] == 0) { ?>
                        <div class="footeractionbtn btn_like" data-feed_id="<?php echo $feed['pk_feed_id']; ?>" data-like_status="0"><i class="fa fa-thumbs-up"></i> Like</div>
                    <?php } else {
                        ?> 
                        <div class="footeractionbtn btn_like blue" data-feed_id="<?php echo $feed['pk_feed_id']; ?>" data-like_status="1"><i class="fa fa-thumbs-up"></i> Liked</div>
                    <?php }
                    ?>
                    <div class="footeractionbtn btn_comment"><i class="fa fa-comment"></i> Comment</div>
                    <div class="footeractionbtn btn_share" data-feed_id="<?php echo $feed['pk_feed_id']; ?>"><i class="fa fa-share"></i> Share</div>
                </div>
                <div class="clearfix"></div>
                <div class="line"></div>
                <?php if ($feed['rem_comments'] > 0) { ?>
                    <div><a class="more_comments">View <?php echo $feed['rem_comments']; ?> more comments</a></div>
                <?php } ?> 
                <div class="feedcomments">

                    <?php
                    $last_id = 0;
                    if ($comments) {
                        foreach ($comments as $com) {
//                            print_r($com);

                            $last_id = $com['pk_comment_id'];
                            ?> 
                            <div class="commentsingle" data-last_id="<?php echo $com['pk_comment_id']; ?>" data-feed_id="<?php echo $feed['pk_feed_id']; ?>">
                                <div class="com_data_block">
                                    <div class="comuser">
                                        <img  src="<?php echo $com['thumb']; ?>" />
                                    </div>
                                    <div class="commenttext">
                                        <div class="comuname">
                                            <a href="<?php echo site_url() . "articles/listing/" . $com['slug']; ?>"><?php echo $com['name']; ?></a>
                                            <?php if ($feed['remove_feed'] == true or $com['remove_com'] == true) { ?>
                                                <div class="remove btn_remove_comment" data-id="<?php echo $com['pk_comment_id']; ?>"><i class="fa fa-times"></i></div>


                                            <?php } ?>
                                            <?php if ($com['fk_user_id'] == $this->session->userdata('user_id')) { ?>
                                                <div class="remove btn_edit_comment" data-id="<?php echo $com['pk_comment_id']; ?>"><i class="fa fa-pencil"></i></div>


                                            <?php } ?>


                                            <div class="comdate"><?php echo timeAgo($com['created_at']); ?></div>
                                        </div>
                                        <div class="com_text_temp" style="display: none;"><?php echo trim($com['content']); ?></div>
                                        <div class="com_text_block">
                                            <?php echo makeClickableLinks($com['content']); ?> 
                                        </div>
                                        <div class="line"></div>
                                    </div>
                                </div>
                                <div class="com_edit_block">

                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div class="line"></div>
                <div class="feedcommentspost">
                    <div class="fcpostouter">
                        <div class="comuser">
                            <img  src="<?php echo $this->session->userdata('image'); ?>" />
                        </div>
                        <div class="commenttext">
                            <form name="com_form" class="com_form">
                                <p class="emoji-picker-container">
                                    <textarea placeholder="Comment... " rows="1" name="com_text"  class="txtcommentbox com_text autoline" data-emojiable="true"></textarea>
                                </p>
                                <input type="hidden" name="feed_id" value="<?php echo $feed['pk_feed_id']; ?>"/>
                                <input type="hidden" name="last_id" class="com_last_id" value="<?php echo $last_id; ?>"/>
                            </form>
                            <div class="btnpostcomment">Post Comment</div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <?php
    }
}
if (!$load) {
    ?>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $(".commentloader").hide();

        });

    </script>

    <?php
}
?>
