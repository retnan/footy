<?php
$base = base_url() . PUBLIC_DIR . "fassets/";

$back2 = getBackground("3", "0", TRUE);
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if (count($back2) > 0) {
    $bg = $back2[0];
}

if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?>
<style>
    .footer{display: none !important;}
</style>
<section class="newsfeed">
    <?php
    if ($ud) {
        // $this->load->view("front/profile/profileheader.php", array("ud" => $ud, "follow_status" => $follow_status));
    }
    ?>

    <div class="row strip " style="margin-top:10px;">
        <div class="col-md-12">
            <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                News Feed
            </div>
        </div>
    </div>

    <div class="row staticcheck">
        <div class="col-md-3 hide_mobile">
            <div class="fixer2" style="position: relative;">
                <div class="fixerinner2">
                    <div class="leftpanel">
                        <div class="staticstarter2"></div>
                        <div class="clearfix"></div>
                        <a href="<?php echo site_url() . "followers/" . $ud['slug']; ?>">
                            <div class="panelheader">
                                Following
                            </div>
                        </a>
                        <div class="panelcontent">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if (isset($followings) && $followings) {
                                        // var_dump($followings);
                                        foreach ($followings as $row) {
                                      		
                                            ?>
                                            <div class="followerconout" style="width:24%;" data-toggle="tooltip" data-placement="top" title="<?php echo $row['name']; ?>">
                                                <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>">
                                                    <div class="followercon">
                                                        <img src="<?php echo $row['thumb']; ?>" />
                                                    </div>
                                                </a>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?> 
                                        <h4 class="text-center">No Followings</h4>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <script type="text/javascript">
                                    $(function() {
                                        $('[data-toggle="tooltip"]').tooltip();
                                    });</script>

                            </div>
                        </div>
                    </div>


                    <div class="leftpanel">
                        <div class="clearfix"></div>
                        <a href="<?php echo site_url() . "followers/" . $ud['slug']; ?>">
                            <div class="panelheader">
                                Followers
                            </div>
                        </a>
                        <div class="panelcontent">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if ($followers) {
                                        foreach ($followers as $row) {
                                            ?>
                                            <div class="followerconout"    data-toggle="tooltip" data-placement="top" title="<?php echo $row['name']; ?>">
                                                <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>">
                                                    <div class="followercon">
                                                        <img src="<?php echo $row['thumb']; ?>" />
                                                    </div>
                                                </a>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?> 
                                        <h4 class="text-center">No Followers</h4>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="leftpanel">
                        <div class="clearfix"></div>
                        <div class="panelheader">
                            <a href="<?php echo site_url() . "profile/gallery/" . $ud['slug']; ?>" style="color: #fff;">Gallery</a>
                        </div>
                        <div class="panelcontent">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if (count($user_gallery) > 0) { ?>
                                        <?php
                                        foreach ($user_gallery as $gallery) {
                                            if ($gallery['vid'] == false) {
                                                ?> 
                                                <div class="galleryconout"  data-toggle="tooltip" data-placement="top" title="">                         <div class="gallerycon">
                                                        <a class="fancybox-effects-a" href="<?php echo $gallery['large_image']; ?>" title="" data-fancybox-group="gallery"> <img src="<?php echo $gallery['thumb']; ?>" /></a>
                                                    </div>

                                                </div>
                                            <?php } else { ?>
                                                <div class="galleryconout"  data-toggle="tooltip" data-placement="top" title="">

                                                    <div class="gallerycon">
                                                        <a class="fancybox-media" href="<?php echo $gallery['vid_link']; ?>"><img src="<?php echo $gallery['thumb']; ?>" /></a> 
                                                    </div>

                                                </div>

                                            <?php } ?> <?php }
                                        ?> 
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="staticdevider2"></div>
                    <?php if ($poll_data) { ?>
                        <div class="feedpoll" style="background: url(<?php echo $poll_data['image']; ?>); background-size: cover;">
                            <div class="content">
                                <div class="text">
                                    <?php echo $poll_data['title']; ?>
                                </div>
                                <div style="    text-align: center;

                                     color: #fff;
                                     margin-top: 5px;
                                     width: 100%;
                                     font-size: 12px;">
                                    <?php echo $poll_data['option1'] . ": " . $poll_data['votes']['perA']; ?>%
                                    &nbsp; &nbsp; <?php echo $poll_data['option2'] . ": " . $poll_data['votes']['perB']; ?>%
                                    <?php if (trim($poll_data['option3']) != "") { ?>
                                        &nbsp; &nbsp; <?php echo $poll_data['option3'] . ": " . $poll_data['votes']['perC']; ?>%
                                    <?php } ?>
                                    <?php if (trim($poll_data['option4']) != "") { ?>
                                        &nbsp; &nbsp; <?php echo $poll_data['option4'] . ": " . $poll_data['votes']['perD']; ?>%
                                    <?php } ?>
                                    <?php if (trim($poll_data['option5']) != "") { ?>
                                        &nbsp; &nbsp; <?php echo $poll_data['option5'] . ": " . $poll_data['votes']['perE']; ?>%
                                    <?php } ?>

                                </div>
                                <div class="button poll_answer" style="margin-top: 5px;" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="1"><?php echo $poll_data['option1']; ?></div>
                                <div class="button poll_answer" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="2"><?php echo $poll_data['option2']; ?></div>
                                <?php if (trim($poll_data['option3']) != "") { ?>
                                    <div class="button poll_answer"  data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="3"><?php echo $poll_data['option3']; ?></div>
                                <?php } ?>
                                <?php if (trim($poll_data['option4']) != "") { ?>
                                    <div class="button poll_answer"  data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="4"><?php echo $poll_data['option4']; ?></div>
                                <?php } ?>
                                <?php if (trim($poll_data['option5']) != "") { ?>
                                    <div class="button poll_answer"  data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="4"><?php echo $poll_data['option5']; ?></div>
                                <?php } ?>
                            </div>                       
                        </div>
                    <?php } else { ?>

                        <div class="feedpoll" style="background: url(<?php echo $base; ?>images/pollhomebg.jpg);">                         
                            <div class="content">
                                <div class="text">
                                    Is Mourinho  Right For United?
                                </div>
                                <div class="button">Yes</div>
                                <div class="button">No</div>
                            </div>                                
                        </div>
                    <?php } ?>
                    <div class="promoted">
                        <ul>
                            <li>
                                <a href="#">Read This...</a>
                            </li>
                            <?php
                            $sp_articles = getAticles('A', 1, 0, 0, 0, 7);
                            if ($sp_articles) {
                                foreach ($sp_articles as $sp) {
                                    ?>
                                    <li>
                                        <a href="<?php echo site_url() . ARTICLE_TAG . "/" ?><?php echo $sp['slug'] ?>"><?php echo $sp['title']; ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 padding_0_desktop" >
            <div class="feedposter">
                <div class="postercon">
                    <div class="feeduser">
                        <img  src="<?php echo $this->session->userdata("image"); ?>" />
                        <div class="arrow"></div>
                        <div class="arrow1"></div>
                    </div>
                    <div class="feedtextcon">
                        <form method="post" id="postform" action="<?php echo site_url() . "profile/feedsave" ?>" enctype="multipart/form-data">
                            <p class="emoji-picker-container">
                                <textarea placeholder="What is in your mind?" name="posttext"  id="posttext"  class="autoline" data-emojiable="true"></textarea>
                            </p>
                            <input type="file" name="fileimage" id="fileimage" accept="image/x-png, image/gif, image/jpeg" style="display: none;" />
                            <input type="file" name="filevideo" id="filevideo" accept="video/mp4,video/x-m4v,video/*"   style="display: none;" />
                            <input type="text" name="filetype" id="filetype" value="N" style="display: none;" />
                        </form>
                    </div>
                </div>
                <div class="line"></div>
                <div class="feedaction">
                    <div class="feedbutton" onclick="$('#fileimage').click();"><i class="fa fa-picture-o"></i> Image</div>
                    <div class="feedbutton" onclick="$('#filevideo').click();"><i class="fa fa-video-camera"></i> Video</div>
                    <div class="feedfilename"></div>
                    <div class="feedbutton pull-right btnactionfeed" style="margin-right: 0px;" >Post</div>
                </div>
                <div class="waiting"><img src="<?php echo $base; ?>images/input-spinner.gif" /></div>
            </div>
            <div class="uploadpouter" style="display: none;">
                <div class="uploadpinner" style="width: 30%;"></div>
            </div>

            <div class="feedcontainer" data-ref_id="<?php echo $ref_id ?>" data-user_id="<?php echo $user_id ?>">
                <!--**************All Feed Container****************-->
            </div>
            <div class="commentloader">
                <div class="loadmore">View more</div>
                <div class="loaderview"><img src="<?php echo $base; ?>images/loadingBar.gif" /></div>
            </div>

        </div>
        <div class="col-md-4 hide_mobile">
            <div class="fixer" style="position: relative;">
                <div class="fixerinner">
                    <?php
                    $this->load->view("front/includes/right_panel_news.php", array('bottom' => false));
                    ?>
                </div>

            </div>
        </div>
    </div>
    <div class="com_edit_temp" style="display: none;">
        <div class="fcpostouter">
            <div class="comuser">
                <img  src="<?php echo $this->session->userdata('image'); ?>" />
            </div>
            <div class="commenttext">
                <form name="com_edit" class="com_edit">
                    <p class="emoji-picker-container">
                        <textarea placeholder="Comment... " rows="1" name="com_text"  class="txtcommentbox com_text autoline" data-emojiable="true"></textarea>
                    </p>
                    <input type="hidden" name="com_id" class="com_id" value=""/>

                </form>
                <div class="btneditcomment">Update </div>
                <div class="btncancelcomment">Cancel </div>
            </div>
        </div>
    </div>


</section>

