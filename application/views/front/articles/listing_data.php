<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>

<div class="data">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-md-12">
            <div class="searchbox">
                <div class="searchbtn">
                    <div class="btnblack searchBtn">
                        Search
                    </div>
                </div>
                <div class="searchtext">
                    <input type="text" class="txt_searchbox" value="<?php echo $search; ?>" placeholder="Search..." >
                </div>
            </div>
        </div>
    </div>

    <div class="articlelist">
        <?php
        $result = false;
        if (isset($users) and count($users['data']) > 0) {
            $result = true;
            foreach ($users['data'] as $key => $user) {
                $ud = $user['ud'];
                $uh = $user['uh'];
                ?>
                <div class="singlewriter">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="user" style="padding: 5px;">
                                <div class="manouter">
                                    <div class="maninner">
                                        <img src="<?php echo $ud['thumb']; ?>">
                                    </div>
                                </div>
                                <div class="footyouter">
                                    <div class="footyinner">
                                        <img src="<?php echo $ud['club_image']; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <h3><a href="<?php echo site_url() . "articles/listing/" . $ud['slug'] ?>"><?php echo $ud['name']; ?></a> </h3>
                            <div class="clubname"><?php echo $ud['club_name']; ?></div>
                            <div class="clearfix"></div>
                            <div class="profootercon">
                                <ul>
                                    <li>
                                        <div class="proval followers"><?php echo $uh['followers']; ?></div>
                                        <div class="prolabel">Followers</div>
                                    </li>
                                    <li>
                                        <div class="proval"><?php echo $uh['following']; ?></div>
                                        <div class="prolabel">Following</div>
                                    </li>
                                    <li>
                                        <div class="proval"><?php echo $user['points']; ?></div>
                                        <div class="prolabel">Points</div>
                                    </li>
                                    <li>
                                        <div class="proval"><?php echo $uh['art_count']; ?></div>
                                        <div class="prolabel">Articles</div>
                                    </li>
                                    <li>
                                        <div class="proval"><?php echo $uh['reads']; ?></div>
                                        <div class="prolabel">Reads</div>
                                    </li>
                                </ul>
                                <a href="<?php echo site_url() . "articles/listing/" . $ud['slug'] ?>" class="borderbutton pull-right">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        if (count($data) > 0) {
            foreach ($data as $row) {
                ?> 
                <div class="singlearticle">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="<?php echo site_url() . ARTICLE_TAG . "/"; ?><?php echo $row['slug'] ?>" ><img src="<?php echo $row['thumb']; ?>" /></a>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-11"><h3><a href="<?php echo site_url() . ARTICLE_TAG . "/"; ?><?php echo $row['slug'] ?>" ><?php echo $row['title']; ?></a></h3></div>
                                <?php if ($row['fk_user_id'] == $this->session->userdata("user_id")) { ?>
                                    <div class="col-md-1">
                                        <div class="dropdown-toggle pull-right">
                                            <div class="feedmenu" style="cursor: pointer;font-size: 24px;" data-toggle="dropdown" ><i class="fa fa-angle-down"></i></div>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url() . "articles/edit/" . $row['pk_post_id']; ?>" class="delete_feed" data-id="12">Edit</a></li>
                                                <li><a href="#" data-url="<?php echo site_url() . "articles/delete/" . $row['pk_post_id']; ?>" class="delete_post">Delete</a></li>

                                            </ul>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div>
                                <?php echo wordLimiter(strip_tags(trim($row['content']), 150)); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9 col-md-offset-3">
                            <div class="articlestrip">
                                <a href="<?php echo site_url() . ARTICLE_TAG . "/"; ?><?php echo $row['slug'] ?>" class="viewall">
                                    View Details
                                </a>
                                <div class="commentcount">
                                    <i class="fa fa-comments"></i>
                                    <span><?php echo $row['comments']; ?></span>
                                </div>
                                <div class="viewcount">
                                    <i class="fa fa-eye"></i>
                                    <span><?php echo $row['views']; ?></span>
                                </div>

                                <div class="uname"><a href="<?php echo site_url() . "articles/listing/" . $row['user_slug'] ?>">by <?php echo $row['posted_by']; ?></a></div>
                                <div class="uimage"><a href="<?php echo site_url() . "articles/listing/" . $row['user_slug'] ?>"><img src="<?php echo $row['user_image']; ?>"></a></div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php
            }
        } else {
            if ($result == false) {
                if ($source == "search") {
                    ?>
                    <h3 style="width: 100%; text-align: center;">No Search Result Available</h3>
                    <?php
                } else {
                    ?>
                    <h3 style="width: 100%; text-align: center;">No Article Available</h3>
                    <?php
                }
            }
        }
        ?>


    </div>
    <?php echo $page; ?>



</div>