<?php
$ads = getAds("small", $page);
$google_ads = getCMSContent("ad_ggl_small");
$i = 0;
foreach ($articles['data'] as $article) {

    if ($i == 2) {
        foreach ($ads as $ad) {
            ?>
            <div class="ad_section ad_section1">
                <a href="<?php echo $ad['link']; ?>" target="_blank">
                    <img src="<?php echo $ad['image'] ?>" style="max-width: 100%;" />
                </a>
            </div>
            <?php
        }
    }
    if ($i == 0 or $i % 10 == 0) {
        ?>
        <div class="gglad_section gglad_section1">
            <?php echo $google_ads; ?>
        </div>
        <?php
    }
    $i++;
    ?>
    <?php
    $main_cat = $article['categories'][0]['slug'];
    $link = site_url() . ARTICLE_TAG . "/" . $main_cat . "/" . $article['slug'];
    ?>
    <div class="ar_sec_1">
        <div class="ar_image">
            <a href="<?php echo $link; ?>">
                <img src="<?php echo $article['thumb']; ?>"  alt="<?php echo $article['image_alt']; ?>" />
            </a>            
        </div>
        <div class="ar_desc">
            <a href="<?php echo $link; ?>" class="ar_title"><?php echo $article['title']; ?></a>
            <div class="ar_share_box">
                <div class="ar_share_button pshare_button">
                    <i class="fa fa-share-alt"></i>
                    Share
                    <div class="sconteiner">
                        <a class="slink btnfbShare" data-href="<?php echo $link; ?>"><i class="fa fa-facebook"></i> Facebook</a>
                        <a class="slink btnttShare" data-href="<?php echo $link; ?>"><i class="fa fa-twitter"></i> Twitter</a>
                        <a class="slink btngglShare" data-href="<?php echo $link; ?>"><i class="fa fa-google-plus"></i> Google+</a>
                        <a class="slink btnlnkShare" data-href="<?php echo $link; ?>" style="display: none"><i class="fa fa-linkedin"></i> Linkedin</a>
                        <?php if ($article['image'] != "") { ?>
                            <a class="slink btnpinShare" data-href="<?php echo $link; ?>" data-desc="<?php echo html_escape($article['title']) ?>" data-image="<?php echo $article['thumb'] ?>"><i class="fa fa-pinterest"></i> Pinterest</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="ar_time"> <?php echo timeAgoHindi(strtotime($article['created_at'])); ?></div>
            </div>
        </div>
    </div>
    <?php
}
if (count($articles['data']) == 0) {
    ?>
    <div class="nomore"></div>
    <?php
}
?>