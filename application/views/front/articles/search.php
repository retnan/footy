<?php
$backall = getBackground("2", "0");
$back6 = getBackground("4", "0");

$bg = "";
if (count($backall) > 0) {
    $bg = $backall[0];
}
if (count($back6) > 0) {
    $bg = $back6[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?>
<style>
    .single-result{margin-bottom: 5px;}
    .single-result h3{
        margin: 0;
        margin-top: 5px;
        font-size: 18px;
        color: #B05739;
    }
    .searchresultdata .single-result{background: #f9fcfe;}
    .searchresultdata .single-result:nth-of-type(2n-1){
        background: #F9F9F9;
    }
    .searchresultdata .single-result:hover{ background: #fefbf9;}
    .searchcat{text-align: right; margin-right: 5px; color: #999;}
</style>
<div class="row">
    <div class="col-md-12">
        <h2 style="color: #df4a43; margin-top: 0px; width: auto; float: left;">
            Search result for "<?php echo $key; ?>"
        </h2>
        <div style="float:right; width: 728px; height: 90px; margin-top: -15px"><?php echo getCMSContent("ad_ggl_top"); ?></div>
    </div>
    <div class="col-md-12">
        <div style="padding: 5px;" class="searchresultdata">
            <?php
            foreach ($data as $dt) {
                $category = $dt['category'];
                foreach ($dt['article'] as $art) {
                    ?>
                    <a href="<?php echo $art['url']; ?>">
                        <div class="single-result">
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="<?php echo $art['image']; ?>" style="width: 100%;; padding-right: 0;" />
                                </div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3><?php echo $art['title']; ?></h3>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="searchcat"><?php echo $category ?></div>
                                        </div>
                                    </div>
                                    <p><?php echo $art['content']; ?></p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php
                }
            }
            ?>

        </div>
    </div>
</div>