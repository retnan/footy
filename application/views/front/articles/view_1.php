<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?> 
<section class="articleheader">
    <?php if ($article) { ?>
        <div class="row">
            <div class="col-md-5">
                <div class="box1">
                    <div class="boxheader"><?php echo $user_data['name']; ?></div>
                    <div class="boxcontent">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="user">
                                    <div class="manouter">
                                        <div class="maninner">
                                            <!--<img src="<?php echo $base; ?>images/laughing.jpg" />-->
                                            <img src="<?php echo $user_data['thumb']; ?>" />
                                        </div>
                                    </div>
                                    <div class="footyouter">
                                        <div class="footyinner">
                                            <!--<img src="<?php echo $base; ?>images/clubs/manchester_city.png" />-->
                                            <img src="<?php echo $user_data['club_image']; ?>" />
                                        </div>
                                    </div>                                                
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="clearfix"></div>
                                <div class="victory">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <img src="<?php echo $base; ?>images/victor.png" />
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <img src="<?php echo $base; ?>images/victor.png" />
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <img src="<?php echo $base; ?>images/victor.png" />
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <img src="<?php echo $base; ?>images/victor.png" />
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 15px;">
                                    <div class="borderbutton"><?php echo $user_pos; ?></div>
                                    <div class="borderbutton">Follow</div>
                                    <div class="borderbutton">Message</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="teamheader">
                    <img src="<?php echo $base; ?>images/teamheader.jpg" />
                    <img class="logo" src="<?php echo $user_data['club_image']; ?>" />
                    <div class="sociallinks">

                        <?php $linkurl = site_url() . ARTICLE_TAG . "/" . $article['slug'] ?>  

                        <a  class="facebook btnfbShare" data-href="<?php echo $linkurl; ?>" data-desc="<?php echo html_escape($article['title']) ?>"><i class="fa fa-facebook"></i></a>
                        <a  class="twitter btnttShare" data-href="<?php echo $linkurl; ?>" data-desc="<?php echo html_escape($article['title']) ?>"><i class="fa fa-twitter"></i></a>
                        <a class="insta btngglShare" data-href="<?php echo $linkurl; ?>" data-desc="<?php echo html_escape($article['title']) ?>"><i class="fa fa-google-plus"></i></a>
                        <a  class="feed btnlnkShare" data-href="<?php echo $linkurl; ?>" data-desc="<?php echo html_escape($article['title']) ?>"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</section>
<section class="articlecontent">


    <div class="row">
        <div class="col-md-8">
            <?php
            if ($article) {
                if ($article['file_type'] == 'V') {
                    ?>
                    <div class="articleupper">
                        <iframe class="articlevideo" src="<?php echo $article['vid_link']; ?>" frameborder="0" allowfullscreen></iframe>

                        <h1 style="color: #222;"><?php echo $article['title']; ?> </h1>
                    </div>
                <?php } ?>

                <?php if ($article['file_type'] == 'I') {
                    ?>
                    <div class="articleupper">
                        <img  src="<?php echo $article['image']; ?>" />
                        <div class="articletitle">
                            <h1><?php echo $article['title']; ?> </h1>
                        </div>
                    </div>
                <?php } ?>
                <?php
                if (trim($article['file_type']) == '') {
                    ?>
                    <h1 style="color: #222;"><?php echo $article['title']; ?> </h1>
                <?php } ?>
                <div class = "articledata">
                    <div class = "articlestrip">
                        <div class = "commentcount">
                            <i class = "fa fa-comments"></i>
                            <span><?php echo $article['cm_count'] ?></span>
                        </div>
                        <div class = "viewcount">
                            <i class = "fa fa-eye"></i>
                            <span><?php echo $article['pv_count']; ?></span>
                        </div>
                    </div>
                    <div class = "articletext">
                        <?php echo $article['content']; ?>



                    </div>

                </div>
                <div class = "articletags">
                    <?php
                    if (count($article['tags']) > 0) {
                        foreach ($article['tags'] as $row) {
                            ?> 
                            <div><?php echo $row['tag']; ?></div>

                            <?php
                        }
                    }
                    ?>

                </div>
                <?php if ($article['comment_allow'] == '1') { ?>
                    <div class="clearfix"></div>
                    <div><h2 class="subheading">Post Your Comment</h2></div>
                    <hr>
                    <div class="alert alert-success comment_success" style="display: none;"><i class="fa fa-check"></i> Comment Saved</div>
                    <div class="articlecommentbox">
                        <form action="" id="comment_form">
                            <textarea class = "" name="comment" id="comment" cols = "15"></textarea>
                            <input type="hidden" id="post_type" name="post_type" value="A">
                            <input type="hidden" id="post_id" name="post_id" value="<?php echo $article['pk_post_id']; ?>"></form>

                        <div><div class = "borderbutton btn_comment">Post Comment</div></div>
                    </div>
                    
                    
                    <div><h2  class="subheading">Comments</h2></div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="commentcontainer">
                            
                          
                            
                            
                            <div class="commetsingle">
                                <div class="comimage">
                                    <img src="<?php echo $base."images/laughing.jpg" ?>" />
                                </div>
                                <div class="comcontent">
                                    <div class="arraowleft"></div>
                                    <div class="comheader">
                                        Rajwant Singhalese
                                        <div class="comdate"><i class="fa fa-clock"></i> 2 hours ago</div>
                                    </div>
                                    <div class="comtext">
                                        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="commetsingle">
                                <div class="comimage">
                                    <img src="<?php echo $base."images/laughing.jpg" ?>" />
                                </div>
                                <div class="comcontent">
                                    <div class="arraowleft"></div>
                                    <div class="comheader">
                                        Rajwant Singhalese
                                        <div class="comdate"><i class="fa fa-clock"></i> 2 hours ago</div>
                                    </div>
                                    <div class="comtext">
                                        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="commetsingle">
                                <div class="comimage">
                                    <img src="<?php echo $base."images/laughing.jpg" ?>" />
                                </div>
                                <div class="comcontent">
                                    <div class="arraowleft"></div>
                                    <div class="comheader">
                                        Rajwant Singhalese
                                        <div class="comdate"><i class="fa fa-clock"></i> 2 hours ago</div>
                                    </div>
                                    <div class="comtext">
                                        <div>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="commentloader ">
                                <div class="loadmore" onclick="">View more comments</div>
                                <div class="loaderview"><img src="<?php echo $base; ?>images/loadingBar.gif" /></div>
                            </div>
                            
                            
                            
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="articleupper">

                    <h1 style="color: #222;">Oops! Article not found </h1>
                </div>
            <?php } ?>
            <div class = "clearfix"></div>
            <div class = "poll">
                <img src = '<?php echo $base; ?>images/pollbg.jpg' />
                <div class = "polltitle">Who should start in Goal for Spanish's Euro Squad</div>
                <div class="polloption option1">De Gea</div>
                <div class="polloption option2">Casillas</div>
            </div>
        </div>
        <div class="col-md-4">
            <?php
            $this->load->view("front/includes/right_panel_news.php", array());
            ?>
        </div>

    </div>
        <div class="row">
            <div class="col-md-12">
                <div class="longadd">
                    <img src="<?php echo $base; ?>images/addlong.png" style="width: 100%;" />
                </div>
            </div>
        </div>
        <div class="row">

            <?php
            $this->load->view("front/includes/bottom_panel_news.php", array());
            ?>

        </div>
</section>
<script type="text/javascript">
       var page=1;
    function loadComments(){
            $('.commentloader').addClass('loading');
            var post_data = {post_id:$("#post_id").val(),ptype:'A',page:page};
            $.post(base_url + "articles/loadcomment", post_data, function(res) {
             
                alert(res);
                $('.commentloader').removeClass('loading');
             
            });
    }
    $(".loadmore").click(function() {
       page++;
       loadComments();
    });
    $(document).ready(function(e) {
        loadComments();
        $(".articlecommentbox .btn_comment").click(function() {
            var post_data = $("#comment_form").serialize();
            $.post(base_url + "articles/save_comment", post_data, function(res) {
                if (res.status == 1) {
                    $(".comment_success").fadeIn();
                } else {
                    alert("Please Login");
                }
            }, 'json');
        });
    });
</script>
