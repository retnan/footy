<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
$trend_news = getAticles('N', 0, 0, $club['pk_club_id'], 0, 9, false);
$top_news = getTopAticles('N', $club['pk_club_id'], 0, 9);
$backall = getBackground("2", "0");
$back1 = getBackground("6", $club['fk_league_id']);
$back2 = getBackground("5", $club['pk_club_id'], true);
$back3 = getBackground("5", $club['pk_club_id']);
$bg = "";
if (count($backall) > 0) {
    $bg = $backall[0];
}
if (count($back1) > 0) {
    $bg = $back1[0];
}
if (count($back2) > 0) {
    $bg = $back2[0];
}
if (count($back3) > 0) {
    $bg = $back3[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?>
<section class="homefirstcol">

    <div class="row">
        <div class="col-md-8">
            <div class="row strip">
                <div class="col-md-3" style="padding: 0;">
                    <a class="blacklabel" href="<?php echo site_url() . "articles/topwriters/" . $club['slug']; ?>">
                        Top Writers
                    </a>
                </div>
                <div class="col-md-9" style="padding-left:  0;">
                    <div class="stripicons">
                        <?php
                        if ($hp_writers) {
                            foreach ($hp_writers as $pos => $row) {
                                ?>
                                <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>" title="<?php echo $row['name']; ?>">
                                    <img src="<?php echo $row['thumb']; ?>" />
                                </a>
                                <?php
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-8">
            <div class="clubinfo">
                <div class="clubinfoin row">

                    <div class="clubmain col-md-9">
                        <h3><?php echo $club['title']; ?></h3>
                        <div class="clearfix"></div>
                        <div>
                            <?php if ($club['team_manager'] != "") { ?>
                                <span><strong>Team Manager:</strong> <?php echo $club['team_manager']; ?> </span>
                            <?php } ?>
                            <?php if ($club['captain'] != "") { ?>
                                <span style="margin-left: 5px;"> <strong>Captain:</strong> <?php echo $club['captain']; ?> </span>
                            <?php } ?>
                        </div>
                        <?php if ($club['stadium'] != "") { ?>
                            <div style="margin-top:4px;margin-bottom: 4px;"><span><strong>Stadium:</strong> <?php echo $club['stadium']; ?> </span></div>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <div style="margin-top:1px;"><?php echo $club['description']; ?></div>


                    </div>
                    <div class="clublogo col-md-3">
                        <img src="<?php echo $club['logo']; ?>"/>
                    </div>
                </div>
                <div class="cinfofooter">
                    <ul>
                        <li style="cursor:pointer; " onclick="window.location.href = '<?php echo site_url() . "articles/topfanz/" . $club['slug']; ?>'">
                            <div class="value"><?php echo $fan_count; ?></div>
                            <div class="title">Fanz</div>
                        </li>
                        <li style="cursor:pointer; " onclick="window.location.href = '<?php echo site_url() . "articles/topwriters/" . $club['slug']; ?>'">
                            <div class="value"><?php echo $wr_count; ?></div>
                            <div class="title">Writers</div>
                        </li>
                        <li >
                            <div class="value"><?php echo $art_count; ?></div>
                            <div class="title">Articles</div>
                        </li>
                        <li>
                            <div class="value"><?php echo $art_reads; ?></div>
                            <div class="title">Reads</div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix" ></div>
            </div>
            <div class="clearfix" style="margin-bottom: 15px;" ></div>
            <div class="profilenav" >
                <?php for ($i = 0; $i < $club['trophies']; $i++) { ?>
                    <div class="trophyicon pull-left"><img src="<?php echo $base; ?>images/victor.png"></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-4">
            <?php
            $gc = trim(getCMSContent("ad_ggl_square"));
            $ads = getSiteAds(5, $club['pk_club_id'], "square");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" />
                    </a>
                </div>
                <?php
            } else {
                echo $gc;
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">




            <div class="row strip">
                <div class="col-md-12">
                    <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                        News/Articles
                    </div>
                </div>
            </div>
            <div id="listing_data"></div>
            <?php
            paginationScriptFront(base_url() . "articles/clublisting_data", "listing_data", "post_type=A&status=1&club=" . $club['pk_club_id']);
            ?>

            <?php
            $this->load->view("front/includes/poll_horizontal.php", array("poll_data" => $poll_data));
            ?>

        </div>

        <div class="col-md-4">
            <?php
            $this->load->view("front/includes/right_panel_news.php", array());
            ?>
        </div>

    </div>
    <div class="row hide_mobile">
        <div class="col-md-12">
            <?php
            $gc = trim(getCMSContent("ad_ggl_large"));
//            $ads = getAds("large");

            $ads = getSiteAds(5, $club['pk_club_id'], "large");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" style="width: 100%;" />
                    </a>
                </div>
                <?php
            } else {
                echo $gc;
            }
            ?>
        </div>
    </div>
    <div style="margin-bottom: 20px" class="clearfix"></div>
    <div class="row">

        <?php
        $this->load->view("front/includes/bottom_panel_news.php", array());
        ?>

    </div>




</section>
