<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
$positions = array("1" => "1st", "2" => "2nd", "3" => "3rd");
$months = array("1" => "Jan", "2" => "Feb", "3" => "Mar", "4" => "Apr", "5" => "May", "6" => "Jun", "7" => "Jul", "8" => "Aug", "9" => "Sep", "10" => "Oct", "11" => "Nov", "12" => "Dec");
$laurals = $user_data['laurals'];
$league_id = $article['fk_league_id'];
$club_id = $article['fk_club_id'];
$headall = getMashHead("2", "");
$head = getMashHead("5", $club_id);
$head2 = getMashHead("5", $club_id, TRUE);
$head3 = getMashHead("6", $league_id);
//print_r($head);
//print_r($head2);

$mashhed = $base . "images/teamheader.jpg";
if (count($headall) > 0) {
    $mashhed = $headall[0]['image'];
}
if (count($head3) > 0) {
    $mashhed = $head3[0]['image'];
}
if (count($head2) > 0) {
    $mashhed = $head2[0]['image'];
}
if (count($head) > 0) {
    $mashhed = $head[0]['image'];
}

$backall = getBackground("2", "0");
$back = getBackground("1", $league_id);
$back2 = getBackground("1", $league_id, TRUE);
$back3 = getBackground("6", $league_id);
$back4 = getBackground("5", $club_id, TRUE);
$back5 = getBackground("5", $club_id);
$back6 = getBackground("4", "0");

$bg = "";
if (count($backall) > 0) {
    $bg = $backall[0];
}
if (count($back2) > 0) {
    $bg = $back2[0];
}
if (count($back) > 0) {
    $bg = $back[0];
}
if (count($back3) > 0) {
    $bg = $back3[0];
}
if (count($back4) > 0) {
    $bg = $back4[0];
}
if (count($back5) > 0) {
    $bg = $back5[0];
}
if (count($back6) > 0) {
    $bg = $back6[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?> 
<section class="articleheader userprofiledata" style="overflow: visible;" data-uname="<?php echo html_escape((isset($user_data['name']) ? $user_data['name'] : "")); ?>"  data-user_id="<?php echo (isset($user_data['pk_user_id']) ? $user_data['pk_user_id'] : ""); ?>">
    <?php if ($article) { ?>
        <div class="row">
            <div class="col-md-5">
                <div class="box1">
                    <div class="boxheader"><a href="<?php echo site_url() . "articles/listing/" . $user_data['slug'] ?>" style="color:#fff;"><?php echo $user_data['name']; ?></a></div>
                    <div class="boxcontent">
                        <div class="row">
                            <div class="col-md-4 col-sm-2 col-xs-3">
                                <div class="user">
                                    <div class="manouter">
                                        <div class="maninner">
                                            <!--<img src="<?php echo $base; ?>images/laughing.jpg" />-->
                                            <a href="<?php echo site_url() . "articles/listing/" . $user_data['slug'] ?>">
                                                <img src="<?php echo $user_data['thumb']; ?>" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="footyouter">
                                        <div class="footyinner">
                                            <!--<img src="<?php echo $base; ?>images/clubs/manchester_city.png" />-->
                                            <img src="<?php echo $user_data['club_image']; ?>" />
                                        </div>
                                    </div>                                                
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-10 col-xs-9">
                                <div class="clearfix"></div>
                                <div class="victory">
                                    <div class="row" style="min-height: 61px;">

                                        <?php foreach ($laurals as $lr) { ?>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <div data-toggle="tooltip" data-placement="top" data-original-title="<?php echo $positions[$lr['position']] . " rank in " . $months[$lr['l_month']] . " " . $lr['l_year']; ?>"><img src="<?php echo $base; ?>images/victor.png"></div>
                                            </div>
                                        <?php } ?>


                                    </div>
                                </div>
                                <div style="margin-top: 15px;">
                                    <div class="borderbutton"><?php echo $user_pos; ?></div>

                                    <div class="borderbutton btn_unfollow" <?php if ($follow_status == "0" or $follow_status == "NA") { ?> style="display: none;" <?php } ?> data-id="<?php echo $user_data['pk_user_id']; ?>"><i class="fa fa-check"></i> Following</div>



                                    <div class="borderbutton btn_follow" <?php if ($follow_status == "1" or $follow_status == "NA") { ?> style="display: none;" <?php } ?> data-id="<?php echo $user_data['pk_user_id']; ?>">Follow</div>


                                    <a class="borderbutton" href="<?php echo site_url() . "articles/listing/" . $user_data['slug'] ?>" style="display: none;" >View Profile</a>



                                    <?php
                                    if ($this->session->userdata("user_id") > 0) {
                                        if ($follow_status != "NA") {
                                            ?>
                                            <div data-toggle="modal" data-target="#messageModal" class="borderbutton ajax" data-url="<?php echo site_url() . "profile/send_message_form/" . $user_data['pk_user_id']; ?>">Message</div>    
                                            <?php
                                        } else {
                                            
                                        }
                                    } else {
                                        ?>
                                        
                                         <?php } ?>


                                    <?php if ($follow_status != "NA") { ?>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="teamheader">
                    <img src="<?php echo $mashhed; ?>" />
                    <img class="logo" src="<?php echo $user_data['club_image']; ?>" />
                    <div class="sociallinks">

                        <?php $linkurl = site_url() . ARTICLE_TAG . "/" . $article['slug'] ?>  

                        <a  class="facebook btnfbShare" data-href="<?php echo $linkurl; ?>" data-desc="<?php echo html_escape($article['title']) ?>"><i class="fa fa-facebook"></i></a>
                        <a  class="twitter btnttShare" data-href="<?php echo $linkurl; ?>" data-desc="<?php echo html_escape($article['title']) ?>"><i class="fa fa-twitter"></i></a>
                        <a class="insta btngglShare" data-href="<?php echo $linkurl; ?>" data-desc="<?php echo html_escape($article['title']) ?>"><i class="fa fa-google-plus"></i></a>
                        <a  class="feed btnlnkShare" data-href="<?php echo $linkurl; ?>" data-desc="<?php echo html_escape($article['title']) ?>"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</section>
<section class="articlecontent">


    <div class="row">
        <div class="col-md-8">
            <?php
            if ($article) {
                if ($article['file_type'] == 'V') {
                    ?>
                    <div class="articleupper">
                        <iframe class="articlevideo" src="<?php echo $article['vid_link']; ?>" frameborder="0" allowfullscreen></iframe>

                        <h1 style="color: #222;"><?php echo $article['title']; ?> </h1>
                    </div>
                <?php } ?>

                <?php if ($article['file_type'] == 'I') {
                    ?>
                    <div class="articleupper">
                        <img  src="<?php echo $article['image']; ?>" />
                        <div class="articletitle">
                            <h1><?php echo $article['title']; ?> </h1>
                        </div>
                    </div>
                <?php } ?>
                <?php
                if (trim($article['file_type']) == '') {
                    ?>
                    <h1 style="color: #222;"><?php echo $article['title']; ?> </h1>
                <?php } ?>
                <div class = "articledata">
                    <div class = "articlestrip">
                        <div class = "commentcount">
                            <i class = "fa fa-comments"></i>
                            <span><?php echo $article['cm_count'] ?></span>
                        </div>
                        <div class = "viewcount">
                            <i class = "fa fa-eye"></i>
                            <span><?php echo $article['pv_count']; ?></span>
                        </div>
                    </div>
                    <div class = "articletext">
                        <?php echo $article['content']; ?>



                    </div>

                </div>
                <div class = "articletags">
                    <?php
                    if (count($article['tags']) > 0) {
                        foreach ($article['tags'] as $row) {
                            ?> 
                            <div><?php echo $row['tag']; ?></div>

                            <?php
                        }
                    }
                    foreach ($article['htags'] as $htag) {
                        ?>
                        <div><?php echo ltrim($htag, '#'); ?></div>
                        <?php
                    }
                    ?>

                </div>


                <?php if ($article['comment_allow'] == '1') { ?>
                    <div class="clearfix"></div>
                    <div><h2 class="subheading">Post Your Comment</h2></div>
                    <hr>
                    <div class="alert alert-success comment_success" style="display: none;"><i class="fa fa-check"></i> Comment Saved</div>

                    <?php
                    $disabled = "";
                    if ($this->session->userdata("user_id") == "" || $this->session->userdata("user_id") == "0") {
                        $disabled = "disabled";
                        ?>
                        <div class="alert alert-warning"><i class="fa fa-lock"></i> Please <a href="<?php echo base_url() . 'login'; ?>"> Login</a> or <a href="<?php echo base_url() . 'signup'; ?>">Sign up</a> to post comments</div>
                    <?php } ?>

                    <div class="articlecommentbox">
                        <form action="" id="comment_form">
                            <p class="emoji-picker-container">
                                <textarea class = "" <?php echo $disabled; ?> name="comment" id="comment" cols = "15" data-emojiable="true"></textarea>
                            </p>
                            <input type="hidden" id="post_type" name="post_type" value="A">
                            <input type="hidden" id="post_id" name="post_id" value="<?php echo $article['pk_post_id']; ?>"></form>

                        <div><div class ="borderbutton btn_comment">Post Comment</div></div>
                    </div>


                    <div><h2  class="subheading">Comments</h2></div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="commentcontainer">



                            </div>
                            <div class="commentloader">
                                <div class="loadmore" onclick="">View more comments</div>
                                <div class="loaderview"><img src="<?php echo $base; ?>images/loadingBar.gif" /></div>
                            </div>



                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="articleupper">

                    <h1 style="color: #222;">Oops! Article not found </h1>
                </div>
            <?php } ?>
            <div class = "clearfix"></div>
            <?php
            $this->load->view("front/includes/poll_horizontal.php", array("poll_data" => $poll_data));
            ?>
        </div>
        <div class="col-md-4">
            <?php
            $this->load->view("front/includes/right_panel_news.php", array('bottom' => true));
            ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="longadd">
                <?php
                $gc = trim(getCMSContent("ad_ggl_large"));
//            $ads = getAds("large");
                if ($gc == "") {
                    $ads = getAds("large");
                    $cnt = count($ads);
                    if ($cnt > 0) {
                        $num = array_rand($ads);
                        ?>
                        <div class="leftadd">
                            <a href="<?php echo $ads[$num]['link'] ?>">
                                <img src="<?php echo $ads[$num]['image']; ?>" style="width: 100%;" />
                            </a>
                        </div>
                        <?php
                    }
                } else {
                    echo $gc;
                }
                ?>
            </div>
        </div>
    </div>
    <div class="row">

        <?php
        $this->load->view("front/includes/bottom_panel_news.php", array());
        ?>

    </div>
    <div class="com_edit_temp" style="display:none;">
        <div class="fcpostouter">
            <div class="comuser">
                <img  src="<?php echo $this->session->userdata('image'); ?>" />
            </div>
            <div class="commenttext">
                <form name="com_edit" class="com_edit">
                    <p class="emoji-picker-container">
                        <textarea placeholder="Comment... " rows="2" name="com_text"  class="txtcommentbox com_text" data-emojiable="true" data-emoji-input="unicode"></textarea>
                    </p>
                    <input type="hidden" name="com_id" class="com_id" value=""/>

                </form>
                <div class="btneditcomment">Update </div>
                <div class="btncancelcomment">Cancel </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).on('click', ".poll_answer", function(e) {

        var post_data = $(this).data();
        $.confirm({
            title: 'Poll Voting',
            content: 'Are you sure to Vote your Option',
            confirmButton: 'Sure',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(base_url + "home/save_poll", post_data, function(res) {
                    alert("Your Vote has been Saved");

                });

            }
        });
    });
    $(document).on('click', ".btn_edit_comment", function(e) {

        var $this = $(this);
        var parent = $this.closest(".commetsingle");
        parent.find(".com_data_block").hide();
        $(".com_edit_temp .com_text").html(parent.find(".com_text_temp").html());
        $(".com_edit_temp .com_id").val($this.data("id"));
        parent.find(".com_edit_block").html($(".com_edit_temp").html());
    });
    $(document).on('click', ".btncancelcomment", function(e) {
        var $this = $(this);
        var parent = $this.closest(".commetsingle");
        parent.find(".com_edit_block").html("");
        parent.find(".com_data_block").show();

    });
    $(document).on('click', ".btneditcomment", function(e) {
        var $this = $(this);
        var parent = $this.closest(".commetsingle");
        parent.find(".com_text_block").html(parent.find(".com_edit_block .com_text").val());
        parent.find(".com_text_temp").html(parent.find(".com_edit_block .com_text").val());

        var post_data = parent.find(".com_edit_block form").serialize();
        $.post(site_url + "articles/commentupdate", post_data, function(res) {
            $(".btncancelcomment").click();
        });


    });

    var page = 1;
    function loadComments() {
        $('.commentloader').addClass('loading');
        var post_data = {post_id: $("#post_id").val(), ptype: 'A', page: page};
        $.post(base_url + "articles/loadcomment", post_data, function(res) {
            $('.commentloader').removeClass('loading');
            $("#comment").val("");
            $('.commentcontainer').append(res);
        });
    }
    $(".loadmore").click(function() {
        page++;
        loadComments();
    });
    $(document).ready(function(e) {
        $('[data-toggle="tooltip"]').tooltip();
        loadComments();
        $(".articlecommentbox .btn_comment").click(function() {
            if ($("#comment").val() == "") {
                return;
            }
            var post_data = $("#comment_form").serialize();
            $.post(base_url + "articles/save_comment", post_data, function(res) {
                if (res.status == 1) {
                    $('#comment_form').find("input[type=text], textarea").val("");
                    $('#comment_form').find('[contenteditable=true]').empty();
                    $(".commentloader").show();
                    page = 1;
                    $('.commentcontainer').html("");
                    loadComments();

                    $(".comment_success").fadeIn();
                } else {
                    alert("Please Login");
                }
            }, 'json');
        });

    });
    $(document).on('click', ".btn_remove_comment", function(e) {
        var com_container = $(this).closest(".commetsingle");
        var post_data = $(this).data();
        $.confirm({
            title: 'Remove Comment',
            content: 'Are you sure to remove this comment',
            confirmButton: 'Remove',
            confirmButtonClass: 'btn-danger',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(base_url + "articles/deletecomment", post_data, function(res) {
                    com_container.fadeOut(function() {
                        com_container.remove();

                    });

                });

            }
        });
    });
</script>
