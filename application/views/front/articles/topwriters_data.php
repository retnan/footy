<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
//print_r($data[0]);
?>

<div class="data">   
    <div class="articlelist">
        <?php
        if ($data) {
            foreach ($data as $key => $user) {
                $ud = $user['ud'];
                $uh = $user['uh'];
                ?> 
                <div class="singlewriter">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="user" style="padding: 5px;">
                                <div class="manouter">
                                    <div class="maninner">
                                        <img src="<?php echo $ud['thumb']; ?>">
                                    </div>
                                </div>
                                <div class="footyouter">
                                    <div class="footyinner">
                                        <img src="<?php echo $ud['club_image']; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <h3><a href="<?php echo site_url() . "articles/listing/" . $ud['slug'] ?>"><?php echo $ud['name']; ?></a> <span class="rank">Rank #<?php echo $user['pos']; ?></span></h3>
                            <div class="clubname"><?php echo $ud['club_name']; ?></div>
                            <div class="clearfix"></div> 
                            <div class="profootercon">
                                <ul>
                                    <li>
                                        <div class="proval followers"><?php echo $uh['followers']; ?></div>
                                        <div class="prolabel">Followers</div>
                                    </li>
                                    <li>
                                        <div class="proval"><?php echo $uh['following']; ?></div>
                                        <div class="prolabel">Following</div>
                                    </li>
                                    <li>
                                        <div class="proval"><?php echo $user['points']; ?></div>
                                        <div class="prolabel">Points</div>
                                    </li>
                                    <li>
                                        <div class="proval"><?php echo $uh['art_count']; ?></div>
                                        <div class="prolabel">Articles</div>
                                    </li>
                                    <li>
                                        <div class="proval"><?php echo $uh['reads']; ?></div>
                                        <div class="prolabel">Reads</div>
                                    </li>
                                </ul>
                                <a href="<?php echo site_url() . "articles/listing/" . $ud['slug'] ?>" class="borderbutton pull-right">View Profile</a>
                            </div>
                        </div>
                    </div>            
                </div>
                <?php
            }
        } else {
            ?>
            <div class="singlewriter" style="min-height: 150px;">
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="text-align: center;margin-top: 60px;">No Writers Available</h3>
                    </div></div></div>
    <?php } ?>
    </div>
<?php echo $page; ?>
</div>

