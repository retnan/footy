<?php
if ($comments) {
    foreach ($comments['data'] as $comment) {

        // print_r($comment);
        ?>
        <div class="commetsingle">
            <div class="com_data_block">
                <div class="comimage">
                    <img src="<?php echo $comment['thumb']; ?>" />
                </div>
                <div class="comcontent">
                    <div class="arraowleft"></div>
                    <div class="comheader">
                        <?php echo $comment['name']; ?>
                        <?php if ($comment['remove_com'] == true) { ?>
                            <div class="remove btn_remove_comment" data-id="<?php echo $comment['pk_comment_id']; ?>"><i class="fa fa-times"></i></div>
                        <?php } ?>
                        <?php if ($comment['fk_user_id'] == $this->session->userdata('user_id')) { ?>
                            <div class="remove btn_edit_comment" data-id="<?php echo $comment['pk_comment_id']; ?>"><i class="fa fa-pencil"></i></div>


                        <?php } ?>
                        <div class="comdate"><i class="fa fa-clock"></i> <?php
                            echo timeAgo(strtotime($comment['created_at']));
                            ?></div> 

                    </div>
                    <div class="com_text_temp" style="display: none;"><?php echo trim($comment['content']); ?></div>

                    <div class="com_text_block comtext">
                        <div><?php echo nl2br(makeClickableLinks($comment['content'])); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="com_edit_block">

            </div>                            
        </div>
        <?php
    }
}
if (!$comments['load']) {
    ?>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $(".commentloader").hide();
        });
    </script>

    <?php
}
?>