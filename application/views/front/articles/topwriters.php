<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
//print_r($clubs);
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?>
<section class="articlecontent">
    <div class="row">
        <div class="col-md-8">
            <div class="row strip">
                <div class="col-md-12">
                    <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                        Top Writers
                    </div>
                </div>
            </div>
            <div class="row strip">
                <div class="col-md-4 col-md-offset-5">                    
                    <select style="width: 100%; padding: 5px;" id="tw_league" data-club_id="<?php echo $club_id; ?>">
                        <option value="0">All League</option>
                        <?php
                        if ($leagues) {
                            foreach ($leagues as $key => $val) {
                                ?>                        
                                <option value="<?php echo $key; ?>" <?php if ($league_id == $key) echo 'selected'; ?>><?php echo $val; ?></option>                   
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-3">                    
                    <select style="width: 100%; padding: 5px;" id="tw_club">
                        <option value="0">All Club</option>                       
                    </select>
                </div>
            </div>
            <div class="tw_users"></div>

        </div>

        <div class="col-md-4">
            <?php
            $this->load->view("front/includes/right_panel_news.php", array());
            ?>
        </div>

    </div>


    <div class="row">
        <div class="col-md-12">
            <?php
            $gc = trim(getCMSContent("ad_ggl_large"));
//            $ads = getAds("large");

            $ads = getSiteAds(4, "0", "large");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" style="width: 100%;" />
                    </a>
                </div>
                <?php
            } else {
                echo $gc;
            }
            ?>
        </div>
    </div>
    <div class="row">

        <?php
        $this->load->view("front/includes/bottom_panel_news.php", array());
        ?>

    </div>


</section>
<script type="text/javascript">
    var first = true;
    function getWriters() {
        var post_data = {league_id: $("#tw_league").val(), club_id: $("#tw_club").val()};
        $.post(base_url + "articles/loadtopwriters", post_data, function(res) {
            $(".tw_users").html(res);
        });
    }
    $(document).ready(function(e) {
        $("#tw_league").on("change", function() {
            var post_data = {league_id: $("#tw_league").val(), club_id: $("#tw_league").data("club_id")};
            $.post(base_url + "articles/getclubs", post_data, function(res) {
                $("#tw_club").html(res);
                getWriters();
            });
        });
        $("#tw_league").change();
        $("#tw_club").on("change", function() {
            getWriters();
        });

    });
</script>