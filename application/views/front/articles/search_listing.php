<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>
<section class="articlecontent">


    <div class="row">
        <div class="col-md-8">
            <div class="row strip">
                <div class="col-md-12">
                    <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                        Search Result
                    </div>
                </div>
            </div>
            <div id="listing_data"></div>
            <?php
            paginationScriptFront(base_url() . "articles/listing_data", "listing_data", "status=1&source=search");
            ?>
            <?php
            $this->load->view("front/includes/poll_horizontal.php", array("poll_data" => $poll_data));
            ?>
        </div>

        <div class="col-md-4">
            <?php
            $this->load->view("front/includes/right_panel_news.php", array());
            ?>
        </div>

    </div>


    <div class="row strip">
        <div class="col-md-12">

            <?php
            $gc = trim(getCMSContent("ad_ggl_large"));
//            $ads = getAds("large");

            $ads = getSiteAds(4, "0", "large");
            $cnt = count($ads);
            $ads = getAds("large");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" style="width: 100%;" />
                    </a>
                </div>
                <?php
            } else {
                echo $gc;
            }
            ?>
        </div>
    </div>

    <div class="row">

        <?php
        $this->load->view("front/includes/bottom_panel_news.php", array());
        ?>

    </div>


</section>

<script type="text/javascript">
    $(document).on('click', ".poll_answer", function(e) {

        var post_data = $(this).data();
        $.confirm({
            title: 'Poll Voting',
            content: 'Are you sure to Vote your Option',
            confirmButton: 'Sure',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(base_url + "home/save_poll", post_data, function(res) {
                    alert("Your Vote has been Saved");

                });

            }
        });
    });
</script>
