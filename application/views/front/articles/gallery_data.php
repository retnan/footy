<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
$user_id = $this->session->userdata("user_id");
?>

<div class="data">

    <div class="row" style="margin-bottom: 15px; display:none;">
        <div class="col-md-12">
            <div class="searchbox">
                <div class="searchbtn">
                    <div class="btnblack searchBtn">
                        Search
                    </div>
                </div>
                <div class="searchtext">
                    <input type="text" class="txt_searchbox" value="<?php echo $search; ?>" placeholder="Search..." >
                </div>
            </div>
        </div>
    </div>

    <div class="galletylist"> 
        <?php
        if (count($data) > 0) {
            foreach ($data as $gallery) {
                if ($gallery['vid'] == false) {
                    ?> 
                    <div class="galleryconout"  data-toggle="tooltip" data-placement="top" title="">
                        <div class="gallerycon">
                            <?php if ($user_id == $gallery['user_id']) { ?>
                                <div class="deletebtn" data-id='<?php echo $gallery['id']; ?>'>
                                    <i class="fa fa-trash-o"></i>
                                </div>
                            <?php } ?>
                            <a class="fancybox-effects-a" href="<?php echo $gallery['large_image']; ?>" title="" data-fancybox-group="gallery"> 
                                <img src="<?php echo $gallery['thumb']; ?>" />
                            </a>
                        </div>

                    </div>
                <?php } else { ?>
                    <div class="galleryconout"  data-toggle="tooltip" data-placement="top" title="">
                        <a href="#">
                            <div class="gallerycon">
                                <?php if ($user_id == $gallery['user_id']) { ?>
                                    <div class="deletebtn" data-id='<?php echo $gallery['id']; ?>'><i class="fa fa-trash-o"></i></div>
                                <?php } ?>
                                <a class="fancybox-media" href="<?php echo $gallery['vid_link']; ?>"><img src="<?php echo $gallery['thumb']; ?>" /></a> 
                            </div>
                        </a>
                    </div>

                <?php } ?> <?php }
            ?> 
        <?php } else {
            ?>
            <h3 style="width: 100%; text-align: center;">No Image Available</h3>
            <?php
        }
        ?>


    </div>
    <div class="clearfix"></div>
    <?php echo $page; ?>



</div>

