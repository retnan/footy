<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?> 
<section class="articleheader">
    <div class="row hide">
        <div class="col-md-5">
            <div class="box1">
                <div class="boxheader">Ibkdreams</div>
                <div class="boxcontent">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="user">
                                <div class="manouter">
                                    <div class="maninner">
                                        <img src="<?php echo $base; ?>images/laughing.jpg" />
                                    </div>
                                </div>
                                <div class="footyouter">
                                    <div class="footyinner">
                                        <img src="<?php echo $base; ?>images/clubs/manchester_city.png" />
                                    </div>
                                </div>                                                
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="clearfix"></div>
                            <div class="victory">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <img src="<?php echo $base; ?>images/victor.png" />
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <img src="<?php echo $base; ?>images/victor.png" />
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <img src="<?php echo $base; ?>images/victor.png" />
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <img src="<?php echo $base; ?>images/victor.png" />
                                    </div>
                                </div>
                            </div>
                            <div style="margin-top: 15px;">
                                <div class="borderbutton">1st</div>
                                <div class="borderbutton">Follow</div>
                                <div class="borderbutton">Message</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="teamheader">
                <img src="<?php echo $base; ?>images/teamheader.jpg" />
                <img class="logo" src="<?php echo $base; ?>images/clubs/manchester_united.png" />
                <div class="sociallinks">
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="insta"><i class="fa fa-instagram"></i></a>
                    <a href="#" class="feed"><i class="fa fa-rss"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="articlecontent" style="margin-bottom: 50px;">
    <div class="row">
        <div class="col-md-8">
            <div class="row strip">
                <div class="col-md-12">
                    <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                        Edit Article
                    </div>
                </div>
            </div>



            <?php $file_type = array("I" => "Image", "V" => "Video Link"); ?>

            <div class="bd-example" data-example-id="">
                <?php $art_tags = explode(",", $article['tags']); ?>
                <form id="article_form" style="margin-bottom: 0px;" enctype="multipart/form-data">
                    <div class="alert alert alert-success article_success" style="display: none;">Article has been saved.</div>
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="">Article League</label>
                                <select name="league" id="league" class="form-control">
                                    <option value="">Select League</option>
                                    <?php foreach ($leagues as $key => $league) { ?>
                                        <option value="<?php echo $key; ?>" <?php echo ($article['fk_league_id'] == $key) ? "selected" : ""; ?>><?php echo $league; ?></option>
                                    <?php } ?>
                                </select>  
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="">Article Club</label>
                                <select name="club" id="club" class="form-control">
                                    <option value="">Select Club</option>
                                    <?php foreach ($clubs as $key => $club) { ?>
                                        <option value="<?php echo $key; ?>" <?php echo ($article['fk_club_id'] == $key) ? "selected" : ""; ?>><?php echo $club; ?></option>
                                    <?php } ?>

                                </select>  
                            </fieldset>
                        </div>
                    </div>
                    <fieldset class="form-group">
                        <label for="">Article Title</label>
                        <input type="text" class="form-control required" value="<?php echo $article['title']; ?>" id="title" name="title" placeholder="Title" />                       
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="">Article Tags</label>              
                        <select class="article_tags form-control" id="tags" value="<?php echo $article['tags']; ?>" placeholder="Article tags" name="tags[]"  multiple="multiple">
                            <?php
                            if (count($art_tags) > 0) {
                                foreach ($art_tags as $tag) {
                                    ?> 
                                    <option value="<?php echo $tag; ?>" selected><?php echo $tag; ?></option>
                                    <?php
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="">Media Type</label>              
                        <select name="file_type" id="file_type" class="form-control">
                            <option value="">Select Media</option>
                            <?php foreach ($file_type as $key => $type) { ?>
                                <option value="<?php echo $key; ?>" <?php if ($article['file_type'] == $key) echo 'selected'; ?>><?php echo $type; ?></option>
                            <?php } ?>
                        </select>

                    </fieldset>
                    <?php if ($article['vid_link'] != "") { ?>
                        <fieldset class="media_video form-group hide">
                            <label for="">Current Video</label>                       
                            <div><iframe class="articlevideo" src="<?php echo $article['vid_link']; ?>" frameborder="0" allowfullscreen></iframe></div>
                        </fieldset>
                    <?php } ?>
                    <fieldset class="media_video form-group hide">
                        <label for="">Youtube Video Link</label>                       
                        <input type="text" placeholder="Youtube Link" id="youtube_link2" name="youtube_link2" class="form-control">
                        <input type="text" placeholder="Youtube Link" id="youtube_link" name="youtube_link" class="form-control" style="display: none;">

                        <span class="label label-info">Note: </span> <small> i.e. http://youtube.com/vid?abcd</small>
                    </fieldset>

                    <fieldset class="form-group media_img hide">
                        <label >Current Image</label>
                        <div ><img src="<?php echo $article['thumb']; ?>" class="img-polaroid" width="200"/></div>

                    </fieldset>
                    <fieldset class="form-group media_img hide">

                        <label >Upload Image</label>
                        <input type="file" class="form-control-file"  name="file_name" id="file_file" />
                        <span class="label label-info">Note: </span> <small> Only jpg, gif, jpeg and png files are allowed</small>
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="">Content</label>                      
                        <textarea class="editor_content form-control" name="content" id="editor_content" style="visibility: hidden;" rows="10"><?php echo $article['content']; ?></textarea>
                    </fieldset>
                    <input type="hidden" value="A" name="post_type">
                    <input type="hidden" value="<?php echo $article['pk_post_id']; ?>" name="post_id" >
                    <button type="button" class="btn btn-success save_article">Update Article</button>
                </form>
            </div>

        </div>
        <div class="col-md-4">
            <?php
            $this->load->view("front/includes/right_panel_news.php", array('bottom' => false));
            ?>
        </div>
    </div>


</section>
<script type="text/javascript">
    function matchYoutubeUrl(url) {
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        var matches = url.match(p);
        if (matches) {
            return matches[1];
        }
        return false;
    }

    var editor;
    $(document).ready(function(e) {
        $("#league").on("change", function() {
            var post_data = {league_id: $("#league").val()};
            $.post(base_url + "articles/getclubs", post_data, function(res) {
                $("#club").html(res);
                $("#club").closest("div").hide().fadeIn();

            });
        });
        $("#file_type").change(function() {


            if ($(this).val() == 'I') {
                $(".media_img").removeClass("hide");
                $(".media_img input#youtube_link").addClass("required");
                $(".media_video input#youtube_link").removeClass("required");
                $(".media_video").addClass("hide");
            } else if ($(this).val() == 'V') {
                $(".media_video").removeClass("hide");
                $(".media_video input#youtube_link").addClass("required");
                $(".media_img input#youtube_link").removeClass("required");
                $(".media_img").addClass("hide");
            } else {

                $(".media_img input,.media_video input").removeClass("required");
                $(".media_img,.media_video").addClass("hide");
            }
        });

        $("#file_type").change();
        var art_tags = $(".article_tags").select2({tags: true, placeholder: "Article Tags", tokenSeparators: [',']});
        $("#youtube_link2").on("blur", function() {
            var url = $("#youtube_link2").val();
            var id = matchYoutubeUrl(url);
            if (id != false) {
                $("#youtube_link").val(id).blur();
                $("#youtube_link").closest(".control-group").addClass("success").removeClass("error");
                $("#youtube_link").closest(".control-group").find("span.help-inline").html("").addClass("hide");
            } else {
                $("#youtube_link").val('').blur();
                $("#youtube_link").closest(".control-group").addClass("error").removeClass("success");
                $("#youtube_link").closest(".control-group").find("span.help-inline").html("Invalid youtube URL").removeClass("hide");
            }
        });

        if (editor)
            editor.destroy();
        editor = CKEDITOR.replace("editor_content", {
        });



        $(".save_article").click(function() {
            var content = editor.getData();
            $(".editor_content").val(content);
            if ($("#article_form").valid() == false) {
                return;
            }
            App.blockUI($("#article_form"));
            var formData = new FormData($("form#article_form")[0]);
            $.ajax({
                url: base_url + "articles/updatearticle",
                type: 'POST',
                data: formData,
                async: false,
                success: function(data) {
                    App.unblockUI($("#article_form"));
                    $(".article_success").hide().fadeIn();
                },
                error: function(data) {
                    App.unblockUI($("#article_form"));
//                    $.gritter.add({title: 'Article Error', text: 'Error in Saving Article'});
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });

    });
</script>