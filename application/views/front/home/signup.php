<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>
<div class="container" style="margin-top: 25px;">


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="loginbox">
                <h2>Welcome to FootyFanz</h2>
                <h3>By Fanz. For Fanz.</h3>
                <form id="signupform">
                    <div class="loglabel">Sign Up</div>
                    <div class="invalidfields">Email already registered</div>
                    <div class="validfields"></div>
                    <div class="loginfield">
                        <input type="text" placeholder="What's Your Name" name="name" />
                    </div>
                    <div class="loginfield">
                        <input type="text" placeholder="Create Your Username" name="username" />
                    </div>
                    <div class="loginfield">
                        <input type="text" placeholder="What's Your Email Address" name="email" />
                    </div>
                    <div class="loginfield">
                        <input type="password" placeholder="Add a Password" name="password" id='password' />
                    </div>
                    <div class="loginfield">
                        <select name="club">
                           <option value="">Choose your favorite club</option>
                            <?php foreach ($clubs as $id => $club) { ?>
                                <option value="<?php echo $id; ?>"><?php echo $club ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="loginfield" >
                        <div class="loglabel showwait" style="display:none;">Please wait...</div>
                        <div class=" btnlogin actionRegister">Register</div>
                    </div>
                </form>
                <div style="text-align: center; color: #fff;">
                    Already registered? <a href="<?php echo site_url() . "home/login/index" ?>" style="padding: 3px; color: #fff; text-decoration: underline;">Login now</a>
                </div>
                <div class="loglabel" style="margin-top: 5px;">Link Your Social Media</div>

                <div class=" socialloginbuttons">
                    <a style="cursor: pointer;" class="btnfacebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="btntwitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="footerlogo">
                    <img src="<?php echo $base; ?>images/logo.png" />
                </div>
                <br />

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(e) {
        $(".actionRegister").on('click', function() {
            $("#signupform .invalidfields").hide();
            $("#signupform .validfields").hide();
            $("#signupform .actionRegister").hide();
            $("#signupform .showwait").show();
            var post_data = $("#signupform").serialize();
            $.post(site_url + "user/registration", post_data, function(res) {
                $("#signupform .showwait").hide();
                $("#signupform .actionRegister").show();
                if (res.status == "1") {
                    $("#signupform .validfields").html(res.msg).fadeIn();
                    $("#signupform")[0].reset();
                } else {
                    $("#signupform .invalidfields").html(res.msg).fadeIn();
                }
            }, "JSON");
        });
        $("#signupform #password").on('keyup', function(e) {
            if (e.keyCode == 13)
            {
                $(".actionRegister").click();
            }
        });


    });

</script>


<script type="text/javascript">var newWin;
    $(document).ready(function(e) {
        $(".btnfacebook").on('click', function() {
            $(".close-btn").click();
            var url = site_url + 'facebook/connect';
            newWin = popupwindow(url, "Facebook Login", 800, 500);
            if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
                window.location = url;
            }
        });
    });
    function afterLogin($status, $login_id) {
        newWin.close();
        window.location.href = site_url + "dashboard";
    }</script>