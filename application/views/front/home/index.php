<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
//print_r($hp_news);
$main_news = $hp_news[0];
$trend_news = getHomePageNews('N', 0, 0, 0, 0, 9, false);
$sp_news = getHomePageNews('N', 1, 0, 0, 0, 9, false);
$backall = getBackground("2", "0");
$back = getBackground("1", $league_id);
$back2 = getBackground("1", $league_id, TRUE);
$back3 = getBackground("6", $league_id);

$bg = "";
if (count($backall) > 0) {
    $bg = $backall[0];
}
if (count($back2) > 0) {
    $bg = $back2[0];
}
if (count($back) > 0) {
    $bg = $back[0];
}
if (count($back3) > 0) {
    $bg = $back3[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?>
<section class="homefirstcol">
    <div class="row">
        <div class="col-md-8">


            <div id="myCarousel" class="carousel slide">
                <div class="carousel-inner">
                    <?php foreach ($hp_news as $k => $main_news) { ?>
                        <div class="item <?php echo ($k == "0") ? "active" : ""; ?>">
                            <a href="<?php echo site_url() . NEWS_TAG . "/"; ?><?php echo $main_news['slug'] ?>">
                                <div class="homeslider">
                                    <img src="<?php echo $main_news['image']; ?>" />

                                    <div class="sliderfooter">
                                        <div class="line"></div>

                                        <div>
                                            <div class="hs_heading"><?php echo $main_news['title']; ?></div>
                                            <div class="clear clearfix"></div>
                                            <div class="uname">by <a href="<?php echo site_url() . "articles/listing/" . $main_news['user_slug']; ?>"><?php echo $main_news['posted_by']; ?></a></div>
                                            <div class="uimage"><img src="<?php echo $main_news['user_image']; ?>" /></div>                                            
                                            <div class="ucomments"><i class="fa fa-comments-o"></i> <?php echo $main_news['comments']; ?></div>
                                            <div class="uviews"><i class="fa fa-eye"></i> <?php echo $main_news['views']; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>  
                    <?php } ?>

                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>

            </div>
            <script>
                $(document).ready(function() {
                    $('#myCarousel').carousel({
                        interval: 5000
                    });
                    $('#myCarousel').carousel('cycle');

                });
            </script>




            <div class="clearfix"></div>
            <div class="row slidersmall">
                <?php
                foreach ($hp_news as $key => $row) {
                    if ($key == 0)
                        continue;
                    ?> 
                    <div class="col-md-4 ">
                        <a href="<?php echo site_url() . NEWS_TAG . "/"; ?><?php echo $main_news['slug'] ?>">
                            <div class="homeslidersmall">
                                <img src="<?php echo $row['image']; ?>" />
                                <div class="smalltitle">
                                    <?php echo $row['title']; ?>
                                </div>
                            </div>
                        </a>
                    </div>

                <?php }
                ?>


            </div>




        </div>
        <div class="col-md-4">
            <div class="show_mobile" style="margin-top:15px;"></div>
            <div class="writerboard">
                <div class="boardheader">
                    Top Writerz
                    <a href="<?php echo site_url() . "articles/topwriters/" . $league_id . "/0"; ?>">More</a>
                </div>
                <table class="table postable" style="margin-bottom: 0;">
                    <tr>
                        <th>Pos</th>
                        <th>Writers</th>
                        <th>Pts</th>
                    </tr>
                    <?php
                    if ($hp_writers) {
                        foreach ($hp_writers as $pos => $row) {
                            ?> 
                            <tr>
                                <td><?php echo $pos + 1; ?> <span class="symbolup"><i class="fa fa-sort-up"></i></span></td>
                                <td style=""><a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>"><?php echo $row['name']; ?> <span class="boarduser"><img src="<?php echo $row['thumb']; ?>" /></span></a></td>
                                <td><?php echo $row['points']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>                  
                </table>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="row strip">
        <div class="col-md-3">
            <div class="blacklabel">
                Trending News
            </div>
        </div>
        <div class="col-md-9 hide_mobile">
            <div class="stripicons">               
                <img src="<?php echo $base; ?>images/iconfire.png" />
            </div>
        </div>
    </div>
    <div class="row strip">
        <div class="col-md-8">
            <div class="row trendnews">
                <div class="col-md-7" >
                    <?php
                    // print_r($trend_news);

                    if ($trend_news) {
                        ?>
                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $trend_news[0]['slug']; ?>">
                            <div class="tnewsblock">
                                <img src="<?php echo $trend_news[0]['large']; ?>" />

                                <div class="tnbfooter">
                                    <div class="heading"><?php echo $trend_news[0]['title']; ?></div>
                                    <div class="uname">by <?php echo $trend_news[0]['user']['name']; ?></div>
                                    <div class="uimage"><img src="<?php echo $trend_news[0]['user']['profile_pic']; ?>"></div>
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                </div>

                <div class="col-md-4">
                    <div class="promoted extra-pimp">
                        <ul>
                            <li>
                                <a>More News...</a>

                            </li>
                            <?php
                            if ($trend_news and count($trend_news) > 1) {
                                foreach ($trend_news as $key => $news) {
                                    if ($key == 0)
                                        continue;
                                    ?> <li>
                                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $news['slug']; ?>"><?php echo $news['title']; ?></a>
                                    </li> <?php
                                }
                                ?>

                            <?php } ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">

            <?php
            $gc = trim(getCMSContent("ad_ggl_square"));
            if ($gc == "") {
                $ads = getAds("square");
                $cnt = count($ads);
                if ($cnt > 0) {
                    $num = array_rand($ads);
                    ?>
                    <div class="leftadd">
                        <a href="<?php echo $ads[$num]['link'] ?>">
                            <img src="<?php echo $ads[$num]['image']; ?>" />
                        </a>
                    </div>
                    <?php
                }
            } else {
                echo $gc;
            }
            ?>

        </div>
    </div>



    <?php if (count($ffplus) > 0) { ?>
        <div class="row strip">
            <div class="col-md-3">
                <div class="blacklabel">
                    Footyfanz+
                </div>
            </div>
            <div class="col-md-9 hide_mobile">
                <div class="stripicons">
                    <img src="<?php echo $base; ?>images/iconfire.png" />
                </div>
            </div>
        </div>
        <div class="row strip">
            <div class="col-md-12">

                <div class="homeffpluscon">
                    <?php foreach ($ffplus as $ffp) { ?>
                        <div class="homeffplusitem">
                            <a href="<?php echo site_url() . "articles/listing/" . $ffp['slug']; ?>">
                                <img src="<?php echo $ffp['article_pic']; ?>" />

                                <div class="textname">
                                    <div class="imgname" style="width: 40px; float: left; border-radius: 40px; overflow: hidden">
                                        <img src="<?php echo $ffp['profile_pic']; ?>" />
                                    </div>
                                    <div class="imgname2" style="margin-left: 50px;">
                                        <div class="texttitle"><?php echo $ffp['art_title'] ?></div>
                                        <div><?php echo $ffp['name'] ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                </div>

            </div>
        </div>
    <?php } ?>
<!--                <img src="<?php echo $base; ?>images/footyfanz.jpg" style="width: 100%;" />-->

    <div class="row strip">
        <div class="col-md-3">
            <div class="writerboard">
                <div class="boardheader">
                    Top Fanz
                    <a href="<?php echo site_url() . "articles/topfanz/" . $league_id . "/0"; ?>">More</a>
                </div>
                <table class="table postable small">
                    <?php
                    if ($hp_fanz) {
                        foreach ($hp_fanz as $pos => $row) {
                            ?> 
                            <tr>
                                <td><?php echo $pos + 1; ?> <span class="symbol"><i class="fa fa-sort-up"></i></span></td>
                                <td><a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>"><?php echo (strlen($row['name']) > 17) ? substr($row['name'], 0, 15) . "..." : $row['name']; ?> <span class="boarduser"><img src="<?php echo $row['thumb']; ?>" /></span></a></td>
                                <td><?php echo $row['points']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>                                     
                </table>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-7" >
                    <?php if ($poll_data) { ?>
                        <div class="homepoll"  style="background: url(<?php echo $poll_data['image']; ?>); background-size: cover; height: 565px;">                          
                            <div class="content" style="height: 320px;">
                                <div class="text">
                                    <?php echo $poll_data['title']; ?>
                                </div>
                                <div style="    text-align: center;

                                     color: #fff;
                                     margin-top: 5px;
                                     width: 100%;
                                     font-size: 8px;">
                                    <?php echo $poll_data['option1'] . ": " . $poll_data['votes']['perA']; ?>%
                                    &nbsp; &nbsp; <?php echo $poll_data['option2'] . ": " . $poll_data['votes']['perB']; ?>%
                                    <?php if (trim($poll_data['option3']) != "") { ?>
                                        &nbsp; &nbsp; <?php echo $poll_data['option3'] . ": " . $poll_data['votes']['perC']; ?>%
                                    <?php } ?>
                                    <?php if (trim($poll_data['option4']) != "") { ?>
                                        &nbsp; &nbsp; <?php echo $poll_data['option4'] . ": " . $poll_data['votes']['perD']; ?>%
                                    <?php } ?>
                                    <?php if (trim($poll_data['option5']) != "") { ?>
                                        &nbsp; &nbsp; <?php echo $poll_data['option5'] . ": " . $poll_data['votes']['perE']; ?>%
                                    <?php } ?>

                                </div>
                                <div class="button poll_answer firstbutton" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="1"><?php echo $poll_data['option1']; ?></div>
                                <div class="button poll_answer" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="2"><?php echo $poll_data['option2']; ?></div>
                                <?php if (trim($poll_data['option3']) != "") { ?>
                                    <div class="button poll_answer"  data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="3"><?php echo $poll_data['option3']; ?></div>
                                <?php } ?>
                                <?php if (trim($poll_data['option4']) != "") { ?>
                                    <div class="button poll_answer"  data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="4"><?php echo $poll_data['option4']; ?></div>
                                <?php } ?>
                                <?php if (trim($poll_data['option5']) != "") { ?>
                                    <div class="button poll_answer"  data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="4"><?php echo $poll_data['option5']; ?></div>
                                <?php } ?>
                            </div>
                            <div class="sociallinks" style="display:none">
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="insta"><i class="fa fa-instagram"></i></a>
                                <a href="#" class="feed"><i class="fa fa-rss"></i></a>
                            </div>
                        </div>
                    <?php } else { ?>
                        <!--                        <div class="homepoll">
                                                    <img src="<?php echo $base; ?>images/pollhomebg.jpg" />
                                                    <div class="content">
                                                        <div class="text">
                                                            Is Mourinho  Right For United?
                                                        </div>
                                                        <div class="button">Yes</div>
                                                        <div class="button">No</div>
                                                    </div>
                                                    <div class="sociallinks">
                                                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                                        <a href="#" class="insta"><i class="fa fa-instagram"></i></a>
                                                        <a href="#" class="feed"><i class="fa fa-rss"></i></a>
                                                    </div>
                                                </div>-->
                    <?php } ?>
                </div>
                <div class="col-md-5">
                    <div class="writerboard">                                        
                        <table class="table postable small">
                            <tr>
                                <th style="width: 35px;">#</th>
                                <th>Team</th>
                                <th>Pts</th>
                            </tr>
                            <?php
                            $tot_teams = count($team_points);
                            foreach ($team_points as $key => $row) {
                                $cls = "";
                                if ($key == 0) {
                                    $cls = "yellow";
                                }
                                if ($key >= $tot_teams - 3) {
                                    $cls = "red";
                                }
                                ?>  
                                <tr class="<?php echo $cls; ?>" >
                                    <td><?php echo $key + 1; ?> </td>
                                    <td><a href="<?php echo site_url() . "club/" . $row['slug']; ?>"><?php echo $row['title']; ?></a> </td>
                                    <td><?php echo intval($row['points']); ?></td>
                                </tr> <?php }
                            ?>                           


                        </table>
                    </div>
                </div>
            </div>

            <div class="row ">
                <div class="col-md-12" style="margin-top: 15px;">
                    <?php
                    $gc = getCMSContent("ad_ggl_large");
                    if ($gc == "") {
                        $ads = getAds("small");
                        $cnt = count($ads);
                        if ($cnt > 0) {
                            $num = array_rand($ads);
                            ?>

                            <a href="<?php echo $ads[$num]['link'] ?>">
                                <img src="<?php echo $ads[$num]['image']; ?>" style="width:100%;" />
                            </a>

                            <?php
                        }
                    } else {
                        echo $gc;
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
    <div class="row strip">
        <div class="col-md-3">
            <div class="blacklabel">
                Sponsored News
            </div>
        </div>
        <div class="col-md-9 hide_mobile">
            <div class="stripicons">               
                <img src="<?php echo $base; ?>images/iconcoin.png" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="row trendnews">
                <div class="col-md-8">
                    <?php if ($sp_news) { ?>
                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $sp_news[0]['slug']; ?>">
                            <div class="tnewsblock">
                                <img src="<?php echo $sp_news[0]['large']; ?>" />

                                <div class="tnbfooter">
                                    <div class="heading"><?php echo $sp_news[0]['title']; ?></div>
                                    <div class="uname">by <?php echo $sp_news[0]['user']['name']; ?></div>
                                    <div class="uimage"><img src="<?php echo $sp_news[0]['user']['profile_pic']; ?>"></div>
                                </div>
                            </div>
                        </a>
                    <?php } ?>

                </div>
                <div class="col-md-4">
                    <div class="promoted">
                        <ul>
                            <li>
                                <a>More News...</a>
                            </li>
                            <?php
                            if ($sp_news and count($sp_news) > 1) {
                                foreach ($sp_news as $key => $news) {
                                    if ($key == 0)
                                        continue;
                                    ?> <li>
                                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $news['slug']; ?>"><?php echo $news['title']; ?></a>
                                    </li> <?php
                                }
                                ?>

                            <?php } ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php
            $gc = trim(getCMSContent("ad_ggl_square"));
            $ads = getSiteAds(1, "0", "square");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" />
                    </a>
                </div>
                <?php
            } else {
                echo $gc;
            }
            ?>
        </div>
    </div>
    <div class="row strip">
        <div class="col-md-12">
            <img src="<?php echo $base; ?>images/statattrack.jpg" style="width: 100%;" />                            
        </div>
    </div>



    <div class="row strip">
        <div class="col-md-3">
            <div class="blacklabel">
                Club News
            </div>
        </div>
        <div class="col-md-9 hide_mobile">
            <div class="stripicons">           
                <img src="<?php echo $base; ?>images/iconfootball.png" />
            </div>
        </div>
    </div>

    <?php
    //Bottom Latest Club Wise News Section
    $this->load->view("front/includes/bottom_club_news.php", array());
    //End of Bottom Latest Club Wise News Section
    ?>
    <div class="row strip">
        <div class="col-md-3">
            <div class="blacklabel">
                Gallery
            </div>
        </div>
        <div class="col-md-9 hide_mobile">
            <div class="stripicons">              
                <img src="<?php echo $base; ?>images/icongallery.png" />
            </div>
        </div>
    </div>

    <div class="row strip gallery">
        <div class="col-md-8">
            <div class="row">
                <div class="videodata" style="display: none;">
                    <?php echo json_encode($hp_videos); ?>
                </div>



                <script type="text/javascript">
                    $(document).on('click', ".poll_answer", function(e) {
                        var post_data = $(this).data();
                        $.confirm({
                            title: 'Poll Voting',
                            content: 'Are you sure to Vote your Option',
                            confirmButton: 'Sure',
                            confirmButtonClass: 'btn-success',
                            animation: 'scale',
                            animationClose: 'top',
                            opacity: 0.5,
                            confirm: function() {
                                $.post(base_url + "home/save_poll", post_data, function(res) {
                                    alert("Your Vote has been Saved");

                                });

                            }
                        });
                    });
                    $(document).ready(function(e) {
                        var videodata = $.parseJSON($(".videodata").html());
                        var videodatasize = videodata.length;
                        var currvideokey = 0;
                        $.each(videodata, function(k, v) {
                            if (k == 0) {
                                $("#vd1").attr("src", v['vid_link']);
                                $(".gallerttext").html(v['title']);
                            } else {
                                $("#vd" + (k + 1)).attr("src", v['vid_image']);
                                $("#vd" + (k + 1)).closest("a").data("key", k);
                            }
                        });
                        $(".smallvideo a").click(function() {
                            var curr_key = $(this).data("key");
                            $("#vd1").attr("src", videodata[curr_key]['vid_link']);
                            $(".gallerttext").html(videodata[curr_key]['title']);
                            currvideokey = curr_key;
                            for (x = 0; x < videodatasize; x++) {
                                var newkey = curr_key + x + 1;
                                newkey = newkey % videodatasize;
                                $("#vd" + (x + 2)).attr("src", videodata[newkey]['vid_image']);
                                $("#vd" + (x + 2)).closest("a").data("key", newkey);
                            }
                            return false;
                        });
                        $(".gallerypre").click(function() {
                            var curr_key = currvideokey - 1;
                            $("#vd1").attr("src", videodata[curr_key]['vid_link']);
                            $("#vd1").height($("#vd1").parent().height());
                            $("#vd1").prop("height", $("#vd1").parent().height());
                            $(".gallerttext").html(videodata[curr_key]['title']);
                            currvideokey = curr_key;
                            for (x = 0; x < videodatasize; x++) {
                                var newkey = curr_key + x;
                                newkey = newkey % videodatasize;
                                $("#vd" + (x + 2)).attr("src", videodata[newkey]['vid_image']);
                                $("#vd" + (x + 2)).closest("a").data("key", newkey);
                            }
                            return false;
                        });
                        $(".gallerynext").click(function() {
                            var curr_key = currvideokey + 1;
                            $("#vd1").attr("src", videodata[curr_key]['vid_link']);
                            $(".gallerttext").html(videodata[curr_key]['title']);
                            currvideokey = curr_key;
                            for (x = 0; x < videodatasize; x++) {
                                var newkey = curr_key + x;
                                newkey = newkey % videodatasize;
                                $("#vd" + (x + 2)).attr("src", videodata[newkey]['vid_image']);
                                $("#vd" + (x + 2)).closest("a").data("key", newkey);
                            }
                            return false;
                        });

                    });
                </script>



                <div class="col-md-6">
                    <div class="largevideo">
                        <iframe class="articlevideo" id="vd1" src="" frameborder="0" allowfullscreen style=""></iframe>                       
                    </div>



                    <?php if ($hp_videos == "aaa") { ?>
                        <?php if ($hp_videos[0]['vid'] and $hp_videos[0]['type'] == "V") { ?>
                            <video width="100%" controls>
                                <source src="<?php echo $hp_videos[0]['vid_link']; ?>" type="video/mp4">
                            </video>
                        <?php } ?>
                        <?php if ($hp_videos[0]['vid'] and $hp_videos[0]['type'] == "Y") { ?>
                            <iframe class="articlevideo" src="<?php echo $hp_videos[0]['vid_link']; ?>" frameborder="0" allowfullscreen></iframe>

                        <?php } ?>

                    <?php } ?>

                </div>
                <div class="col-md-3">
                    <div class="show_mobile" style="height: 10px;"></div>
                    <div class="smallvideo">
                        <a href="#">
                            <img id="vd2" src="" style="width: 100%;" />
                            <i class="fa fa-play-circle-o"></i>
                        </a>
                    </div>
                    <div class="" style="height: 9px;"></div>
                    <div class="smallvideo">
                        <a href="#">
                            <img id="vd3" src="" style="width: 100%;" />
                            <i class="fa fa-play-circle-o"></i>
                        </a>
                    </div>




                    <?php
                    if ($hp_videos and count($hp_videos) > 1 and 1 == 2) {
                        foreach ($hp_videos as $key => $vid) {
                            if ($key == 0)
                                continue;
                            if ($key == 3)
                                break;
                            ?>             <div class="videoimage">
                            <?php if ($vid['vid'] and $vid['type'] == "V") { ?>

                                    <video width="100%" controls>
                                        <source src="<?php echo $vid['vid_link']; ?>" type="video/mp4">
                                    </video>
                                <?php } ?>
                                <?php if ($vid['vid'] and $vid['type'] == "Y") { ?>

                                    <iframe class="articlevideo" src="<?php echo $vid['vid_link']; ?>" frameborder="0" allowfullscreen></iframe>
                                    </video>
                                <?php } ?>
                            </div> <?php
                        }
                        ?>

                    <?php } ?>

                </div>
                <div class="col-md-3">
                    <div class="show_mobile" style="height: 10px;"></div>
                    <div class="smallvideo">
                        <a href="#">
                            <img id="vd4" src="" style="width: 100%;" />
                            <i class="fa fa-play-circle-o"></i>
                        </a>
                    </div>
                    <div class="" style="height: 9px;"></div>
                    <div class="smallvideo">
                        <a href="#">
                            <img id="vd5" src="" style="width: 100%;" />
                            <i class="fa fa-play-circle-o"></i>
                        </a>
                    </div>



                    <?php
                    if ($hp_videos and count($hp_videos) > 3 and 1 == 3) {
                        foreach ($hp_videos as $key => $vid) {
                            if ($key < 3)
                                continue;
                            ?>             <div class="videoimage">
                            <?php if ($vid['vid'] and $vid['type'] == "V") { ?>

                                    <video width="100%" controls>
                                        <source src="<?php echo $vid['vid_link']; ?>" type="video/mp4">
                                    </video>
                                <?php } ?>
                                <?php if ($vid['vid'] and $vid['type'] == "Y") { ?>

                                    <iframe class="articlevideo" src="<?php echo $vid['vid_link']; ?>" frameborder="0" allowfullscreen></iframe>
                                    </video>
                                <?php } ?>
                            </div> <?php
                        }
                        ?>

                    <?php } ?>   
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="gallerynav">
                        <a class="btnpre gallerypre">Previous</a>
                        <a class="btnnext gallerynext">Next</a>
                        <div class="gallerttext">Highlights of Liverpool vs Manchester United </div>

                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php
            $gc = trim(getCMSContent("ad_ggl_square"));
            $ads = getSiteAds(1, "0", "square");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" />
                    </a>
                </div>
                <?php
            } else {
                echo $gc;
            }
            ?>
        </div>       
    </div>



    <div class="row strip">
        <div class="col-md-12">

            <?php
            $gc = trim(getCMSContent("ad_ggl_square"));
            $ads = getSiteAds(1, "0", "large");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" />
                    </a>
                </div>
                <?php
            } else {
                echo $gc;
            }
            ?>
        </div>
    </div>











</section>