<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>

<div class="container">


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="loginbox">
                <h2>Welcome to FootyFanz</h2>
                <h3>By Fanz. For Fanz.</h3>   
                <?php if($status=="1"){ ?>
                <form id="resetform" >
                    <div class="loglabel">Reset Password</div>
                    <div class="invalidfields">Invalid Username or Email</div>
                    <div class="validfields"></div>
                    <div class="loginfield">
                        <input type="password" placeholder="New Password" name="password" id="password" />
                    </div>
                    <div class="loginfield">
                        <input type="password" placeholder="Confirm Password" name="cpassword" id="cpassword" />
                    </div>
                    <input type="hidden" name="id" value="<?php echo $id ?>" />
                    <input type="hidden" name="code" value="<?php echo $code ?>" />
                    <div class="loginfield">
                        <div class=" btnlogin actionReset">Reset Password</div>
                    </div>
                </form>   
                <?php }else{ ?>
                <div class="invalidfields" style="display: block; margin:50px 0px; ">Invalid Password Reset Link.<br> Please go to <a href="<?php echo site_url()."login" ?>">Login Page</a> and try again.</div>
                <?php } ?>
                <div class="footerlogo">
                    <img src="<?php echo $base; ?>images/logo.png" />
                </div>
                <br />
                <br />
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(e) {
    


        $(".actionReset").on('click', function() {
            $("#resetform .invalidfields").hide();
            $("#resetform .validfields").hide();
            var post_data = $("#resetform").serialize();
            $.post(site_url + "user/resetpasswordfinal", post_data, function(res) {
                if (res.status == "1") {
                    window.location.href=site_url+"login";
                } else {
                    $("#resetform .invalidfields").html(res.msg).show();
                }
            }, "JSON");
        });
        $("#cpassword").on('keyup', function(e) {
            if (e.keyCode == 13)
            {
                $("#resetform .actionReset").click();
            }
        });


    });
</script>
<script type="text/javascript">var newWin;
    $(document).ready(function(e) {
        $(".btnfacebook").on('click', function() {            
            var url = site_url + 'facebook/connect';
            newWin = popupwindow(url, "Facebook Login", 800, 500);
            if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
                window.location = url;
            }
        });
        
        $(".btntwitter").on('click', function() {            
            var url = site_url + 'user/twitterlogin';
            newWin = popupwindow(url, "Twitter Login", 800, 500);
            if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
                window.location = url;
            }
        });
    });
    function afterLogin($status, $login_id) {
         newWin.close();
        if($status=="1"){
            window.location.href = site_url + "dashboard";
        }else{
            window.location.href = site_url + "login";
        }       
        
    }</script>
