<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
//print_r($hp_news);
$main_news = $hp_news[0];
$trend_news = getHomePageNews('N', 0, 0, 0, 0, 9, false);
$sp_news = getHomePageNews('N', 1, 0, 0, 0, 9, false);
?>
<section class="homefirstcol">
    <div class="row">
        <div class="col-md-8">


            <div id="myCarousel" class="carousel slide">
                <div class="carousel-inner">
                    <?php foreach ($hp_news as $k => $main_news) { ?>
                        <div class="item <?php echo ($k == "0") ? "active" : ""; ?>">
                            <a href="<?php echo site_url() . NEWS_TAG . "/"; ?><?php echo $main_news['slug'] ?>">
                                <div class="homeslider">
                                    <img src="<?php echo $main_news['image']; ?>" />

                                    <div class="sliderfooter">
                                        <div class="line"></div>

                                        <div>
                                            <div class="hs_heading"><?php echo $main_news['title']; ?></div>
                                            <div class="clear clearfix"></div>
                                            <div class="uname">by <a href="<?php echo site_url() . "newsfeed/" . $main_news['user_slug']; ?>"><?php echo $main_news['posted_by']; ?></a></div>
                                            <div class="uimage"><img src="<?php echo $main_news['user_image']; ?>" /></div>                                            
                                            <div class="ucomments"><i class="fa fa-comments-o"></i> <?php echo $main_news['comments']; ?></div>
                                            <div class="uviews"><i class="fa fa-eye"></i> <?php echo $main_news['views']; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>  
                    <?php } ?>

                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>

            </div>
            <script>
                $(document).ready(function() {
                    $('#myCarousel').carousel({
                        interval: 5000
                    });
                    $('#myCarousel').carousel('cycle');

                });
            </script>




            <div class="clearfix"></div>
            <div class="row slidersmall">
                <?php
                foreach ($hp_news as $key => $row) {
                    if ($key == 0)
                        continue;
                    ?> 
                    <div class="col-md-4 ">
                        <a href="<?php echo site_url() . NEWS_TAG . "/"; ?><?php echo $main_news['slug'] ?>">
                            <div class="homeslidersmall">
                                <img src="<?php echo $row['image']; ?>" />
                                <div class="smalltitle">
                                    <?php echo $row['title']; ?>
                                </div>
                            </div>
                        </a>
                    </div>

                <?php }
                ?>


            </div>




        </div>
        <div class="col-md-4">
            <div class="show_mobile" style="margin-top:15px;"></div>
            <div class="writerboard">
                <div class="boardheader">
                    Top Writer
                    <a href="<?php echo site_url() . "articles/topwriters"; ?>">More</a>
                </div>
                <table class="table postable">
                    <tr>
                        <th>Pos</th>
                        <th>Writers</th>
                        <th>Pts</th>
                    </tr>
                    <?php
                    if ($hp_writers) {
                        foreach ($hp_writers as $pos => $row) {
                            ?> 
                            <tr>
                                <td><?php echo $pos + 1; ?> <span class="symbolup"><i class="fa fa-sort-up"></i></span></td>
                                <td><a href="<?php echo site_url() . "newsfeed/" . $row['slug']; ?>"><?php echo $row['name']; ?> <span class="boarduser"><img src="<?php echo $row['thumb']; ?>" /></span></a></td>
                                <td><?php echo $row['points']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>                  
                </table>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="row strip">
        <div class="col-md-3">
            <div class="blacklabel">
                Trending News
            </div>
        </div>
        <div class="col-md-9 hide_mobile">
            <div class="stripicons">               
                <img src="<?php echo $base; ?>images/iconfire.png" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="row trendnews">
                <div class="col-md-8">
                    <?php
                    // print_r($trend_news);

                    if ($trend_news) {
                        ?>
                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $trend_news[0]['slug']; ?>">
                            <div class="tnewsblock">
                                <img src="<?php echo $trend_news[0]['large']; ?>" />

                                <div class="tnbfooter">
                                    <div class="heading"><?php echo $trend_news[0]['title']; ?></div>
                                    <div class="uname">by <?php echo $trend_news[0]['user']['name']; ?></div>
                                    <div class="uimage"><img src="<?php echo $trend_news[0]['user']['profile_pic']; ?>"></div>
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                </div>
                <div class="col-md-4">
                    <div class="promoted">
                        <ul>
                            <li>
                                <a>More News...</a>

                            </li>
                            <?php
                            if ($trend_news and count($trend_news) > 1) {
                                foreach ($trend_news as $key => $news) {
                                    if ($key == 0)
                                        continue;
                                    ?> <li>
                                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $news['slug']; ?>"><?php echo $news['title']; ?></a>
                                    </li> <?php
                                }
                                ?>

                            <?php } ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">

            <?php
            echo getCMSContent("ad_ggl_square");
            $ads = getAds("square");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" />
                    </a>
                </div>
                <?php
            }
            ?>

        </div>
    </div>
    <div class="row strip">
        <div class="col-md-12">
            <img src="<?php echo $base; ?>images/footyfanz.jpg" style="width: 100%;" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="writerboard">
                <div class="boardheader">
                    Top Fanz
                    <a href="<?php echo site_url() . "articles/topfanz"; ?>">More</a>
                </div>
                <table class="table postable small">
                    <?php
                    if ($hp_fanz) {
                        foreach ($hp_fanz as $pos => $row) {
                            ?> 
                            <tr>
                                <td><?php echo $pos + 1; ?> <span class="symbol"><i class="fa fa-sort-up"></i></span></td>
                                <td><a href="<?php echo site_url() . "newsfeed/" . $row['slug']; ?>"><?php echo $row['name']; ?> <span class="boarduser"><img src="<?php echo $row['thumb']; ?>" /></span></a></td>
                                <td><?php echo $row['points']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>                                     
                </table>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-7">
                    <div class="homepoll">
                        <img src="<?php echo $base; ?>images/pollhomebg.jpg" />
                        <div class="content">
                            <div class="text">
                                Is Mourinho  Right For United?
                            </div>
                            <div class="button">Yes</div>
                            <div class="button">No</div>
                        </div>
                        <div class="sociallinks">
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="insta"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="feed"><i class="fa fa-rss"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="writerboard">                                        
                        <table class="table postable small">
                            <tr>
                                <th style="width: 35px;">#</th>
                                <th>Team</th>
                                <th>Pts</th>
                            </tr>
                            <?php
                            $tot_teams = count($team_points);
                            foreach ($team_points as $key => $row) {
                                $cls = "";
                                if ($key == 0) {
                                    $cls = "yellow";
                                }
                                if ($key >= $tot_teams - 3) {
                                    $cls = "red";
                                }
                                ?>  
                                <tr class="<?php echo $cls; ?>" >
                                    <td><?php echo $key + 1; ?> </td>
                                    <td><a href="<?php echo site_url() . "club/" . $row['slug']; ?>"><?php echo $row['title']; ?></a> </td>
                                    <td><?php echo intval($row['points']); ?></td>
                                </tr> <?php }
                            ?>                           


                        </table>
                    </div>
                </div>
            </div>

            <div class="row strip">
                <div class="col-md-12">
                    <?php
                    echo getCMSContent("ad_ggl_large");
                    $ads = getAds("small");
                    $cnt = count($ads);
                    if ($cnt > 0) {
                        $num = array_rand($ads);
                        ?>

                        <a href="<?php echo $ads[$num]['link'] ?>">
                            <img src="<?php echo $ads[$num]['image']; ?>" style="width:100%;" />
                        </a>

                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
    <div class="row strip">
        <div class="col-md-3">
            <div class="blacklabel">
                Sponsored News
            </div>
        </div>
        <div class="col-md-9 hide_mobile">
            <div class="stripicons">               
                <img src="<?php echo $base; ?>images/iconcoin.png" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="row trendnews">
                <div class="col-md-8">
                    <?php if ($sp_news) { ?>
                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $sp_news[0]['slug']; ?>">
                            <div class="tnewsblock">
                                <img src="<?php echo $sp_news[0]['large']; ?>" />

                                <div class="tnbfooter">
                                    <div class="heading"><?php echo $sp_news[0]['title']; ?></div>
                                    <div class="uname">by <?php echo $sp_news[0]['user']['name']; ?></div>
                                    <div class="uimage"><img src="<?php echo $sp_news[0]['user']['profile_pic']; ?>"></div>
                                </div>
                            </div>
                        </a>
                    <?php } ?>

                </div>
                <div class="col-md-4">
                    <div class="promoted">
                        <ul>
                            <li>
                                <a>More News...</a>
                            </li>
                            <?php
                            if ($sp_news and count($sp_news) > 1) {
                                foreach ($sp_news as $key => $news) {
                                    if ($key == 0)
                                        continue;
                                    ?> <li>
                                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $news['slug']; ?>"><?php echo $news['title']; ?></a>
                                    </li> <?php
                                }
                                ?>

                            <?php } ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php
            echo getCMSContent("ad_ggl_square");
//        $ads = getAds("square");
            $ads = getSiteAds(1, "0", "square");
//            print_r($ads);
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" />
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="row strip">
        <div class="col-md-12">
            <img src="<?php echo $base; ?>images/statattrack.jpg" style="width: 100%;" />                            
        </div>
    </div>



    <div class="row strip">
        <div class="col-md-3">
            <div class="blacklabel">
                Club News
            </div>
        </div>
        <div class="col-md-9 hide_mobile">
            <div class="stripicons">           
                <img src="<?php echo $base; ?>images/iconfootball.png" />
            </div>
        </div>
    </div>

    <?php
    //Bottom Latest Club Wise News Section
    $this->load->view("front/includes/bottom_club_news.php", array());
    //End of Bottom Latest Club Wise News Section
    ?>
    <div class="row strip">
        <div class="col-md-3">
            <div class="blacklabel">
                Gallery
            </div>
        </div>
        <div class="col-md-9 hide_mobile">
            <div class="stripicons">              
                <img src="<?php echo $base; ?>images/icongallery.png" />
            </div>
        </div>
    </div>

    <div class="row strip gallery">
        <div class="col-md-8">
            <div class="row">
                <div class="videodata" style="display: none;">
                    <?php echo json_encode($hp_videos); ?>
                </div>



                <script type="text/javascript">
                    $(document).ready(function(e) {
                        var videodata = $.parseJSON($(".videodata").html());
                        var videodatasize = videodata.length;
                        var currvideokey = 0;
                        $.each(videodata, function(k, v) {
                            if (k == 0) {
                                $("#vd1").attr("src", v['vid_link']);
                                $(".gallerttext").html(v['title']);
                            } else {
                                $("#vd" + (k + 1)).attr("src", v['vid_image']);
                                $("#vd" + (k + 1)).closest("a").data("key", k);
                            }
                        });
                        $(".smallvideo a").click(function() {
                            var curr_key = $(this).data("key");
                            $("#vd1").attr("src", videodata[curr_key]['vid_link']);
                            $(".gallerttext").html(videodata[curr_key]['title']);
                            currvideokey = curr_key;
                            for (x = 0; x < videodatasize; x++) {
                                var newkey = curr_key + x + 1;
                                newkey = newkey % videodatasize;
                                $("#vd" + (x + 2)).attr("src", videodata[newkey]['vid_image']);
                                $("#vd" + (x + 2)).closest("a").data("key", newkey);
                            }
                            return false;
                        });
                        $(".gallerypre").click(function() {
                            var curr_key = currvideokey - 1;
                            $("#vd1").attr("src", videodata[curr_key]['vid_link']);
                            $("#vd1").height($("#vd1").parent().height());
                            $("#vd1").prop("height", $("#vd1").parent().height());
                            $(".gallerttext").html(videodata[curr_key]['title']);
                            currvideokey = curr_key;
                            for (x = 0; x < videodatasize; x++) {
                                var newkey = curr_key + x;
                                newkey = newkey % videodatasize;
                                $("#vd" + (x + 2)).attr("src", videodata[newkey]['vid_image']);
                                $("#vd" + (x + 2)).closest("a").data("key", newkey);
                            }
                            return false;
                        });
                        $(".gallerynext").click(function() {
                            var curr_key = currvideokey + 1;
                            $("#vd1").attr("src", videodata[curr_key]['vid_link']);
                            $(".gallerttext").html(videodata[curr_key]['title']);
                            currvideokey = curr_key;
                            for (x = 0; x < videodatasize; x++) {
                                var newkey = curr_key + x;
                                newkey = newkey % videodatasize;
                                $("#vd" + (x + 2)).attr("src", videodata[newkey]['vid_image']);
                                $("#vd" + (x + 2)).closest("a").data("key", newkey);
                            }
                            return false;
                        });

                    });
                </script>



                <div class="col-md-6">
                    <div class="largevideo">
                        <iframe class="articlevideo" id="vd1" src="" frameborder="0" allowfullscreen style=""></iframe>                       
                    </div>



                    <?php if ($hp_videos == "aaa") { ?>
                        <?php if ($hp_videos[0]['vid'] and $hp_videos[0]['type'] == "V") { ?>
                            <video width="100%" controls>
                                <source src="<?php echo $hp_videos[0]['vid_link']; ?>" type="video/mp4">
                            </video>
                        <?php } ?>
                        <?php if ($hp_videos[0]['vid'] and $hp_videos[0]['type'] == "Y") { ?>
                            <iframe class="articlevideo" src="<?php echo $hp_videos[0]['vid_link']; ?>" frameborder="0" allowfullscreen></iframe>

                        <?php } ?>

                    <?php } ?>

                </div>
                <div class="col-md-3">
                    <div class="show_mobile" style="height: 10px;"></div>
                    <div class="smallvideo">
                        <a href="#">
                            <img id="vd2" src="" style="width: 100%;" />
                            <i class="fa fa-play-circle-o"></i>
                        </a>
                    </div>
                    <div class="show_mobile" style="height: 10px;"></div>
                    <div class="smallvideo">
                        <a href="#">
                            <img id="vd3" src="" style="width: 100%;" />
                            <i class="fa fa-play-circle-o"></i>
                        </a>
                    </div>




                    <?php
                    if ($hp_videos and count($hp_videos) > 1 and 1 == 2) {
                        foreach ($hp_videos as $key => $vid) {
                            if ($key == 0)
                                continue;
                            if ($key == 3)
                                break;
                            ?>             <div class="videoimage">
                            <?php if ($vid['vid'] and $vid['type'] == "V") { ?>

                                    <video width="100%" controls>
                                        <source src="<?php echo $vid['vid_link']; ?>" type="video/mp4">
                                    </video>
                                <?php } ?>
                                <?php if ($vid['vid'] and $vid['type'] == "Y") { ?>

                                    <iframe class="articlevideo" src="<?php echo $vid['vid_link']; ?>" frameborder="0" allowfullscreen></iframe>
                                    </video>
                                <?php } ?>
                            </div> <?php
                        }
                        ?>

                    <?php } ?>

                </div>
                <div class="col-md-3">
                    <div class="show_mobile" style="height: 10px;"></div>
                    <div class="smallvideo">
                        <a href="#">
                            <img id="vd4" src="" style="width: 100%;" />
                            <i class="fa fa-play-circle-o"></i>
                        </a>
                    </div>
                    <div class="show_mobile" style="height: 10px;"></div>
                    <div class="smallvideo">
                        <a href="#">
                            <img id="vd5" src="" style="width: 100%;" />
                            <i class="fa fa-play-circle-o"></i>
                        </a>
                    </div>



                    <?php
                    if ($hp_videos and count($hp_videos) > 3 and 1 == 3) {
                        foreach ($hp_videos as $key => $vid) {
                            if ($key < 3)
                                continue;
                            ?>             <div class="videoimage">
                            <?php if ($vid['vid'] and $vid['type'] == "V") { ?>

                                    <video width="100%" controls>
                                        <source src="<?php echo $vid['vid_link']; ?>" type="video/mp4">
                                    </video>
                                <?php } ?>
                                <?php if ($vid['vid'] and $vid['type'] == "Y") { ?>

                                    <iframe class="articlevideo" src="<?php echo $vid['vid_link']; ?>" frameborder="0" allowfullscreen></iframe>
                                    </video>
                                <?php } ?>
                            </div> <?php
                        }
                        ?>

                    <?php } ?>   
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="gallerynav">
                        <a class="btnpre gallerypre">Previous</a>
                        <a class="btnnext gallerynext">Next</a>
                        <div class="gallerttext">Highlights of Liverpool vs Manchester United </div>

                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php
            echo getCMSContent("ad_ggl_square");
//            $ads = getAds("square");
            $ads = getSiteAds(1, "0", "square");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" />
                    </a>
                </div>
                <?php
            }
            ?>
        </div>       
    </div>



    <div class="row strip">
        <div class="col-md-12">

            <?php
            echo getCMSContent("ad_ggl_large");
//            $ads = getAds("large");
            $ads = getSiteAds(1, "0", "large");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" style="width: 100%;" />
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>











</section>