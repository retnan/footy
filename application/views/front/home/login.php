<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>

<div class="container">


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="loginbox">
                <h2>Welcome to FootyFanz</h2>
                <h3>By Fanz. For Fanz.</h3>
                <form id="loginform">
                    <div class="loglabel">Log In</div>
                    <div class="invalidfields">Invalid Username or Password</div>
                    <div class="loginfield">
                        <input type="text" placeholder="Username or Email" name="email" />
                    </div>
                    <div class="loginfield">
                        <input type="password"  placeholder="Password" name="password" id="password" />
                    </div>
                    <div class="loginfield">
                        <div class=" btnlogin actionLogin">Login</div>
                    </div>
                    <div class="forgotlink" onclick="$('#loginform').hide();
                            $('#resetform').fadeIn();">Forgot Password?</div>
                </form>
                <form id="resetform" style="display: none;">
                    <div class="loglabel">Reset Password</div>
                    <div class="invalidfields">Invalid Username or Email</div>
                    <div class="validfields"></div>
                    <div class="loginfield">
                        <input type="text" placeholder="Email" name="email" id="resetemail" />
                    </div>

                    <div class="loginfield">
                        <div class=" btnlogin actionReset">Reset Password</div>
                    </div>
                    <div class="forgotlink" onclick="$('#resetform').hide();
                            $('#loginform').fadeIn();">Back to Login</div>
                </form>
                <div class="loglabel">Log In Via</div>

                <div class=" socialloginbuttons">
                    <a  class="btnfacebook" style="cursor: pointer">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a class="btntwitter" style="cursor: pointer">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="footerlogo">
                    <img src="<?php echo $base; ?>images/logo.png" />
                </div>
                <br />
                <br />

            </div>
        </div>
    </div>









</div>


<script type="text/javascript">
    $(document).ready(function(e) {
        $(".actionLogin").on('click', function() {
            $("#loginform .invalidfields").hide();
            var post_data = $("#loginform").serialize();
            $.post(site_url + "user/defaultlogin", post_data, function(res) {

                if (res.status == "1") {
<?php if ($nextpage == "index") { ?>
                        window.location.href = site_url;
<?php } else { ?>
                        window.location.href = res.url;
<?php } ?>

                } else {
                    $("#loginform .invalidfields").html(res.msg).show();
                }
            }, "JSON");
        });
        $("#loginform #password").on('keypress', function(e) {
            if (e.keyCode == 13)
            {
                $(".actionLogin").click();
                e.preventDetault();
                return false;
            }
        });


        $(".actionReset").on('click', function() {
            $("#resetform .invalidfields").hide();
            $("#resetform .validfields").hide();
            var post_data = $("#resetform").serialize();
            $.post(site_url + "user/forgotpassword", post_data, function(res) {
                if (res.status == "1") {
                    $("#resetemail").val('');
                    $("#resetform .validfields").html(res.msg).show();
                } else {
                    $("#resetform .invalidfields").html(res.msg).show();
                }
            }, "JSON");
        });
        $("#resetform").on("submit", function(e) {
            e.preventDetault();
            return false;
        });
        $("#resetemail").on('keypress', function(e) {
            if (e.keyCode == 13)
            {
                e.preventDefault();
                $("#resetform .actionReset").click();
                return false;
            }
        });


    });
</script>
<script type="text/javascript">var newWin;
    $(document).ready(function(e) {
        $(".btnfacebook").on('click', function() {
            var url = site_url + 'facebook/connect';
            newWin = popupwindow(url, "Facebook Login", 800, 500);
            if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
                window.location = url;
            }
        });

    });
    function afterLogin($status, $login_id) {
        newWin.close();
        if ($status == "1") {
<?php if ($nextpage == "index") { ?>
                window.location.href = site_url;
<?php } else { ?>
                window.location.href = site_url + "dashboard";
<?php } ?>
        } else {
            window.location.href = site_url + "login";
        }

    }</script>
