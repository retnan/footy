<?php
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
<?php }
?><div class="row">
    <div class="col-md-12">
        <div class="heading_block">
            Privacy Policy
        </div>       
    </div>
    <div class="col-md-12">
        <div style="padding: 5px; margin-top: 20px;">
            <?php echo getCMSContent("privacy"); ?>
        </div>
    </div>
</div>