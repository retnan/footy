<?php
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
<?php }
?> <style>
    .cmscon h1,.cmscon h2,.cmscon h3,.cmscon h4,.cmscon h5,.cmscon h6{
        color: #333;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="heading_block" style="font-size:20px; margin-top: 20px;">
            FAQ'S
        </div>       
    </div>
    <div class="col-md-12">
        <div style="padding: 5px; margin-top: 20px;" class="cmscon">
            <?php echo getCMSContent("faq"); ?>
        </div>
    </div>
</div>