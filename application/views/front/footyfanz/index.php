<?php
$base = site_url() . PUBLIC_DIR . "fassets/images/";
$backall = getBackground("2", "0");
$back6 = getBackground("7", "0");
$bg = "";
if (count($backall) > 0) {
    $bg = $backall[0];
}
if (count($back6) > 0) {
    $bg = $back6[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>");background-repeat: repeat-y;}
    </style>
    <?php
}
?>
<style>
    .ff-container-new .col-md-8{
        width: 60%;
        padding-right: 10px;
    }
    .ff-container-new .col-md-4{
        width: 40%;
        padding-left: 10px;
    }

    @media (max-width: 967px) {
        .ff-container-new .col-md-8{
            width: 100%;
            padding-right: 0px;
        }
        .ff-container-new .col-md-4{
            width: 100%;
            padding-left:0px;
        }
        .ff-single{width:50%;}
        .ff-single2{width:50%;}

    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="ff-head">
            <img src="<?php echo site_url() . PUBLIC_DIR; ?>fassets/images/footyfanzplus.jpg" />
        </div>
    </div>
</div>

<div class="row strip">
    <div class="col-md-12">
        <div class="ff-menu">
            <div class="ff-menu-item btnSearchName">
                <div class="ff-menu-title">Search By Name</div>
            </div>
            <div class="ff-menu-item ff-menu-item-league">
                <div class="ff-menu-title">Search By League</div>
                <div class="ff-leaguges">
                    <ul>
                        <?php foreach ($leagues as $key => $league) { ?>
                            <li>
                                <a data-id="<?php echo $key; ?>" data-clubid="0" class="btn-ff-league"><?php echo $league['title']; ?></a>
                                <div class="secondlevelouter">
                                    <ul class="secondlevel">
                                        <?php foreach ($league['clubs'] as $keyc => $clb) { ?>
                                            <li>
                                                <a data-id="<?php echo $key; ?>" data-clubid="<?php echo $keyc; ?>" class="btn-ff-league"><?php echo $clb; ?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </li>                            
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="ff-menu-item ff-menu-item-league">
                <div class="ff-menu-title">Search By Club</div>
                <div class="ff-leaguges" >
                    <ul style="height: 500px; overflow-y: scroll; ">
                        <?php foreach ($clubs as $key => $league) { ?>
                            <li>
                                <a data-id="0" data-clubid="<?php echo $key; ?>" class="btn-ff-league"><?php echo $league; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(e) {
        $(document).on("click", ".btn-ff-league", function() {
            window.location.href = base_url + "footyfanz/filtered/" + $(this).data("id") + "/" + $(this).data("clubid");
        });
        $(document).on("click", ".btnSearchName", function() {
            if ($(".nameSearch").css("display") == "none") {
                $(".nameSearch").slideDown();
            } else {
                $(".nameSearch").slideUp();
            }

        });
        $(document).on("click", ".btnActionName", function() {
            var txt_search_name = $.trim($("#txt_search_name").val());
            if (txt_search_name != "") {
                window.location.href = base_url + "footyfanz/filtered/0/0/?name=" + encodeURIComponent(txt_search_name);
            }
        });
    });
</script>



<div class="row strip hide">
    <div class="col-md-3 col-sm-6 col-md-offset-6 col-sm-offset-0">

        <div class="searchdd">
            <select name="league" id="league" class="form-control" style="width: 100%; padding: 5px;">
                <option value="">All Leagues</option>
                <?php foreach ($leagues as $key => $league) { ?>
                    <option value="<?php echo $key; ?>"><?php echo $league['title']; ?></option>
                <?php } ?>
            </select>  
        </div>

    </div>
    <div class="col-md-3 col-sm-6">

        <div class="searchdd">
            <select name="club" style="width: 100%; padding: 5px;" id="club" class="form-control">
                <option value="">Select Club</option>

            </select>  
        </div>

    </div>
</div>


<div class="row nameSearch" style="margin-bottom: 15px; display: none; ">

    <div class="col-md-12">
        <div class="searchbox">
            <div class="searchbtn btnActionName">
                <div class="btnblack searchBtn ">
                    Search
                </div>
            </div>


            <div class="searchtext">
                <input type="text" class="txt_searchbox" id="txt_search_name" value="" placeholder="Search...">
            </div>
        </div>
    </div>
</div>


<div class="row" style="margin-bottom: 15px; display:none;">

    <div class="col-md-12">
        <div class="searchbox">
            <div class="searchbtn">
                <div class="btnblack searchBtn searchBtnMain">
                    Search
                </div>
            </div>


            <div class="searchtext">
                <input type="text" class="txt_searchbox" id="txt_search" value="" placeholder="Search...">
            </div>
        </div>
    </div>
</div>
<div class="row ff-result ff-container-new">

</div>

<div class="commentloader loading">
    <div class="ff-more loadmore">More FootyFanz Plus</div>
    <div class="loaderview"><img src="<?php echo site_url() . PUBLIC_DIR; ?>images/loadingBar.gif" /></div>
</div>
<?php
$ads = getAds("large");
//          $ads = getSiteAds(4,"0","large");
$cnt = count($ads);
if ($cnt > 0) {
    $num = array_rand($ads);
    ?>
    <div class="" style="margin-bottom: 25px;margin-top: 20px;">
        <div class="leftadd">
            <a href="<?php echo $ads[$num]['link'] ?>">
                <img src="<?php echo $ads[$num]['image']; ?>" style="width: 100%;" />
            </a>
        </div>
    </div>

    <?php
}
?>

<script type="text/javascript">
    var page = 1;
    function getMembers(type) {
        var post_data = {page: page, league: $("#league").val(), club: $("#club").val(), search_key: $("#txt_search").val()};
        $('.commentloader').addClass('loading');
        $.post(base_url + "footyfanz/getlist", post_data, function(res) {
//            alert(res);
            $('.commentloader').removeClass('loading');
            if (type == "new") {
                $(".ff-result").html("");
            }
            $(".ff-result").append(res);
        });
    }
    $(document).on('click', ".loadmore", function(e) {
        page++;
        getMembers("append");
    });
    $(document).ready(function(e) {

        $("#league").on("change", function() {
            page = 1;
            getMembers("new");
            var post_data = {league_id: $("#league").val()};
            $.post(base_url + "footyfanz/getclubs", post_data, function(res) {
                $("#club").html(res);
                $("#club").closest("div").hide().fadeIn();

            });
        });
        $("#club").on("change", function() {
            page = 1;
            getMembers("new");
        });
        $(".searchBtnMain").on("click", function() {
            page = 1;
            getMembers("new");
        });
        $("#league").change();
    });
    $(document).on("click", ".ff_follow", function() {
        var follow_user_id = $(this).data("id");
        var fname = $(this).closest(".ff-image").find(".ff-name").html();
        $.confirm({
            title: 'Follow ' + fname,
            content: 'Are you sure to follow ' + fname,
            confirmButton: 'Yes',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(base_url + "footyfanz/follow", {follow_user_id: follow_user_id}, function(res) {
                    var jres = $.parseJSON(res);
                    if (jres.status == "1") {
                        $(".searchBtnMain").click();
                    } else {
                        alert("Please login to follow " + $.trim(fname));
                    }

                });
            }
        });
    });
</script>
