<div class="col-md-12">
    <div class="ff-con">
        <?php if ($page == 1 and count($data) == 0) {
            ?> 
            <h2 style="text-align: center;margin-top: 50px; margin-bottom: 100px;">No FootyFanz Members Available</h2>
        <?php }
        ?>

        <?php
        foreach ($data as $key => $row) {
            // print_r($row);
            ?>
            <div class="ff-single-filtered">
                <div class="ff-in">
                    <a href="<?php echo site_url() . ARTICLE_TAG . "/" . $row['slug']; ?>">
                        <div class="ff-image">
                            <img src="<?php echo $row['thumb']; ?>" />
                            <div class="ff-name">
                                <div class="pname"><?php echo $row['title']; ?></div>
                                <div class="pclub"><?php echo $row['posted_by']; ?></div>

                            </div>
                        </div>
                    </a>

                </div>
            </div>

            <?php if (($key + 1) % 6 == 0 && $key > 4) { ?>
                <?php
                $ads = getAds("large");
                $cnt = count($ads);
                if ($cnt > 0) {
                    $num = array_rand($ads);
                    ?>
                    <div class="clear clearfix"></div>
                    <div class="col-md-12" style="margin-bottom:5px;margin-top: 15px;">
                        <div class="leftadd">
                            <a href="<?php echo $ads[$num]['link'] ?>">
                                <img src="<?php echo $ads[$num]['image']; ?>" style="width: 100%;" />
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            <?php } ?>

        <?php } ?>
    </div>
</div>

<?php if (!$load) {
    ?>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $(".commentloader").hide();

        });

    </script>

    <?php
}
?>

