<?php
$base = site_url() . PUBLIC_DIR . "fassets/images/";
?>
<div class="row">
    <div class="col-md-12">
        <div class="ff-head">
            <img src="<?php echo site_url() . PUBLIC_DIR; ?>fassets/images/footyfanzplus.jpg" />
        </div>
    </div>
</div>

<div class="row strip">
    <div class="col-md-3 col-sm-6 col-md-offset-6 col-sm-offset-0">

        <div class="searchdd">
            <select name="league" id="league" class="form-control" style="width: 100%; padding: 5px;">
                <option value="">Select League</option>
                <?php foreach ($leagues as $key => $league) { ?>
                    <option value="<?php echo $key; ?>"><?php echo $league; ?></option>
                <?php } ?>
            </select>  
        </div>

    </div>
    <div class="col-md-3 col-sm-6">

        <div class="searchdd">
            <select name="club" style="width: 100%; padding: 5px;" id="club" class="form-control">
                <option value="">Select Club</option>

            </select>  
        </div>

    </div>
</div>

<div class="row" style="margin-bottom: 15px;">

    <div class="col-md-12">
        <div class="searchbox">
            <div class="searchbtn">
                <div class="btnblack searchBtn">
                    Search
                </div>
            </div>


            <div class="searchtext">
                <input type="text" class="txt_searchbox" id="txt_search" value="" placeholder="Search...">
            </div>
        </div>
    </div>
</div>

<div class="row ff-result">
    <div class="col-md-8">
        <div class="ff-con">
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>
    <div class="col-md-4">
        <div class="leftadd">
            <a href="#">
                <img src="<?php echo $base; ?>logo_146821278651.jpg">
            </a>
        </div>
        <div class="ff-con2">
            <div class="ff-single2">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>
                </div>
            </div>
            <div class="ff-single2">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>
                </div>
            </div>
            <div class="ff-single2">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>
                </div>
            </div>
            <div class="ff-single2">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="row ff-result">
    <div class="col-md-8">
        <div class="ff-con">
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>
            <div class="ff-single">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>
    <div class="col-md-4">
        <div class="leftadd">
            <a href="#">
                <img src="<?php echo $base; ?>logo_146821278651.jpg">
            </a>
        </div>
        <div class="ff-con2">
            <div class="ff-single2">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>
                </div>
            </div>
            <div class="ff-single2">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>
                </div>
            </div>
            <div class="ff-single2">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>
                </div>
            </div>
            <div class="ff-single2">
                <div class="ff-in">
                    <div class="ff-image">
                        <img src="<?php echo $base; ?>laughing.jpg" />
                        <div class="ff-name">
                            Apho
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="commentloader">
    <div class="ff-more loadmore">More FootyFanz Plus</div>
    <div class="loaderview"><img src="<?php echo $base; ?>images/loadingBar.gif" /></div>
</div>

<div class="" style="margin-bottom: 15px;">
    <img src="<?php echo site_url() . PUBLIC_DIR; ?>fassets/images/lgadd.jpg" style="width: 100%;" />
</div>
<script type="text/javascript">
        var page = 1;
    function getMembers() {
        var post_data = {page: page, league: $("#league").val(),club:$("#club").val(),search_key:$("#txt_search").val()};
        $('.commentloader').addClass('loading');
        $.post(base_url + "footyfanz/getlist", post_data, function(res) {
//            alert(res);
            $('.commentloader').removeClass('loading');
            $(".ff-result").html(res);
        });
    }
     $(document).on('click', ".loadmore", function(e) {
        page++;
        getMembers();
    });
    $(document).ready(function(e) {
        
        $("#league").on("change",function(){
            page=1;
            getMembers();
       var post_data={league_id:$("#league").val()};       
       $.post(base_url+"footyfanz/getclubs",post_data,function(res){
           $("#club").html(res);
           $("#club").closest("div").hide().fadeIn();
          
       });
       });
       $("#club").on("change",function(){
            page=1;
            getMembers();
       });
       $(".searchBtn").on("click",function(){
            page=1;
            getMembers();
       });
   });
</script>
