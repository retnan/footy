<div style="margin-top: 10px; clear: both;"></div>
<div class="clear clearfix">
    <div class="col-md-8">
        <div class="ff-con">
            <?php if ($page == 1 and count($data) == 0) {
                ?> 
                <h2 style="text-align: center;margin-top: 50px;">No FootyFanz Members Available</h2>
            <?php }
            ?>

            <?php
            for ($i = 0; $i < 6; $i++) {
                if (!isset($data[$i])) {
                    break;
                }
                $row = $data[$i];
                ?>
                <div class="ff-single">
                    <div class="ff-in">
                        <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>">
                            <div class="ff-image">
                                <img src="<?php echo $row['profile_pic']; ?>" />
                                <?php if ($row['follow'] == FALSE) { ?>
                                    <a class="ff_follow" data-id="<?php echo $row['pk_user_id'] ?>">
                                        <img src="<?php echo site_url("public/fassets/images/add-icon.png") ?>" />
                                    </a>
                                    <?php
                                }

                                if ($row['ff_verify'] == "1") {
                                    ?>
                                    <div class="ff_followed" >
                                        <img src="<?php echo site_url("public/fassets/images/tick.png") ?>" />
                                    </div>
                                <?php } ?>
                                <?php if ($row['club_image'] != "") { ?>

                                    <a class="ff_club" href="<?php echo site_url() . "club/" . $row['club_slug'] ?>">
                                        <img src="<?php echo $row['club_image'] ?>" />
                                    </a>
                                <?php } ?>
                                <div class="ff-name">
                                    <?php echo $row['name']; ?>
                                </div>
                            </div>
                        </a>

                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="col-md-4">
        <?php
        $ads = getAds("square");
//             $ads = getSiteAds(4,"0","large");
        $cnt = count($ads);
        if ($cnt > 0) {
            $num = array_rand($ads);
            ?>
            <div class="clear clearfix" style="height: 0px;"></div>
            <div class="leftadd" style="margin-bottom: 10px;">
                <a href="<?php echo $ads[$num]['link'] ?>">
                    <img src="<?php echo $ads[$num]['image']; ?>" style="width: 100%;" />
                </a>
            </div>
            <div class="clear clearfix"  style="height: 0px;"></div>
            <?php
        }
        ?>

        <div class="ff-con2">
            <?php if (count($data) > 9 and 1 == 2) { ?>

                <?php
                for ($i = 9; $i < 11; $i++) {
                    $row = $data[$i];
                    ?>
                    <div class="ff-single2">
                        <div class="ff-in">
                            <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>">
                                <div class="ff-image">
                                    <img src="<?php echo $row['profile_pic']; ?>" />
                                    <?php if ($row['follow'] == FALSE) { ?>
                                        <a class="ff_follow" data-id="<?php echo $row['pk_user_id'] ?>">
                                            <img src="<?php echo site_url("public/fassets/images/add-icon.png") ?>" />
                                        </a>
                                        <?php
                                    }
                                    if ($row['ff_verify'] == "1") {
                                        ?>
                                        <div class="ff_followed" >
                                            <img src="<?php echo site_url("public/fassets/images/tick.png") ?>" />
                                        </div>
                                    <?php } ?>
                                    <?php if ($row['club_image'] != "") { ?>
                                        <a class="ff_club" href="<?php echo site_url() . "club/" . $row['club_slug'] ?>">
                                            <img src="<?php echo $row['club_image'] ?>" />
                                        </a>
                                    <?php } ?>
                                    <div class="ff-name">
                                        <?php echo $row['name']; ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>        
            <?php } ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="ff-con" style="margin-top: 0px;">
            <?php if (count($data) > 6) { ?>

                <?php
                for ($i = 6; $i < count($data); $i++) {
                    $row = $data[$i];
                    ?>
                    <div class="ff-single2">
                        <div class="ff-in">
                            <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>">
                                <div class="ff-image">
                                    <img src="<?php echo $row['profile_pic']; ?>" />
                                    <?php if ($row['follow'] == FALSE) { ?>
                                        <a class="ff_follow" data-id="<?php echo $row['pk_user_id'] ?>">
                                            <img src="<?php echo site_url("public/fassets/images/add-icon.png") ?>" />
                                        </a>
                                        <?php
                                    }
                                    if ($row['ff_verify'] == "1") {
                                        ?>
                                        <div class="ff_followed" >
                                            <img src="<?php echo site_url("public/fassets/images/tick.png") ?>" />
                                        </div>
                                    <?php } ?>
                                    <?php if ($row['club_image'] != "") { ?>
                                        <a class="ff_club" href="<?php echo site_url() . "club/" . $row['club_slug'] ?>">
                                            <img src="<?php echo $row['club_image'] ?>" />
                                        </a>
                                    <?php } ?>
                                    <div class="ff-name">
                                        <?php echo $row['name']; ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
<?php if (!$load) {
    ?>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $(".commentloader").hide();

        });

    </script>

    <?php
}
?>
<script type="text/javascript">

</script>
