<?php
$base = site_url() . "public/uassets/";
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?>
<style>
    .start_button{background: #076F85; color: #fff; padding-left: 40px; padding-right: 40px; }
    .start_button:hover{background: #117191; color: #fff;}
</style>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page_heading">My Account</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="left_sidebar">
                <ul>
                    <li class="active">
                        <a href="<?php echo site_url() . "user" ?>"><i class="fa fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url() . "user/password" ?>"><i class="fa fa-key"></i> Passsword</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url() . "user/setting" ?>"><i class="fa fa-cog"></i> Setting</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="main_content_box">
                <h3 class="sub_heading">Account Profile</h3>
                <p class="sub_text">Set your profile to look better.</p>
                <div class="line_single"></div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="" style="position: relative;">
                            <img src="<?php echo $data['image']; ?>" class="img-polaroid img-thumbnail pro_logo" width="100%" onerror="$(this).attr('src','<?php echo site_url() . "public/assets/image/male_img.jpg" ?>');" />
                            <div class="edit_profile_image btn_upload_logo">
                                <i class="fa fa-pencil"></i>
                            </div>
                        </div>

                        <div class="sub_title logo_progress" style="display: none;">
                            <div class="sp_progress" style="">
                                <div class="sp_progress_val">
                                    30%
                                </div>
                                <div class="sp_prgress_in">
                                    <div class="sp_progress_bar" style="width: 30%;"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-8">
                        <form class="form-vertical" id="profile_form">
                            <?php if ($this->session->flashdata('update_success') == 'yes') { ?>
                                <div class="alert alert-success update_success" style="padding: 8px 10px;">
                                    Profile updated.
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('update_success') == 'verified') { ?>
                                <div class="alert alert-success update_success" style="padding: 8px 10px;">
                                    Your account activated successfully. Please update your profile.<br>                                    
                                </div>
                            <?php } ?>
                            <div class="alert alert-danger profile_alert" style="display: none;padding: 8px 10px;">
                            </div>                            
                            <div class="form-group">
                                <label for="pwd">Name:</label>
                                <input type="text" class="form-control" placeholder="Name" id="profile_name"
                                       value="<?php echo $data['name']; ?>" name="profile_name">
                            </div>                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn_pro_update  pull-right">Update</button>
                            </div>
                        </form>
                        <div class="clearfix" style="margin-bottom: 30px;"></div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<form id="logoform" method="post" enctype="multipart/form-data" action="<?php echo site_url() . "user/updatelogo"; ?>">
    <input type="file" class="hide" name="logo" id="logo" accept="image/*" onchange="$(this.form).submit();"/>   
</form>
<script type="text/javascript">
    $(document).ready(function(e) {
        $(".btn_pro_update").on('click', function(e) {
            e.preventDefault();
            if ($("#profile_name").val() != "") {
                var post_data = $("#profile_form").serialize();
                $.post(site_url + "user/profileupdate", post_data, function(res) {
                    var jdata = $.parseJSON(res);
                    if (jdata.status == "1") {
                        window.location.reload();
                    } else {
                        $(".profile_alert").html(jdata.msg).fadeIn();
                    }
                });
            } else {
                $(".profile_alert").html("Please fill profile details.").fadeIn();
            }
        });
        setTimeout(function() {
            $(".update_success").slideUp();
        }, 3000);
        $(".disconnect_instagram").on('click', function(e) {
            e.preventDefault();
            $.post(site_url + "user/dis_instagram", {}, function(res) {
                $(".disconnect_instagram").parent().html(res);
            });
        });
        $(".disconnect_twitter").on('click', function(e) {
            e.preventDefault();
            $.post(site_url + "user/dis_twitter", {}, function(res) {
                $(".disconnect_twitter").parent().html(res);
            });
        });


        $('#logoform').ajaxForm({
            //dataType: 'json',
            beforeSubmit: function() {
                $(".sf-msg-error").html("").hide();
                $(".logo_progress").show();
                $(".logo_progress .sp_progress_val").html("0%");
                $(".logo_progress .sp_progress_bar").css("width", "0%");
            },
            uploadProgress: function(event, position, total, percentComplete) {
                if (percentComplete == 100) {
                } else {
                    $(".logo_progress .sp_progress_bar").css("width", percentComplete + '%');
                    $(".logo_progress .sp_progress_val").html(percentComplete + '%');
                }
            },
            success: function(json) {
                $(".logo_progress").hide();
                $jdata = $.parseJSON(json);
                if ($jdata.status == "1") {
                    $(".pro_logo").attr("src", $jdata.img);
                } else {
                    $(".sf-msg-error").html($jdata.msg).show();
                }
                //Your Custom JS Code Here
            }
        });
        $(".btn_upload_logo").click(function() {
            $('#logo').click();
        });


    });
</script>
