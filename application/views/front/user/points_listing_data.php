<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>

<div class="data">

    <div class="row" style="margin-bottom: 15px; display:none;">
        <div class="col-md-12">
            <div class="searchbox">
                <div class="searchbtn">
                    <div class="btnblack searchBtn">
                        Search
                    </div>
                </div>
                <div class="searchtext">
                    <input type="text" class="txt_searchbox" value="<?php echo $search; ?>" placeholder="Search..." >
                </div>
            </div>
        </div>
    </div>

    <div class="pointlist">


        <?php
        if (count($data) > 0) {
            ?>
            <table class="table table-full-width ">
                <tr>
                    <th>Date</th>
                    <th>Points</th>
                    <th>Remark</th>
                    <th>Activity By</th>
                </tr>

                <?php
                foreach ($data as $row) {
                    ?>

                    <tr class="writer<?php echo $row["writer"]; ?>">
                        <td><?php echo date("d-M-Y", strtotime($row["created_at"])); ?></td>
                        <td><?php echo $row["points"]; ?></td>
                        <td><?php echo $row["type"]; ?></td>
                        <td>
                            <?php if ($row["user_name"] != "") { ?>
                                <a href="<?php echo base_url() . "articles/listing/" . $row["userslug"]; ?>"> <?php echo $row["user_name"]; ?></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
        } else {
            ?>
            <h3 style="width: 100%; text-align: center;">No Point Available</h3>
            <?php
        }
        ?>


    </div>
    <?php echo $page; ?>



</div>