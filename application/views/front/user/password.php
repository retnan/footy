<?php
$base = site_url() . "public/uassets/";
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
<?php }
?>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page_heading">Account Settings</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="left_sidebar">
                <ul>
                    <li>
                        <a href="<?php echo site_url() . "user/index/" ?>"><i class="fa fa-user"></i> Profile</a>
                    </li>
                    <li class="active">
                        <a href="<?php echo site_url() . "user/password" ?>"><i class="fa fa-key"></i> Passsword</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url() . "user/setting" ?>"><i class="fa fa-cog"></i> Setting</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="main_content_box">
                <h3 class="sub_heading">Account Password</h3>
                <p class="sub_text">Set your password for your accounts.</p>
                <div class="line_single"></div>
                <div class="row">
                    <div class="col-md-2">
                        &nbsp;
                    </div>
                    <div class="col-md-8">
                        <form class="form-vertical" id="profile_form">
                            <?php if ($this->session->flashdata('update_success') == 'yes') { ?>
                                <div class="alert alert-success update_success" style="padding: 8px 10px;">
                                    Password updated.
                                </div>
                            <?php } ?>

                            <div class="alert alert-danger profile_alert" style="display: none;padding: 8px 10px;">
                            </div>
                            <div class="form-group">
                                <label for="pwd">New Password:</label>
                                <input type="password" class="form-control" placeholder="New Password" id="new_password"
                                       value="" name="new_password">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Confirm Password:</label>
                                <input type="password" class="form-control" placeholder="Confirm Password" id="confirm_password"
                                       value="" name="confirm_password">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn_password_update  pull-right">Update</button>
                            </div>
                        </form>
                        <div class="clearfix" style="margin-bottom: 35px;"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(e) {
        $(".btn_password_update").on('click', function(e) {
            e.preventDefault();
            $(".profile_alert").hide();
            if ($("#new_password").val() != "" && $("#confirm_password").val() != "") {
                if ($("#new_password").val() != $("#confirm_password").val()) {
                    $(".profile_alert").html("Confirm password did not match.").fadeIn();
                    return;
                }
                var post_data = $("#profile_form").serialize();
                $.post(site_url + "user/passwordupdate", post_data, function(res) {
                    var jdata = $.parseJSON(res);
                    if (jdata.status == "1") {
                        window.location.reload();
                    } else {
                        $(".profile_alert").html(jdata.msg).fadeIn();
                    }
                });
            } else {
                $(".profile_alert").html("Please enter password.").fadeIn();
            }
        });
        setTimeout(function() {
            $(".update_success").slideUp();
        }, 3000);
    });
</script>
