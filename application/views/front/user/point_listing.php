<?php
$base = base_url() . PUBLIC_DIR . "fassets/";

$backall = getBackground("2", "0");
$back6 = getBackground("4", "0");

$bg = "";
if (count($backall) > 0) {
    $bg = $backall[0];
}
if (count($back6) > 0) {
    $bg = $back6[0];
}

if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>"); background-repeat: repeat-y;}
    </style>
    <?php
}
?>
<style>
    .writer1 td{background: #efffe9;}
</style>
<section class="articlecontent">
    <div class="row">
        <div class="col-md-12">
            <?php
            if (isset($ud) and $ud) {
                $this->load->view("front/profile/profileheader.php", array("ud" => $ud, "follow_status" => $follow_status));
            }
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">




            <div class="row strip">
                <div class="col-md-12">
                    <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                        Points
                    </div>
                </div>
            </div>
            <div id="listing_data"></div>
            <?php
            paginationScriptFront(base_url() . "user/point_listing_data", "listing_data", "user_id=" . $user_id);
            ?>



        </div>

        <div class="col-md-4">
            <?php
            $this->load->view("front/includes/right_panel_news.php", array());
            ?>
        </div>

    </div>


    <div class="row hide_mobile">
        <div class="col-md-12">
            <?php
            $gc = trim(getCMSContent("ad_ggl_large"));
//            $ads = getAds("large");

            $ads = getSiteAds(4, "0", "large");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>">
                        <img src="<?php echo $ads[$num]['image']; ?>" style="width: 100%;" />
                    </a>
                </div>
                <?php
            } else {
                echo $gc;
            }
            ?>
        </div>
    </div>
    <div class="row">

        <?php
        $this->load->view("front/includes/bottom_panel_news.php", array());
        ?>

    </div>


</section>
<script type="text/javascript">

    $(document).on('click', '.delete_post', function() {
        var url = $(this).data("url");
        $.confirm({
            title: 'Delete Article',
            content: 'Are you sure to delete your Article',
            confirmButton: 'Yes',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(url, {}, function(res) {
                    $(".searchBtn").click();
                    alert("Article has been deleted");

                });

            }
        });

    });
</script>
