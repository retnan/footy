<?php

class viewer {

    private $ci;

    function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->model("notification_model");
    }

    public function fview($view, $data) {
        $this->ci->load->model("master_model");
        $categories=$this->ci->master_model->getCategories();
        if($this->ci->session->userdata("user_id")!=''){
         $data['noti_count']=$this->ci->notification_model->countNewNotifications($this->ci->session->userdata("user_id"));
              $data['dl_noti']=$this->ci->notification_model->getNotificationDrugs($this->ci->session->userdata("user_id"),'DL');
              $data['cp_noti']=$this->ci->notification_model->getNotificationDrugs($this->ci->session->userdata("user_id"),'CP');
              $data['act_noti']=$this->ci->notification_model->getNotificationActivity($this->ci->session->userdata("user_id"));
        }
        $data['content']= $this->ci->load->view("front/" . $view, $data,TRUE);
        $data['categories']=$categories;
        if(!isset($data['menu'])){
            $data['menu']='0-0';
        }
        $this->ci->load->view('front/includes/layout.php',$data);
    }
    public function cview($view, $data=array(),$layout=true) {
        if($layout==FALSE){
            $this->ci->load->view("company/" . $view, $data);
        }else{
            $this->ci->load->model("drug_model");
             $data['noti_count']=$this->ci->drug_model->countNewNotifications($this->ci->session->userdata("company_id"));
             $data['drugs']=$this->ci->drug_model->getNotificationDrugs($this->ci->session->userdata("company_id"));
             $data['content']= $this->ci->load->view("company/" . $view, $data,TRUE);
            if(!isset($data['menu'])){
                $data['menu']='0-0';
            }  
            $this->ci->load->view('company/includes/layout.php',$data);  
        }
    }
    public function aview($view, $data=array(),$layout=true) {
        
        if($layout==FALSE){
            $this->ci->load->view("admin/" . $view, $data);
        }else{
            $this->ci->load->model("drug_model");
            $data['drug_request']=$this->ci->drug_model->getDrugRequest();
            $data['content']= $this->ci->load->view("admin/" . $view, $data,TRUE);
            if(!isset($data['menu'])){
                $data['menu']='0-0';
            }  
            $this->ci->load->view('admin/includes/layout.php',$data);  
        }
    }
    public function uview($view, $data,$layout=true) {
        if($layout==FALSE){
            $this->ci->load->view("user/" . $view, $data);
        }else{
              $data['noti_count']=$this->ci->notification_model->countNewNotifications($this->ci->session->userdata("user_id"));
              $data['dl_noti']=$this->ci->notification_model->getNotificationDrugs($this->ci->session->userdata("user_id"),'DL');
              $data['cp_noti']=$this->ci->notification_model->getNotificationDrugs($this->ci->session->userdata("user_id"),'CP');
              $data['act_noti']=$this->ci->notification_model->getNotificationActivity($this->ci->session->userdata("user_id"));
   
            $data['content']= $this->ci->load->view("user/" . $view, $data,TRUE);
            if(!isset($data['menu'])){
                $data['menu']='0-0';
            }
            $this->ci->load->view('user/includes/layout.php',$data);          
        }        
    }

    public function apiview($view, $data) {
        $this->ci->load->view('api/' . $view, $data);
    }

    public function emailview($view, $data=array()) {            
        $data['content']= $this->ci->load->view("email/" . $view, $data,TRUE);
        return $this->ci->load->view('email/layout.php',$data,TRUE);
    }
}

?>
