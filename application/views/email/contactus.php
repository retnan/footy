<h3><?php echo ($data['type'] == "F") ? 'Feedback' : 'Contact' ?> Message from <?php echo $data['name'] ?></h3>
<p>
    <strong>Name:</strong> <?php echo $data['name'] ?><br>
    <strong>Email:</strong> <?php echo $data['email'] ?><br>
    <strong>Contact At:</strong> <?php echo date('M d, Y h:i A', strtotime(getTimestamp())) ?><br>
    <strong>Phone:</strong> <?php echo $data['phone'] ?><br>
    <strong>Subject:</strong> <?php echo $data['subject'] ?><br>
    <strong>Message:</strong> <?php echo $data['message'] ?><br>
</p>