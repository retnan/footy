<?php
$ads = $data;
//print_r($location);
$curr_sub_type = array_keys($location);

$curr_leagues = array();
if (isset($location[6])) {
    $curr_leagues = $location[6];
}
$curr_clubs = "";
if (isset($location[5])) {
    $curr_clubs = implode(',', $location[5]);
}
$typo = array("head" => "Mashhead", "poll" => "Poll", "bg" => "Background");
$backpath = array("head" => "admin/images/index/", "poll" => "admin/poll/", "bg" => "admin/images/index/");
?>
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage <?php echo $typo[$type]; ?> Placement
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Manage <?php echo $typo[$type]; ?> Placement
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Manage <?php echo $typo[$type]; ?> Placement</div>
                <div class="tools">
                    <a href="<?php echo base_url() . $backpath[$type] . $type; ?>" class="btn mini btn-warning yellow" style="margin-top: -4px;">
                        <i class="icon-backward"></i> Back to List</a>
                </div>

            </div>
            <div class="portlet-body">
                <div class="row-fluid  portfolio-block">
                    <div class="span3">
                        <?php
                        $image = base_url() . NO_IMAGE;
                        if ($type == "poll") {
                            if (is_file(POLL_DIR . $ads['image'])) {
                                $image = base_url() . POLL_DIR . $ads['image'];
                            }
                        } else {
                            if (is_file(MEDIA_DIR . $ads['path'])) {
                                $image = base_url() . MEDIA_DIR . $ads['path'];
                            }
                        }
                        ?>
                        <img src="<?php echo $image ?>" class="img-polaroid" alt="" style="width: 100%" />
                    </div>
                    <div class="span9 portfolio-text" style="overflow: visible">
                        <div class="row-fluid">
                            <div class="span10">
                                <div class="span12" style="float: left;">
                                    <?php if ($type == "poll") { ?>
                                        <h3 style="margin:0px 0; width:auto; float: left;"><?php echo $data['title'] ?></h3>

                                    <?php } else { ?>
                                        <h3 style="margin:0px 0; width:auto; float: left;"><?php echo $typo[$type]; ?> <?php echo $index; ?></h3>
                                    <?php } ?>
                                    <?php if ($type == "poll") { ?>
                                        <h3 style="margin:0px 0; width:auto; float: left;">From <?php echo date("d/m/Y", strtotime($ads['start_date'])); ?> to <?php echo date("d/m/Y", strtotime($ads['end_date'])); ?></h3>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>                        
                    </div>
                </div>
                <form action="#" class="form-horizontal" id="form_ads" name="form_ads" style="margin-bottom: 0px;">
                    <div class="row-fluid">
                        <div class="control-group">
                            <label class="control-label">Choose Place of <?php echo $typo[$type]; ?></label>
                            <div class="controls"> 
                                <?php
                                foreach ($sub_types as $key => $value) {
                                    ?> 
                                    <label class="checkbox">
                                        <input type="checkbox" name="sub_type[]" class="sub_type" style="display: none" id="chk<?php echo $key; ?>" <?php if (in_array($key, $curr_sub_type)) echo 'checked'; ?> data-rule-required="true"  value="<?php echo $key; ?>" /> <?php echo $value; ?>
                                    </label>           

                                <?php }
                                ?>
                            </div>
                        </div>
                        <div class="row-fluid club_league_block" style="display: none;">
                            <div class="control-group">
                                <h4> Club Page Ads</h4>
                                <label class="control-label">Choose League</label>
                                <div class="controls"> 
                                    <select class="m-wrap" name="club_league" id="club_league" data-clubs="<?php echo $curr_clubs; ?>">
                                        <option value="0">All Leagues</option>
                                        <?php
                                        if ($leagues) {
                                            foreach ($leagues as $key => $value) {
                                                ?> 
                                                <option value="<?php echo $key; ?>" <?php if ($current_club_league == $key) echo 'selected'; ?>><?php echo $value; ?></option>        

                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>   
                                <div class="controls" style="margin-top: 5px;">
                                    <span class="label label-info">Note:</span> <span class="text-info">if you choose All Leagues, <?php echo $typo[$type]; ?> will display at all Clubs of All Leagues.</span>
                                </div>

                            </div>
                            <div class="control-group club_section margin-top-10" style="display: none;">
                                <label class="control-label">Choose Club(s)</label>
                                <div class="controls club_block">

                                </div>
                                <div class="controls" style="margin-top: 5px;">
                                    <span class="label label-info">Note:</span> <span class="text-info">if you does not choose any club, <?php echo $typo[$type]; ?> will display at all Clubs of selected League.</span>
                                </div>

                            </div>
                        </div>
                        <div class="control-group league_block" style="display: none; margin-top: 10px; ">
                            <h4> League Page <?php echo $typo[$type]; ?> </h4>
                            <label class="control-label">Choose League</label>
                            <div class="controls"> 
                                <?php
                                foreach ($leagues as $key => $value) {
                                    ?> 
                                    <label class="checkbox">
                                        <input type="checkbox" name="leagues[]" style="display: none" id="chk<?php echo $key; ?>" <?php if (in_array($key, $curr_leagues)) echo 'checked'; ?>  value="<?php echo $key; ?>" /> <?php echo $value; ?>
                                    </label>           

                                <?php }
                                ?>

                            </div>


                        </div>

                    </div>
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <input type="hidden" name="type" value="<?php echo $type; ?>">
                    <div class="control-group" style="margin-top: 10px; ">

                        <label class="control-label"></label>
                        <div class="controls"> 
                            <div class="btn btn-primary green save_location">Save Location</div>

                        </div>


                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<script type="text/javascript">
    function update_location() {
        App.blockUI($("#form_ads").parent());

        var formData = new FormData($("#form_ads")[0]);
        $.ajax({
            url: base_url + "admin/placement/update_location",
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#form_ads").parent());
                $.gritter.add({
                    title: "<?php echo $typo[$type]; ?>",
                    text: "<?php echo $typo[$type]; ?> Location Saved"
                });


            },
            error: function(data) {
                App.unblockUI($("#form_ads").parent());
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }


    $(document).ready(function(e) {
        $(".sub_type").change(function() {
            if ($("#chk5").is(':checked')) {
                $(".club_league_block").show();
            }
            else {
                $(".club_league_block").hide();
            }
            if ($("#chk6").is(':checked')) {
                $(".league_block").show();
            }
            else {
                $(".league_block").hide();
            }

        });
        $("#club_league").on('change', function() {
            if ($(this).val() == 0) {
                $(".club_section").hide();
                $(".club_block").html("");
                return;
            }
            var post_data = {league_id: $(this).val(), curr_clubs: $(this).data("clubs")};
            $.post(base_url + "admin/ads/get_clubs", post_data, function(res) {
                $(".club_section").fadeIn();
                $(".club_block").html(res);
                App.initUniform();
            });
        });

        $(".sub_type").change();


        $("#club_league").change();
        $(".save_location").click(function() {
            update_location();
        });
    });

</script>

