<?php $base = base_url() . PUBLIC_DIR . "assets/"; ?>
<link href="<?php echo $base; ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base; ?>plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base; ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base; ?>css/style-metro.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base; ?>css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base; ?>css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base; ?>css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo $base; ?>plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base; ?>plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base; ?>plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $base; ?>plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css"  />
<link href="<?php echo $base; ?>plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $base; ?>css/pages/profile.css" rel="stylesheet" />
<link href="<?php echo $base; ?>css/pages/search.css" rel="stylesheet" />
<link  href="<?php echo $base; ?>plugins/data-tables/DT_bootstrap.css" rel="stylesheet"/>
<link href="<?php echo $base; ?>css/pages/search.css" rel="stylesheet" />
<link href="<?php echo $base; ?>plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet"/>

<script src="<?php echo $base; ?>plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
<link href="<?php echo $base; ?>css/themes/blue.css" rel="stylesheet" />
<script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
</script>
