<div class="text-right">Total Email: <?php echo $count; ?></div>
<div class="row-fluid " style="margin-top: 0px;">
    <form action="#" class="form-horizontal" id="form_email_edit" name="form_email_edit" style="margin-bottom: 0px;">
        <div class="control-group margin-top-10">
            <label class="control-label">Email Subject: <span class="required">*</span></label>
            <div class="controls">
                <input type="text" class="m-wrap span12 required" name="subject" id="subject" value="<?php echo $subject; ?>" />
            </div>

        </div>
        <div class="control-group margin-top-10">
            <textarea id="editor_content"><?php echo $message ?></textarea>
            <input type="hidden" id="pk_category_id" name="pk_category_id" value="<?php echo $category_id; ?>" />
            <input type="hidden" id="league_id" name="league_id" value="<?php echo $league_id; ?>" />
        </div>
        <div class="control-group margin-top-10" style="margin-bottom: 0px;">
            <label class="control-label"></label>
            <div class="controls">
                <button type="button" id="btn_update" class="btn green pull-right"><i class="icon-save"></i> Send</button>
            </div>
        </div>
    </form>
</div>
<!--end span9-->
<script type="text/javascript">
    $(document).ready(function(e) {
        //if (editor)
        // editor.destroy();
        editor = CKEDITOR.replace("editor_content");
        $("#btn_update").click(function() {
            if ($("#form_email_edit").valid() == false) {
                return;
            }
            App.blockUI($("#form_email_edit"));
            var content = editor.getData();
            var post_data = {"content": content, "id": $("#pk_category_id").val(),league_id: $("#league_id").val(), "subject": $("#subject").val()};
            $.post(base_url + "admin/newsletter/sendNewsLetter", post_data, function(res) {
                $("#subject").val("");
                $("#editor_content").val("");
                editor.setData("");
                App.unblockUI($("#form_email_edit"));
                $.gritter.add({
                    title: 'Newsletter',
                    text: 'News has been sent'
                });
                $("#email_selector").change();
                $("#email_selector").change();
            });
        });

    });
</script>