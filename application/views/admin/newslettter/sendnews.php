<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Send News
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Newsletter
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Send News
                </li>          
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>    
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="row-fluid">
            <div class="portlet box blue">
                <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                    <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Send News</div>
                    <select class="m-wrap" id="email_selector1" style="float: right;display: none; width: 200px; margin: -5px -1px -10px 0px;">
                        <option value="0">All Clubs</option>
                        <?php foreach ($categories as $key => $category) {
                            ?>
                            <option value="<?php echo $key ?>" <?php echo ($key == $log['fk_club_id']) ? "selected" : "" ?>><?php echo $category ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
                <div class="portlet-body">
                      <div class="row-fluid">
                        <div class="span5 offset2">
                            <div class="control-group">
                                <label class="control-label">League </label>
                                <div class="controls">
                                    <select name="league" id="league" class="m-wrap span12">
                                        <option value="">All League</option>
                                        <?php foreach ($leagues as $key => $league) { ?>
                                            <option value="<?php echo $key; ?>" <?php echo ($key == $log['fk_league_id']) ? "selected" : "" ?>><?php echo $league; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span5">
                            <div class="control-group">
                                <label class="control-label">Club </label>
                                <div class="controls">
                                    <select name="email_selector" id="email_selector" class="m-wrap span12">
                                        <option value="">All Club</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="loaddata" data-url="<?php echo site_url() . "admin/newsletter/email_view" ?>" data-subject="<?php echo urlencode($log['subject']); ?>" data-message="<?php echo urlencode($log['message']); ?>"  data-category="<?php echo $log['fk_club_id']; ?>" data-league="<?php echo $log['fk_league_id']; ?>">

                        </div>
                    </div>                 
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function load_email(){
            $(".loaddata").data("league", $("#league").val());
            $(".loaddata").data("category", $("#email_selector").val());
            $(".loaddata").data("subject", encodeURIComponent($("#subject").val()));
            $(".loaddata").data("message", encodeURIComponent(editor.getData()));
            App.loadData($(".loaddata"));
}
    $(document).ready(function(e) {
        $("#league").on("change", function() {
            var post_data = {league_id: $("#league").val()};
            $.post(base_url + "admin/article/getclubs", post_data, function(res) {
                $("#email_selector").html(res);
                $("#email_selector").closest("div").hide().fadeIn();
                load_email();

            });
        });
        
        $("#email_selector").on('change', function() {
           load_email();
        });
        $(".email_delete").live('click', function() {
            $container = $(".portlet")
            var post_data = $(this).data();
            App.blockUI($container);
            $.post(base_url + "admin/newsletter/deletemail", post_data, function(res) {
                var jdata = $.parseJSON(res);
                $.gritter.add({
                    title: jdata.title,
                    text: jdata.text
                });
                App.unblockUI($container);
                $(".pagination li.cpageval").addClass("inactive").click();
            });
        });
    });

</script>

<script type="text/javascript">
    var editor;
</script>