<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Email Log
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Newsletter
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Email Log
                </li>          
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>    
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="row-fluid">
            <div class="portlet box blue">
                <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                    <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Email Log List</div>
                </div>
                <div class="portlet-body">
                    <div class="row-fluid">
                        <div id="brand_list"></div>
                        <?php
                        paginationScript(base_url() . "admin/newsletter/emaillog_list", "brand_list", "category=");
                        ?>                      
                    </div>                 
                </div>
            </div>
        </div>
    </div>
</div>

