<div class="data">
    <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
        <div class="search-form">
            <div class="chat-form" style="margin-top: 0px">
                <div class="input-cont span10" style="margin-right:0px">
                    <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                </div>
                <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <?php if (count($data) > 0) { ?>
            <table class="table table-bordered table-hover table-full-width table-striped">
                <tr>
                    <th style="width: 50px;">#</th>
                    <th>Email</th>
                    <th>Club</th>
                    <th style="width: 70px;">Action</th>
                </tr>

                <?php
                $i = 1;
                foreach ($data as $email) {
                    ?>
                    <tr>
                        <td><?php echo ($page_number * 30 + $i++); ?></td>
                        <td><?php echo $email['email']; ?></td>
                        <td><?php
                            if ($email['fk_club_id'] > 0) {
                                echo $email['title'];
                            } else {
                                echo "";
                            }
                            ?></td>
                        <td><div class="btn mini red pull-right email_delete" data-id="<?php echo $email['pk_newsletter_id'] ?>">Remove</div></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
        }
        if (count($data) == 0) {
            ?>
            <h4 style="text-align: center">No Email Available</h4>
        <?php }
        ?>

    </div>
    <?php echo $page; ?>
</div>



<script type="text/javascript">
    $(".email_delete").easyconfirm({locale: {
            title: 'Delete Email',
            text: 'Are you sure to delete this email',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});


</script>