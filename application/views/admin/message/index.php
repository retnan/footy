<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Send Message
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Messages
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Send Message
                </li>          
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>    
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="row-fluid">
            <form action="#" class="form-horizontal" id="form_message" name="form_message" style="margin-bottom: 0px;">
                <div class="portlet box blue">
                    <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                        <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Send Message</div>
                        <select class="m-wrap" name="league" id="league" style="float: right; width: 200px; margin: -5px -1px -10px 0px;">
                            <option value="0">All Leagues</option>
                            <?php foreach ($leagues as $key => $league) {
                                ?>
                                <option value="<?php echo $key ?>"><?php echo $league ?></option>
                            <?php }
                            ?>
                        </select>
                    </div>
                    <div class="portlet-body">
                        <div class="row-fluid">
                            <div class="control-group club_section margin-top-10" style="display: none;">
                                <label class="control-label">Choose Club(s)</label>
                                <div class="controls club_block">

                                </div>
                                <div class="controls">
                                    <span class="label label-info">Note:</span> <span class="text-info">if you does not choose any club, message will be send to all users of selected League.</span>
                                </div>

                            </div>

                            <div class="control-group margin-top-10">
                                <label class="control-label">Title <span class="required">*</span></label>
                                <div class="controls">
                                    <input type="text" class="m-wrap span12 required" name="title" id="title" />
                                </div>

                            </div>
                            <div class="control-group margin-top-10">
                                <label class="control-label">Message <span class="required">*</span></label>
                                <div class="controls">
                                    <textarea id="content" class="m-wrap span12 required" rows="8" name="content"></textarea>
                                </div>

                            </div>
                            <div class="control-group margin-top-10">


                            </div>
                            <div class="control-group margin-top-10" style="margin-bottom: 0px;">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <button type="button" id="btn_update" class="btn green pull-right"><i class="icon-save"></i> Send</button>
                                </div>
                            </div>

                        </div>                 
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(e) {

        $("#league").on('change', function() {
            if ($(this).val() == 0) {
                $(".club_section").hide();
                $(".club_block").html("");
                return;
            }
            var post_data = {"league_id": $(this).val()};
            $.post(base_url + "admin/message/get_clubs", post_data, function(res) {
                $(".club_section").fadeIn();
                $(".club_block").html(res);
                App.initUniform();
            });
        });

        $("#btn_update").click(function() {
            if ($("#form_message").valid() == false) {
                return;
            }
            App.blockUI($("#form_message"));
            var post_data = $("#form_message").serialize();
            $.post(base_url + "admin/message/save_message", post_data, function(res) {
                App.unblockUI($("#form_message"));
                $("#form_message")[0].reset();
                $("#league").val("0");
                $("#league").change();
                $.gritter.add({
                    title: 'Message',
                    text: 'Message has been sent Successfully'
                });

            });
        });
    });

</script>
