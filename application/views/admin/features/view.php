<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Features
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Features
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Create Features
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="row-fluid">
            <div class="portlet box blue">
                  <?php echo form_open_multipart('admin/Features/do_upload')?>
                <div class="portlet-body">
                      <div class="row-fluid">
                        <div class="span3 ">
                            <div class="control-group">
                                <label class="control-label">Title </label>
                                <div class="controls">
                                    <input type ="textbox" name="title" id="league" class="m-wrap span12" placeholder="Enter the title"/>
                                </div>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">File </label>
                                <div class="controls">
                                    <input type = "file" name="userfile" id="email_selector" class="m-wrap span12"/>
                                </div>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Link </label>
                                <div class="controls">
                                      <input type ="textbox" name="link" id="league" class="m-wrap span12" placeholder = "Enter the advert link"/>
                                </div>
                            </div>
                        </div>
                        <div class="span1">
                            <div class="control-group">
                                <label class="control-label">Start Date </label>
                                <div class="controls">
                                      <input type ="textbox" name="start" id="league" class="m-wrap span12" placeholder = "YY/MM/DD"/>
                                </div>
                            </div>
                        </div>
                        <div class="span1">
                            <div class="control-group">
                                <label class="control-label">End Date </label>
                                <div class="controls">
                                      <input type ="textbox" name="end" id="league" class="m-wrap span12" placeholder = "YY/MM/DD"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12" style="padding-top:5px">
                    <input type="submit" name="create" value="create" class="btn btn-danger">
                  </div>
                  <?php
                    // if(isset($_POST['create']))
                    // {
                    //   $name = $this->input->post('title');
                    //   $date = $this->input->post('date');
                    //   $pic_name = $_FILES["pic_det"]["name"];
            				//   $pic_size = $_FILES['pic_det']['size'];
            				//   $pic_tempname = $_FILES['pic_det']['tmp_name'];
                    //   $image_url = $this->Features->features_upload($name,$pic_size,$pic_tempname);
                    //   $persist = $this->Features->create_features($name,$image_url,$date);
                    //   if ($persist)
                    //   {
                    //     echo "<p class = 'alert alert-success'>Features posted</p>";
                    //   }
                    //   else
                    //   {
                    //     echo "<p class = 'alert alert-danger'>Features not posted</p>";
                    //   }
                    // }
                   ?>
                  </form>
                    <div class="row-fluid">
                      <!--set all matches here-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script type="text/javascript">
function load_email(){
            $(".loaddata").data("league", $("#league").val());
            $(".loaddata").data("category", $("#email_selector").val());
            $(".loaddata").data("subject", encodeURIComponent($("#subject").val()));
            $(".loaddata").data("message", encodeURIComponent(editor.getData()));
            App.loadData($(".loaddata"));
}
    $(document).ready(function(e) {
        $("#league").on("change", function() {
            var post_data = {league_id: $("#league").val()};
            $.post(base_url + "admin/article/getclubs", post_data, function(res) {
                $("#email_selector").html(res);
                $("#email_selector").closest("div").hide().fadeIn();
                load_email();

            });
        });

        $("#email_selector").on('change', function() {
           load_email();
        });
        $(".email_delete").live('click', function() {
            $container = $(".portlet")
            var post_data = $(this).data();
            App.blockUI($container);
            $.post(base_url + "admin/newsletter/deletemail", post_data, function(res) {
                var jdata = $.parseJSON(res);
                $.gritter.add({
                    title: jdata.title,
                    text: jdata.text
                });
                App.unblockUI($container);
                $(".pagination li.cpageval").addClass("inactive").click();
            });
        });
    });

</script>

<script type="text/javascript">
    var editor;
</script> -->
