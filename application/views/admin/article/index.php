<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Article List
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Articles
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Article List
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Article List</div>
            </div>
            <div class="portlet-body">
                <form class="form-horizontal" id="article_form" style="margin-bottom: 0px;" enctype="multipart/form-data">
                    <div class="alert alert alert-success article_success" style="display: none;">Article has been saved.</div>
                    <div class="row-fluid">
                        <div class="span5">
                            <div class="control-group">
                                <label class="control-label" style="width: 80px;">Article Type</label>
                                <div class="controls" style="margin-left:110px;">                                    
                                    <label class="checkbox line span5">
                                        <input type="checkbox" name="featured" id="featured"  style="display: none" value="1" /> Featured
                                    </label>
                                    <label class="checkbox line span5">
                                        <input type="checkbox" name="sponsored" id="sponsored"  style="display: none" value="1" /> Sponsored
                                    </label>

                                </div>
                            </div>
                        </div>
                        <div class="span3 offset1">
                            <div class="control-group">
                                <label class="control-label" style="width: auto;">League</label>
                                <div class="controls" style="margin-left: 60px;">
                                    <select name="league" id="league" class="m-wrap span12" >
                                        <option value="">Select League</option>
                                        <?php foreach ($leagues as $key => $val) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group" >
                                <label class="control-label" style="width: auto;">Club</label>
                                <div class="controls" style="margin-left: 60px;">
                                    <select name="club" id="club" class="m-wrap span12" >
                                        <option value="">Select Club</option>
                                        <?php foreach ($clubs as $key => $val) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="article_loader" class="loaddata" data-featured="0" data-sponsored="0" data-club_id="0" data-league_id="0" data-post_type='A' data-user_id='0' data-url="<?php echo base_url() . ADMIN_DIR . "article/article_loader"; ?>"></div>


                    <div class="clearfix"></div>
                </form>

            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<script type="text/javascript">
    $(document).ready(function(e) {
        $("#league").on('change', function() {
            $("#article_loader").data("league_id", $(this).val());
            App.loadData($("#article_loader"));

            var post_data = {"league_id": $(this).val()};
            $.post(base_url + "admin/article/get_clubs", post_data, function(res) {
                $("#club").html(res);
            });
        });

        $("#club").on('change', function() {
            $("#article_loader").data("club_id", $(this).val());
            App.loadData($("#article_loader"));

        });

        $("#featured").on('change', function() {
            if ($(this).is(":checked")) {
                $("#article_loader").data("featured", "1");
            } else {
                $("#article_loader").data("featured", "0");
            }
            App.loadData($("#article_loader"));
        });
        $("#sponsored").on('change', function() {
            if ($(this).is(":checked")) {
                $("#article_loader").data("sponsored", "1");
            } else {
                $("#article_loader").data("sponsored", "0");
            }
            App.loadData($("#article_loader"));
        });

    });
</script>
