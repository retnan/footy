<?php $file_type = array("I" => "Image", "V" => "Video Link"); ?>
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Add Article
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Articles
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Add Articles
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Add Article</div>
            </div>
            <div class="portlet-body">
                <form class="form-vertical" id="article_form" style="margin-bottom: 0px;" enctype="multipart/form-data">
                    <div class="alert alert alert-success article_success" style="display: none;">Article has been saved.</div>
                    <div class="row-fluid">
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">League </label>
                                <div class="controls">
                                    <select name="league" id="league" class="m-wrap span12">
                                        <option value="">All League</option>
                                        <?php foreach ($leagues as $key => $league) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $league; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Club </label>
                                <div class="controls">
                                    <select name="club" id="club" class="m-wrap span12">
                                        <option value="">All Club</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid" >
                        <div class="span4">
                            <div class="control-group">                               
                                <div class="controls">
                                    <label class="checkbox line span6">
                                        <input type="checkbox" name="featured_status" value="1" id="featured_status"   /> <span class="text-success"> Mark Featured</span>
                                    </label>
                                    <label class="checkbox line span6">
                                        <input type="checkbox" name="sponser_status" value="1" id="sponser_status"    /> <span class="text-info">Mark Sponsored</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">                             
                                <div class="controls">
                                    <label class="checkbox line">
                                        <input type="checkbox" name="comment_allow" id="comment_allow"   value="1" checked="" /> <span class="text-warning">Allow Comments from Users</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="control-group">
                        <label class="control-label" style="">Article Title <span class="required">*</span></label>
                        <div class="controls" style="">
                            <input type="text" placeholder="Article Title" id="article_title" name="title" class="m-wrap required span12">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" style="">Article Tags </label>
                        <div class="controls" style="">
                            <input type="text" placeholder="Article Tags" id="article_tags" name="tags" class="m-wrap span12 article_tags">

                        </div>
                    </div>

                    <div class="row-fluid" style="height: 115px; margin-top: 55px;">
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Media Type </label>
                                <div class="controls">
                                    <select name="file_type" id="file_type" class="m-wrap span12">
                                        <option value="">Select Media</option>
                                        <?php foreach ($file_type as $key => $type) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $type; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="span6 media_video hide">
                            <div class="control-group ">
                                <label class="control-label" style="">Youtube Video Link<span class="required">*</span></label>
                                <div class="controls" style="">
                                    <input type="text" placeholder="Youtube Link" id="youtube_link2" name="youtube_link2" class="m-wrap span12">
                                    <input type="text" placeholder="Youtube Link" id="youtube_link" name="youtube_link" class="m-wrap span12" style="display: none;">
                                    <span for="youtube_link" class="help-inline hide">This field is required.</span>

                                </div>
                                <span class="label label-info">Note: </span> <small>http://youtube.com/vid?abcd</small>

                            </div>
                        </div>
                        <div class="span6 media_img hide">
                            <div class="control-group ">
                                <label class="control-label" style="">Article Image <span class="required">*</span></label>
                                <div class="controls">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-file">
                                            <span class="fileupload-new">Select file to upload</span>
                                            <span class="fileupload-exists">Change</span>
                                            <input type="file" class="default file_file" name="file_name" id="file_file" data-msg-accept="Choose a valid file" accept="jpg|jpeg|gif|png">
                                        </span>
                                        <span class="fileupload-preview"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                        <span class="help-inline file_alert file_img_alert1" for="file_file" style="margin-top: -2px; margin-left: 10px; display:none"></span>
                                    </div>
                                    <span class="label label-info">Note: </span> <small>Only jpg, gif, jpeg and png files are allowed</small>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="clearfix clear"></div>
                    <div class="row-fluid" >
                        <div class="control-group">
                            <label class="control-label">Article Content </label>


                        </div>

                    </div>
                    <div class="row-fluid" >
                        <textarea class="editor_content" name="content" id="editor_content" style="visibility: hidden; min-height:500px;"></textarea>

                    </div>
                    <div style="margin-top: 10px;">
                        <div class="btn green save_article pull-right">Save</div>
                    </div>
                    <input type="hidden" value="A" name="post_type">
                    <div class="clearfix"></div>
                </form>

            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>
<script type="text/javascript">
    function matchYoutubeUrl(url) {
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        var matches = url.match(p);
        if (matches) {
            return matches[1];
        }
        return false;
    }

    var editor;
    $(document).ready(function(e) {
        $("#league").on("change", function() {
            var post_data = {league_id: $("#league").val()};
            $.post(base_url + "admin/article/getclubs", post_data, function(res) {
                $("#club").html(res);
                $("#club").closest("div").hide().fadeIn();

            });
        });

        $(".article_tags").select2({tags: [""]});
//        $(".article_tags").select2({tags: [""],tokenSeparators: [","]});
        $("#youtube_link2").on("blur", function() {
            var url = $("#youtube_link2").val();
            var id = matchYoutubeUrl(url);
            if (id != false) {
                $("#youtube_link").val(id).blur();
                $("#youtube_link").closest(".control-group").addClass("success").removeClass("error");
                $("#youtube_link").closest(".control-group").find("span.help-inline").html("").addClass("hide");
            } else {
                $("#youtube_link").val('').blur();
                $("#youtube_link").closest(".control-group").addClass("error").removeClass("success");
                $("#youtube_link").closest(".control-group").find("span.help-inline").html("Invalid youtube URL").removeClass("hide");
            }
        });

        if (editor)
            editor.destroy();
        editor = CKEDITOR.replace("editor_content", {
        });



        $(".save_article").click(function() {
            var content = editor.getData();
            $(".editor_content").val(content);
            if ($("#article_form").valid() == false) {
                return;
            }


            App.blockUI($("#article_form"));
            var formData = new FormData($("form#article_form")[0]);
            $.ajax({
                url: base_url + "admin/article/savearticle",
                type: 'POST',
                data: formData,
                async: false,
                success: function(data) {
                    App.unblockUI($("#article_form"));
                    $.gritter.add({
                        title: 'Article',
                        text: 'Article has been Saved'
                    });
                    editor.setData('');
                    $("#article_form")[0].reset();
                    $("#article_form .control-group").removeClass("success");
                    $("#file_type").val("");
                    $(".fileupload-exists").click();
                    $(".article_success").hide().fadeIn();
                    $("#file_type").change();
                    $(".article_tags").select2('data', null);
                    $('#featured_status,#sponser_status').parents('span').removeClass("checked").end().removeAttr("checked").change();
                    App.initUniform();
                },
                error: function(data) {
                    App.unblockUI($("#article_form"));
                    $.gritter.add({title: 'Article Error', text: 'Error in Saving Article'});
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
        $("#file_type").change(function() {


            if ($(this).val() == 'I') {
                $(".media_img").removeClass("hide");
                $(".media_img input#youtube_link").addClass("required");
                $(".media_video input#youtube_link").removeClass("required");
                $(".media_video").addClass("hide");
            } else if ($(this).val() == 'V') {
                $(".media_video").removeClass("hide");
                $(".media_video input#youtube_link").addClass("required");
                $(".media_img input#youtube_link").removeClass("required");
                $(".media_img").addClass("hide");
            } else {

                $(".media_img input,.media_video input").removeClass("required");
                $(".media_img,.media_video").addClass("hide");
            }
        });

    });
</script>
