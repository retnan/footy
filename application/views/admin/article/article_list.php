<div class="data">
    <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
        <div class="search-form">
            <div class="chat-form" style="margin-top: 0px">
                <div class="input-cont span10" style="margin-right:0px">
                    <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                </div>
                <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <?php if (count($data) > 0) { ?>
            <?php
            foreach ($data as $article) {
                ?>
                <div class="row-fluid  portfolio-block">
                    <div class="span2">
                        <img src="<?php echo $article['thumb'] ?>" class="img-polaroid" style="width: 100%" />
                    </div>
                    <div class="span10 portfolio-text" style="overflow: visible">
                        <div class="row-fluid">
                            <div class="span10">
                                <div class="span12">
                                    <h3 style="margin:0px 0;font-size: 19px; float: left;"><?php echo $article['title']; ?></h3> <?php if ($article['status'] != "1") { ?>
                                        <div class="label label-warning pull-right" style="margin-top: 5px;">Inactive</div>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                    <span style="width: auto; float: left;" class="event_details">
                                        <h5 style="margin:0px 0px; margin-top: 0px;">
                                            <i class="icon-calendar"></i> <?php echo date("M d, Y h:i A", strtotime($article['created_at'])); ?> &nbsp;&nbsp;&nbsp; <i class="icon-eye-open"></i> <?php echo $article['views']; ?> Views  &nbsp;&nbsp;&nbsp; <i class="icon-screenshot"></i> Featured: <?php echo ($article['featured_status'] == "1") ? "Yes" : "No"; ?> &nbsp;&nbsp;&nbsp;
                                            <i class="icon-screenshot"></i> Sponsored: <?php echo ($article['sponser_status'] == "1") ? "Yes" : "No"; ?> &nbsp;&nbsp;&nbsp;
                                            <?php if ($article['comment_allow'] == "1") { ?>
                                                <span class="text-success"> <i class="icon-chat"></i> Comments Allowed: Yes</span>
                                            <?php } else {
                                                ?> 
                                                <span class="text-error"> <i class="icon-chat"></i> Comments Allowed: No</span>
                                            <?php }
                                            ?>
                                        </h5></span>
                                </div>
                            </div>
                            <div class="span2 pull-right">
                                <div class="task-config-btn btn-group pull-right">
                                    <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                    <ul class="dropdown-menu pull-right" style="width: 175px;">
                                        <li><a  href="<?php echo base_url() . ADMIN_DIR . "article/edit/" . $article['pk_post_id']; ?>" target="_blank"> <i class="icon icon-pencil"></i> View/Edit</a></li>
                                        <li><a  href="<?php echo base_url() . ADMIN_DIR . "article/comments/" . $article['pk_post_id']; ?>" target="_blank"> <i class="icon icon-comment"></i> View Comments</a></li>
                                        <?php if ($article['comment_allow'] != "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/comment_status"; ?>" data-action="1" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-check"></i> Allow Comments</a></li>
                                        <?php } ?>
                                        <?php if ($article['comment_allow'] == "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/comment_status"; ?>" data-action="0" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-ban-circle"></i> Deny Comments</a></li>
                                        <?php } ?>
                                            
                                       <?php if ($article['post_type'] == "A") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/post_status"; ?>" data-action="N" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-check"></i> Convert as News</a></li>
                                        <?php } ?>
                                         <?php if ($article['post_type'] == "N") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/post_status"; ?>" data-action="A" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-ban-circle"></i> Convert as Article</a></li>
                                        <?php } ?>

                                        <?php if ($article['featured_status'] != "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/featured"; ?>" data-action="1" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-check"></i> Mark Featured</a></li>
                                        <?php } ?>
                                        <?php if ($article['featured_status'] == "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/featured"; ?>" data-action="0" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-ban-circle"></i> Remove Featured</a></li>
                                        <?php } ?>
                                        <?php if ($article['sponser_status'] != "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/sponsored"; ?>" data-action="1" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-check"></i> Mark Sponsored</a></li>
                                        <?php } ?>
                                        <?php if ($article['sponser_status'] == "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/sponsored"; ?>" data-action="0" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-ban-circle"></i> Remove Sponsored</a></li>
                                        <?php } ?>
                                        <?php if ($article['status'] != "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/actions"; ?>" data-action="1" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-check"></i> Activate</a></li>
                                        <?php } ?>
                                        <?php if ($article['status'] == "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/actions"; ?>" data-action="0" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-pause"></i> Deactivate</a></li>
                                        <?php } ?>
                                        <li><a class="article_action" href="#" data-image="<?php echo $article['file_name']; ?>" data-href="<?php echo base_url() . ADMIN_DIR . "article/deleteaction"; ?>" data-action="3" data-id="<?php echo $article['pk_post_id']; ?>"> <i class="icon icon-remove"></i> Delete</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid margin-top-10">
                            <span style="width: auto; float: left;">
                                <h5 style="margin:0px 0px; margin-top: 0px;">
                                    <?php if ($article['fk_club_id'] > 0) { ?>
                                        <i class="icon icon-file"></i> Club: <span class="text-info"><?php echo $clubs[$article['fk_club_id']]; ?> </span> &nbsp; &nbsp;

                                    <?php } ?>

                                </h5></span>
                        </div>

                        <div class="row-fluid margin-top-10">
                            <p><?php echo wordLimiter(strip_tags($article['content']), 500) ?></p>
                        </div>
                    </div>
                </div>

                <?php
            }
            ?>
            <?php
        }
        if (count($data) == 0) {
            ?>
            <h4 style="text-align: center">No Articles Available</h4>
        <?php }
        ?>

    </div>
    <?php echo $page; ?>
</div>


<script type="text/javascript">
    $(".article_action[data-action=3]").easyconfirm({locale: {
            title: 'Delete Article',
            text: 'Are you sure to delete this article',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
    $(".article_action").click(function(e) {
        e.preventDefault();
        $container = $(this).parents(".portfolio-block");
        App.blockUI($container);
        var post_data = $(this).data();
        $.post($(this).data("href"), post_data, function(res) {
            var jdata = $.parseJSON(res);
            $.gritter.add({
                title: jdata.title,
                text: jdata.text
            });
            App.unblockUI($container);
            $(".pagination li.cpageval").removeClass("active").addClass("inactive").click();
        });
    });
</script>