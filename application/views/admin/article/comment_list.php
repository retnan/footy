<div class="data">
    <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
        <div class="search-form">
            <div class="chat-form" style="margin-top: 0px">
                <div class="input-cont span10" style="margin-right:0px">
                    <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                </div>
                <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <?php if (count($data) > 0) { ?>
            <?php
            foreach ($data as $row) {
                ?>
                <div class="row-fluid  portfolio-block">
                    <div class="span2">
                        <img src="<?php echo $row['thumb'] ?>" class="img-polaroid" style="width: 100%" />
                    </div>
                    <div class="span10 portfolio-text" style="overflow: visible">
                        <div class="row-fluid">
                            <div class="span10">
                                <div class="span10" style="float: left;">
                                    <h3 style="margin:0px 0;font-size: 21px;"><?php echo $row['name']; ?></h3>

                                </div>
                                <?php if ($row['status'] == "0") { ?>
                                    <div class="label label-warning pull-right" style="margin-top: 5px;">Inactive</div>
                                <?php } ?>
                            </div>
                            <div class="span2 pull-right">
                                <div class="task-config-btn btn-group pull-right">
                                    <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                    <ul class="dropdown-menu pull-right">

                                        <?php if ($row['status'] != "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "article/commentactions"; ?>" data-action="1" data-id="<?php echo $row['pk_comment_id']; ?>"> <i class="icon icon-check"></i> Activate</a></li>
                                        <?php } else { ?>

                                            <li><a class="article_action" href="#"  data-href="<?php echo base_url() . ADMIN_DIR . "article/commentactions"; ?>" data-action="0" data-id="<?php echo $row['pk_comment_id']; ?>"> <i class="icon icon-remove"></i> Deactivate</a></li>
                                        <?php } ?>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div>

                            <div class="text-success" style="font-weight: 600; width: auto; float: left; margin-right: 20px;">Club:
                                <?php echo $clubs[$row['fk_club_id']] ?>
                            </div>
                            <div class="text-warning" style="font-weight: 600; width: auto; float: left; margin-right: 20px;">Email:
                                <?php echo $row['email'] ?>
                            </div>
                            <span style="width: auto; float: right;" class="event_details">
                                <i class="icon-calendar"></i> <?php echo date("M d, Y h:i A", strtotime($row['created_at'])); ?>
                            </span>
                        </div>
                        <div class="clearfix"></div>
                        <p><?php echo nl2br(makeClickableLinks($row['content'])) ?></p>
                    </div>
                </div>
                <?php
            }
            ?>
            <?php
        }
        if (count($data) == 0) {
            ?>
            <h4 style="text-align: center">No Comment Available</h4>
        <?php }
        ?>

    </div>
    <?php echo $page; ?>
</div>


<script type="text/javascript">
    $(".article_action[data-action=2]").easyconfirm({locale: {
            title: 'Delete Comment',
            text: 'Are you sure to delete this comment',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
    $(".article_action").click(function(e) {
        e.preventDefault();
        $container = $(this).parents(".portfolio-block");
        App.blockUI($container);
        var post_data = $(this).data();
        $.post($(this).data("href"), post_data, function(res) {
            var jdata = $.parseJSON(res);
            $.gritter.add({
                title: jdata.title,
                text: jdata.text
            });
            App.unblockUI($container);
            $(".pagination li.cpageval").removeClass("active").addClass("inactive").click();
        });
    });
</script>