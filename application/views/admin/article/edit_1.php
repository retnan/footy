<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Edit Article
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Articles
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Edit Articles
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Edit Article</div>
            </div>
            <div class="portlet-body">
                <form class="form-vertical" id="article_form" style="margin-bottom: 0px;" enctype="multipart/form-data">
                    <div class="alert alert alert-success article_success" style="display: none;">Article has been saved.</div>
                    <div class="row-fluid">
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Category <span class="required">*</span></label>
                                <div class="controls">
                                    <select name="category" id="category" class="m-wrap span12" data-rule-required="true">
                                        <option value="">Select Category</option>
                                        <?php foreach ($categories as $key => $cat) { ?>
                                            <option value="<?php echo $key; ?>" <?php echo ($key == $article['category_id']) ? "selected" : "" ?>><?php echo $cat; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Sub-Category</label>
                                <div class="controls">
                                    <select name="subcategory" id="sub_category" class="m-wrap span12">
                                        <option value="">Select Sub Category</option>
                                        <?php foreach ($subcategories as $key => $cat) { ?>
                                            <option value="<?php echo $key; ?>" <?php echo ($key == $article['subcategory_id']) ? "selected" : "" ?>><?php echo $cat; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">&nbsp;</label>
                                <div class="controls">
                                    <label class="checkbox">
                                        <input type="checkbox" name="homepage" id="home_page"  style="display: none" value="1" <?php echo ($article['homepage'] == "1") ? "checked" : "" ?> /> Home Page View
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Display Column</label>
                                <div class="controls">
                                    <select name="column" id="column" class="m-wrap span12">
                                        <option value="1" <?php echo ($article['display_column'] == "1") ? "selected" : "" ?>>First</option>
                                        <option value="2" <?php echo ($article['display_column'] == "2") ? "selected" : "" ?>>Second</option>
                                        <option value="3" <?php echo ($article['display_column'] == "3") ? "selected" : "" ?>>Third</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" style="">Article Title <span class="required">*</span></label>
                        <div class="controls" style="">
                            <input type="text" placeholder="Article Title" id="article_title" name="title" value="<?php echo $article['title'] ?>" class="m-wrap required span12">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" style="">Article Slug <span class="required">*</span></label>
                        <div class="controls" style="">
                            <input type="text" placeholder="Article Slug" id="article_slug" name="slug" class="m-wrap required span12" value="<?php echo $article['slug'] ?>">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" style="">Header Meta</label>
                        <div class="controls" style="">
                            <textarea class="m-wrap  span12" name="header_meta" id="header_meta" style=" min-height:100px;"><?php echo $article['heder_meta'] ?></textarea>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span10">
                            <div class="control-group">
                                <label class="control-label" style="">Article Image</label>
                                <div class="controls">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-file">
                                            <span class="fileupload-new">Select file to upload</span>
                                            <span class="fileupload-exists">Change</span>
                                            <input type="file" class="default file_file" name="article_image" id="file_file" data-msg-accept="Choose a valid file" accept="jpg|jpeg|gif|png">
                                        </span>
                                        <span class="fileupload-preview"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                        <span class="help-inline file_alert file_img_alert1" for="file_file" style="margin-top: -2px; margin-left: 10px; display:none"></span>
                                    </div>
                                    <span class="label label-info">Note: </span> <small>Only jpg, gif, jpeg and png files are allowed</small>
                                </div>
                            </div>
                        </div>
                        <div class="span2">
                            <img src="<?php echo $article['thumb']; ?>" style="width: 100%;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" style="">Image Alt</label>
                        <div class="controls" style="">
                            <input type="text" placeholder="Image Alt" id="image_alt" name="image_alt" value="<?php echo $article['image_alt']; ?>" class="m-wrap  span12">
                        </div>
                    </div>

                    <div id='translControl' style="width:100px; margin-bottom: 5px; display: none;"></div>
                    <div class="control-group">
                        <label class="control-label" style="">Hindi Typing Box (Type here for hindi and paste in editor)</label>
                        <div class="controls" style="">
                            <textarea class="m-wrap  span12" name="hindi" id="hindi_typing" style=" min-height:100px;"></textarea>
                        </div>
                    </div>


                    <div class="row-fluid" style="margin-top: 20px;">
                        <textarea class="editor_content" name="content" id="editor_content" style="visibility: hidden; min-height:500px;"><?php echo $article['content'] ?></textarea>

                    </div>
                    <div style="margin-top: 10px;">
                        <div class="btn green save_article pull-right">Save</div>
                    </div>
                    <div class="clearfix"></div>
                    <input type="hidden" name="pk_article_id" value="<?php echo $article['pk_article_id']; ?>" />
                    <input type="hidden" name="image" value="<?php echo $article['image']; ?>" />
                </form>

            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>


<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<?php echo site_url() . "public/fassets/js/translation.js" ?>"></script>
<link href="<?php echo site_url() . "public/fassets/css/translation.css" ?>" type="text/css" rel="stylesheet"/>
<script type="text/javascript">
    // Load the Google Transliteration API
    google.load("elements", "1", {
        packages: "transliteration"
    });
    function onLoad() {
        var options = {
            sourceLanguage: 'en',
            destinationLanguage: ['hi'],
            shortcutKey: 'ctrl+g',
            transliterationEnabled: true
        };
        // Create an instance on TransliterationControl with the required
        // options.
        var control = new google.elements.transliteration.TransliterationControl(options);
        // Enable transliteration in the textfields with the given ids.
        var ids = ["hindi_typing"];
        control.makeTransliteratable(ids);
        // Show the transliteration control which can be used to toggle between
        // English and Hindi and also choose other destination language.
        control.showControl('translControl');
    }
    google.setOnLoadCallback(onLoad);
</script>



<script type="text/javascript">
    var editor;
    $(document).ready(function(e) {
        if (editor)
            editor.destroy();
        editor = CKEDITOR.replace("editor_content");



        $(".save_article").click(function() {
            var content = editor.getData();
            $(".editor_content").val(content);
            if ($("#article_form").valid() == false) {
                return;
            }


            App.blockUI($("#article_form"));
            var formData = new FormData($("form#article_form")[0]);
            $.ajax({
                url: base_url + "admin/article/updatearticle",
                type: 'POST',
                data: formData,
                async: false,
                success: function(data) {
                    App.unblockUI($("#article_form"));
                    $.gritter.add({
                        title: 'Article',
                        text: 'Article has been Updated'
                    });
                    window.location.reload();
                },
                error: function(data) {
                    App.unblockUI($("#article_form"));
                    $.gritter.add({title: 'Article Error', text: 'Error in Saving Article'});
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
        $("#category").on('change', function() {
            var post_data = {"category": $(this).val()};
            $.post(base_url + "admin/article/getsubcategory", post_data, function(res) {
                $("#sub_category").html(res);
            });
        });
    });
</script>
