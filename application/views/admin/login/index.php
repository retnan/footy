<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>Login</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <?php $base = base_url() . PUBLIC_DIR . "assets/"; ?>
        <link href="<?php echo $base; ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base; ?>plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base; ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base; ?>css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base; ?>css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base; ?>css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base; ?>css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo $base; ?>plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base; ?>plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo $base; ?>plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
        <link href="<?php echo $base; ?>css/pages/login.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base; ?>css/themes/blue.css" rel="stylesheet" />
        <script type="text/javascript">
            var base_url = "<?php echo base_url(); ?>";
        </script>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login" style="background-color: #1570a6 !important;">
        <!-- BEGIN LOGO -->
<!--        <div class="logo">
            <img src="<?php echo $base; ?>img/logo/logo_small.png" alt="" /> 
        </div>-->
 <div class="logo" style="width: 350px;">
            <h3 style="color: #fff;font-size: 30px;font-weight: 600;">Administrator Login</h3>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="form-vertical login-form" id="login-form" action="" method="post">
                <h3 class="form-title">Login to Admin account</h3>
                <div class="alert alert-error login_error  hide">                   
                    <span>Enter username and password.</span>
                </div>
                <div class="control-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="controls">
                        <div class="input-icon left">
                            <i class="icon-user"></i>
                            <input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="txt_username" data-rule-required="true" data-msg-required="Username is required" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="controls">
                        <div class="input-icon left">
                            <i class="icon-lock"></i>
                            <input class="m-wrap placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="txt_password"  data-rule-required="true" data-msg-required="Password is required" />
                        </div>
                    </div>
                </div>	
                <div class="form-actions">

                    <button type="submit" class="btn green pull-right">
                        Login <i class="m-icon-swapright m-icon-white"></i>
                    </button>            
                </div>

            </form>
            <!-- END LOGIN FORM -->        
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="form-vertical forget-form" action="index.html" method="post">
                <h3 >Forget Password ?</h3>
                <p>Enter your e-mail address below to reset your password.</p>
                <div class="control-group">
                    <div class="controls">
                        <div class="input-icon left">
                            <i class="icon-envelope"></i>
                            <input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn">
                        <i class="m-icon-swapleft"></i> Back
                    </button>
                    <button type="submit" class="btn green pull-right">
                        Submit <i class="m-icon-swapright m-icon-white"></i>
                    </button>            
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->

        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright">
    &copy; 2015 - <?PHP echo date("Y"); ?> <?php echo SITE_NAME ?>
        </div>
        <!-- END COPYRIGHT -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

        <!-- JS Files and Scripts Section -->

        <script src="<?php echo $base; ?>plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<?php echo $base; ?>plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script> 
        <script src="<?php echo $base; ?>plugins/jquery.autocomplete.js" type="text/javascript"></script> 
        <script src="<?php echo $base; ?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $base; ?>plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
        <!--[if lt IE 9]>
        <script src="<?php echo $base; ?>plugins/excanvas.min.js"></script>
        <script src="<?php echo $base; ?>plugins/respond.min.js"></script>  
        <![endif]-->   
        <script src="<?php echo $base; ?>plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript" ></script>
        <script src="<?php echo $base; ?>plugins/jquery-validation/dist/additional-methods.min.js" type="text/javascript" ></script>
        <script src="<?php echo $base; ?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo $base; ?>plugins/jquery.blockui.min.js" type="text/javascript"></script>  
        <script src="<?php echo $base; ?>plugins/jquery.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo $base; ?>plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>

        <script type="text/javascript" src="<?php echo $base; ?>plugins/select2/select2.min.js"></script>  
        <script src="<?php echo $base; ?>scripts/ui-modals.js"></script> 



        <script src="<?php echo $base; ?>scripts/app.js"></script>
        <!-- END PAGE LEVEL SCRIPTS --> 
        <script>
            jQuery(document).ready(function() {
                App.init();

                $('#login-form').validate({
                    errorPlacement: function(error, element) {
                        error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
                    },
                    success: function(label) {
                        label.addClass('hide').addClass('help-inline') // mark the current input as valid and display OK icon
                                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group

                    },
                    submitHandler: function(form) {
                        admin_login();
                        return false;
                    }
                });
            });

            function admin_login() {
                var post_data = $('#login-form').serialize();
                $.post(base_url + "admin/login/verify", post_data, function(res) {

                    if (res.status == "success") {
                        window.location = base_url + res.page;
                    } else {
                        $('.login_error').html(res.msg).hide().fadeIn(300);
                    }
                }, 'json');
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>