<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                <?php echo $content["title"] ?>
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Page
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <?php echo $content["title"] ?>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Edit Page</div>
                <div class="btn yellow pull-right btn_update" style="margin-top: -5px;">Update</div>
            </div>
            <div class="portlet-body">
                <div class="row-fluid">

                    <div class="control-group">
                        <label class="control-label" style="">Header Meta</label>
                        <div class="controls" style="">
                            <textarea class="m-wrap  span12" name="header_meta" id="header_meta" style=" min-height:100px;"><?php echo $content['header_meta'] ?></textarea>
                        </div>
                    </div>

                    <div id='translControl' style="width:100px; margin-bottom: 5px; display: none;"></div>
                    <div class="control-group">
                        <label class="control-label" style="">Hindi Typing Box (Type here for hindi and paste in editor)</label>
                        <div class="controls" style="">
                            <textarea class="m-wrap  span12" name="hindi" id="hindi_typing" style=" min-height:100px;"></textarea>
                        </div>
                    </div>

                    <textarea class="editor_content" name="editor_content" id="editor_content" style="visibility: hidden; min-height:500px;"><?php echo $content['content']; ?></textarea>
                    <input type="hidden" id="pk_page_id" value="<?php echo $content['pk_page_id']; ?>" />
                </div>
            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<?php echo site_url() . "public/fassets/js/translation.js" ?>"></script>
<link href="<?php echo site_url() . "public/fassets/css/translation.css" ?>" type="text/css" rel="stylesheet"/>
<script type="text/javascript">
    // Load the Google Transliteration API
    google.load("elements", "1", {
        packages: "transliteration"
    });
    function onLoad() {
        var options = {
            sourceLanguage: 'en',
            destinationLanguage: ['hi'],
            shortcutKey: 'ctrl+g',
            transliterationEnabled: true
        };
        // Create an instance on TransliterationControl with the required
        // options.
        var control = new google.elements.transliteration.TransliterationControl(options);
        // Enable transliteration in the textfields with the given ids.
        var ids = ["hindi_typing"];
        control.makeTransliteratable(ids);
        // Show the transliteration control which can be used to toggle between
        // English and Hindi and also choose other destination language.
        control.showControl('translControl');
    }
    google.setOnLoadCallback(onLoad);
</script>

<script type="text/javascript">
    var editor;
    $(document).ready(function(e) {
        if (editor)
            editor.destroy();
        editor = CKEDITOR.replace("editor_content");

        $(".btn_update").click(function() {
            var content = editor.getData();
            var post_data = {"content": content, "header_meta": $("#header_meta").val(), "pk_page_id": $("#pk_page_id").val()};
            $.post(base_url + "admin/pages/saveeditor", post_data, function(res) {
                $.gritter.add({
                    title: 'Page',
                    text: 'Page has been Updated'
                });
            });
        });

    });
</script>
