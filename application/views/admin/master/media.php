<style>
    .sp_progress{width: 100%; }
    .sp_progress .sp_prgress_in{width: 100%; height: 10px; background: #EDEDED; margin-right: 80px; border-radius: 3px;}
    .sp_progress .sp_prgress_in .sp_progress_bar{ height: 10px; background: #10a062; }
    .sp_progress .sp_progress_val{width: 70px; float: right; margin-left: 10px; line-height: 10px; font-size: 10px; text-align: right; margin-right: 10px;}
</style>
<?php

function return_bytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val) - 1]);
    switch ($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }

    return $val;
}
?>
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Media
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Master
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Media
                </li>
                <li style="float: right;">Server Max Upload Size: <?php echo return_bytes(ini_get('upload_max_filesize')) / (1024 * 1024); ?> MB &nbsp;</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="row-fluid" style="margin-bottom: 10px;">
            <div class="span10">
                <div class="sub_title logo_progress" style="display: none;">
                    <div class="sp_filename">
                        xyz.jpeg
                    </div>
                    <div class="sp_progress" style="">
                        <div class="sp_progress_val">
                            30%
                        </div>
                        <div class="sp_prgress_in">
                            <div class="sp_progress_bar" style="width: 30%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2 pull-right">
                <div  class=" btn green pull-right" onclick="$('#logo').click();"><i class="icon-plus"></i> Add Media</div>
            </div>
        </div>
        <div id="media_list"></div>
        <?php
        paginationScript(base_url() . "admin/master/media_list", "media_list", "");
        ?>

    </div>

    <form id="logoform" method="post" enctype="multipart/form-data" action="<?php echo site_url() . "admin/master/uploadmedia"; ?>">
        <input type="file" class="hide" name="logo" id="logo"  onchange="$(this.form).submit();"/>
    </form>

    <!-- END Modal on Page--> 
    <!-- END PAGE CONTAINER--> 
</div>

<script type="text/javascript">
    $(document).ready(function(e) {
        $('#logoform').ajaxForm({
            //dataType: 'json',
            beforeSubmit: function() {
                $(".sf-msg-error").html("").hide();
                $(".logo_progress").show();
                $(".logo_progress .sp_progress_val").html("0%");
                $(".logo_progress .sp_progress_bar").css("width", "0%");
                $(".sp_filename").html("Uploading: " + $("#logo").val().replace(/.*[\/\\]/, ''));
            },
            uploadProgress: function(event, position, total, percentComplete) {
                if (percentComplete == 100) {
                } else {
                    $(".logo_progress .sp_progress_bar").css("width", percentComplete + '%');
                    $(".logo_progress .sp_progress_val").html(percentComplete + '%');
                }
            },
            success: function(json) {
                $(".logo_progress").hide();
                $jdata = $.parseJSON(json);
                if ($jdata.status == "1") {
                    $.gritter.add({
                        title: "Upload Media",
                        text: $jdata.msg
                    });
                    $(".searchBtn").click();
                } else {
                    $(".sf-msg-error").html($jdata.msg).show();
                }
            }
        });
    });
</script>
