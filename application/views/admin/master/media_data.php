<div class="row-fluid">
    <div class="portlet box blue">
        <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
            <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Media List</div>
        </div>
        <div class="portlet-body">
            <div class="data">
                <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
                    <div class="search-form">
                        <div class="chat-form" style="margin-top: 0px">
                            <div class="input-cont span10" style="margin-right:0px">
                                <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                            </div>
                            <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
                        </div>
                    </div>            
                </div>
                <?php
                if ($data) {
                    ?> 
                    <table class="table table-striped table-bordered table-hover table-full-width tbl_data">
                        <thead>
                            <tr>
                                <th style="width:60px;">S. No.</th>
                                <th>Filename</th>
                                <th>Path</th>
                                <th>Upload Date</th>                                
                                <th style="width:70px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $i = $start;
                            foreach ($data as $row) {
                                ?> 
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><a href="<?php echo site_url() . MEDIA_DIR . $row['media'] ?>" target="_blank"><?php echo $row['name']; ?></a></td>
                                    <td><?php echo site_url() . MEDIA_DIR . $row['media'] ?></td>
                                    <td><?php echo date("d M, Y", strtotime($row['created_at'])) ?></td>                                    
                                    <td>
                                        <a href="#" data-id='<?php echo $row['pk_media_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>/master/media_delete" class="master_delete btn small mini red"><i class="icon-remove"></i> Delete</a>

                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php
                } else {
                    ?> <h4 class="text-center">No Media Available</h4> <?php
                }
                ?>
                <?php echo $page; ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".master_delete").easyconfirm({locale: {
            title: 'Delete Article',
            text: 'Are you sure to delete this media',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
</script>

