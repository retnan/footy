<style>
    .sp_progress{width: 100%; }
    .sp_progress .sp_prgress_in{width: 100%; height: 10px; background: #EDEDED; margin-right: 80px; border-radius: 3px;}
    .sp_progress .sp_prgress_in .sp_progress_bar{ height: 10px; background: #10a062; }
    .sp_progress .sp_progress_val{width: 70px; float: right; margin-left: 10px; line-height: 10px; font-size: 10px; text-align: right; margin-right: 10px;}
</style>
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                League
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Master
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    League
                </li>          
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="row-fluid" style="margin-bottom: 10px;">

            <div class="span3 pull-right">
                <a href="<?php echo base_url() . ADMIN_DIR; ?>/master/league_form" data-target="#modal_league_add" data-toggle="modal" class="remote btn green pull-right"><i class="icon-plus"></i> Add League</a>
            </div>
        </div>
        <div class="row-fluid">
    <div class="portlet box blue">
        <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
            <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> League List</div>
           
        </div>
        <div class="portlet-body">
        <div id="league_list"></div>
        <?php
        paginationScript(base_url() . "admin/master/league_list", "league_list", "");
        ?>
        </div>
    </div>
</div>
    </div>

    <form id="logoform" method="post" enctype="multipart/form-data" action="<?php echo site_url() . "admin/master/uploadmedia"; ?>">
        <input type="file" class="hide" name="logo" id="logo"  onchange="$(this.form).submit();"/>
    </form>

    <!-- END Modal on Page--> 
    <!-- END PAGE CONTAINER--> 
    <div id="modal_league_add" class="modal hide fade" data-width="800" data-height="450">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h3><i class="icon-reorder"></i> Add League</h3>
        </div>
        <div class="modal-body"  style="overflow:hidden; min-height:320px; ">

        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn">Close</button>
            <button type="button" class="btn green" onclick='$("#league_add").submit();'>Add League</button>
        </div>
    </div>
    <div id="modal_league_edit" class="modal hide fade" data-width="800" data-height="450">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h3><i class="icon-reorder"></i> Edit League</h3>
        </div>
        <div class="modal-body"  style="overflow:hidden; min-height:320px; ">

        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn">Close</button>
            <button type="button" class="btn green" onclick='$("#league_edit").submit();'>Update League</button>
        </div>
    </div>
</div>


