<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Category
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Master
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Category
                </li>          
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="row-fluid" style="margin-bottom: 10px;">
            <div class="span3 pull-right">
                <a href="<?php echo base_url() . ADMIN_DIR; ?>/master/category_form" data-target="#modal_category_add" data-toggle="modal" class="remote btn green pull-right"><i class="icon-plus"></i> Add Category</a>

            </div>
        </div>
        <div id="category_loader" class="loaddata" data-category="" data-type="" data-top="" data-url="<?php echo base_url() . ADMIN_DIR . "master/category_loader"; ?>"></div>

        <button type="button" class="hide" id="loadData" onclick="App.loadData($('#cat_data'))">Get Data</button>
    </div>


    <!-- Modal on Page--> 
    <div id="modal_category_add" class="modal hide fade" data-width="750" data-height="350">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h3><i class="icon-reorder"></i> Add Category</h3>
        </div>
        <div class="modal-body"  style="overflow:hidden; min-height:320px; ">

        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn">Close</button>
            <button type="button" class="btn green" onclick='$("#category_add").submit();'>Add Category</button>
        </div>
    </div>
    <div id="modal_category_edit" class="modal hide fade" data-width="750" data-height="350">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h3><i class="icon-reorder"></i> Edit Category</h3>
        </div>
        <div class="modal-body"  style="overflow:hidden; min-height:320px; ">

        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn">Close</button>
            <button type="button" class="btn green" onclick='$("#category_edit").submit();'>Update Category</button>
        </div>
    </div>

    <!-- END Modal on Page--> 
    <!-- END PAGE CONTAINER--> 
</div>