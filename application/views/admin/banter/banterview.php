<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Matchroom
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Matchroom
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Create Banter
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="row-fluid">
            <div class="portlet box blue">
                <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                    <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Create Matchroom</div>
                    <select class="m-wrap" id="email_selector1" style="float: right;display: none; width: 200px; margin: -5px -1px -10px 0px;">
                        <option value="0">All Clubs</option>
                        <?php foreach ($categories as $key => $category) {
                            ?>
                            <option value="<?php echo $key ?>" <?php echo ($key == $log['fk_club_id']) ? "selected" : "" ?>><?php echo $category ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
                <form method="post" action = "Adminbanter/persist">
                <div class="portlet-body">
                      <div class="row-fluid">
                        <div class="span3 ">
                            <div class="control-group">
                                <label class="control-label">League </label>
                                <div class="controls">
                                    <select name="league" id="league" class="m-wrap span12">
                                        <option value="">All League</option>
                                        <?php foreach ($all_leagues as $league) { ?>
                                            <option value="<?php echo $league['pk_league_id']; ?>"><?php echo $league['title']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Club One </label>
                                <div class="controls">
                                    <select name="club_one" id="email_selector" class="m-wrap span12">
                                        <option value=""></option>
                                        <?php foreach ($all_clubs as $key =>$club) { ?>
                                            <option value="<?php echo $club['title']; ?>"><?php echo $club['title']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Club Two </label>
                                <div class="controls">
                                    <select name="club_two" id="email_selector" class="m-wrap span12">
                                      <option value=""></option>
                                      <?php foreach ($all_clubs as $key => $club) { ?>
                                          <option value="<?php echo $club['title']; ?>"><?php echo $club['title']; ?></option>
                                      <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Status </label>
                                <div class="controls">
                                    <select name="status" id="email_selector" class="m-wrap span12">
                                        <option value=""></option>
                                        <option value="0">Upcoming</option>
                                        <option value="1">Live</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12" style="padding-top:5px">
                    <input type="submit" name="create" value="create" class="btn btn-danger">
                  </div>
                  </form>
                    <div class="row-fluid">
                      <!--set all matches here-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script type="text/javascript">
function load_email(){
            $(".loaddata").data("league", $("#league").val());
            $(".loaddata").data("category", $("#email_selector").val());
            $(".loaddata").data("subject", encodeURIComponent($("#subject").val()));
            $(".loaddata").data("message", encodeURIComponent(editor.getData()));
            App.loadData($(".loaddata"));
}
    $(document).ready(function(e) {
        $("#league").on("change", function() {
            var post_data = {league_id: $("#league").val()};
            $.post(base_url + "admin/article/getclubs", post_data, function(res) {
                $("#email_selector").html(res);
                $("#email_selector").closest("div").hide().fadeIn();
                load_email();

            });
        });

        $("#email_selector").on('change', function() {
           load_email();
        });
        $(".email_delete").live('click', function() {
            $container = $(".portlet")
            var post_data = $(this).data();
            App.blockUI($container);
            $.post(base_url + "admin/newsletter/deletemail", post_data, function(res) {
                var jdata = $.parseJSON(res);
                $.gritter.add({
                    title: jdata.title,
                    text: jdata.text
                });
                App.unblockUI($container);
                $(".pagination li.cpageval").addClass("inactive").click();
            });
        });
    });

</script>

<script type="text/javascript">
    var editor;
</script> -->
