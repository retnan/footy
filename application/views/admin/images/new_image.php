<style type="text/css">
    .help-inline.valid {
        display: none !important;
    }
</style>
<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="400">
    <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
    <div class="row-fluid">
        <form action="#" id="image_add" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                <label class="control-label" style="">Image <span class="required">*</span></label>
                <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-file">
                            <span class="fileupload-new">Select file to upload</span>
                            <span class="fileupload-exists">Change</span>
                            <input type="file" class="default file_file required" name="logo" id="file_file" data-msg-accept="Choose a valid file" accept="jpg|jpeg|gif|png">
                        </span>
                        <span class="fileupload-preview"></span>
                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                        <span class="help-inline file_alert file_img_alert1" for="file_file" style="margin-top: -2px; margin-left: 10px; display:none"></span>
                    </div>
                    <span class="label label-info">Note: </span> <small>Only jpg, gif, jpeg and png files are allowed</small>
                </div>
            </div>

        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_club_add .scroller");
    $('#club_add').validate({
        submitHandler: function(form) {
            add_club();
            return false;
        }
    });
</script>


