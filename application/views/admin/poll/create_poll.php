<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Opinion Poll
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-group"></i>
                    Opinion Poll
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Create Poll
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="row-fluid" style="margin-bottom: 10px;">

            <div class="span3 pull-right">
                <a href="<?php echo base_url() . ADMIN_DIR; ?>/poll" class="btn yellow pull-right"><i class="icon-plus"></i> Back To List</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet box blue">
                <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                    <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i>  Create Poll</div>

                </div>
                <div class="portlet-body">
                    <div class="row-fluid">
                        <form action="#" id="poll_add" data-url="<?php echo base_url() . ADMIN_DIR; ?>/poll/save" class="form-horizontal" enctype="multipart/form-data">
                            <div class="row-fluid">
                                <div class="alert alert-success span6" style="display: none;">
                                    Poll Created Successfully
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Title <span class="required">*</span></label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="Title" id="title" name="title" class="m-wrap required span9">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Type <span class="required">*</span></label>
                                <div class="controls">
                                    <select name="type" class="required">
                                        <option value="">Select Type</option>
                                        <option value="V">Vertical</option>
                                        <option value="H">Horizontal</option>

                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Start Date <span class="required">*</span></label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="Start Date" id="start_date" name="start_date" class="m-wrap required span7 datepicker">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">End Date <span class="required">*</span></label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="End Date" value="" id="end_date" name="end_date" class="m-wrap required span7 datepicker">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Poll Image <span class="required">*</span></label>
                                <div class="controls">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-file">
                                            <span class="fileupload-new">Select file to upload</span>
                                            <span class="fileupload-exists">Change</span>
                                            <input type="file" class="default file_file required" name="image" id="file_file" data-msg-accept="Choose a valid file" accept="jpg|jpeg|gif|png">
                                        </span>
                                        <span class="fileupload-preview"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                        <span class="help-inline file_alert file_img_alert1" for="file_file" style="margin-top: -2px; margin-left: 10px; display:none"></span>
                                    </div>
                                    <span class="label label-info">Note: </span> <small>Only jpg, gif, jpeg and png files are allowed</small>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" style="">Option1 <span class="required">*</span></label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="i.e. Yes" value="" id="option1" name="option1" class="m-wrap  span7 required">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Option2 <span class="required">*</span></label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="i.e. No" value="" id="option2" name="option2" class="m-wrap  span7 required">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Option3 </label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="" value="" id="option3" name="option3" class="m-wrap  span7 ">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Option4 </label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="" value="" id="option4" name="option4" class="m-wrap  span7 ">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Option5 </label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="" value="" id="option5" name="option5" class="m-wrap  span7 ">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style=""></label>
                                <div class="controls"  style="">
                                    <button type="button" class="btn green" onclick='$("#poll_add").submit();'>Create Poll</button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<!-- Modal on Page-->

<script type="text/javascript">
    $(document).ready(function(e) {
        $(".datepicker").datepicker({'format': 'dd/mm/yyyy', autoclose: true});
    });
    function create_poll() {
        App.blockUI($("#poll_add").parent("div"));

        var formData = new FormData($("#poll_add")[0]);
        $.ajax({
            url: $("#poll_add").data("url"),
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#poll_add").parent("div"));
//                var jdata = $.parseJSON(data);
                $("#poll_add .alert-success").show();
                $("#poll_add")[0].reset();
                setTimeout(function() {
                    $("#poll_add .alert-success").hide();
                }, 4000);
            },
            error: function(data) {
                App.unblockUI($("#poll_add").parent("div"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function update_ads() {
        App.blockUI($("#ads_edit"));

        var formData = new FormData($("#ads_edit")[0]);
        $.ajax({
            url: $("#ads_edit").data("url"),
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#ads_edit"));
                var jdata = $.parseJSON(data);
                if (jdata.status == "1") {
                    $.gritter.add({
                        title: "Ads",
                        text: jdata.msg
                    });
                    $("#modal_ads_edit .close_button").click();

                    $(".pagination li.active.cpageval").addClass("inactive").click();
                }
            },
            error: function(data) {
                App.unblockUI($("#ads_edit"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
    $(document).ready(function(e) {
        setTimeout(function() {
            $('#poll_add').validate({
                submitHandler: function(form) {
                    create_poll();
                    return false;
                }
            });
        }, 500);

    });
</script>

