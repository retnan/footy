<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Google Ads
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Ads
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Google Ads
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Google Ads Script</div>
                <div class="btn yellow pull-right" style="margin-top: -5px;" onclick="update_ads();">Update</div>
            </div>
            <div class="portlet-body">
                <div class="row-fluid">
                    <form action="#" id="ads_google_form" class="form-horizontal" enctype="multipart/form-data"
                          data-url="<?php echo base_url() . ADMIN_DIR; ?>ads/google_update">
                        <div class="control-group" style="display: none">
                            <label class="control-label" style="">Small Ad Script</label>
                            <div class="controls"  style="">
                                <textarea placeholder="Small Ad Script" rows="5" id="ad_ggl_small" name="ad_ggl_small"  class="m-wrap  span7 "><?php echo getCMSContent("ad_ggl_small"); ?></textarea>
                            </div>
                        </div>

                        <div class="control-group" style="display: none">
                            <label class="control-label" style="">Medium Ad Script</label>
                            <div class="controls"  style="">
                                <textarea placeholder="Medium Ad Script" rows="5" id="ad_ggl_mid" name="ad_ggl_mid" class="m-wrap  span7 "><?php echo getCMSContent("ad_ggl_mid"); ?></textarea>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" style="">Large Ad Script</label>
                            <div class="controls"  style="">
                                <textarea placeholder="Large Ad Script" rows="5" id="ad_ggl_large" name="ad_ggl_large" class="m-wrap  span7 "><?php echo getCMSContent("ad_ggl_large"); ?></textarea>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" style="">Square Ad Script</label>
                            <div class="controls"  style="">
                                <textarea placeholder="Square Ad Script" rows="5" id="ad_ggl_square" name="ad_ggl_square" class="m-wrap  span7 "><?php echo getCMSContent("ad_ggl_square"); ?></textarea>
                            </div>
                        </div>





                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<script type="text/javascript">

    function update_ads() {
        App.blockUI($("#ads_google_form"));

        var formData = new FormData($("#ads_google_form")[0]);
        $.ajax({
            url: $("#ads_google_form").data("url"),
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#ads_google_form"));
                var jdata = $.parseJSON(data);
                if (jdata.status == "1") {
                    $.gritter.add({
                        title: "Google Ads",
                        text: jdata.msg
                    });

                }
            },
            error: function(data) {
                App.unblockUI($("#ads_google_form"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

</script>

