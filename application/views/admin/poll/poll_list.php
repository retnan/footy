<div class="data">
    <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
        <div class="search-form">
            <div class="chat-form" style="margin-top: 0px">
                <div class="input-cont span10" style="margin-right:0px">
                    <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                </div>
                <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <?php if (count($data) > 0) { ?>
            <?php
            foreach ($data as $row) {
                $voting = $row['voting'];
                // print_r($row);
                ?>
                <div class="portfolio-block">
                    <div class="row-fluid ">
                        <div class="span2">
                            <img src="<?php echo $row['thumb'] ?>" style="width: 100%" />
                        </div>
                        <div class="span10 portfolio-text" style="overflow: visible">
                            <div class="row-fluid">
                                <div class="span10">
                                    <div class="span12" style="float: left;">
                                        <h3 style="margin:0px 0; width:auto; float: left;"><?php echo $row['title']; ?></h3>
                                        <?php if ($row['status'] != "1") { ?>
                                            <div class="label label-warning pull-right" style="margin-top: 10px;">Inactive</div>
                                        <?php } ?>

                                    </div>
                                </div>
                                <div class="span2 pull-right">
                                    <div class="task-config-btn btn-group pull-right">
                                        <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                        <ul class="dropdown-menu pull-right">

                                            <li><a href="<?php echo base_url() . ADMIN_DIR . "poll/edit_poll/" . $row['pk_poll_id']; ?>" class="remote"> <i class="icon icon-pencil"></i> Edit</a></li>

                                            <li><a href="<?php echo base_url() . ADMIN_DIR . "placement/manage/poll/" . $row['pk_poll_id'] . "/" . $row['type']; ?>"> <i class="icon icon-file"></i> Manage Placement</a></li>

                                            <?php if ($row['status'] != "1") { ?>
                                                <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "poll/actions"; ?>" data-action="1" data-id="<?php echo $row['pk_poll_id']; ?>"> <i class="icon icon-check"></i> Activate</a></li>
                                            <?php } ?>
                                            <?php if ($row['status'] == "1") { ?>
                                                <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "poll/actions"; ?>" data-action="0" data-id="<?php echo $row['pk_poll_id']; ?>"> <i class="icon icon-pause"></i> Deactivate</a></li>
                                            <?php } ?>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="span10">
                                    <h4 style="margin:0px 0; width:auto; float: left;">From <?php echo date("d/m/Y", strtotime($row['start_date'])); ?> to <?php echo date("d/m/Y", strtotime($row['end_date'])); ?></h4>

                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span10">
                                    Option 1: <span class="text-info"> <?php echo $row['option1']; ?></span>
                                    &nbsp;&nbsp;&nbsp; Option 2: <span class="text-info"> <?php echo $row['option2']; ?></span>
                                    <?php if (trim($row['option3']) != "") { ?>
                                        &nbsp;&nbsp;&nbsp; Option 3: <span class="text-info"> <?php echo $row['option3']; ?></span>
                                    <?php } ?>
                                    <?php if (trim($row['option4']) != "") { ?>
                                        &nbsp;&nbsp;&nbsp; Option 4: <span class="text-info"> <?php echo $row['option4']; ?></span>
                                    <?php } ?>
                                    <?php if (trim($row['option5']) != "") { ?>
                                        &nbsp;&nbsp;&nbsp; Option 5: <span class="text-info"> <?php echo $row['option5']; ?></span>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span10">
                                    Votes for <?php echo $row['option1']; ?>: <span class="text-success" style="font-size: 16px;"> <?php echo $voting['countA']; ?> (<?php echo $voting['perA']; ?>%)</span>
                                    &nbsp;&nbsp; Votes for <?php echo $row['option2']; ?>: <span class="text-success" style="font-size: 16px;"> <?php echo $voting['countB']; ?> (<?php echo $voting['perB']; ?>%)</span>
                                    <?php if (trim($row['option3']) != "") { ?>
                                        &nbsp;&nbsp; Votes for <?php echo $row['option3']; ?>: <span class="text-success" style="font-size: 16px;"> <?php echo $voting['countC']; ?> (<?php echo $voting['perC']; ?>%)</span>
                                    <?php } ?>
                                    <?php if (trim($row['option4']) != "") { ?>
                                        &nbsp;&nbsp; Votes for <?php echo $row['option4']; ?>: <span class="text-success" style="font-size: 16px;"> <?php echo $voting['countD']; ?> (<?php echo $voting['perD']; ?>%)</span>
                                    <?php } ?>
                                    <?php if (trim($row['option5']) != "") { ?>
                                        &nbsp;&nbsp; Votes for <?php echo $row['option5']; ?>: <span class="text-success" style="font-size: 16px;"> <?php echo $voting['countE']; ?> (<?php echo $voting['perE']; ?>%)</span>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row-fluid">
                                <div class="span10">
                                    <h3>Site Placement:</h3>
                                    <div class="clearfix"></div>
                                    Location:
                                    <?php
                                    $available = false;
                                    $curr_sub_type = array_keys($row['placement']['location']);
                                    $curr_leagues = array();
                                    if (isset($row['placement']['location'][6])) {
                                        $curr_leagues = $row['placement']['location'][6];
                                    }
                                    $curr_clubs = "";
                                    if (isset($row['placement']['location'][5])) {
                                        $curr_clubs = implode(',', $row['placement']['location'][5]);
                                    }

                                    foreach ($row['placement']['sub_types'] as $key => $value) {
                                        if (in_array($key, $curr_sub_type)) {
                                            $available = TRUE;
                                            ?>
                                            <div class="label label-mini label-info"> <?php echo $value; ?></div>

                                            <?php
                                        }
                                    }
                                    if ($curr_leagues != "" and count($curr_leagues) > 0) {
                                        ?>
                                        <br>
                                        Leagues:
                                        <?php
                                        foreach ($row['placement']['leagues'] as $key => $value) {
                                            if (in_array($key, $curr_leagues)) {
                                                $available = TRUE;
                                                ?>
                                                <div class="label label-mini label-info"> <?php echo $value; ?></div>

                                                <?php
                                            }
                                        }
                                    }

                                    if ($curr_clubs != "" and count($curr_clubs) > 0) {
                                        ?>
                                        <br>
                                        Clubs:
                                        <?php
                                        // var_dump($row);
                                        foreach ($row['placement']['current_club_names'] as $key => $value) {
                                            $available = TRUE;
                                            ?>
                                            <div class="label label-mini label-info"> <?php echo $value; ?></div>
                                            <?php
                                        }
                                        if (in_array("5", $curr_sub_type) && count($row['placement']['current_club_names']) == 0) {
                                            $available = TRUE;
                                            ?>
                                            <div class="label label-mini label-info"> All Clubs</div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php
            }
            ?>
            <?php
        }
        if (count($data) == 0) {
            ?>
            <h4 style="text-align: center">No Ads Available</h4>
        <?php }
        ?>

    </div>
    <?php echo $page; ?>
</div>


<script type="text/javascript">
    $(".article_action[data-action=3]").easyconfirm({locale: {
            title: 'Delete Article',
            text: 'Are you sure to delete this article',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
    $(".article_action").click(function(e) {
        e.preventDefault();
        $container = $(this).parents(".portfolio-block");
        App.blockUI($container);
        var post_data = $(this).data();
        $.post($(this).data("href"), post_data, function(res) {
            var jdata = $.parseJSON(res);
            $.gritter.add({
                title: jdata.title,
                text: jdata.text
            });
            App.unblockUI($container);
            $(".pagination li.cpageval").removeClass("active").addClass("inactive").click();
        });
    });
</script>