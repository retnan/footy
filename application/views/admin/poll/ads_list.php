<div class="data">
    <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
        <div class="search-form">
            <div class="chat-form" style="margin-top: 0px">
                <div class="input-cont span10" style="margin-right:0px">
                    <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                </div>
                <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <?php if (count($data) > 0) { ?>
            <?php
            foreach ($data as $ads) {
                ?>
                <div class="row-fluid  portfolio-block">
                    <div class="span2">
                        <img src="<?php echo $ads['thumb'] ?>" style="width: 100%" />
                    </div>
                    <div class="span10 portfolio-text" style="overflow: visible">
                        <div class="row-fluid">
                            <div class="span10">
                                <div class="span12" style="float: left;">
                                    <h3 style="margin:0px 0; width:auto; float: left;">From <?php echo date("d/m/Y", strtotime($ads['start_date'])); ?> to <?php echo date("d/m/Y", strtotime($ads['end_date'])); ?></h3>
                                    <?php if ($ads['status'] != "1") { ?>
                                        <div class="label label-warning pull-right" style="margin-top: 10px;">Inactive</div>
                                    <?php } ?>

                                    <span style="width: 100%; float: left;" class="event_details">
                                        <a href="<?php echo $ads['ad_url'] ?>" target="_blank"><?php echo $ads['ad_url'] ?></a>
                                    </span>
                                </div>
                            </div>
                            <div class="span2 pull-right">
                                <div class="task-config-btn btn-group pull-right">
                                    <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a  href="<?php echo $ads['thumb'] ?>" target="_blank"> <i class="icon icon-eye-open"></i> View</a></li>
                                        <li><a data-target="#modal_ads_edit" data-toggle="modal" href="<?php echo base_url() . ADMIN_DIR . "ads/ads_form/" . $ads['ad_type'] . "/" . $ads['pk_ad_id']; ?>" target="_blank" class="remote"> <i class="icon icon-pencil"></i> Edit</a></li>
                                        <li><a href="<?php echo base_url() . ADMIN_DIR . "ads/manage/" . $ads['ad_type'] . "/" . $ads['pk_ad_id']; ?>"> <i class="icon icon-file"></i> Manage Placement</a></li>
                                        <?php if ($ads['status'] != "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "ads/actions"; ?>" data-action="1" data-id="<?php echo $ads['pk_ad_id']; ?>"> <i class="icon icon-check"></i> Activate</a></li>
                                        <?php } ?>
                                        <?php if ($ads['status'] == "1") { ?>
                                            <li><a class="article_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "ads/actions"; ?>" data-action="0" data-id="<?php echo $ads['pk_ad_id']; ?>"> <i class="icon icon-pause"></i> Deactivate</a></li>
                                        <?php } ?>
                                        <li><a class="article_action" href="#" data-image="<?php echo $ads['image']; ?>" data-href="<?php echo base_url() . ADMIN_DIR . "ads/deleteaction"; ?>" data-action="3" data-id="<?php echo $ads['pk_ad_id']; ?>"> <i class="icon icon-remove"></i> Delete</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>                        
                    </div>
                </div>
                <?php
            }
            ?>
            <?php
        }
        if (count($data) == 0) {
            ?>
            <h4 style="text-align: center">No Ads Available</h4>
        <?php }
        ?>

    </div>
    <?php echo $page; ?>
</div>


<script type="text/javascript">
    $(".article_action[data-action=3]").easyconfirm({locale: {
            title: 'Delete Article',
            text: 'Are you sure to delete this article',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
    $(".article_action").click(function(e) {
        e.preventDefault();
        $container = $(this).parents(".portfolio-block");
        App.blockUI($container);
        var post_data = $(this).data();
        $.post($(this).data("href"), post_data, function(res) {
            var jdata = $.parseJSON(res);
            $.gritter.add({
                title: jdata.title,
                text: jdata.text
            });
            App.unblockUI($container);
            $(".pagination li.cpageval").removeClass("active").addClass("inactive").click();
        });
    });
</script>