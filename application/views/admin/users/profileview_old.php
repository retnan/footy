<div class="container-fluid  " >
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                <?php echo $user['name']; ?> 
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    User Profile
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <?php echo $user['name']; ?> 
                </li>          
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="row-fluid">
            <div class="portlet box blue">
                <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                    <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Details</div>
                    <div class="tools" style="margin-right: 10px;margin-top: 0px;"><a href="<?php echo base_url() . 'admin/users/'; ?>"><div class="label label-info" style="padding: 5px;">Back to List</div></a></div>
                </div>
                <div class="portlet-body">
                    <div class="row-fluid">
                        <div class="span2">
                            <img src="<?php echo $user['profile_pic']; ?>" class=" img-polaroid" style="width: 100%;">
                        </div>
                        <div class="span6">
                            <div class="row-fluid">
                                <h3 style="margin: 0px 0px;"><?php echo $user['name']; ?> </h3>                               
                            </div>
                            <div class="row-fluid">
                                <div class="span12">
                                    <?php if ($user['gender'] != "") { ?>
                                        Gender: <span class="text-info"><?php echo $user['gender']; ?> </span> &nbsp; &nbsp;

                                    <?php } ?>
                                    <?php if ($user['dob'] != "" and $user['dob'] != '0000-00-00') { ?>
                                        Date of Birth: <span class="text-info"><?php echo date('M d, Y', strtotime($user['dob'])); ?> </span> &nbsp; &nbsp;

                                    <?php } ?>

                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span12"> <?php if ($user['username'] != "") { ?>
                                        Username: <span class="text-info"><?php echo $user['username']; ?> </span> &nbsp; &nbsp;

                                    <?php } ?>
                                    Email: <span class="text-info"><?php echo $user['email']; ?></span> &nbsp; &nbsp;



                                    <?php if ($user['phone_no'] != "") { ?>
                                        Phone No.: <span class="text-info"><?php echo $user['phone_no']; ?></span> &nbsp; &nbsp;

                                    <?php } ?>
                                    <div class="clearfix"></div>

                                </div>
                            </div>
                            <div class="row-fluid margin-top-20">
                                <div class="span12"> 

                                    <?php if ($user['fk_club_id'] > 0) { ?>
                                        Club joined:  &nbsp; &nbsp;
                                        <div class="label label-warning"><?php echo $clubs[$user['fk_club_id']]; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <strong>About: </strong><?php echo nl2br($user['about_me']); ?>
                            </div>
                        </div> 
                        <div class="span4 margin-top-10">
                            <div class="portlet sale-summary">
                                <div class="portlet-title">
                                    <div class="caption">Highlights</div>
                                </div>
                                <div class="portlet-body">
                                    <ul class="unstyled">
                                        <li>
                                            <span class="sale-info">Followers</span> 
                                            <span class="sale-num">5</span>
                                        </li>
                                        <li>
                                            <span class="sale-info">Points</span> 
                                            <span class="sale-num">550</span>
                                        </li>
                                        <li>
                                            <span class="sale-info">Articles Written</span> 
                                            <span class="sale-num">20</span>
                                        </li>
                                        <li>
                                            <span class="sale-info">Clubs</span> 
                                            <span class="sale-num">20 </span>
                                        </li>
                                        <li style="display:none;">
                                            <span class="sale-info">League</span> 
                                            <span class="sale-num">10</span>
                                        </li>

                                        <li>
                                            <span class="sale-info">Following</span> 
                                            <span class="sale-num">100</span>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix" style="margin-top: 20px;"></div>


                </div>
            </div>
        </div>
        <div class="portlet box green">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Article List</div>
            </div>
            <div class="portlet-body">
                <div id="article_loader" class="loaddata" data-featured="0" data-sponsored="0" data-club_id="0" data-post_type='A' data-league_id="0" data-user_id='<?php echo $user['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR . "article/article_loader"; ?>"></div>
            </div></div>
    </div>
</div>



