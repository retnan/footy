<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="200">
     <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
                <div class="row-fluid">
                    <form action="#" id="country_edit" class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label" style="">Country Name <span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" placeholder="Country Name" id="country" name="country" value="<?php echo $con_data['country']; ?>" class="m-wrap required span7">
                                <input type="hidden" name="pk_country_id" value="<?php echo $id;?>"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
<script type="text/javascript">
  init_scroll("#modal_country_edit .scroller");
    $('#country_edit').validate({
        submitHandler: function(form) {
            update_country();
            return false;
        }
    });
</script>


