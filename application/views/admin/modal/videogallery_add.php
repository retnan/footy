<style type="text/css">
    .help-inline.valid {
        display: none !important;
    }
</style>
<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="350">
    <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
    <div class="row-fluid">
        <form action="#" id="video_add" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                <label class="control-label" style="">Title <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Title" id="title" name="title" class="m-wrap required span8">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Youtube Link <span class="required">*</span></label>
                <div class="controls">
                    <input type="text" placeholder="Youtube Link" id="youtube_link2" name="youtube_link2" class="m-wrap span8">
                    <input type="text" placeholder="Youtube Link" id="youtube_link" name="youtube_link" class="m-wrap  span8" style="display: none;">
                    <span for="youtube_link" class="help-inline hide">This field is required.</span>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_club_add .scroller");
    $('#video_add').validate({
        submitHandler: function(form) {
            add_video();
            return false;
        }
    });


</script>


