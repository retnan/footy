<style type="text/css">
    .help-inline.valid {
        /*display: none !important;*/
    }
</style>
<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="250">
    <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
    <div class="row-fluid">
        <form action="#" id="page_edit" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                <label class="control-label" style="">Title <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Title" id="title" name="title" class="m-wrap required span7" value="<?php echo $page[0]['title'] ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">Slug <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Slug" id="slug" name="slug" class="m-wrap required span7 " value="<?php echo $page[0]['slug'] ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">Order <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Order" id="order" name="order" class="m-wrap required span7 numberonly" value="<?php echo $page[0]['menu_order'] ?>">
                </div>
            </div>
            <input type="hidden" name="pk_page_id" value="<?php echo $page[0]['pk_page_id']; ?>" />
        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_page_edit .scroller");
    $('#page_edit').validate({
        submitHandler: function(form) {
            edit_page();
            return false;
        }
    });
</script>


