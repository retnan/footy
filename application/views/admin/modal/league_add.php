<style type="text/css">
    .help-inline.valid {
        display: none !important;
    }
</style>
<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="400">
    <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
    <div class="row-fluid">
        <form action="#" id="league_add" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                <label class="control-label" style="">League Name <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="League Name" id="title" name="title" class="m-wrap required span7">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">League Logo <span class="required">*</span></label>
                <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-file">
                            <span class="fileupload-new">Select file to upload</span>
                            <span class="fileupload-exists">Change</span>
                            <input type="file" class="default file_file required" name="logo" id="file_file" data-msg-accept="Choose a valid file" accept="jpg|jpeg|gif|png">
                        </span>
                        <span class="fileupload-preview"></span>
                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                        <span class="help-inline file_alert file_img_alert1" for="file_file" style="margin-top: -2px; margin-left: 10px; display:none"></span>
                    </div>
                    <span class="label label-info">Note: </span> <small>Only jpg, gif, jpeg and png files are allowed</small>
                </div>
            </div>



            <div class="control-group">
                <label class="control-label" style="">Description</label>
                <div class="controls" style="">
                    <textarea class="m-wrap  span9" name="description" id="description" style=" min-height:80px;"></textarea>
                </div>
            </div>


        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_league_add .scroller");
    $('#league_add').validate({
        submitHandler: function(form) {
            add_league();
            return false;
        }
    });
</script>


