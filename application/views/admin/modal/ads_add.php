<style type="text/css">
    .help-inline.valid {
        display: none !important;
    }
</style>
<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="420">
    <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
    <div class="row-fluid">
        <form action="#" id="ads_add" class="form-horizontal" enctype="multipart/form-data"
              data-url="<?php echo base_url() . ADMIN_DIR; ?>ads/ads_save/<?php echo $type ?>/0">
            <div class="control-group">
                <label class="control-label" style="">Start Date <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Start Date" id="start_date" name="start_date" class="m-wrap required span7 datepicker">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">End Date <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="End Date" value="" id="end_date" name="end_date" class="m-wrap required span7 datepicker">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">Link <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Link" value="" id="ad_url" name="ad_url" class="m-wrap required span7 url">
                </div>
            </div>

            <input type="hidden" name="type" value="<?php echo $type; ?>" />

            <div class="control-group">
                <label class="control-label" style="">Ads Image <span class="required">*</span></label>
                <div class="controls"  style="">

                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 180px;">

                            <img src="<?php echo base_url() . NO_IMAGE; ?>" alt="" />
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
                            <span class="btn btn-file"><span class="fileupload-new">Pick Image</span>
                                <span class="fileupload-exists">Change</span>
                                <input type="file" name="logo" accept="jpg|gif|png|JPG|GIF|PNG" data-msg-accept="Please choose jpg, gif or png image." class="default required" /></span>
                            <a href="#" class="btn fileupload-exists red" data-dismiss="fileupload"><i class="icon-remove"></i> </a>
                        </div> <span for="logo" style="color: #b94a48;" class="help-inline hide">Please choose jpg, gif or png image.</span>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_ads_add .scroller");
    $(".datepicker").datepicker({'format': 'dd/mm/yyyy', autoclose: true});
    $('#ads_add').validate({
        submitHandler: function(form) {
            add_ads();
            return false;
        }
    });
</script>


