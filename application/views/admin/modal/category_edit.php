<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="340">
    <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
    <div class="row-fluid">
        <form action="#" id="category_edit" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                <label class="control-label" style="">Category Name <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Category Name" value="<?php echo $cat_data['category']; ?>" id="category" name="category" class="m-wrap required span7">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">Category Slug <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Category Slug" value="<?php echo $cat_data['slug']; ?>" id="slug" name="slug" class="m-wrap required span7">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" style="">Header Meta</label>
                <div class="controls" style="">
                    <textarea class="m-wrap  span12" name="header_meta" id="header_meta" style=" min-height:100px;"><?php echo $cat_data['header_meta']; ?></textarea>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Category Level</label>
                <div class="controls">
                    <select name="und">
                        <option value="">Parent Category</option>
                        <?php foreach ($cats as $cat) { ?>
                            <option value="<?php echo $cat['pk_category_id'] ?>" <?php echo ($cat['pk_category_id'] == $cat_data['parent']) ? 'selected' : '' ?>><?php echo $cat['category'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <input type="hidden" name="cat_id" value="<?php echo $id; ?>"/>
        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_category_edit .scroller");
    $('#category_edit').validate({
        submitHandler: function(form) {
            update_category();
            return false;
        }
    });
</script>


