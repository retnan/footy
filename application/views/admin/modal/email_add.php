<style type="text/css">
    .help-inline.valid {
        display: none !important;
    }
</style>
<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="340">
    <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
    <div class="row-fluid">
        <form action="#" id="email_add" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                <label class="control-label" style="">Email <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Email" id="email" name="email" class="m-wrap required span7">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Club</label>
                <div class="controls">
                    <select name="category">
                        <option value="">All Clubs</option>
                        <?php foreach ($categories as $k => $cat) { ?>
                            <option value="<?php echo $k ?>"><?php echo $cat ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>           

        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_email_add .scroller");
    $('#email_add').validate({
        submitHandler: function(form) {
            add_email();
            return false;
        }
    });
</script>


