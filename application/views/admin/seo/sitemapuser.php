<?php echo '<?xml version="1.0" encoding="UTF-8" ?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo base_url(); ?></loc>
        <priority>1.0</priority>
    </url>
    <?php
    foreach ($data['post'] as $dt) {
        ?>
        <url>
            <loc><?php echo base_url() . "followers/" . $dt['slug']; ?></loc>
            <priority>0.7</priority>
        </url>
    <?php } ?>
</urlset>