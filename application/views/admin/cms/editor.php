<?php
$category = array(
    'contact' => 'Contact',
    'privacy' => 'Privacy',
    'terms' => 'Terms of Use',
    '404' => 'Not Found',
    'legal' => 'Fegal',
    'contactus' => 'Contact Us',
    'faq' => "FAQ's",
    'feedback' => 'Feedback Text'
);
?> 
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                <?php echo $category[$type] ?>
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    CMS
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <?php echo $category[$type] ?>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Edit Content</div>
                <div class="btn yellow pull-right btn_update" style="margin-top: -5px;">Update</div>
            </div>
            <div class="portlet-body">
                <div class="row-fluid">
                    <textarea class="editor_content" name="editor_content" id="editor_content" style="visibility: hidden; min-height:500px;"><?php echo $content; ?></textarea>
                    <input type="hidden" id="content_type" value="<?php echo $type; ?>" />
                </div>
            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<script type="text/javascript">
    var editor;
    $(document).ready(function(e) {
        if (editor)
            editor.destroy();
        editor = CKEDITOR.replace("editor_content");

        $(".btn_update").click(function() {
            var content = editor.getData();
            var post_data = {"content": content, "type": $("#content_type").val()};
            $.post(base_url + "admin/cms/saveeditor", post_data, function(res) {
                $.gritter.add({
                    title: 'CMS',
                    text: 'Content has been Updated'
                });
            });
        });

    });
</script>
