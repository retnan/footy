<style>
    ul.videogallery{margin: -5px; padding: 0; }
    ul.videogallery li {width:33.3%; float: left;  display:block;}
    ul.videogallery li .singlecontent{padding: 5px; }
    .imgtitle{padding: 4px; font-size: 14px;  text-overflow: ellipsis;

  /* Required for text-overflow to do anything */
  white-space: nowrap;
  overflow: hidden;}
</style>

<div class="row-fluid">
    <div class="portlet box blue">
        <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
            <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Video List</div>
        </div>
        <div class="portlet-body">
            <div class="data">
                <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
                    <div class="search-form">
                        <div class="chat-form" style="margin-top: 0px">
                            <div class="input-cont span10" style="margin-right:0px">
                                <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                            </div>
                            <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
                        </div>
                    </div>            
                </div>
                <?php
                if ($data) {
                    ?> 

                    <ul class="videogallery">

                        <?php
                        $i = $start;
                        foreach ($data as $row) {
                            $image = IMG_GALLERY . $row['file_name'];
                            if (!is_file($image)) {
                                $image = NO_IMAGE;
                            }
                            ?> 
                            <li>
                                <div class="singlecontent">
                                    <div>
                                        <img src="<?php echo site_url() . $image ?>" style="width: 100%;" />
                                    </div>
                                    <div class="imgtitle"><?php echo $row['title'] ?></div>
                                    <div class="imgnav">
                                        <div class="btn btn-mini mini red pull-right delete_video" data-id="<?php echo $row['pk_gallery_id']; ?>" style="margin-left: 5px;">Delete</div>
                                        <?php if ($row['status'] == "1") { ?>
                                            <a href="#" data-id='<?php echo $row['pk_gallery_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>cms/gallery_status/0" class="master_status btn small mini yellow pull-right"> Deactivate </a>
                                        <?php } else { ?>
                                            <a href="#" data-id='<?php echo $row['pk_gallery_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>cms/gallery_status/1" class="master_status btn mini green pull-right"> Activate </a>
                                        <?php } ?>
                                    </div>
                                    <div class="clear clearfix"></div>
                                </div>
                                <div class="clear clearfix"></div>
                                <hr style="margin: 5px;">
                            </li>
                        <?php } ?>

                        <?php
                    } else {
                        ?> <h4 class="text-center">No Video Available</h4> <?php
                    }
                    ?>
                </ul>
                <div class="clear clearfix"></div>
                <?php echo $page; ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".delete_video").easyconfirm({locale: {
            title: 'Delete Video',
            text: 'Are you sure to delete this video',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
    $(".delete_video").click(function() {
        var post_data = {id: $(this).data("id")};
        $.post(base_url + "admin/cms/gallery_delete", post_data, function(res) {
            var jdata = $.parseJSON(res);
            if (jdata.status == "1") {
                $(".cpageval").removeClass("active").addClass("inactive").click();
                $.gritter.add({
                    title: res.title,
                    text: res.text
                });
            }
        });
    });
</script>

