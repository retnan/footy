<?php
$data = getCMSContents("tiles");
$i = 1;

foreach ($data as $key => $dt) {

    $dt = unserialize($dt);
    $dt['pic'] = (isset($dt['pic'])) ? $dt['pic'] : "";
    $dt['link_title'] = (isset($dt['link_title'])) ? $dt['link_title'] : "";
    $dt['link'] = (isset($dt['link'])) ? $dt['link'] : "";
    $dt['footer_text'] = (isset($dt['footer_text'])) ? $dt['footer_text'] : "";
    $dt['title'] = (isset($dt['title'])) ? $dt['title'] : "";
    $dt['description'] = (isset($dt['description'])) ? $dt['description'] : "";
    ?>
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="icon-reorder"></i>Tiles <?php echo $i++; ?></div>
            <div class="btn mini red pull-right remove_tile" style="margin:-5px 0 -5px 0; " data-id="<?php echo $key; ?>">Remove</div>
        </div>
        <div class="portlet-body">
            <form class="form-horizontal">
                <div class="row-fluid">
                    <div class="span4">
                        <div class="row-fluid">
                            <input type="hidden" name="current_image" value="<?php echo $dt['pic']; ?>">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 180px;">
                                    <?php
                                    if (is_file(PUBLIC_DIR . LOGO_DIR . "/" . $dt['pic'])) {
                                        $img_title = "Change Image";
                                        ?>
                                        <img src="<?php echo base_url() . PUBLIC_DIR . LOGO_DIR . "/" . $dt['pic']; ?>" alt="" />
                                        <?php
                                    } else {
                                        $img_title = "Select Image";
                                        ?>
                                        <img src="<?php echo base_url() . NO_IMAGE; ?>" alt="" />
                                    <?php } ?>

                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-file"><span class="fileupload-new"><?php echo $img_title; ?></span>
                                        <span class="fileupload-exists">Change</span>
                                        <input type="file" name="logo" accept="jpg|gif|png" data-msg-accept="Please choose jpg, gif or png image." class="default" /></span>
                                    <a href="#" class="btn fileupload-exists red" data-dismiss="fileupload"><i class="icon-remove"></i> </a>
                                </div> <span for="logo" style="color: #b94a48;" class="help-inline hide">Please choose jpg, gif or png image.</span>
                            </div>
                        </div>
                    </div>
                    <div class="span8">
                        <div class="control-group">
                            <label class="control-label" style="">Link Title <span class="required">*</span></label>
                            <div class="controls"  style="">
                                <input type="text" placeholder="Link Title" id="link_title" name="link_title" class="m-wrap required span10" value="<?php echo $dt['link_title']; ?>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" style="">Link <small>(Optional)</small></label>
                            <div class="controls"  style="">
                                <input type="text" placeholder="Link" id="link" name="link" class="m-wrap url span10"  value="<?php echo $dt['link']; ?>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" style="">Title <span class="required">*</span></label>
                            <div class="controls"  style="">
                                <input type="text" placeholder="Title" id="title" name="title" class="m-wrap required span10"  value="<?php echo $dt['title']; ?>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" style="">Footer Text <span class="required">*</span></label>
                            <div class="controls"  style="">
                                <input type="text" placeholder="Footer Text" id="footer_text" name="footer_text" class="m-wrap required span10"  value="<?php echo $dt['footer_text']; ?>">
                            </div>
                        </div>
                        <input type="hidden" value="<?php echo $key ?>" name="pk_cms_id" />
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label" style="">Description <span class="required">*</span></label>
                        <div class="controls"  style="">
                            <textarea type="text" placeholder="Description" id="description" name="description" class="m-wrap required span10"><?php echo $dt['description']; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="btn green update_tile pull-right">Update</div>
                </div>
            </form>
        </div>
    </div>
<?php } ?>
<script type="text/javascript">
    $(".update_tile").click(function() {
        var form = $(this).parents('form');
        if (form.valid()) {
            update_tiles(form);
        }
    });



    $(".remove_tile").easyconfirm({locale: {
            title: 'Remove Tile?',
            text: 'Are you sure to remove this tile?',
            button: ['No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
</script>

