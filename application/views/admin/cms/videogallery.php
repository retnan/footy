<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                CMS
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Master
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Video Gallery
                </li>          
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="row-fluid" style="margin-bottom: 10px;">

            <div class="span3 pull-right">
                <a href="<?php echo base_url() . ADMIN_DIR; ?>cms/video_form" data-target="#modal_video_add" data-toggle="modal" class="remote btn green pull-right"><i class="icon-plus"></i> Add Video</a>
            </div>
        </div>
        <div id="club_list"></div>
        <?php
        paginationScript(base_url() . "admin/cms/video_list", "club_list", "");
        ?>

    </div>



    <!-- END Modal on Page--> 
    <!-- END PAGE CONTAINER--> 
    <div id="modal_video_add" class="modal hide fade" data-width="650" data-height="350">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h3><i class="icon-reorder"></i> Add Video</h3>
        </div>
        <div class="modal-body"  style="overflow:hidden; min-height:320px; ">

        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btnmodalclose">Close</button>
            <button type="button" class="btn green" onclick='$("#video_add").submit();'>Save</button>
        </div>
    </div>
</div>


<script type="text/javascript">
    function matchYoutubeUrl(url) {
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        var matches = url.match(p);
        if (matches) {
            return matches[1];
        }
        return false;
    }
    function add_video() {
        var url = $("#youtube_link2").val();
        var id = matchYoutubeUrl(url);
        if (id != false) {
            $("#youtube_link").val(id).blur();
            $("#youtube_link").closest(".control-group").addClass("success").removeClass("error");
            $("#youtube_link").closest(".control-group").find("span.help-inline").html("").addClass("hide").hide();
            var post_data = $("#video_add").serialize();
            App.blockUI($("#video_add"));
            $.post(base_url + "admin/cms/savegallery", post_data, function(res) {
                App.unblockUI($("#video_add"));
                $jdata = $.parseJSON(res);
                if ($jdata.status == "1") {
                    $.gritter.add({
                        title: "Gallery",
                        text: $jdata.msg
                    });
                    $(".searchBtn").click();
                    $("#modal_video_add .btnmodalclose").click();
                } else {

                }
            });
        } else {
            $("#youtube_link").val('').blur();
            $("#youtube_link").closest(".control-group").addClass("error").removeClass("success");
            $("#youtube_link").closest(".control-group").find("span.help-inline").html("Invalid youtube URL").removeClass("hide").show();
        }
    }
</script>
