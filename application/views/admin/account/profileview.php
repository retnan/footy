<div class="portlet box blue">
    <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
        <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> My Profile</div>
    </div>
    <div class="portlet-body">


        <div class="profile" style="min-height: 200px">
            <div class="span3"> 
                <img src="<?php echo $profile['thumb']; ?>" style="width: 100%;" />
            </div>
        </div>

        <div class="span9">
            <div class="row-fluid">
                <div class="row-fluid">
                    <div class="span10"><h2 style="margin-top: 0px"><?php echo $profile['firstname'] . " " . $profile['lastname']; ?></h2></div>
                    <div class="span2 pull-right">
                        <div class="btn blue mini pull-right profile_view" onclick="$('.profile_view').hide();
                                $('.profile_edit').fadeIn();"><i class="icon-edit"></i> Edit</div>  
                        <div class="btn gray mini pull-right profile_edit" onclick="$('.profile_edit').hide();
                                $('.profile_view').fadeIn();" style="display:none">Cancel Edit</div>
                    </div>
                </div>

            </div>
            <div class="row-fluid profile_view">
                <div class="clearfix"></div>
                <blockquote class="hero">
                    <div class="clearfix"></div>                            
                    <h4>Company Name: <span class="text"> <?php echo 'Test'; ?>   </span></h4>
                    <div class="clearfix"></div>
                </blockquote>
                <?php if ($profile['fk_profession_id'] = '2') { ?>
                    <div class="clearfix"></div>
                    <blockquote class="hero">
                        <div class="clearfix"></div>                            
                        <h4>Speciality: <span class="text"> <?php echo $profile['speciality']; ?>   </span></h4>
                        <div class="clearfix"></div>
                    </blockquote>
                    <div class="clearfix"></div>
                    <blockquote class="hero">
                        <div class="clearfix"></div>                            
                        <h4>Segment: <span class="text"> <?php echo $profile['profession']; ?>   </span></h4>
                        <div class="clearfix"></div>
                    </blockquote>
                <?php } ?>
            </div>
            <div class="row-fluid profile_edit" style="display:none">
                <form action="#" class="form-horizontal" id="form_profile_edit" name="form_profile_edit">
                    <div class="control-group">
                        <label class="control-label">First Name<span class="required">*</span> </label>
                        <div class="controls">
                            <input type="text" placeholder="First Name" id="first_name" name="first_name" value="<?php echo $profile['firstname'] ?>" class="m-wrap span8 required" />                                
                        </div>                        
                    </div>
                    <div class="control-group">
                        <label class="control-label">Last Name<span class="required">*</span> </label>
                        <div class="controls">
                            <input type="text" placeholder="Last Name" id="last_name" name="last_name" value="<?php echo $profile['lastname'] ?>" class="m-wrap span8 required" />                                
                        </div>                        
                    </div>

                </form>
            </div>

        </div>      
        <?php //print_r($profile); ?>      
        <!--end span9-->
    </div>
    <div class="clearfix"></div>
</div>





