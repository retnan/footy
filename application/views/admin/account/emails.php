<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Email Setting
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Account
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Email Setting
                </li>          
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <style type="text/css">
        .form-horizontal .control-label{
            width: 180px;
        }
        .form-horizontal .controls {
            margin-left: 200px;
        }
    </style>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="row-fluid">
            <div class="portlet box blue">
                <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                    <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Email Setting</div>
                    <select class="m-wrap" id="email_selector" style="float: right; width: 200px; margin: -5px -1px -10px 0px;">
                        <?php foreach ($emails as $email) {
                            ?>
                            <option value="<?php echo $email['pk_email_id'] ?>"><?php echo $email['title'] ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
                <div class="portlet-body">
                    <div class="row-fluid">
                        <div class=" loaddata" data-url="<?php echo site_url() . "admin/account/getemail" ?>" data-id="1">

                        </div>
                    </div>                 
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(e) {
        $("#email_selector").on('change', function() {
            $(".loaddata").data("id", $(this).val());
            App.loadData($(".loaddata"));
        });
    });

</script>

<script type="text/javascript">
    var editor;
</script>