<?php $hints = explode("|", $email['hint']);
?>
<div class="row-fluid">
    <?php foreach ($hints as $hint) { ?>
        <div style="text-info"><?php echo $hint; ?></div>
    <?php } ?>
</div>


<div class="row-fluid " style="margin-top: 10px;">
    <form action="#" class="form-horizontal" id="form_profile_edit" name="form_email_edit" style="margin-bottom: 0px;">
        <div class="control-group margin-top-10">
            <textarea id="editor_content"><?php echo $email['content']; ?></textarea>
            <input type="hidden" id="pk_email_id" name="pk_email_id" value="<?php echo $email['pk_email_id']; ?>" />
        </div>
        <div class="control-group margin-top-10" style="margin-bottom: 0px;">
            <label class="control-label"></label>
            <div class="controls">
                <button type="button" id="btn_update" class="btn green pull-right"><i class="icon-save"></i> Update</button>
            </div>
        </div>
    </form>
</div>

<!--end span9-->
</div>
<div class="clearfix"></div>
</div>





<script type="text/javascript">

    $(document).ready(function(e) {
        //if (editor)
        // editor.destroy();
        editor = CKEDITOR.replace("editor_content");
        $("#btn_update").click(function() {
            var content = editor.getData();
            var post_data = {"content": content, "id": $("#pk_email_id").val()};
            $.post(base_url + "admin/account/saveemail", post_data, function(res) {
                $.gritter.add({
                    title: 'Email Setting',
                    text: 'Email Setting has been Updated'
                });
            });
        });

    });
</script>