<div class="data">
    <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
        <div class="search-form">
            <div class="chat-form" style="margin-top: 0px">
                <div class="input-cont span10" style="margin-right:0px">
                    <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                </div>
                <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <?php if (count($data) > 0) { ?>
            <?php
            foreach ($data as $review) {
                $image = parseUserImage($review['fbid'], $review['upic'], $review['ugender'], "large");
                ?>
                <div class="row-fluid  portfolio-block">
                    <div class="span2">
                        <img src="<?php echo $image ?>" style="width: 100%" />
                    </div>
                    <div class="span10 portfolio-text" style="overflow: visible">
                        <div class="row-fluid">
                            <div class="span9">
                                <div class="span12" style="float: left;">
                                    <h4 style="margin:3px 0;"><?php echo $review['uname']; ?></h4>

                                    <span style="width: auto; float: left;" class="event_details">
                                        <h5 style="margin:0px 0px; margin-top: -2px;">
                                            <i class="icon-calendar"></i> <?php echo date("M d, Y h:i A", strtotime($review['created_at'])); ?>
                                        </h5></span>
                                </div>
                            </div>
                            <?php if ($review['status'] == "0") { ?>
                                <div class="span1"><span class="label label-success">Pending</span></div>
                            <?php } ?>
                            <div class="span2 pull-right">
                                <div class="task-config-btn btn-group pull-right">
                                    <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <?php if ($review['status'] != "1") { ?>
                                            <li><a class="review_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "reviews/actions"; ?>" data-action="1" data-id="<?php echo $review['id']; ?>"> Approve</a></li>
                                        <?php } ?>
                                        <?php if ($review['status'] != "2") { ?>
                                            <li><a class="review_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "reviews/actions"; ?>" data-action="2" data-id="<?php echo $review['id']; ?>"> Reject</a></li>
                                        <?php } ?>
                                        <li><a class="review_action" href="#" data-href="<?php echo base_url() . ADMIN_DIR . "reviews/actions"; ?>" data-action="3" data-id="<?php echo $review['id']; ?>"> Delete</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>                        
                        <div class="clearfix"></div>
                        <p><?php echo $review['review'] ?></p>
                    </div>
                </div>
                <?php
            }
            ?>
            <?php
        }
        if (count($data) == 0) {
            ?>
            <h4 style="text-align: center">No Review Available</h4>
        <?php }
        ?>

    </div>
    <?php echo $page; ?>
</div>


<script type="text/javascript">
    $(".review_action[data-action=3]").easyconfirm({locale: {
            title: 'Delete Review',
            text: 'Are you sure to delete this review',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
    $(".review_action").click(function(e) {
        e.preventDefault();
        $container = $(this).parents(".portfolio-block");
        App.blockUI($container);
        var post_data = $(this).data();
        $.post($(this).data("href"), post_data, function(res) {
            App.unblockUI($container);
            $container.slideUp(function() {
                $(this).remove();
            });
        });
    });
</script>