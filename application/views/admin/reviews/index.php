<style>
    .portfolio-text h5{color: #1570A6;}
</style>
<?php $types = array("1" => "1", "2" => "0", "3" => "2"); ?>
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                <span class="text-info"> Reviews</span>
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Dashboard
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Reviews
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <style type="text/css">
        @media (min-width: 480px){
            .form-horizontal .control-label{
                width: 180px;
            }
            .form-horizontal .controls {
                margin-left: 200px;
            }
        }
    </style>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->
    <div class="page-content-body">
        <div class="portlet box green fest_list_container">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>Reviews List</div>
            </div>
            <div class="portlet-body">
                <div class="row-fluid">
                    <div class="span12">
                        <!--BEGIN TABS-->
                        <div id="review_list"></div>
                        <?php
                        paginationScript(base_url() . ADMIN_DIR . "reviews/review_list", "review_list", "type=" . $type);
                        ?>
                        <!--END TABS-->
                    </div>

                </div>

            </div>
        </div>
        <div class="fest_form_container" style="display: none" data-url="<?php echo base_url() . "colleges/fest/form" ?>" data-id="">

        </div>
    </div>
    <!-- END PAGE CONTAINER-->
</div>
