<?php

class viewer {

    private $ci;

    function __construct() {
        $this->ci = & get_instance();
    }

    public function fview($view, $data = array(), $layout = true) {
        if ($layout == FALSE) {
            $this->ci->load->view("front/" . $view, $data);
        } else {
            $data['content'] = $this->ci->load->view("front/" . $view, $data, TRUE);
            if (!isset($data['menu'])) {
                $data['menu'] = '0-0';
            }
            $this->ci->load->model("master_model");
            $this->ci->load->model("user_model");
            $data['data_league'] = $this->ci->master_model->getHeaderLeagueClub();           
            $data['counts'] = $this->ci->user_model->getHeaderCounts(); 
            $data['noti_header'] = $this->ci->user_model->getNotifications($this->ci->session->userdata("user_id"), 1, 5, false);
            $league_id=  $this->ci->session->userdata("site_league");
            //echo $this->ci->session->userdata("site_league")." Header";
            if($league_id=="" or $league_id=="0"){
                $league_id="1";
            }
           
            $clubs=$this->ci->master_model->getHeaderLeagueClub($league_id);  
            $data['club_list'] = $clubs[$league_id];
            $this->ci->load->view('front/includes/layout.php', $data);
        }
    }

    public function loginview($view, $data = array(), $layout = true) {
        if ($layout == FALSE) {
            $this->ci->load->view("front/" . $view, $data);
        } else {
            $data['content'] = $this->ci->load->view("front/" . $view, $data, TRUE);
            $this->ci->load->view('front/includes/layoutlogin.php', $data);
        }
    }

    public function cview($view, $data = array(), $layout = true) {
        if ($layout == FALSE) {
            $this->ci->load->view("colleges/" . $view, $data);
        } else {
            $data['content'] = $this->ci->load->view("colleges/" . $view, $data, TRUE);
            if (!isset($data['menu'])) {
                $data['menu'] = '0-0';
            }
            $this->ci->load->view('colleges/includes/layout.php', $data);
        }
    }

    public function aview($view, $data = array(), $layout = true) {
        if ($layout == FALSE) {
            $this->ci->load->view("admin/" . $view, $data);
        } else {
            $data['content'] = $this->ci->load->view("admin/" . $view, $data, TRUE);
            if (!isset($data['menu'])) {
                $data['menu'] = '0-0';
            }
            $this->ci->load->view('admin/includes/layout.php', $data);
        }
    }

    public function uview($view, $data, $layout = true) {
        if ($layout == FALSE) {
            $this->ci->load->view("user/" . $view, $data);
        } else {
            $data['content'] = $this->ci->load->view("user/" . $view, $data, TRUE);
            if (!isset($data['menu'])) {
                $data['menu'] = '0-0';
            }
            $this->ci->load->view('user/includes/layout.php', $data);
        }
    }

    public function emailview($view, $data = array()) {
        $data['content'] = $this->ci->load->view("email/" . $view, $data, TRUE);
        return $this->ci->load->view('email/layout.php', $data, TRUE);
    }

}

?>
