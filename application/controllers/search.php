<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('auth_model');
        $this->load->model('master_model');
        $this->load->model('video_model');
    }

    public function index($param = '') {
        $cat_data = $this->category_model->getCategories();
        $this->viewer->fview('search.php', array('cat_data' => $cat_data));
    }

    public function view($param = '') {
        echo "This is FRONT VIEW";
        // $this->fviewer->fview('login.php', array('users' => "Sheetal"));
    }

    public function category($cat_id = 1) {
        $popular = $data = $this->video_model->getPopularVideos(5);
        $liked = $data = $this->video_model->getVideosByOrder(1, 3, '', 'liked');
        $recent = $data = $this->video_model->getRecentWatch(1, 5, '', '');
        $top = $this->master_model->getTopCategories();

        $data = array(
            'section' => array(
                '0' => array(
                    'title' => 'Recent Watch',
                    'data' => $recent['data']
                ),
                '1' => array(
                    'title' => 'Popular',
                    'data' => $popular['data']
                ),
                '2' => array(
                    'title' => 'Most Liked',
                    'data' => $liked['data']
                ),
            ),
            'topcats' => $top,
            'category' => $cat_id
        );
        $this->viewer->fview('category.php', $data);
    }

    public function video() {
        $search = $this->input->get('q');
        $category = $this->input->get('cat');
        $college = $this->input->get('col');
        $fest = $this->input->get('f');
        $recent = $data = $this->video_model->getRecentWatch(1, 5, '', '');

        $popular = $data = $this->video_model->getPopularVideos(5);
        $liked = $data = $this->video_model->getVideosByOrder(1, 3, '', 'liked');
        $top = $this->master_model->getTopCategories();
        $data = array(
            'section' => array(
                '0' => array(
                    'title' => 'Recent Watch',
                    'data' => $recent['data']
                ),
                '1' => array(
                    'title' => 'Popular',
                    'data' => $popular['data']
                ),
                '2' => array(
                    'title' => 'Most Liked',
                    'data' => $liked['data']
                ),
            ),
            'topcats' => $top,
            'search' => $search,
            'category' => $category,
            'college' => $college,
            'fest' => $fest
        );
        $this->viewer->fview('search_videos.php', $data);
    }

    public function college($col_id = 1) {
        $recent = $data = $this->video_model->getRecentWatch(1, 5, '', '');
        $popular = $data = $this->video_model->getPopularVideos(5);
        $liked = $data = $this->video_model->getVideosByOrder(1, 3, '', 'liked');
        $top = $this->master_model->getTopCategories();

        $data = array(
            'section' => array(
                '0' => array(
                    'title' => 'Recent Watch',
                    'data' => $recent['data']
                ),
                '1' => array(
                    'title' => 'Popular',
                    'data' => $popular['data']
                ),
                '2' => array(
                    'title' => 'Most Liked',
                    'data' => $liked['data']
                ),
            ),
            'topcats' => $top,
            'college' => $col_id
        );
        $this->viewer->fview('college_video.php', $data);
    }

    public function collegesearch() {
        $search = $this->input->get('q');
        $recent = $data = $this->video_model->getRecentWatch(1, 5, '', '');
        $popular = $data = $this->video_model->getPopularVideos(5);
        $liked = $data = $this->video_model->getVideosByOrder(1, 3, '', 'liked');
        $top = $this->master_model->getTopCategories();
        $data = array(
            'section' => array(
                '0' => array(
                    'title' => 'Recent Watch',
                    'data' => $recent['data']
                ),
                '1' => array(
                    'title' => 'Popular',
                    'data' => $popular['data']
                ),
                '2' => array(
                    'title' => 'Most Liked',
                    'data' => $liked['data']
                ),
            ),
            'topcats' => $top,
            'search' => $search
        );
        $data['css'] = array('css/tm-buddypress.css');
        $this->viewer->fview('search_college.php', $data);
    }

}
