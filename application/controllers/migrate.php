<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migrate extends CI_Controller {

    public $clubmap = array(
        "Barclays Premier League" => "1",
        "LaLiga BBVA" => "8",
        "Bundesliga" => "7",
        "US Major League Soccer" => "4",
        "Serie A" => "6",
        "Ligue 1" => "5",
        "China Super League" => "3",
        "Nigerian Pro Football League" => "2"
    );
    public $clubid = array(
        "Liverpool FC" => "34",
        "Real Madrid FC" => "33",
        "Arsenal FC" => "32",
        "Chelsea FC" => "31",
        "Barcelona FC" => "30",
        "Manchester United" => "29",
        "Manchester United FC" => "29",
        "Everton FC" => "36",
        "Manchester City" => "52",
        "A.S. Roma" => "55",
        "Inter Milan" => "56",
        "Hannover 96" => "92",
        "1899 Hoffenheim" => "101",
        "Bordeaux" => "104",
        "3SC" => "125",
        "Real Madrid C.F." => "130",
        "LA Galaxy" => "151",
        "MFM FC" => "175",
        "Ikorodu United F.C." => "176",
        "AS Monaco FC" => "81",
        "West Ham United" => "11"
    );

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model("master_model");
        $this->load->model("article_model");
        $this->load->model("user_model");
    }

    public function emails() {
        $users = $this->db->get("users")->result_array();

        foreach ($users as $user) {
            $profile = $this->db->get_where("user_profile", array("fk_user_id" => $user['pk_user_id']))->result_array();
            $data = array(
                "email" => $user['email'],
                "fk_club_id" => $profile[0]['fk_club_id'],
                "status" => "1"
            );
            //$this->db->insert("newsletter",$data);
        }
    }

    public function clubslug() {
        $clubs = $this->db->get("master_club")->result_array();
        foreach ($clubs as $club) {
            $this->load->library('slug');
            $this->slug->set_config(array('table' => 'master_club', 'field' => 'slug', 'title' => 'title', 'id' => 'pk_club_id'));
            $rslug = $this->slug->create_uri($club['title'], $club['pk_club_id']);
            $this->db->update("master_club", array('slug' => $rslug), array('pk_club_id' => $club['pk_club_id']));
        }
    }

    public function articles() {
        $articles = $this->db->get("footyfanz_articles")->result_array();
        foreach ($articles as $art) {
            $club_id = "";
            $league_id = "";
            if (isset($this->clubid[$art['article_club']])) {
                $club_id = $this->clubid[$art['article_club']];
                $league_id = $this->getLeagueByClub($club_id);
            }
            $title = $art['article_title'];
            $content = $art['article_content'];
            $tags = $art['article_tags'];
            $user_id = $this->getUserIdByEmail($art['writers_email']);
            $image_name = "";
            $path_img = "public/images/articles_images/" . $art['article_link'] . ".png";

            if (is_file($path_img)) {
                $name = $user_id . "_" . time() . rand(1, 99);
                $image_name = $name . "." . "png";
                @copy($path_img, IMG_ARTICLE . $image_name);
                resizeFile(IMG_ARTICLE . $image_name, 800);
                @copy($path_img, IMG_ARTICLE . "thumb/" . $image_name);
                resizeFile(IMG_ARTICLE . "thumb/" . $image_name, 120);
            }
            $insert = array(
                "title" => $title,
                "content" => $content,
                "file_name" => $image_name,
                "file_type" => "I",
                "post_type" => "N",
                "featured_status" => "0",
                "sponser_status" => "0",
                "fk_club_id" => $club_id,
                "fk_league_id" => $league_id,
                "fk_user_id" => $user_id,
                "is_deleted" => "0",
                "comment_allow" => "1",
                "slug" => ""
            );


            print_r($insert);

            $this->db->insert("post", $insert);
            $id = $this->db->insert_id();

            if ($image_name != "") {
                @copy(IMG_ARTICLE . $file, USER_GALLERY . $image_name);
                @copy(IMG_ARTICLE . "thumb/" . $image_name, USER_GALLERY . "thumb/" . $image_name);
                $data = array(
                    "file_name" => $image_name,
                    "fk_user_id" => $user_id,
                    "status" => "1",
                    "file_type" => "I"
                );
                $this->db->insert("user_uploads", $data);
            }

            //**********Adding Reference in News Feed************
            $this->article_model->addFeedFromArticle($user_id, $id);
            //Maintaine Slug Code

            $this->load->library('slug');
            $this->slug->set_config(array('table' => 'post', 'field' => 'slug', 'title' => 'title', 'id' => 'pk_post_id'));
            $rslug = $this->slug->create_uri($insert['title'], $id);
            $this->db->update("post", array('slug' => $rslug), array('pk_post_id' => $id));
            $this->user_model->SavePoints($user_id, $id, 'W_ART_WRT');
            $this->article_model->saveTags($id, $tags);
        }
    }

    public function getUserIdByEmail($email) {
        $user = $this->db->get_where("users", array("email" => $email))->result_array();
        if (count($user) > 0) {
            return $user[0]['pk_user_id'];
        } else {
            return "1";
        }
    }

    public function getLeagueByClub($club_id) {
        $league = $this->db->get_where("master_club", array("pk_club_id" => $club_id))->result_array();
        if (count($league) > 0) {
            return $league[0]['fk_league_id'];
        } else {
            return "1";
        }
    }

    public function update_user_league($param = '') {
        $users = $this->db->get("user_profile")->result_array();
        foreach ($users as $user) {
            $league_id = $this->getLeagueByClub($user['fk_club_id']);
            $upd_data = array('fk_league_id' => $league_id);
            echo $user['fk_user_id'] . "<br>";
            $this->db->update("users", $upd_data, array('pk_user_id' => $user['fk_user_id']));
        }
    }

    public function test() {
        echo $this->getUserIdByEmail("sheetalkumar105@gmail.com");
    }

    public function users($param = '') {
        $users = $this->db->get("footyfanz_users")->result_array();
        foreach ($users as $user) {
            $club_id = "";
            if (isset($this->clubid[$user['club']])) {
                $club_id = $this->clubid[$user['club']];
            }
            $insert = array(
                "firstname" => $user['first_name'],
                "lastname" => $user['last_name'],
                "username" => $user['footy_name'],
                "slug" => strtolower($user['footy_name']),
                "password" => $user['password'],
                "email" => $user['email'],
                "status" => "1",
                "is_deleted" => "0",
                "created_at" => getTimestamp()
            );
            //$this->db->insert("users",$insert);
            $id = $this->db->insert_id();
            $image_name = "";
            $path_img = "public/images/users_images/" . $user['footy_name'] . ".png";
            if (is_file($path_img)) {
                $info = pathinfo($path_img);
                $name = $id . "_" . time();
                $image_name = $name . "." . $info['extension'];
//                @copy($path_img, IMG_PROFILE . $image_name);
//                resizeFile(IMG_PROFILE . $image_name, 120);
//                @copy(IMG_PROFILE . $image_name, IMG_PROFILE ."thumb/". $image_name);
//                resizeFile(IMG_PROFILE ."thumb/". $image_name, 50);
            }
            $profile = array(
                "fk_user_id" => $id,
                "gender" => ($user['gender'] == "female") ? "Female" : "Male",
                "profile_pic" => $image_name,
                "phone_no" => $user['phone'],
                "about_me" => $user['about_me'],
                "phone_no" => $user['phone'],
                "fk_club_id" => $club_id
            );
            // $this->db->insert("user_profile",$profile);
            print_r($insert);
            print_r($profile);
        }
    }

    public function clubs() {
        $clubs = $this->db->get("footyfanz_clubs")->result_array();
        foreach ($clubs as $club) {
            $club_name = $club['club_name'];
            $club_league = $club['club_league'];
            $league_id = "";
            if (isset($this->clubmap[$club_league])) {
                $league_id = $this->clubmap[$club_league];
            }

            if ($league_id != "") {
                $team_manager = $club['club_info1'];
                $captain = $club['club_info2'];
                $stadium = $club['club_info3'];
                $description = $club['club_info4'];
                $image_name = "";
                $path_img = "public/images/clubs_images/" . $club['club_image_id'] . ".png";
                if (file_exists($path_img)) {
                    $image_name = "club_" . time() . rand(1, 100) . "." . "png";
                    // @copy($path_img, CLUB_IMG_DIR . $image_name);
                    // resizeFile(CLUB_IMG_DIR . $image_name, 150);
                }
                $insert = array(
                    "title" => $club_name,
                    "logo" => $image_name,
                    "description" => $description,
                    "team_manager" => $team_manager,
                    "captain" => $captain,
                    "stadium" => $stadium,
                    "fk_league_id" => $league_id,
                    "status" => "1",
                    "is_deleted" => "0",
                );
                print_r($insert);
                // $this->db->insert("master_club",$insert);
            }
        }
    }

    public function dict($id) {

        $SQl = "SELECT * FROM dictionary2 WHERE id='$id'";
        $data = $this->db->query($SQl)->result_array();
        $updateArray = array();
        foreach ($data as $dt) {
            $SQL = "SELECT DISTINCT(m) as mean, wt FROM dictionary WHERE w='" . $this->db->escape_str($dt['w']) . "' AND m!=''";
            $results = $this->db->query($SQL)->result_array();
            $arr = array();
            foreach ($results as $res) {
                $arr[] = array(
                    't' => $res['wt'],
                    'm' => $res['mean']
                );
            }
            $updateArray = array(
                'id' => $dt['id'],
                'm' => json_encode($arr),
                't' => $results[0]['mean']
            );

            $this->db->update("dictionary2", $updateArray, array("id" => $dt['id']));
        }
        // $this->db->update_batch('dictionary2', $updateArray, 'id');
    }

    function deleteDeletedPoints() {
        $this->db->query("DELETE FROM user_points WHERE user_points.pk_point_id IN(
SELECT GROUP_CONCAT(up.pk_point_id) FROM user_points up
LEFT OUTER JOIN news_feed on news_feed.pk_feed_id=up.fk_ref_id
WHERE up.point_type in ('NF_TEXT','NF_IMG','NF_VID','NF_SHARE','NF_COM','W_NF_COM','NF_LIKE','W_NF_LIKE') AND news_feed.pk_feed_id is null
    )");


        //SELECT * FROM user_points LEFT OUTER JOIN news_feed on news_feed.pk_feed_id=user_points.fk_ref_id WHERE user_points.point_type in ('NF_TEXT','NF_IMG','NF_VID','NF_SHARE','NF_COM','W_NF_COM','NF_LIKE','W_NF_LIKE') ORDER BY news_feed.pk_feed_id
        //3772,10098,18015,18300,18466,18468,19077,19078,19460,21042,21112,21130,15620,16169,16170,16683,16932,17353,17354,17355,17658,18663,21024,23215,23216,23817,23818,24336,24337,24339,24340,25122,25123,18016,18301,18467,18469,19461,21131,16760,16761,16933,16938,20636,22951,24722,21734,22332,22867,22928,23212,23214,23237,23592,23594,23628,23630,23632,23634,23638,23640,23642,23644,23648,23652,23656,23658,23670,23690,23826,23950,23952,23954,23956,23973,24008,24040,24320,24328,24333,24335,24658,24660,24665,24667,24669,24671,24717,24719,24721,24724,24730,24754,24758,24760,24762,24764,24907,24959,24961,24963,25005,25007,25009,25011,25013,25626,25633,25635,25637,26491,26718,26726,26731,26733,26765,21733,22331,22866,22927,23211,23213,23236,23591,23593,23627,23629,23631,23633,23637,23639,23641,23643,23647,23651,23655,23657,23669,23675,23689,23825,23949,23951,23953,23955,23972,24007,24039,24319,24327,24332,24334,24657,24659,24664,24666,24668,24670,24716,24718,24720,24723,24729,24753,24757,24759,24761,24763,24906,24958,24960
        //SELECT GROUP_CONCAT(up.pk_point_id)  FROM user_points up
        //LEFT OUTER JOIN post on post.pk_post_id = up.fk_ref_id
        // WHERE up.point_type not in ('NF_TEXT', 'NF_IMG', 'NF_VID', 'NF_SHARE', 'NF_COM', 'W_NF_COM', 'NF_LIKE', 'W_NF_LIKE') AND post.pk_post_id is null
        //2076,2079,2080,2087,2099,2102,2124,2133,2165,2185,2188,2189,2222,2229,2232,2238,2247,2263,2270,2272,2279,2291,2305,2307,2318,2423,2424,2427,2430,2498,2763,2765,2773,2775,2781,2788,2807,2813,2835,2836,2837,2838,2839,2851,2953,2973,2974,2988,3002,3067,3075,3123,3125,3139,3164,3165,3170,3226,3253,3286,3301,3329,3340,3359,3367,3396,3451,3541,3559,3573,3577,3591,3606,3623,3851,3860,4085,4092,4122,4153,4223,4240,4245,4293,4317,4414,4535,4779,4783,4861,4922,4926,5042,5412,5430,5436,5449,5592,5776,6073,6132,6134,6379,6413,6636,6660,6746,6795,6855,6963,7012,7013,7044,7053,7092,7096,7106,7332,7384,7476,7582,7606,7848,7944,8159,8425,8951,8985,8991,9086,9172,9264,9343,9453,9589,9687,9848,10013,10294,10548,10784,10966,11784,11811,11942,12858,12860,12869,12870,13420,13494,13849,13850,13926,13945,14060,14062,14066,14067,14077,14138,14149,14327,14364,14385,14504,14532,14533,14549,14551,14552,14554,14592,14602,14618,14643,14646,14657,14658,14662,14669,14671,14682,14746,14761,14767,14777,14785,14786,14788,14800,14802,14808,148
    }

}
