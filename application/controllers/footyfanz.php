<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Footyfanz extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('auth_model');
        $this->load->model('master_model');
        $this->load->model('article_model');
        $this->load->model('user_model');
    }

    public function index($param = '') {
        $data = array('css' => "fassets/css/jquery-confirm.min.css", 'js' => 'fassets/js/jquery-confirm.min.js');
        $data['hp_fanz'] = $this->article_model->getTopFanz(0, 21);
        $data['leagues'] = $this->master_model->getLeagueKVWithClub("1");
        $data['clubs'] = $this->master_model->getClubKV("1");
        $this->viewer->fview('footyfanz/index.php', $data);
    }

    public function filtered($league = '', $club = '') {
        $name = $this->input->get("name");
        $data = array('css' => "fassets/css/jquery-confirm.min.css", 'js' => 'fassets/js/jquery-confirm.min.js');
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        $data['league'] = $league;
        $data['club'] = $club;
        $data['name'] = $name;
        $this->viewer->fview('footyfanz/filtered.php', $data);
    }

    public function follow() {
        $follow_user_id = $this->input->post("follow_user_id");
        $user_id = $this->session->userdata("user_id");
        if ($user_id > 0) {
            $this->user_model->follow($follow_user_id, $this->session->userdata("user_id"));
            echo json_encode(array("status" => "1"));
        } else {
            echo json_encode(array("status" => "0"));
        }
    }

    public function getclubs() {
        $lid = $this->input->post("league_id");
        $club_id = $this->input->post("club_id");
        $data = array('clubs' => false, 'type' => "select", 'club_id' => $club_id);
        $data['clubs'] = $this->master_model->getClubKVByLeague($lid, "1");
        $this->viewer->aview('utility/clubs.php', $data, false);
    }

    public function getlist($is_api = 0) {
        $sk = $this->input->post();
        $page = $this->input->post("page");
        $current_user_id = $this->session->userdata("user_id");
        $per_page = 11;

        if ($is_api == 1) {
            $page = 1;
            $per_page = 50;
            $current_user_slug = $this->input->post('usr_slug');//production
            // $current_user_slug = 'sheetal';//testing
            $current_user_id = $this->user_model->getUserIDBySlug($current_user_slug);
            $sk = array('league'=>'', 'club'=>'', 'search_key'=>'');
        }

        $data = $this->user_model->getFootyFanz($page, $per_page, $sk, $current_user_id);
        if ($is_api == 1) {
            foreach ($data['data'] as $key => $datum) {
                $data_x['fan_data'][$key] = array(
                    'firstname' => $datum['firstname'],
                    'lastname' => $datum['lastname'],
                    'slug' => $datum['slug'],
                    'profile_pic' => $datum['profile_pic'],
                    'name' => $datum['name'],
                    'club' => array(
                        'name' => ucwords($datum['club_title']),
                        'slug' => $datum['club_slug'],
                        'image' => $datum['club_image']
                    )
                );
            }
            echo json_encode($data_x);
            return;
        }
        $this->viewer->fview('footyfanz/listing.php', $data, false);
    }

    public function getlistfiltered() {
        $sk = $this->input->post();
        $page = $this->input->post("page");
        $data = $this->user_model->getFootyFanzArticles($page, "18", $sk, $this->session->userdata("user_id"));
        $this->viewer->fview('footyfanz/listingfiltered.php', $data, false);
    }

}