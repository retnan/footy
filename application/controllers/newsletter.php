<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletter extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model("master_model");
    }

    public function saveNewsletter() {
        $email = $this->input->post("email");
        $category = $this->input->post("category");
        $captcha = $this->input->post("captcha");
        if ($captcha != $this->session->userdata("newsletter")) {
            echo json_encode(array('status' => '0', 'msg' => "Invalid captcha code"));
            exit();
        }
        $check_query = "SELECT * FROM newsletter WHERE email='" . $this->db->escape_str($email) . "' AND (fk_category_id='" . $this->db->escape_str($category) . "' OR fk_category_id='0') AND status='1'";
        $result = $this->db->query($check_query)->result_array();
        if (count($result) > 0) {
            if ($result[0]['fk_category_id'] == "0") {
                echo json_encode(array('status' => '0', 'msg' => "You have already subscribed for All Categories"));
            } else {
                $cat_names = $this->master_model->getCategoryByID($category);
                $cat_name = $cat_names['category'];
                echo json_encode(array('status' => '0', 'msg' => "You have already subscribed for $cat_name"));
            }
            exit();
        }
        $insert = array(
            'email' => $email,
            'fk_category_id' => $category,
            'status' => '0'
        );
        $this->db->delete("newsletter", $insert);
        $this->db->insert("newsletter", $insert);
        $id = $this->db->insert_id();
        $insert['id'] = $id;
        if ($category > 0) {
            $cat_names = $this->master_model->getCategoryByID($category);
            $cat_name = $cat_names['category'];
            $insert['cat_name'] = $cat_name;
            echo json_encode(array('status' => '1', 'msg' => "You have successfully subscribed for $cat_name.  A verification link has been sent to your email. Please verify your Email by clicking on verification link on your email."));
        } else {
            $insert['cat_name'] = "All Category";
            echo json_encode(array('status' => '1', 'msg' => "You have successfully subscribed for All Categories. A verification link has been sent to your email. Please verify your Email by clicking on verification link on your email."));
        }
        $this->sendVerificationMail($insert);
    }

    function sendVerificationMail($data) {
        $data_eamil = $this->viewer->emailview("newsletter.php", array('data' => $data));
        $this->load->library('email');
        $config = getEmailConfig();
        $this->email->initialize($config);
        $this->email->from(ADMIN_EMAIL, SITE_NAME);
        $this->email->subject(SITE_NAME . " - Newsletter");
        $this->email->message($data_eamil);
        $this->email->to($data['email']);
        $this->email->send();
    }

    public function verification() {
        $email = base64_decode($this->input->get("auth"));
        $id = base64_decode($this->input->get("act_code"));
        $data = $this->db->get_where("newsletter", array("pk_newsletter_id" => $id, "email" => $email))->result_array();
        if (count($data) > 0) {
            $this->db->update("newsletter", array("status" => '1'), array("pk_newsletter_id" => $id));
            $this->viewer->fview('newsletter/confirmation.php', array('status' => '1', "email" => $email));
        } else {
            $this->viewer->fview('newsletter/confirmation.php', array('status' => '0'));
        }
    }

    public function unsubscribe() {
        $email = base64_decode($this->input->get('e'));
        $this->db->delete("newsletter", array("email" => $email));
        $this->viewer->fview('newsletter/confirmation.php', array('status' => '3', "email" => $email));
    }

}
