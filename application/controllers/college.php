<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class College extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->model("college_model");
        $this->load->model("fest_model");
        $this->load->model("review_model");
        $this->load->model("master_model");
    }

    public function getcollegefest() {
        $col_id = $this->input->post('col_id');
        $fests = $this->college_model->getCollegeFest($col_id);
        if (count($fests) > 0) {
            echo '<option value="">Select Fest</option>';
            foreach ($fests as $fest) {
                echo '<option value="' . $fest['id'] . '">' . $fest['name'] . '</option>';
            }
        }
    }

    public function fest($id = "") {
        $this->fest_model->addFestViews($id);
        $fest = $this->fest_model->getFestById($id);
        $fest['rating'] = $this->review_model->getAverageRating("fest", $fest['id']);

        $this->viewer->fview('fest_details.php', array('fest' => $fest, 'css' => array("plugins/js_composer/assets/css/js_composer.css", "plugins/prettyphoto/images/prettyPhoto.css"), 'js' => array("plugins/js_composer/assets/js/js_composer_front.js", "js/jquery/ui/jquery.ui.tabs.min2c18.js?ver=1.10.4", "plugins/js_composer/assets/lib/jquery-ui-tabs-rotate/jquery-ui-tabs-rotate.js", "plugins/prettyphoto/jquery.prettyPhoto.js", "../assets/plugins/supermap/map.js"), "title" => $fest['name'], "title" => $fest['name'], "metadescription" => $fest['cname'] . ", " . $fest['description']));
    }

    public function festlist($search = "") {
        $search = $this->input->get('q');
        $category = $this->input->get('cat');
        $college = $this->input->get('col');
        $fest = $this->input->get('f');
        $fests = $this->fest_model->getFestMaster(15);
        $data = array(
            "search" => $search,
            "fests_master" => $fests,
            'search' => $search,
            'category' => $category,
            'college' => $college,
            'fest' => $fest
        );
        $this->viewer->fview('festlist.php', $data);
    }

    public function search_fest($page = '1') {
        $number = $this->input->post("number");
        $search = $this->input->post("search");
        $header = $this->input->post("header");
        $order = $this->input->post('order');
        $category = $this->input->post('category');
        $college = $this->input->post('college');
        $fest = $this->input->post('fest');
        $category_name = '';
        $college_name = '';
        $fest_name = '';
        if ($category != '') {
            $cat_data = $this->master_model->getFestcategoryByID($category);
            $category_name = $cat_data['name'];
        }
        if ($college != '') {
            $col_data = $this->master_model->getCollegeByID($college);
            $college_name = $col_data['name'];
        }
        if ($fest != '') {
            $fest_data = $this->master_model->getFestByID($fest);
            $fest_name = $fest_data['name'];
        }
        $data['fests'] = $this->fest_model->getFestList($page, 12, $search, $order, $category, $college, $fest);
        $data['order'] = $order;
        $data['header'] = $header;
        $data['page'] = ++$page;
        $data['search'] = $search;
        $data['number'] = $number;
        $data['category'] = $category;
        $data['category_name'] = $category_name;
        $data['college'] = $college;
        $data['college_name'] = $college_name;
        $data['fest'] = $fest;
        $data['fest_name'] = $fest_name;
        $this->viewer->fview('festrender.php', $data, false);
    }

}
