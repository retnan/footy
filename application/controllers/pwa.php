<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pwa extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('login_model');
        $this->load->model('user_model');
        $this->load->model('master_model');
    }

    public function index($param = '') {
        if ($this->session->userdata("user_id") == "") {
            redirect(site_url());
        }
        $data = $this->login_model->getUser($this->session->userdata("user_id"));
        if (is_file(PUBLIC_DIR . PROFILE_IMG_DIR . $data['image'])) {
            $data['image'] = site_url() . PUBLIC_DIR . PROFILE_IMG_DIR . $data['image'];
        } else {
            $data['image'] = site_url() . "public/fassets/images/user.jpg";
        }
        $this->viewer->fview('user/index.php', array("data" => $data));
    }

    public function setting($param = '') {
        if ($this->session->userdata("user_id") == "") {
            redirect(site_url());
        }
        $data = $this->login_model->getUser($this->session->userdata("user_id"));
        if (is_file(PUBLIC_DIR . PROFILE_IMG_DIR . $data['profile_pic'])) {
            $data['image'] = site_url() . PUBLIC_DIR . PROFILE_IMG_DIR . $data['profile_pic'];
        } else {
            $data['image'] = site_url() . "public/fassets/images/user.jpg";
        }
        $this->viewer->fview('user/setting.php', array("data" => $data));
    }

    public function deleteaccount($param = '') {
        if ($this->input->post("action") == "delete") {
            $id = $this->session->userdata("user_id");
            $this->db->delete("comments", array("fk_user_id" => $id));
            $this->db->delete("users", array("pk_user_id" => $id));
            $this->session->sess_destroy();
        }
    }

    public function defaultlogin() {
    
    	return $this->input->post("email");
    	exit();

        $profile = array();
        $response = array('status'=>'0', 'error'=>TRUE, 'error_msg'=>'Invalid', 'user' => $profile, 'msg'=>'Invalid');

        // $user = "sheetal.105";// for testing
        // $password = "123456";// for testing
        $user = trim($this->input->post("email"));
        $password = trim($this->input->post("password"));
        if ($user == "" or $password == "") {
            $response['error_msg'] = $response['msg'] = "Enter username/email and password";
            echo json_encode($response);
            exit();
        }

        $this->db->cache_delete_all();
        $this->db->cache_off();
        $this->db->flush_cache();
        $data = $this->db->query("SELECT * FROM users WHERE email='".$user."' OR username='".$user."' AND password='".$password."' LIMIT 1")->result_array();
        

        if (count($data) > 0) {
            /*if ($data[0]['is_login'] == "YES" && $data[0]['is_logout'] == "NO") {
                $response['error_msg'] = $response['msg'] = "You are logged in in another system";
                echo json_encode($response);
                exit();
            }else{*/
                $this->db->cache_delete_all();
                //$this->db->query("UPDATE users SET is_login = 'YES', is_logout = 'NO' WHERE email='".$user."' OR username='".$user."'");


                if ($data[0]['status'] == "0") {
                    $response['error_msg'] = $response['msg'] = "Inactive account, Please verify your email.";
                    echo json_encode($response);
                    exit();
                }
                if ($data[0]['status'] == "2") {
                    $response['error_msg'] = $response['msg'] = "Deactivated account, Please contact administrator.";
                    echo json_encode($response);
                    exit();
                }
                if ($data[0]['is_deleted'] == "1") {
                    $response['error_msg'] = $response['msg'] = "Deleted account, Please contact administrator.";
                    echo json_encode($response);
                    exit();
                }

                $linkstatus = $this->start_login($data[0]['pk_user_id'], "skm", $is_api);
            /*echo '<pre>', print_r($linkstatus), '</pre>';

            echo '<pre>', print_r($is_api), '</pre>';
            exit();
*/
            if ($is_api == 1) {//added for mobile app
                $profile_photo = trim($linkstatus["image"]);

                if(empty($profile_photo)){
                    $profile_photo = base_url(DEFAULT_USER);
                }

                $profile = array(
                    'id_no' => $data[0]["slug"],
                    "first_name"=> $data[0]["firstname"],
                    "surname"=> $data[0]["lastname"],
                    "email_address"=> $data[0]["email"],
                    "profile_face"=> $profile_photo,
                    'pref_club_name' => $linkstatus['club_slug'],
                    'pref_club_logo' => $linkstatus['club_image']
                    );
                $response['user'] = $profile;
                $response['status'] = "1";
                $response['error'] = FALSE;
                $response['error_msg'] = $response['msg'] = "";
                echo json_encode($response);
            } else if ($linkstatus == 1) {
                echo json_encode(array("status" => "1", "msg" => "", "url" => site_url() . "newsfeed"));
            } else {
                echo json_encode(array("status" => "1", "msg" => "", "url" => site_url() . "user/edit"));
            }
        //}
        } else {
            $response['error_msg'] = $response['msg'] = "Invalid username / password";
            echo json_encode($response);
        }
    }

    public function forgotpassword() {
        $usernameemail = $this->input->post('email');
        if ($usernameemail == "") {
            echo json_encode(array("status" => "0", "msg" => "Please enter email."));
            exit();
        }
        $data = $this->db->where(array("email" => $usernameemail))->get("users")->result_array();
        foreach ($data as $dt) {
            $rand = rand(99999999, 9999999999);
            $this->db->update("users", array("reset_pass_code" => $rand), array("pk_user_id" => $dt['pk_user_id']));
            $dt['reset_pass_code'] = $rand;
            $this->sendRecoveryMail($dt);
        }
        if (count($data) > 0) {
            echo json_encode(array("status" => "1", "msg" => "Password reset link has been <br>sent to your email."));
        } else {
            echo json_encode(array("status" => "0", "msg" => "Invalid email."));
        }
    }

    public function resetpassword($id = '', $resetcode = "") {
        $user = $this->db->get_where("users", array("pk_user_id" => $id, "reset_pass_code" => $resetcode))->result_array();
        if (count($user) > 0) {
            $data['status'] = "1";
        } else {
            $data['status'] = "0";
        }
        $data['id'] = $id;
        $data['code'] = $resetcode;
        $this->viewer->loginview('home/reset.php', $data);
    }

    public function resetpasswordfinal() {
        $user = $this->db->get_where("users", array("pk_user_id" => $this->input->post('id'), "reset_pass_code" => $this->input->post('code')))->result_array();
        if (count($user) > 0) {
            if ($this->input->post('password') == "") {
                echo json_encode(array("status" => "0", "msg" => "Please enter password."));
                return;
            }
            if ($this->input->post('password') == $this->input->post('cpassword')) {
                $this->db->update("users", array("password" => $this->input->post('password'), "reset_pass_code" => ""), array("pk_user_id" => $user[0]['pk_user_id']));
                echo json_encode(array("status" => "1", "msg" => ""));
            } else {
                echo json_encode(array("status" => "0", "msg" => "Confirm password did not match."));
            }
        } else {
            echo json_encode(array("status" => "0", "msg" => "Unable to reset password. Please try again."));
        }
    }

    private function sendRecoveryMail($data) {
        $data_eamil = $this->viewer->emailview("forgot.php", array('data' => $data));

        $this->load->library('email');
        $config = getEmailConfig();
        $this->email->initialize($config);
        $this->email->from(MAIL_USER, SITE_NAME);
        $this->email->subject(SITE_NAME . " Password Recovery");
        $this->email->message($data_eamil);
        $this->email->to($data['email']);
        $this->email->send();
        //$this->email->to('rowdypundir@gmail.com');
        //$this->email->send();
    }

    public function registration($is_api = 0) {

        $name = $this->input->post("name");
        $username = $this->input->post("username");
        $phone_no = $this->input->post("phone_no");
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $club = $this->input->post("club");

        $response = array('status'=>'0', 'error'=>TRUE, 'error_msg'=>'Invalid', 'msg'=>'Invalid');



        if (trim($name) == "") {
            $response['error_msg'] = $response['msg'] = "Please enter your name";
            echo json_encode($response);
            exit();
        }
        if (trim($username) == "") {
            $response['error_msg'] = $response['msg'] = "Please enter a username";
            echo json_encode($response);
            exit();
        }
        if (trim($email) == "") {
            $response['error_msg'] = $response['msg'] = "Please enter your email";
            echo json_encode($response);
            exit();
        }
        if (trim($password) == "") {
            $response['error_msg'] = $response['msg'] = "Please enter your password";
            echo json_encode($response);
            exit();
        }

        if ($this->login_model->checkEmailExist($email)) {
            $response['error_msg'] = $response['msg'] = "Email already registered";
            echo json_encode($response);
            exit();
        }
        if ($this->login_model->checkUsernameExist($username)) {
            $response['error_msg'] = $response['msg'] = "Username already exist.<br>Please enter another username";
            echo json_encode($response);
            exit();
        }

        $parts = explode(" ", $name);
        $lastname = array_pop($parts);
        $firstname = implode(" ", $parts);

        $code = time();
        $data = array(
            'firstname' => $firstname,
            'lastname' => $lastname,
            'username' => $username,
            'email' => $email,
            'email_vcode' => $code,
            'password' => $password,
            'status' => '0',
            'user_role' => "U",
            'fk_league_id' => $this->master_model->getLeagueByClubID($club),
            );
        $this->db->insert("users", $data);
        $id = $this->db->insert_id();
        $this->load->library('slug');
        $this->slug->set_config(array('table' => 'users', 'field' => 'slug', 'title' => 'username', 'id' => 'pk_user_id'));
        $rslug = $this->slug->create_uri($username, $id);
        $this->db->update("users", array('slug' => $rslug), array('pk_user_id' => $id));
        $profile = array(
            'fk_club_id' => $club,
            "fk_user_id" => $id
            );
        $this->db->insert("user_profile", $profile);

        //$this->sendVerificationMail($name, $email, $code);


        $mailMessage = "Thank you for signing up to Footyfanz.";
        $mailMessage .= "\nPlease activate your account by clicking on this link: \n";
        $mailMessage .= "<a href=\"http://www.footyfanz.com/user/activation/?auth=" . base64_encode($email) . "&act_code=" . base64_encode($code)."\">Click here to activate your account</a>";
        $mailSubject = "Activate your account";

        $this->sendEmailMailGunApi($email, $name, $mailSubject, $mailMessage);

        $response_note = "<div style='color:#00ff00'>Registration successfull. \nPlease verify your email by clicking on \nverification link sent on your email.</span>";
        // echo json_encode(array("status" => "1", "msg" => "Registration successfull. <br>Please verify your email by clicking on <br>verification link sent on your email."));
        $response['error'] = FALSE;
        $response['error_msg'] = $response_note;
        $response['msg'] = nl2br($response_note);
        echo json_encode($response);
    }

    public function activation() {
        $email = utf8_encode(base64_decode($this->input->get('auth')));
        $code = utf8_encode(base64_decode($this->input->get('act_code')));
        $data = $this->db->get_where("users", array("email_vcode" => $code, "email" => $email))->result_array();
        if (count($data) > 0) {
            $this->db->update("users", array("status" => '1'), array("email_vcode" => $code, "email" => $email));
            $profile = $this->db->get_where("user_profile", array("fk_user_id" => $data[0]['pk_user_id']))->result_array();
            $league_id = $this->master_model->getLeagueByClubID($profile[0]['fk_club_id']);
            $this->db->insert("newsletter", array("email" => $email, "fk_league_id" => $league_id, "fk_club_id" => $profile[0]['fk_club_id'], "status" => "1"));
        }
        if (count($data) > 0) {
            $this->start_login($data[0]['pk_user_id'], "skm");
            $this->session->set_flashdata('update_success', 'verified');
            redirect("user/edit/");
        } else {
            redirect("home/verification/");
        }
    }

    public function mailtest() {
        $this->sendVerificationMail("Sandeep Pundir", "rowdypundir@gmail.com", "123456");
    }

    public function sendVerificationMail($name, $email, $verification_code) {
        $data['name'] = $name;
        $data['email'] = $email;
        $data['vcode'] = $verification_code;

        $data_eamil = $this->viewer->emailview("signup.php", array('data' => $data, "verification" => ""));

        $this->load->library('email');
        $config = getEmailConfig();
        $this->email->initialize($config);
        $this->email->from(MAIL_USER, SITE_NAME);
        $this->email->subject("Please verify your email");
        $this->email->message($data_eamil);
        $this->email->to($email);
        $this->email->send();
    }


    function sendEmailMailGunApi($email, $name, $subject, $message) {  
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_USERPWD, 'api:key-a345a4115f7af2bebce5d6cb229afc5b');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      /*curl_setopt($ch,CURLOPT_HTTPHEADER, 
        array('Authorization: Basic '.base64_encode('key-a345a4115f7af2bebce5d6cb229afc5b')));*/
curl_setopt($ch, CURLOPT_URL, 
    'https://api.mailgun.net/v3/mg.footyfanz.com/messages');
curl_setopt($ch, CURLOPT_POSTFIELDS, 
    array('from' => 'Footy Fanz Wizard <postmaster@mg.footyfanz.com>',
        'to' => $name.' <'.$email.'>',
        'subject' => $subject,
        'html' => $message));
$result = curl_exec($ch);
curl_close($ch);
return $result;
}




public function twitterlogin() {
    $this->session->unset_userdata("tw_access_token");
    $this->load->library('twconnect');        /* twredirect() parameter - callback point in your application */
    /* by default the path from config file will be used */
    $ok = $this->twconnect->twredirect('user/twitter_callback');
    if (!$ok) {
        echo 'Could not connect to Twitter. Refresh the page or try again later.';
    }
}

public function twitter_callback() {
    $this->load->library('twconnect');
    $ok = $this->twconnect->twprocess_callback();
    if ($ok) {
        $this->twconnect->twaccount_verify_credentials();
        $user = $this->twconnect->tw_user_info;
        if ($user == null) {
            echo '<script>window.opener.afterLogin("0","0");</script>';
        }
        $tw_id = $user->id_str;
        $tw_name = $user->name;
        $tw_image = $user->profile_image_url;
        $access_token = $this->twconnect->tw_access_token;
        $res = $this->db->get_where('users', array('tw_id' => $tw_id));
        if ($res->num_rows > 0) {
            $users = $res->result_array();
            $data = $users[0];
            $profile = $this->db->get_where("user_profile", array("fk_user_id" => $data['pk_user_id']))->result_array();
            if ($profile[0]['profile_pic'] == "") {
                $img = $this->saveProfileImage($user->profile_image_url);
                $this->db->update("user_profile", array("profile_pic" => $img), array("fk_user_id" => $data['pk_user_id']));
            }
            if ($data['status'] != "2") {
                $this->start_login($data['pk_user_id'], "skm");
                echo '<script>window.opener.afterLogin("1","' . $data['pk_user_id'] . '");</script>';
            } else {
                echo '<script>window.opener.afterLogin("0","0");</script>';
            }
        } else {
            $img = $this->saveProfileImage($user->profile_image_url);
            $parts = explode(" ", $user->name);
            $lastname = array_pop($parts);
            $firstname = implode(" ", $parts);
            $emailsplit = explode("@", $user->screen_name);
            $emailsplit[0] = $emailsplit[0];

            $newdata = array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                "username" => ($this->login_model->checkUsernameExist($emailsplit[0])) ? ($emailsplit[0] . time()) : $emailsplit[0],
                'email' => "",
                'password' => rand("1111", "9999"),
                'user_role' => 'U',
                'tw_id' => $user->id_str,
                'status' => "1",
                'created_at' => getTimestamp()
                );
            $this->db->insert("users", $newdata);
            $user_id = $this->db->insert_id();
            $this->load->library('slug');
            $this->slug->set_config(array('table' => 'users', 'field' => 'slug', 'title' => 'username', 'id' => 'pk_user_id'));
            $rslug = $this->slug->create_uri($newdata['username'], $user_id);
            $this->db->update("users", array('slug' => $rslug), array('pk_user_id' => $user_id));
            $profile = array(
                'fk_user_id' => $user_id,
                'gender' => 'Male',
                'profile_pic' => $img
                );
            $this->db->insert("user_profile", $profile);
            $this->start_login($user_id, "skm");
            echo '<script>window.opener.afterLogin("1","' . $user_id . '");</script>';
        }
    } else
    echo '<script>window.opener.afterLogin("0","0");</script>';
}

public function saveProfileImage($url) {
    $content = file_get_contents($url);
        //Store in the filesystem.
    $img = time() . ".jpg";
    $fp = fopen(PUBLIC_DIR . PROFILE_IMG_DIR . $img, "w");
    fwrite($fp, $content);
    fclose($fp);
    return $img;
}

public function start_login($id, $pass, $is_api = 0) {
    $this->db->flush_cache();

    if ($pass != "skm") {
        exit();
    }

    $data = $this->login_model->getUser($id);

    if (is_file(PUBLIC_DIR . PROFILE_IMG_DIR . $data['profile_pic'])) {
            // $data['image'] = "http://10.0.2.2:85/footyfanz/" . PUBLIC_DIR . PROFILE_IMG_DIR . $data['profile_pic'];// for testing with mobile app LOCALLY
        $data['image'] = site_url() . PUBLIC_DIR . PROFILE_IMG_DIR . $data['profile_pic'];
    } else {
        $data['image'] = site_url() . "public/fassets/images/user.jpg";
    }
    $sessiondata = array(
        'user_id' => $data['pk_user_id'],
        'email' => $data['email'],
        'name' => $data['firstname'] . " " . $data['lastname'],
        'image' => $data['image'],
        'club_id' => $data['fk_club_id'],
        'club_slug' => $data['club_slug'],
        'league_id' => $data['fk_league_id'],
        'site_league' => $data['fk_league_id'],
        "profile_status" => $data['profile_status'],
        "club_image" => $data['club_image'],
        "utype" => $data['user_role'],
        "slug" => $data['slug']
        );

        if ($is_api == 1) {// added for mobile
            return $sessiondata;
        }

        $this->session->set_userdata($sessiondata);
        if ($data['profile_status'] == 1) {
            return 1;
        }else{
            return null;
        }
        // print_r($sessiondata);
        // exit();
    }

    public function loginstatus() {
        echo "<pre>";
        print_r($this->session->all_userdata());
        echo "</pre>";
    }

    public function logout() {
        /*$this->session->sess_destroy();
        redirect("");*/

        $usr = $this->session->userdata('email');
        $usrname = $this->session->userdata('user_id');

        $this->db->query("UPDATE users SET is_login = 'NO', is_logout = 'YES' WHERE email='".$usr."' AND pk_user_id='".$usrname."'");

        $this->db->flush_cache();

        $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
        $this->session->sess_destroy();

        //echo $this->load->view("front/includes/header_login.php");
        redirect('/home/login/index');
    }

    public function updatelogo() {

        if (isset($_FILES['logo']) and $_FILES['logo']['error'] == 0) {
            $config['path'] = PUBLIC_DIR . PROFILE_IMG_DIR;
            $config['type'] = 'gif|jpg|png';
            $config['width'] = "300";
            $config['prefix'] = "logo";
            $config['file_name'] = "logo";
            $file = uploadFile($config);
            if ($file) {
                $img = base_url() . PUBLIC_DIR . PROFILE_IMG_DIR . $file;
                $this->session->set_userdata('image', $img);
                $this->db->update("users", array("image" => $file), array("pk_user_id" => $this->session->userdata("user_id")));
                echo json_encode(array("status" => "1", "img" => $img));
            } else {
                echo json_encode(array("status" => "0", "msg" => "Invalid file selected"));
            }
        } else {
            echo json_encode(array("status" => "0", "msg" => "No file selected"));
        }
    }

    public function password($param = '') {
        if ($this->session->userdata("user_id") == "") {
            redirect(site_url());
        }
        $data = $this->login_model->getUser($this->session->userdata("user_id"));
        $this->viewer->fview('user/password.php', array());
    }

    public function profileupdate() {
        $update = array(
            'name' => $this->input->post('profile_name')
            );

        $this->db->update("users", $update, array("pk_user_id" => $this->session->userdata("user_id")));
        $this->session->set_flashdata('update_success', 'yes');
        echo json_encode(array('status' => '1', "msg" => ''));
    }

    public function passwordupdate() {
        $update = array(
            'password' => $this->input->post('new_password')
            );
        $this->db->update("users", $update, array("pk_user_id" => $this->session->userdata("user_id")));
        $this->session->set_flashdata('update_success', 'yes');
        echo json_encode(array('status' => '1', "msg" => ''));
    }

    public function edit() {
        if ($this->session->userdata("user_id") == "") {
            redirect(site_url() . "login");
        }
        $data['ud'] = $this->user_model->getUserDetails($this->session->userdata("user_id"));
        $data['pos'] = $this->user_model->getUserPosition($this->session->userdata("user_id"));
        $data['uh'] = $this->user_model->getUserHighlights($this->session->userdata("user_id"));
        $data['clubs'] = $this->master_model->getClubKV("1");
        $data['css'] = array("fassets/plugins/cropper/css/jquery.awesome-cropper.css", "fassets/plugins/cropper/components/imgareaselect/css/imgareaselect-default.css", "fassets/plugins/datepicker/css/bootstrap-datetimepicker.min.css", "fassets/css/jquery-confirm.min.css");
        $data['js'] = array("fassets/plugins/cropper/components/imgareaselect/scripts/jquery.imgareaselect.js", "fassets/plugins/cropper/build/jquery.awesome-cropper.js", "fassets/plugins/datepicker/js/moment.js", "fassets/plugins/datepicker/js/bootstrap-datetimepicker.min.js", "fassets/js/jquery-confirm.min.js");
        $this->viewer->fview('user/edit.php', $data);
    }

    public function checkusername() {
        $user_id = $this->session->userdata("user_id");
        $updateUser = array(
            'username' => $this->input->post("uname"),
            );
        $user = $this->db->query("SELECT count(*) as row_count FROM users WHERE username='" . $this->input->post("uname") . "' AND pk_user_id!='$user_id'")->row_array();
        $res = array('status' => 0, 'msg' => $this->input->post("uname") . ' Already Exist');
        if ($user['row_count'] == 0) {
            $this->db->update("users", $updateUser, array('pk_user_id' => $user_id));
            $res['status'] = '1';
        }
        echo json_encode($res);
    }

    public function updateprofile() {
        $user_id = $this->session->userdata("user_id");
        if ($user_id == "") {
            redirect(site_url() . "login");
        }
        $updateUser = array(
            'firstname' => $this->input->post("firstname"),
            'lastname' => $this->input->post("lastname"),
            'fk_league_id' => $this->master_model->getLeagueByClubID($this->input->post("fk_club_id")),
            );
        $this->db->update("users", $updateUser, array('pk_user_id' => $user_id));
        $user = $this->db->get_where("users", array("pk_user_id" => $user_id))->result_array();
        $email = trim($this->input->post("email"));
        if ($email != "" and $email != $user[0]['email']) {

        }
        $updateProfile = array(
            'gender' => $this->input->post("gender"),
            'about_me' => $this->input->post("about_me"),
            'phone_no' => $this->input->post("phone_no"),
            'fb_link' => $this->input->post("fb_link"),
            'tw_link' => $this->input->post("tw_link"),
            'fk_club_id' => $this->input->post("fk_club_id"),
            );
        $this->db->update("newsletter", array("fk_league_id" => $this->master_model->getLeagueByClubID($this->input->post("fk_club_id")), "fk_club_id" => $this->input->post("fk_club_id")), array('email' => $email));
        $this->db->update("user_profile", $updateProfile, array('fk_user_id' => $user_id));
        $this->start_login($user_id, "skm");
    }

    public function updateimage() {
        $user_id = $this->session->userdata("user_id");
        if ($user_id > 0) {
            $data = $this->input->post("image");
            list($type, $data) = explode(':', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);
            $path = PUBLIC_DIR . PROFILE_IMG_DIR;
           
            $name = $user_id . "_" . time() . ".jpg";
            file_put_contents($path . $name, $data);
           
            @copy($path . $name, IMG_PROFILE . "thumb/" . $image_name);
            resizeFile(IMG_PROFILE . "thumb/" . $image_name, 50);
            $olddata = $this->db->get_where("user_profile", array("fk_user_id" => $user_id))->result_array();
            if (count($olddata) > 0) {
                @unlink($path . $olddata[0]['profile_pic']);
                $ipath = site_url() . $path . $name;
                $this->db->update("user_profile", array("profile_pic" => $name), array("fk_user_id" => $user_id));
                $this->session->set_userdata("image", $ipath);
                echo $ipath;
            }
        }
    }

    public function points($user = '') {
        $data = array('css' => "fassets/css/jquery-confirm.min.css", 'js' => 'fassets/js/jquery-confirm.min.js');
        $data['user'] = $user;

        if ($user != "") {
            $user_id = $this->user_model->getUserIDBySlug($user);
            if ($user_id > 0) {
                if ($this->session->userdata("user_id") == $user_id) {
                    $data['follow_status'] = "NA";
                } else {
                    $data['follow_status'] = $this->user_model->followStatus($user_id, $this->session->userdata("user_id"));
                }
                $data['pos'] = $this->user_model->getUserPosition($user_id);
                $data['ud'] = $this->user_model->getUserDetails($user_id);
                $data['uh'] = $this->user_model->getUserHighlights($user_id);
                $data['user_id'] = $user_id;
            } else {
                $user_id = $this->session->userdata("user_id");
                $data['user_id'] = $user_id;
            }


            $this->viewer->fview('user/point_listing.php', $data);
        }
    }

    public function point_listing_data($param = '') {
        $page = $this->input->post('page');
        $perpage = 50;
        $searchKey = (isset($_GET['sk'])) ? $_GET['sk'] : "";
        $user_id = $this->input->get('user_id');
        $data = $this->user_model->getPointList($page, $perpage, $searchKey, $user_id);
        $data['page'] = getPaginationFooterFront($page, $perpage, $data['count']);
        $this->viewer->fview('user/points_listing_data.php', $data, false);
    }

    public function fetch_clubs() {
        $clubs = array();
        $response = array('status'=>'0', 'error'=>TRUE, 'error_msg'=>'Invalid', "clubs"=> array(), 'msg'=>'Invalid');

        $clubs_f = $this->master_model->getClubKVM("1");
        if (!empty($clubs_f)) {
            $clubs = arrangeClubs($clubs_f);
            
            $response['status'] = 1;
            $response['error'] = FALSE;
            $response['clubs'] = $clubs;
            $response['error_msg'] = $response['msg'] = "";
        }

        echo json_encode($response);
    }

}