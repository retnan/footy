<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Views extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('auth_model');
        $this->load->model('master_model');
        $this->load->model('video_model');
        $this->load->model('fest_model');
    }

    public function index($param = '') {
        $popular = $data = $this->video_model->getPopularVideos(5);
        $liked = $data = $this->video_model->getVideosByOrder(1, 5, '', 'liked');
        $recent = $data = $this->video_model->getRecentWatch(1, 5, '', '');
        $top = $this->master_model->getTopCategories();
        $banner = $this->video_model->getVideosBanner();
        $fests = $this->fest_model->getHomePageFest();
        $data = array(
            'section' => array(
                '0' => array(
                    'title' => 'Recent Watch',
                    'data' => $recent['data']
                ),
                '1' => array(
                    'title' => 'Popular',
                    'data' => $popular['data']
                ),
                '2' => array(
                    'title' => 'Most Liked',
                    'data' => $liked['data']
                ),
            ),
            'topcats' => $top,
            'fests' => $fests
        );
        $data['banner'] = $banner;
        $this->viewer->fview('index.php', $data);
    }

    public function getcollege($param = '') {
        $key = $this->input->post('key');
        //$key = $param;
        $data = $this->master_model->getCollegeBySearch($key, 20);
        $print = array();
        foreach ($data as $dt) {
            $fq = $this->db->get_where("ko_fest", array('und' => $dt['id']));
            $fqa = $fq->result_array();
            $print[$dt['id']] = array('name' => $dt['name'],
                'fest' => $fqa
            );
        }
        echo json_encode($print);
    }

    public function getvideo($param = '') {
        $key = $this->input->post('key');
        //$key = $param;
        $data = $this->master_model->getVideoBySearch($key, 20);
        $print = array();
        foreach ($data as $dt) {
            $print[$dt['id']] = $dt['name'];
        }
        echo json_encode($print);
    }

    public function videos($page = 1) {
        $order = $this->input->post('order');
        $header = $this->input->post('header');
        $number = $this->input->post('number');
        $data = $this->video_model->getVideosByOrder($page, 12, '', $order);
        $data['order'] = $order;
        $data['header'] = $header;
        $data['page'] = ++$page;
        $data['number'] = $number;
        $this->viewer->fview('videos/videos.php', $data, false);
    }

    public function category_videos($page = 1) {
        $order = $this->input->post('order');
        $header = $this->input->post('header');
        $category = $this->input->post('category');
        $number = $this->input->post('number');
        $data = $this->video_model->getVideosByOrderCategory($page, 12, '', $order, $category);
        $data['order'] = $order;
        $data['header'] = $header;
        $data['page'] = ++$page;
        $data['number'] = $number;
        $category_detail = $this->master_model->getCategoryByID($category);
        $data['cat_data'] = $category_detail;
        $this->viewer->fview('videos/category_videos.php', $data, false);
    }

    public function college_videos($page = 1) {
        $order = $this->input->post('order');
        $header = $this->input->post('header');
        $college = $this->input->post('college');
        $number = $this->input->post('number');
        $data = $this->video_model->getVideosByOrderCollege($page, 12, '', $order, $college);
        $data['order'] = $order;
        $data['header'] = $header;
        $data['page'] = ++$page;
        $data['number'] = $number;
        $college_detail = $this->master_model->getCollegeByID($college);
        $data['col_data'] = $college_detail;
        $this->viewer->fview('videos/college_videos.php', $data, false);
    }

    public function search_videos($page = 1) {
        //print_r($this->input->post());
        $order = $this->input->post('order');
        $header = $this->input->post('header');
        $search = $this->input->post('search');
        $number = $this->input->post('number');
        $category = $this->input->post('category');
        $college = $this->input->post('college');
        $user = $this->input->post('user');
        $fest = $this->input->post('fest');
        $category_name = '';
        $college_name = '';
        $fest_name = '';
        if ($category != '') {
            $cat_data = $this->master_model->getCategoryByID($category);
            $category_name = $cat_data['name'];
        }
        if ($college != '') {
            $col_data = $this->master_model->getCollegeByID($college);
            $college_name = $col_data['name'];
        }
        if ($fest != '') {
            $fest_data = $this->master_model->getFestByID($fest);
            $fest_name = $fest_data['name'];
        }
        $data = $this->video_model->getVideosByOrder($page, 12, $search, $order, $category, $college, $fest, $user);
        $data['order'] = $order;
        $data['header'] = $header;
        $data['page'] = ++$page;
        $data['search'] = $search;
        $data['number'] = $number;
        $data['category'] = $category;
        $data['category_name'] = $category_name;
        $data['college'] = $college;
        $data['college_name'] = $college_name;
        $data['fest'] = $fest;
        $data['fest_name'] = $fest_name;
        $this->viewer->fview('videos/search_videos.php', $data, false);
    }

    public function search_college($page = 1) {
        $order = $this->input->post('order');
        $header = $this->input->post('header');
        $search = $this->input->post('search');
        $number = $this->input->post('number');
        $data = $this->master_model->getCollegeSearch($page, 12, $search);
        $data['order'] = $order;
        $data['header'] = $header;
        $data['page'] = ++$page;
        $data['search'] = $search;
        $data['number'] = $number;
        $this->viewer->fview('college/search_college.php', $data, false);
    }

    public function video($slug = "") {
        if (is_numeric($slug)) {
            $video = $this->video_model->getVideoById($slug);
        } else {
            $video = $this->video_model->getVideoBySlug($slug);
        }
        $this->load->model("comment_model");
        $this->load->model("like_model");
        $comments = $this->comment_model->getVideoComments($video['id']);
        $like_status = $this->like_model->checkVideoLiked($video['id'], $this->session->userdata("user_id"));
        $this->video_model->increaseVideoViews($video['id'], $this->session->userdata("user_id"));
        $this->viewer->fview('video_view.php', array('data' => $video, 'comments' => $comments, 'like_status' => $like_status));
    }

    public function comment() {
        $user = $this->session->userdata("user_id");
        $comment = $this->input->post("comment");
        $vid = _decode($this->input->post("vid"));
        $this->load->model("comment_model");
        $this->load->model("user_model");
        $udata = $this->user_model->getUserBasic($user);
        $this->comment_model->postVideoComment($user, $vid, $comment);
        if ($udata['id'] != '') {
            $url = base_url() . "profile/view/" . $udata ['id'];
            $name = $udata['name'];
        } else {
            $url = 'javascript:void(0);';
            $name = "College Media";
        }
        $datetime = getTimestamp();
        echo '<li class="in">
                <img class="avatar" alt="" src="' . parseUserImage($udata['sid'], $udata['pic'], $udata['gender'])
        . '" style="margin-left: 0px;" />
                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="' . $url . '" class="name">' . $name . '</a>
                                                    <span class="datetime">at ' . date('M d, Y h:i A', strtotime($datetime)) . '</span>
                                                    <span class="body">
                                                        ' . $comment . '
                                                    </span>
                                                </div>
                                            </li>';
    }

    public function likeunlike() {
        $user = $this->session->userdata("user_id");
        $vid = _decode($this->input->post("vid"));
        $action = $this->input->post("task");
        $this->load->model("like_model");
        $this->like_model->likeUnlikeVedeo($vid, $user, $action);
        $video = $this->video_model->getVideoById($vid);
        $this->load->model("like_model");
        $like_status = $this->like_model->checkVideoLiked($vid, $user);
        echo $this->viewer->fview('videos/video_footer_like.php', array('data' => $video, 'like_status' => $like_status), FALSE);
    }

}
