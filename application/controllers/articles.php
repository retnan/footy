<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Articles extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model("master_model");
        $this->load->model("article_model");
        $this->load->model("user_model");
        $this->load->model('poll_model');
    }

    public function index($param = '') {
        $data['categories'] = $this->master_model->getCategoriesKV("1");

        $this->viewer->fview('home/index.php', $data);
    }

    public function listing($user = '') {
        $data = array('css' => "fassets/css/jquery-confirm.min.css", 'js' => 'fassets/js/jquery-confirm.min.js');
        $data['poll_data'] = $this->poll_model->getPoll("type='H'", "2", "0");
        $pollfeed = $this->poll_model->getPoll("type='H'", "4", "0");
        if ($pollfeed) {
            $data['poll_data'] = $pollfeed;
        }
        $data['user'] = $user;

        if ($user != "") {
            $user_id = $this->user_model->getUserIDBySlug($user);
            if ($user_id > 0) {
                if ($this->session->userdata("user_id") == $user_id) {
                    $data['follow_status'] = "NA";
                } else {
                    $data['follow_status'] = $this->user_model->followStatus($user_id, $this->session->userdata("user_id"));
                }
                $data['pos'] = $this->user_model->getUserPosition($user_id);
                $data['ud'] = $this->user_model->getUserDetails($user_id);
                $data['uh'] = $this->user_model->getUserHighlights($user_id);
            }
        }


        $this->viewer->fview('articles/listing.php', $data);
    }

    public function clublisting($slug = '') {
        $data['club'] = $this->master_model->getClubBySlug($slug);

        if ($data['club']) {
            $post_filter = "(fk_club_id='" . $data['club']['pk_club_id'] . "' OR fk_league_id='" . $data['club']['fk_league_id'] . "') AND ";

            $wrt_data = $this->article_model->getWritersByPoints($data['club']['pk_club_id'], 11);
            $data['hp_writers'] = $wrt_data['wdata'];
            $data['wr_count'] = $wrt_data['wr_count'];
            $fan_data = $this->article_model->getTopFanz($data['club']['pk_club_id'], 13);
            $data['hp_fanz'] = $fan_data['fan_data'];
            $data['fan_count'] = $fan_data['fan_count'];
            $data['art_count'] = $this->article_model->getClubArticles($data['club']['pk_club_id']);
            $data['art_reads'] = $this->article_model->getClubArticleRead($data['club']['pk_club_id']);

            $data['poll_data'] = $this->poll_model->getPoll("type='H'", "2", "0");
            $pollfeed = $this->poll_model->getPoll("type='H'", "4", "0");
            if ($pollfeed) {
                $data['poll_data'] = $pollfeed;
            }

            $this->viewer->fview('articles/clublisting.php', $data);
        } else {
            $this->viewer->fview('general/page_404.php', array());
        }
    }

    public function listing_data($param = '') {
        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        $skk = $this->session->userdata("sk");
        if ($skk != "") {
            $searchKey = $skk;
            $this->session->set_userdata("sk", "");
        } else {
            $searchKey = (isset($_GET['sk'])) ? $_GET['sk'] : "";
        }

        $ftype = $this->input->get();
        $source = $this->input->get("source");

        if (isset($ftype['user']) and $ftype['user'] != "") {
            $ftype['user_id'] = $this->user_model->getUserIDBySlug($ftype['user']);
        }

        //echo $ftype['user_id'];
        $data = $this->article_model->getArticleList($page, $perpage, $searchKey, $ftype);

        if ($source == "search") {
            $data['users'] = $this->article_model->getUserList($page, $perpage, $searchKey, $ftype);
            $count_all = $data['count'] + $data['users']['count'];
            if ($data['count'] < $data['users']['count']) {
                $data['count'] = $data['users']['count'];
            }
            $data['page'] = getPaginationFooterFront($page, $perpage, $data['count'], $count_all);
        } else {
            $data['page'] = getPaginationFooterFront($page, $perpage, $data['count']);
        }
        $data['clubs'] = $this->master_model->getClubKV();
        $data['search'] = $searchKey;
        $data["source"] = $source;
        $this->viewer->fview('articles/listing_data.php', $data, false);
    }

    public function clublisting_data($param = '') {
        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        $skk = $this->session->userdata("sk");
        if ($skk != "") {
            $searchKey = $skk;
            $this->session->set_userdata("sk", "");
        } else {
            $searchKey = (isset($_GET['sk'])) ? $_GET['sk'] : "";
        }

        $ftype = $this->input->get();
        if (isset($ftype['club']) and $ftype['club'] != "") {
            $ftype['club_id'] = $ftype['club'];
        }

        //echo $ftype['user_id'];
        $data = $this->article_model->getArticleList($page, $perpage, $searchKey, $ftype);
        $data['page'] = getPaginationFooterFront($page, $perpage, $data['count']);
        $data['clubs'] = $this->master_model->getClubKV();
        $data['search'] = $searchKey;
        $this->viewer->fview('articles/listing_data.php', $data, false);
    }

    public function prepost() {
        $this->session->set_userdata("articledata", $this->input->post("data"));
    }

    public function post() {
        $data = array();
        if (!($this->session->userdata("user_id") > 0)) {
            redirect("login");
            return;
        }
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        $data['js'] = array("assets/plugins/ckeditor/ckeditor.js", "fassets/js/select2_v4.js");
        $data['css'] = array("fassets/css/select2_v4.min.css");
        // $article_data = $this->session->userdata("articledata");
        $data['articledata'] = $this->session->userdata("articledata");
        $this->session->set_userdata("articledata", "");
        $this->viewer->fview('articles/post_article.php', $data);
    }

    public function delete($id) {
        $feeds = $this->db->get_where("news_feed", array('feed_ref_id' => $id))->result_array();
        foreach ($feeds as $fd) {
            $this->user_model->deleteAllFeedData($fd["pk_feed_id"]);
        }

        $this->db->delete("post_tags", array('fk_post_id' => $id));
        $this->db->delete("post", array('pk_post_id' => $id));
        $this->db->delete("comments", array('fk_ref_id' => $id, 'type' => 'A'));
        $this->db->delete("news_feed", array('feed_ref_id' => $id));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "W_ART_WRT"));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "ART_COM"));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "ART_SHARE"));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "W_ART_READS"));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "W_ART_SHARE"));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "W_ART_COM"));
    }

    public function edit($id) {
        $data = array();
        if (!($this->session->userdata("user_id") > 0)) {
            redirect("login");
            return;
        }
        $data['article'] = $this->article_model->getArticleById($id);
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        $data['clubs'] = $this->master_model->getClubKVByLeague($data['article']['fk_league_id']);
        $data['js'] = array("assets/plugins/ckeditor/ckeditor.js", "fassets/js/select2_v4.js");
        $data['css'] = array("fassets/css/select2_v4.min.css");
        $this->viewer->fview('articles/edit_article.php', $data);
    }

    public function articleview($article_slug) {

        $data = array(
            'css' => array(
                "fassets/css/jquery-confirm.min.css",
                'assets/plugins/emoji-picker/lib/css/emoji.css'
            ),
            'js' => array(
                'fassets/js/jquery-confirm.min.js',
                'assets/plugins/emoji-picker/lib/js/config.js',
                'assets/plugins/emoji-picker/lib/js/util.js',
                'assets/plugins/emoji-picker/lib/js/jquery.emojiarea.js',
                'assets/plugins/emoji-picker/lib/js/emoji-picker.js',
                'assets/plugins/emoji-picker/lib/js/emoji.js'
            )
        );

        $data['poll_data'] = $this->poll_model->getPoll("type='H'", "2", "0");
        $pollfeed = $this->poll_model->getPoll("type='H'", "4", "0");
        if ($pollfeed) {
            $data['poll_data'] = $pollfeed;
        }

        $data['article'] = $this->article_model->getArticleBySlug($article_slug);
        if ($data['article'] == false) {
            //$this->viewer->fview('general/page_404.php', array());
            //return;
            redirect('/articles/listing');
        }
        $fb_data = array('image' => $data['article']['image'], 'title' => $data['article']['title'], 'content' => $data['article']['content']);
        $this->session->userdata['articlemeta'] = serialize($fb_data);
        $user_id = $data['article']['fk_user_id'];
        if ($data['article']['fk_user_id'] != $this->session->userdata("user_id")) {
            $this->user_model->SavePoints($data['article']['fk_user_id'], $data['article']['pk_post_id'], "W_ART_READS");
        }
        $data['user_data'] = $this->user_model->getUserDetails($user_id);
        $data['user_pos'] = $this->user_model->getUserPosition($user_id);
        if ($this->session->userdata("user_id") == $user_id) {
            $data['follow_status'] = "NA";
        } else {
            $data['follow_status'] = $this->user_model->followStatus($user_id, $this->session->userdata("user_id"));
        }
        // print_r($data['article']);
        $this->viewer->fview('articles/view.php', $data);
    }

    public function getarticles($key = '') {
        $key = $this->input->post('key');
        $data = $this->article_model->getArticleBySearch($key, 6);
        $print = array();
        foreach ($data as $dt) {
            $print[$dt['cslug']]['category'] = $dt['category'];
            $image = site_url() . IMG_ARTICLE . $dt['image'];
            $print[$dt['cslug']]['article'][] = array('url' => base_url() . ARTICLE_TAG . "/" . $dt['cslug'] . "/" . $dt['slug'],
                'title' => $dt['title'],
                'content' => wordLimiter(strip_tags($dt['content']), 100),

                'image' => $image);
        }
        // print_r($print);
        echo json_encode(array("result" => $print, "url" => site_url() . "search/?q=" . $key, "text" => "More stories about \"" . $key . "\""));
    }

    public function postcomment() {
        $user = $this->session->userdata("user_id");
        if ($user == "") {
            echo json_encode(array("status" => "0"));
        } else {
            $captcha = $this->input->post('captcha');
            if ($captcha != $this->session->userdata("comment")) {
                echo json_encode(array('status' => '2', 'msg' => "Invalid captcha code"));
                exit();
            }
            $article_id = $this->input->post('article_id');
            $comment = trim($this->input->post('comment'));
            $data = array(
                'fk_user_id' => $user,
                'fk_article_id' => $article_id,
                'comment' => $comment
            );
            $this->db->insert("comments", $data);
            echo json_encode(array("status" => "1"));
        }
    }

    public function save_comment() {
        if ($this->session->userdata("user_id") > 0) {
            $post_id = $this->input->post('post_id');

            $data = array(
                'content' => $this->input->post('comment'),
                'type' => $this->input->post('post_type'),
                'fk_ref_id' => $this->input->post('post_id'),
                'fk_user_id' => $this->session->userdata("user_id"),
            );
            $this->db->insert("comments", $data);
            $article = $this->db->get_where("post", array("pk_post_id" => $post_id))->result_array();
            $post_type = "article";
            if ($article[0]['post_type'] == "N") {
                $post_type = "post";
            }
            if ($article[0]['fk_user_id'] != $this->session->userdata("user_id")) {
                $noti = array(
                    "type" => "COM_ART",
                    "fk_user_id" => $article[0]['fk_user_id'],
                    "content" => $this->session->userdata("name") . " commented on your " . $post_type,
                    "ref_id" => $post_id,
                    "read_status" => "0",
                    "sent_by" => $this->session->userdata("user_id")
                );
                $this->db->insert("notifications", $noti);
            }
            $this->user_model->SavePoints($data['fk_user_id'], $data['fk_ref_id'], "ART_COM", true);
            $this->user_model->SavePoints($article[0]['fk_user_id'], $data['fk_ref_id'], "W_ART_COM", true);



            echo json_encode(array('status' => '1', 'msg' => "Comment has been Saved"));
        } else {
            echo json_encode(array('status' => '0', 'msg' => ""));
        }
    }

    public function save_image() {
        $action = $this->input->get("action");
        if ($action == "saveimage") {
            move_uploaded_file($_FILES['mainfile']['tmp_name'], PUBLIC_DIR . $_FILES['mainfile']['name']);
            redirect(site_url() . "articles/save_image");
        }
        $this->viewer->fview('articles/upload.php', array(), false);
    }

    public function commentupdate() {
//        print_r($_POST);
//        $this->db->delete("comments", array("pk_comment_id" => $this->input->post('id')));
        $save_data = array(
            "fk_user_id" => $this->session->userdata('user_id'),
            "content" => $this->input->post('com_text'),
        );
        $this->db->update("comments", $save_data, array("pk_comment_id" => $this->input->post('com_id')));
    }

    public function loadcomment() {
        $page = $this->input->post('page');
        $article_id = $this->input->post('post_id');
        $ptype = $this->input->post('ptype');

//        $data['comments'] = $this->article_model->getComments($article_id, $page, PAGING_MED, $ptype);
        $data['comments'] = $this->article_model->getComments($article_id, $page, 2, $ptype);

        $this->viewer->fview('articles/comments.php', $data, false);
    }

    public function deletecomment() {
//        print_r($_POST);
        $this->db->delete("comments", array("pk_comment_id" => $this->input->post('id')));
    }

    public function search($data = "") {

        $q = $this->input->get("search_key");
        $data = array('css' => "fassets/css/jquery-confirm.min.css", 'js' => 'fassets/js/jquery-confirm.min.js');
        $data['poll_data'] = $this->poll_model->getPoll("type='H'", "2", "0");
        $pollfeed = $this->poll_model->getPoll("type='H'", "4", "0");
        if ($pollfeed) {
            $data['poll_data'] = $pollfeed;
        }
        $data['q'] = $q;
        $this->session->set_userdata("sk", $q);
        $this->viewer->fview('articles/search_listing.php', $data);
    }

    public function savearticle() {
        $video_link = $this->input->post('youtube_link');
        $club_id = 0;
        $league_id = 0;
        if ($this->input->post("league") > 0) {
            $club_id = intval($this->input->post("club"));
            $league_id = intval($this->input->post("league"));
        }


        $insert = array(
            'title' => $this->input->post('title'),
            'content' => $this->input->post('content'),
            'featured_status' => '0',
            'sponser_status' => '0',
            'comment_allow' => '1',
            'fk_club_id' => $club_id,
            'fk_league_id' => $league_id,
            'comment_allow' => '1',
            'file_type' => $this->input->post('file_type'),
            'fk_user_id' => $this->session->userdata('user_id'),
            'post_type' => $this->input->post('post_type')
        );
        if ($insert['file_type'] == 'V') {
            $insert['file_name'] = $video_link . ".jpg";
        }
        $this->db->insert('post', $insert);
        $id = $this->db->insert_id();

        // Add notification to all followers and League/club members
        $members = array();
        $selftype = $this->db->get_where("users", array("pk_user_id" => $this->session->userdata('user_id')))->row_array();

        if ($selftype['ff_plus'] == "1" or $selftype['premium'] == "1") {
            $sql = "SELECT user_profile.fk_user_id FROM user_profile  WHERE 1=1";
            $profiles = $this->db->query($sql)->result_array();
            foreach ($profiles as $pro) {
                $members[] = $pro['fk_user_id'];
            }
        } else {
            if ($league_id > 0) {
                $sql = "SELECT user_profile.fk_user_id FROM master_club LEFT OUTER JOIN user_profile ON user_profile.fk_user_id=master_club.pk_club_id WHERE master_club.fk_league_id='$league_id'";
                $profiles = $this->db->query($sql)->result_array();
                foreach ($profiles as $pro) {
                    $members[] = $pro['fk_user_id'];
                }
            }
            $followers = $this->db->query("SELECT fk_user_id FROM followers WHERE fk_ref_id='" . $this->session->userdata('user_id') . "'")->result_array();
            foreach ($followers as $row) {
                $members[] = $row['fk_user_id'];
            }
        }

        $members = array_unique($members);
        $members = array_diff($members, array($this->session->userdata("user_id")));
        if (count($members) > 0) {
            $noti = array();
            foreach ($members as $member) {
                $noti[] = array(
                    "type" => "ART_NEW",
                    "fk_user_id" => $member,
                    "content" => $this->session->userdata("name") . " posted an article.",
                    "ref_id" => $id,
                    "read_status" => "0",
                    "sent_by" => $this->session->userdata("user_id")
                );
            }
            $this->db->insert_batch("notifications", $noti);
        }


        //**********Adding Reference in News Feed************
        $this->article_model->addFeedFromArticle($this->session->userdata('user_id'), $id);
        //Maintaine Slug Code

        $this->load->library('slug');
        $this->slug->set_config(array('table' => 'post', 'field' => 'slug', 'title' => 'title', 'id' => 'pk_post_id'));
        $rslug = $this->slug->create_uri($insert['title'], $id);
        if ($rslug == "0" or $rslug == 0 or $rslug == "") {
            $rslug = $id;
        }
        $this->db->update("post", array('slug' => $rslug), array('pk_post_id' => $id));

        //End of Slug Code
        $this->article_model->updateArticleImage($id, "file_name");
        if ($this->input->post('file_type') == "V") {
            $this->article_model->saveYoutubeImage($video_link);
        }

        $this->user_model->SavePoints($this->session->userdata('user_id'), $id, 'W_ART_WRT');
        $this->article_model->saveTags($id, $this->input->post('tags'));
    }

    public function updatearticle() {
        $id = $this->input->post('post_id');
        $club_id = 0;
        $league_id = 0;
        if ($this->input->post("league") > 0) {
            $club_id = intval($this->input->post("club"));
            $league_id = intval($this->input->post("league"));
        }
        if ($id > 0) {
            $video_link = $this->input->post('youtube_link');
            $update = array(
                'title' => $this->input->post('title'),
                'fk_club_id' => $club_id,
                'fk_league_id' => $league_id,
                'content' => $this->input->post('content'),
                'file_type' => $this->input->post('file_type'),
                'fk_user_id' => $this->session->userdata('user_id')
            );
//        if ($insert['file_type'] == 'V') {
//            $insert['file_name'] = $video_link . ".jpg";
//        }
            $this->db->update('post', $update, array('pk_post_id' => $id));


            //Maintaine Slug Code

            $this->load->library('slug');
            $this->slug->set_config(array('table' => 'post', 'field' => 'slug', 'title' => 'title', 'id' => 'pk_post_id'));
            $rslug = $this->slug->create_uri($id, $id);
            if ($rslug == "0" or $rslug == 0 or $rslug == "") {
                $rslug = $id;
            }
           // $this->db->update("post", array('slug' => $rslug), array('pk_post_id' => $id));

            //End of Slug Code
            $this->article_model->updateArticleImage($id, "file_name");
            if ($this->input->post('file_type') == "V" and $video_link != "") {
                $this->article_model->saveYoutubeImage($video_link);
            }

            //$this->user_model->SavePoints($this->session->userdata('user_id'), $id, 'W_ART_WRT');
        }
    }

    public function getclubs() {
        $lid = $this->input->post("league_id");
        $club_id = $this->input->post("club_id");
        $data = array('clubs' => false, 'type' => "select", 'club_id' => $club_id);
        $data['clubs'] = $this->master_model->getClubKVByLeague($lid, "1");
        $this->viewer->aview('utility/clubs.php', $data, false);
    }

    public function loadtopwriters() {
        $data = $this->input->post();
        $this->viewer->fview('articles/topwritersload.php', $data, false);
    }

    public function topwriters($club_slug = '') {
        $club_id = 0;
        $league_id = 0;
        if (is_numeric($club_slug)) {
            $club_id = 0;
            $league_id = $club_slug;
        } else {
            $club_id = $this->master_model->getClubIDBySlug($club_slug);
            $league_id = $this->master_model->getLeagueByClubID($club_id);
        }

//     $clubs = $this->master_model->getClubKV("1");
        $leagues = $this->master_model->getLeagueKV("1");
        $this->viewer->fview('articles/topwriters.php', array('leagues' => $leagues, 'league_id' => $league_id, 'club_id' => $club_id));
    }

    public function topwriters_data($user = '') {
        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        $skk = $this->session->userdata("sk");
        if ($skk != "") {
            $searchKey = $skk;
            $this->session->set_userdata("sk", "");
        } else {
            $searchKey = (isset($_GET['sk'])) ? $_GET['sk'] : "";
        }
        $ftype = $this->input->get();
        $data = $this->article_model->getTopWritersList($page, $perpage, $searchKey, $ftype);
        $data['page'] = getPaginationFooterFront($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->fview('articles/topwriters_data.php', $data, false);
    }

    public function loadtopfanz() {
        $data = $this->input->post();
        $this->viewer->fview('articles/topfanzload.php', $data, false);
    }

    public function topfanz($club_slug = '') {
        $club_id = 0;
        $league_id = 0;
        if ($club_slug != "") {
            if (is_numeric($club_slug)) {
                $club_id = 0;
                $league_id = $club_slug;
            } else {
                $club_id = $this->master_model->getClubIDBySlug($club_slug);
                $league_id = $this->master_model->getLeagueByClubID($club_id);
            }
        }
//     $clubs = $this->master_model->getClubKV("1");
        $leagues = $this->master_model->getLeagueKV("1");
        $this->viewer->fview('articles/topfanz.php', array('leagues' => $leagues, 'league_id' => $league_id, 'club_id' => $club_id));
    }

    public function topfanz_data($user = '') {
        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        $skk = $this->session->userdata("sk");
        if ($skk != "") {
            $searchKey = $skk;
            $this->session->set_userdata("sk", "");
        } else {
            $searchKey = (isset($_GET['sk'])) ? $_GET['sk'] : "";
        }
        $ftype = $this->input->get();
        $data = $this->article_model->getTopFanzList($page, $perpage, $searchKey, $ftype);
        $data['page'] = getPaginationFooterFront($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->fview('articles/topfanz_data.php', $data, false);
    }

    public function openfolder() {
        if ($handle = opendir(IMG_PROFILE)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    echo "$entry\n";
                    setImageRatio(IMG_PROFILE . $entry, 1);
                }
            }
            closedir($handle);
        }
    }

    public function openfolderthumb() {
        if ($handle = opendir(IMG_PROFILE . "thumb/")) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    echo "$entry\n";
                    setImageRatio(IMG_PROFILE . "thumb/" . $entry, 1);
                }
            }
            closedir($handle);
        }
    }

    public function testslug($id) {
        $data = $this->db->get_where("post", array("pk_post_id" => $id))->row_array();
        $insert['title'] = $data['title'];
        $this->load->library('slug');
        $this->slug->set_config(array('table' => 'post', 'field' => 'slug', 'title' => 'title', 'id' => 'pk_post_id'));
        $rslug = $this->slug->create_uri($insert['title'], $id);
        $this->db->update("post", array('slug' => $rslug), array('pk_post_id' => $id));
        echo $rslug;
    }

}
