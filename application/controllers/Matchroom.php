<?php
class Matchroom extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->library('viewer');
    $this->load->model('banter');
    $this->load->library('pagination');
  }
  
  public function index($is_api = 0)
  {
    $data = array(
      'live_matches'=>$this->banter->get_all_live_matches(),
      'upcoming_matches'=>$this->banter->get_all_upcoming_matches(),
      'ended_matches'=>$this->banter->get_all_ended_matches(),
    );
    

	if ($is_api == 1) {
	    // echo "<pre>".print_r($data, TRUE);
	    echo json_encode($data);
	    return;
	}
      $this->viewer->fview('match-room/match-room',$data);
    // $this->Banter->get_all_upcoming_matches();
  }
  
  //this section adds new banter on a per user basis
  public function add_banter($id,$post)
  {
    $user_id = $this->session->userdata("user_id");

        if ($is_api == 1) {
            $current_user_slug = $this->input->post('usr_slug');
            $post = $this->input->post('banter_post');
            $user_id = $this->user_model->getUserIDBySlug($current_user_slug);
        }

    $save = $this->Banter->add_banter($user_id,$id,$post);

        if ($is_api == 1) {
            $response = array('error' => TRUE, 'error_msg' => 'An error occured...');
            if ($save === TRUE) {
                $response['error'] = FALSE;
                $response['error_msg'] = '';
            }
            echo json_encode($response);
            return;
        }
  }
  
  //this section loads the banter view based on the id passed to it by the link
  public function load_banter($id, $is_api = 0)
  {
    $iden = $id;
    $rows = $this->db->query("SELECT COUNT(*) FROM banter WHERE match_room_id = $iden");
    // echo $rows;
    $config = array(
      //'base_url' => 'http://localhost/footyfanz/matchroom/load_banter/',
      'base_url' => site_url() .'/matchroom/load_banter/',
      'total_rows'=>12,
      'per_page' => 20
    );
    // $data_load = $this->banter->retrieve_all_banter($iden);
    $data = array('banter_id' => $iden);
    $query = $this->db->get_where('banter',$data)->result_array();

        if ($is_api == 1) {
            $data = $this->banter->retrieve_all_banter($iden);
            //echo "<pre>".print_r($data, TRUE);
            echo json_encode($data);
            return;
        }
    
    // $this->viewer->fview('includes/header-script');
    // $this->viewer->fview('includes/header-menu');
    $this->viewer->fview('banter/banterops',$data);
    // $this->viewer->fview('includes/footer');
    // $this->viewer->fview('includes/footer-script');
    
    $this->pagination->initialize($config);
    // echo $this->pagination->create_links();
  }
}
 ?>