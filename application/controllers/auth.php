<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('auth_model');
        $this->load->model('master_model');
        $this->load->model('video_model');
    }

    public function index($param = '') {
        $this->viewer->fview('index.php', array('users' => "Sheetal", 'css' => '../public/assets/plugins/jquery.bxslider/jquery.bxslider.css'));
    }

    public function verify() {
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $data = $this->auth_model->verifyUser($email, $password);
        if ($data) {
            if ($data['sts'] == "1") {
                $result = array('status' => 'success');
                $newdata = array(
                    'user_id' => $data['id'],
                    'username' => $data['name'],
                    'email' => $data['email'],
                    'image' => parseUserImage($data['sid'], $data['pic'], $data['gender']),
                    'logged_in' => TRUE
                    );
                $this->session->set_userdata($newdata);
                $result['user_id'] = $data['id'];
                echo json_encode($result);
            } else {
                echo json_encode(array('status' => 'inactive', 'msg' => 'Your account is not active'));
            }
        } else {
            echo json_encode(array('status' => 'invalid', 'msg' => 'Invalid Email or Password'));
        }
    }

    public function getheader() {
        echo $this->load->view("front/includes/header_login.php");
    }

    public function logout() {
        /*$this->session->sess_destroy();
        echo $this->load->view("front/includes/header_login.php");*/


        $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
        $this->session->sess_destroy();

        echo $this->load->view("front/includes/header_login.php");



    }

}
