<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Videos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->library('youtube');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model('master_model');
        $this->load->model('video_model');
        $this->load->model('college_model');
        if ($this->session->userdata('user_id') == "") {
            redirect("user/login");
        }
    }

    public function index($param = '') {
        
    }

    public function approved($param = '') {
        $category = $this->master_model->getCategoriesKV();
        $this->viewer->uview('videos/approved_list.php', array('menu' => "3-2", 'category' => $category, 'type' => '1', 'js' => array("videos.js")));
    }

    public function pending($param = '') {
        $category = $this->master_model->getCategoriesKV();
        $this->viewer->uview('videos/approved_list.php', array('menu' => "3-3", 'category' => $category, 'type' => '0', 'js' => array("videos.js")));
    }

    public function rejected($param = '') {
        $category = $this->master_model->getCategoriesKV();
        $this->viewer->uview('videos/approved_list.php', array('menu' => "3-4", 'category' => $category, 'type' => '2', 'js' => array("videos.js")));
    }

    public function approved_list_loader() {
        $this->viewer->uview('videos/approved_list_loader.php', $this->input->post(), false);
    }

    public function approvd_list($portlet = "") {
        $user_id = $this->session->userdata('user_id');
        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        $category = $this->input->get('category');
        $college = $this->input->get('college');
        $type = $this->input->get('type');
        $searchKey = isset($_GET['sk']) ? $_GET['sk'] : "";
        $data = $this->video_model->getVideosAllAdmin($type, $category, $page, $perpage, $searchKey, $college, $user_id);
        foreach ($data['data'] as $key => $dt) {
            $data['data'][$key]['categories'] = $this->video_model->getVideoCategory($dt['id']);
            $data['data'][$key]['tags'] = $this->video_model->getVideoTag($dt['id']);
        }
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $data['portlet'] = $portlet;
        $this->viewer->uview('videos/approved_list_page.php', $data, false);
    }

    public function video_status($param = "1") {
        $id = $this->input->post('id');
        $this->db->update("ko_post", array('sts' => $param), array('id' => $id));
        echo $id;
    }

    public function video_delete($param = "") {
        $id = $this->input->post('id');
        $this->db->delete("ko_post", array('id' => $id));
        $this->db->delete("ko_tag_post", array('post_id' => $id, 'post_type' => 'v'));
        $this->db->delete("ko_like", array('pid' => $id, 'typ' => 'v'));
        $this->db->delete("ko_view", array('pid' => $id, 'typ' => 'v'));
        $this->db->delete("ko_comment", array('tid' => $id, 'typ' => 'cov'));
        $this->db->delete("ko_cat_post", array('post_id' => $id));
        echo $id;
    }

    public function edit_video($id) {
        $category = $this->master_model->getCategoriesKV();
        $video = $this->video_model->getVideoById($id);

        $tags = $this->master_model->getDistinctTags();
        $fests = $this->college_model->getCollegeFest($video['collid']);
        $video['col_name'] = $this->college_model->getCollegeNemeById($video['collid']);
        $this->viewer->uview('videos/edit_video.php', array('menu' => "5-1", 'category' => $category, 'video' => $video, 'tags' => $tags, 'fests' => $fests, 'js' => array("videos.js", "../plugins/select2/select2.js"), 'css' => "plugins/select2/select2_metro.css"));
    }

    public function update() {
        $id = $this->input->post('id');
        $cates = $this->input->post('vcategory');
        $tags = $this->input->post('vtags');
        $this->video_model->updateVideoCategory($id, $cates);
        $this->video_model->updateVideoTags($id, $tags);
        $video = array(
            'name' => $this->input->post('title'),
            'descr' => $this->input->post('description'),
            'collid' => $this->input->post('college_id'),
            'fstid' => $this->input->post('vfest'),
        );
        $this->db->update("ko_post", $video, array('id' => $id));
        echo json_encode(array('title' => 'Video Update', 'text' => 'Video has been updated.'));
    }

    public function add() {
        $this->load->library('google');
        $this->viewer->uview('videos/add.php', array('menu' => "3-1", 'js' => array("videos.js", "../plugins/select2/select2.js"), 'css' => "plugins/select2/select2_metro.css"));
    }

    public function search() {
        $search = $this->input->post("search");
        if (FALSE !== filter_var($search, FILTER_VALIDATE_URL)) {
            $vid = $this->youtube->getVidById($search);
            if ($vid != "") {
                $result = $this->youtube->get_video($vid);
            } else {
                $result = $this->youtube->search_video($search);
            }
        } else {
            $result = $this->youtube->search_video($search);
        }
        $vids = array();
        foreach ($result as $res) {
            $vids[] = $res['id'];
        }
        $exist = $this->checkExists($vids);
        $this->viewer->uview('videos/search_result.php', array('result' => $result, 'exist' => $exist), false);
    }

    function checkExists($vids) {
        if (count($vids) > 0) {
            $videos = $this->db->query("SELECT * FROM ko_post WHERE url IN ('" . implode("','", $vids) . "')");
            $res = $videos->result_array();
            $ret = array();
            foreach ($res as $rs) {
                $ret[] = $rs['url'];
            }
            return $ret;
        } else {
            return array();
        }
    }

    public function videoform() {
        $categories = $this->master_model->getCategoriesKV();
        $tags = $this->master_model->getDistinctTags();
        $this->viewer->uview('videos/form.php', array('category' => $categories, 'tags' => $tags), false);
    }

    public function video_save() {
        $vid = $this->input->post("vid");
        $user = $this->session->userdata('user_id');
        // print_r($this->input->post());
        if ($this->video_model->checkVIDExist($vid)) {
            $result = array('status' => 'failed', 'msg' => 'This video is already exist on College Media.');
        } else {
            $tags = $this->input->post("vtags");
            $time = getTimestamp();
            $category = $this->input->post("vcategory");
            $cats = ($category == "") ? '' : implode(",", $category);
            $data = array(
                'sid' => $user,
                'name' => $this->input->post("vtitle"),
                'descr' => $this->input->post("vdescription"),
                'duration' => $this->input->post("vduration"),
                'url' => $this->input->post("vid"),
                'collid' => $this->input->post("college_id"),
                'fstid' => $this->input->post("vfest"),
                'sts' => '0',
                'typ' => 'v',
                'cat' => $cats,
                'date' => $time,
                'time' => strtotime($time)
            );
            $this->db->insert("ko_post", $data);
            $id = $this->db->insert_id();
            $this->load->library('slug');
            $this->slug->set_config(array('table' => 'ko_post', 'field' => 'slug', 'title' => 'name'));
            $rslug = $this->slug->create_uri($data['name'], $id);
            $this->db->update("ko_post", array('slug' => $rslug), array('id' => $id));
            if (is_array($category)) {
                foreach ($category as $ca) {
                    $this->db->insert("ko_cat_post", array('category_id' => $ca, 'post_id' => $id));
                }
            }
            if ($tags != '') {
                $tags = explode(",", $tags);
                foreach ($tags as $tg) {
                    if ($tg != '') {
                        $this->db->insert("ko_tag_post", array('tag' => trim($tg), 'post_id' => $id, 'post_type' => 'v'));
                    }
                }
            }
            $result = array('status' => 'success', 'msg' => 'Video has been saved successfully.');
        }
        echo json_encode($result);
    }

}
