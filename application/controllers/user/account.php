<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model('master_model');
        $this->load->model('college_model');
        $this->load->model('user_model');
        $this->load->model("auth_model");
        if ($this->session->userdata('user_id') == "") {
            redirect("user/login");
        }
    }

    public function index($param = '') {
        
    }

    public function manage() {
        $menu = "5";
        $id = $this->session->userdata('college_id');
        $cdata = $this->college_model->getCollegeById($id);
        $css = array('plugins/bootstrap-fileupload/bootstrap-fileupload.css');
        $js = array('../plugins/bootstrap-fileupload/bootstrap-fileupload.js', 'jquery.confirm.dialog.js', 'manage_college.js', "../plugins/ckeditor/ckeditor.js");
        $this->viewer->uview('account/manage.php', array('menu' => $menu, 'cdata' => $cdata, 'css' => $css, 'js' => $js, 'id' => $id));
    }

    /*     * *****************Profile Actions**************** */

    public function edit_profile($param = '') {
        $data['menu'] = "5";
        $id = $this->session->userdata('college_id');
        $data['cdata'] = $this->college_model->getCollegeByID($id);
        $this->viewer->cview('account/edit_profile.php', $data, false);
    }

    public function profile_save() {
        $data = array(
            'email' => $this->input->post("email"),
            'no' => $this->input->post("contact_no"),
            'address' => $this->input->post("address"),
            'est' => $this->input->post("est"),
            'web' => $this->input->post("website"),
            'course' => $this->input->post("course")
        );
        if (isset($_FILES['logo']) and $_FILES['logo']['error'] == 0) {
            $config['path'] = PUBLIC_DIR . LOGO_DIR . "/";
            $config['type'] = 'gif|jpg|png';
            $config['width'] = "200";
            $config['prefix'] = "logo";
            $config['file_name'] = "logo";
            $file = uploadFile($config);
            $data['pic'] = $file;
            if (trim($this->input->post("current_image")) !== '' and $this->input->post("current_image") != '0') {
                $old_file = $this->input->post("current_image");
                @unlink(PUBLIC_DIR . LOGO_DIR . '/' . $old_file);
            }
        } else {
            if (trim($this->input->post("current_image")) !== '' and $this->input->post("current_image") != '0') {
                $data['pic'] = $this->input->post("current_image");
            }
        }
        $this->db->update('ko_college', $data, "id =" . $this->session->userdata('college_id'));
        $res['status'] = "OK";
        echo json_encode($res);
    }

    public function check_user_email() {
        $email_exist = $this->college_model->checkUserEmail($this->input->post("email"), $this->input->post("user_id"));
        $res['status'] = "OK";
        if ($email_exist) {
            $res['status'] = 'ERROR';
            $res['title'] = "Email Availablity";
            $res['msg'] = $this->input->post("email") . " is already exist";
        }
        echo json_encode($res);
    }

    /*     * ***************User Action**************************** */

    public function profile($param = '') {
        $data['user'] = $this->user_model->getUserById($this->session->userdata('user_id'));
        $data['menu'] = '2-2';
        $this->viewer->uview('account/profile.php', $data);
    }

    function user_save() {
        $data = array(
            'name' => $this->input->post("name"),
            'email' => $this->input->post("email"),
            'designation' => $this->input->post("designation"),
            'contact_no' => $this->input->post("contact_no")
        );
        $this->db->update('ko_coll_users', $data, "id =" . $this->session->userdata('cuser_id'));
        $res = array("status" => 'OK', "msg" => '');
        echo json_encode($res);
    }

    public function password_save($param = '') {
        $old_password = $this->auth_model->verifyOldPassword($this->input->post("old_password"), $this->session->userdata('user_id'));
        if ($old_password == true) {
            $data = array('pass' => sha1($this->input->post("new_password")));
            $this->db->update('ko_user', $data, "id =" . $this->session->userdata('user_id'));
            $res['status'] = 'OK';
        } else {
            $res['status'] = 'INVALID';
            $res['msg'] = '<strong>Old Password</strong> has not matched';
        }
        echo json_encode($res);
    }

    public function change_password($param = '') {
        $password = $this->auth_model->getPassword($this->session->userdata('user_id'));
        $this->viewer->uview('account/change_password.php', array('menu' => "2-3", "password" => $password));
    }

}
