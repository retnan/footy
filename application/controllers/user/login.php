<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->model('auth_model');
        $this->load->helper('cookie');
    }

    public function index($param = '') {
        $this->viewer->uview('login/index.php', array(), false);
    }

    public function verify() {
        $username = $this->input->post("txt_username");
        $password = $this->input->post("txt_password");
        $data = $this->auth_model->verifyUser($username, $password);
        if ($data) {
            $result = array('status' => 'success');
            $newdata = array(
                'user_id' => $data['id'],
                'username' => $data['name'],
                'email' => $data['email'],
                'image' => parseUserImage($data['sid'], $data['pic'], $data['gender']),
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);
            $result['user_id'] = $data['id'];
            $result['page'] = "user/profile";
            echo json_encode($result);
        } else {
            echo json_encode(array('status' => 'invalid', 'msg' => "Wrong Username or Password"));
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

}
