<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
    }

   
    public function index($param = '') {
        //echo "This is INTERNAL USER Index";
         $this->viewer->uview('home.php', array('users' => "Sheetal"));
        // $this->fviewer->fview('login.php', array('users' => "Sheetal"));
    }
    public function profile($param = '') {
        $this->load->model("user_model");
        $profile= $this->user_model->getUser($this->session->userdata('id'));               
        if(is_file(PUBLIC_DIR.PROFILE_IMG_DIR."/large/".$profile['profilepic'])){
            $profile['image']=  base_url().PUBLIC_DIR.PROFILE_IMG_DIR."/large/".$profile['profilepic'];
        }else{
            $profile['image']=  base_url().PUBLIC_DIR."assets/img/default-user.png";
        }
         $this->viewer->uview('profile.php',  array('menu' => "0-0",'profile'=>$profile));        
    }
    public function profileview() {
        $this->load->model("user_model");
        $this->load->model("master_model");
        $professions=$this->master_model->getProfession();   
        $profile= $this->user_model->getUser($this->session->userdata('id'));               
        if(is_file(PUBLIC_DIR.PROFILE_IMG_DIR."/large/".$profile['profilepic'])){
            $profile['image']=  base_url().PUBLIC_DIR.PROFILE_IMG_DIR."/large/".$profile['profilepic'];
        }else{
            $profile['image']=  base_url().PUBLIC_DIR."assets/img/default-user.png";
        }
        $this->viewer->uview('profileview.php',  array('profile'=>$profile,'professions'=>$professions),FALSE);        
    }
    public function view($param = '') {
        echo "This is INTERNAL USER VIEW";
//         $this->fviewer->fview('login.php', array('users' => "Sheetal"));
    }
}
