<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reviews extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model('master_model');
        $this->load->model('review_model');
        if ($this->session->userdata('logged_in') == "") {
            echo '<script>window.location="' . base_url() . 'admin/";</script>';
            exit();
        }
    }

    public function getreview($type) {
        $type2 = array("1" => "1", "2" => "0", "3" => "2");
        $this->viewer->aview('reviews/index.php', array('menu' => "7-$type", 'type' => $type2[$type]));
    }

    public function review_list() {
        $page = $this->input->post('page');

        $perpage = PAGING_MED;
        if (isset($_GET['sk'])) {
            $searchKey = $_GET['sk'];
        } else {
            $searchKey = "";
        }
        $type = $this->input->get("type");
        $data = $this->review_model->getReviewsList($page, $perpage, $searchKey, $type, true);
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->aview('reviews/review_list.php', $data, false);
    }

    public function actions() {
        $action = $this->input->post("action");
        $id = $this->input->post("id");
        if ($action == "1" or $action == "2") {
            $this->db->update("ko_reviews", array("status" => $action), array("id" => $id));
        }
        if ($action == "3") {
            $this->db->delete("ko_reviews", array("id" => $id));
        }
    }

}
