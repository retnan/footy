<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Message extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model("auth_model");
        $this->load->model("email_model");
        $this->load->model("master_model");
        $this->load->model("user_model");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
        }
    }

    public function index($param = '') {
        $data['menu'] = "8-2";
        $data['js'] = array("../plugins/ckeditor/ckeditor.js");
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        $this->viewer->aview('message/index.php', $data);
    }

    public function get_clubs() {
        $league_id = $this->input->post("league_id");
        $data['clubs'] = $this->master_model->getClubKVByLeague($league_id, '1');
        $data['type'] = 'check';
        $this->viewer->aview('utility/clubs.php', $data, false);
    }

    public function save_message() {
        $this->user_model->sendMessage($this->input->post());
    }

    public function emaillog($param = '') {
        $data['menu'] = "6-3";
        $this->viewer->aview('newslettter/emaillog.php', $data);
    }

    public function emailloader() {
        $data = $this->input->post();
        $this->viewer->aview('newslettter/emailloader.php', $data, false);
    }

    public function email_list() {
        $page = $this->input->post('page');
        $category = $this->input->get('category');
        $perpage = 30;
        if (isset($_GET['sk'])) {
            $searchKey = $_GET['sk'];
        } else {
            $searchKey = "";
        }
        $data = $this->email_model->getEmailList($page, $perpage, $searchKey, $category);
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['page_number'] = $page - 1;
        $data['search'] = $searchKey;
        $this->viewer->aview('newslettter/email_list.php', $data, false);
    }

    public function emaillog_list() {
        $page = $this->input->post('page');
        $perpage = 30;
        if (isset($_GET['sk'])) {
            $searchKey = $_GET['sk'];
        } else {
            $searchKey = "";
        }
        $data = $this->email_model->getEmailLogList($page, $perpage, $searchKey);
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['page_number'] = $page - 1;
        $data['search'] = $searchKey;
        $this->viewer->aview('newslettter/emaillog_list.php', $data, false);
    }

    public function deletemail() {
        $id = $this->input->post("id");
        $this->db->delete("newsletter", array('pk_newsletter_id' => $id));
        echo json_encode(array("status" => "1", "title" => "Remove Email", "text" => "Email has been removed"));
    }

    public function sendnews($id = '') {
        $data['menu'] = "6-1";
        $data['js'] = array("../plugins/ckeditor/ckeditor.js");
        $data['categories'] = $this->master_model->getClubKV("1");
        $log = $this->db->get_where("newsletter_log", array("pk_log_id" => $id))->result_array();
        if (count($log) > 0) {
            $log_data = $log[0];
        } else {
            $log_data = array(
                "pk_log_id" => "",
                "subject" => "",
                "message" => "",
                "fk_club_id" => ""
            );
        }
        $data["log"] = $log_data;

        $this->viewer->aview('newslettter/sendnews.php', $data);
    }

    public function email_view() {
        $id = $this->input->post('category');
        $subject = urldecode($this->input->post('subject'));
        $message = urldecode($this->input->post('message'));
        $data['category_id'] = $id;
        $data['subject'] = $subject;
        $data['message'] = $message;
        $data['count'] = $this->email_model->getEmailCountInCategory($id);
        $this->viewer->aview('newslettter/email_view.php', $data, false);
    }

    public function sendNewsLetter() {
        $id = $this->input->post('id');
        $content = $this->input->post('content');
        $subject = $this->input->post('subject');
        $save = array(
            "subject" => $subject,
            "message" => $content,
            "fk_club_id" => $id
        );
        $this->db->insert("newsletter_log", $save);
        $emails = $this->email_model->getEmailsInCategory($id);
        if (count($emails) > 0) {
            $this->load->library('email');
            $config = getEmailConfig();
            $this->email->initialize($config);
            $this->email->from(ADMIN_EMAIL, SITE_NAME);
            $this->email->subject($subject);

            foreach ($emails as $address) {
                $message = $content; // . "<br><br><p style='color:#777'>To unsubscribe news <a href='" . site_url() . "newsletter/unsubscribe/?e=" . base64_encode($address) . "'> click here</a></p>";
                $message = $this->viewer->emailview("newsletter_message.php", array('message' => $message));
                $this->email->message($message);
                $this->email->to($address);
                $this->email->send();
            }
        }
    }

    public function add_form($id = '') {
        if ($id == '') {
            $data['categories'] = $this->master_model->getClubKV("1");
            $this->viewer->aview('modal/email_add.php', $data, false);
        } else {
            $tcat = $this->db->get_where("newsletter", array('pk_newsletter_id' => $id))->result_array();
            $data['cat_data'] = $tcat[0];
            $data['id'] = $id;
            //$data['cats'] = $cates;
            $this->viewer->aview('modal/category_edit.php', $data, false);
        }
    }

    public function email_save() {
        $res = array("status" => 'OK', "msg" => '');
        $data = array("email" => $this->input->post("email"), "status" => "1", "fk_club_id" => $this->input->post("category"));
        $this->db->insert("newsletter", $data);
        echo json_encode($res);
    }

    public function deleteaction() {
        $id = $this->input->post('id');
        $this->db->delete("newsletter_log", array("pk_log_id" => $id));
        echo json_encode(array('status' => '1', 'title' => "Newsletter Log", 'text' => "Log has been deleted"));
    }

}
