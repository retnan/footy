<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model("cms_model");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
            exit();
        }
    }

    public function index($param = '') {
        
    }

    public function editor($type = "") {
        $menu = array(
            'contact' => '1',
            'faq' => '2',
            'terms' => '3',
            'privacy' => '4',
            'legal' => '5',
            'feedback' => '6',
            '404' => '10',
            'contactus' => '12',
            'homemiddletext' => '13'
        );
        $data['content'] = $this->cms_model->getContent($type);
        $data['type'] = $type;
        $data['menu'] = "10-" . $menu[$type];
        $data['js'] = array("../plugins/ckeditor/ckeditor.js");
        $this->viewer->aview('cms/editor.php', $data);
    }

    public function saveeditor() {
        $type = $this->input->post("type");
        $content = $this->input->post("content");
        $this->cms_model->setContent($type, $content);
    }

    public function social() {
        $data['menu'] = "10-2";
        $this->viewer->aview('cms/social.php', $data);
    }

    public function footer() {
        $data['menu'] = "10-4";
        $this->viewer->aview('cms/footer.php', $data);
    }

    public function homemeta() {
        $data['menu'] = "10-15";
        $this->viewer->aview('cms/homemeta.php', $data);
    }

    public function innermeta() {
        $data['menu'] = "10-16";
        $this->viewer->aview('cms/innermeta.php', $data);
    }

    public function savesocial() {
        $socials = $this->input->post();
        //print_r($socials);
        foreach ($socials as $key => $val) {
            $this->cms_model->setContent($key, $val);
        }
    }

    public function savefooter() {
        $socials = $this->input->post();
        //print_r($socials);
        foreach ($socials as $key => $val) {
            $this->cms_model->setContent($key, $val);
        }
    }

    public function savehomemeta() {
        $homemeta = $this->input->post('homemeta');
        $this->cms_model->setContent("homemeta", $homemeta);
    }

    public function saveinnermeta() {
        $homemeta_key = $this->input->post('homemeta_key');
        $homemeta_desc = $this->input->post('homemeta_desc');
        $this->cms_model->setContent("homemeta_key", $homemeta_key);
        $this->cms_model->setContent("homemeta_desc", $homemeta_desc);
    }

    public function videogallery() {
        $data['menu'] = "10-3";
        $data['js'] = array("master.js");
        $this->viewer->aview('cms/videogallery.php', $data);
    }

    public function video_list() {
        $data['menu'] = "";
        $page = $this->input->post('page');
        $perpage = 9;
        $searchKey = isset($_GET['sk']) ? $_GET['sk'] : "";
        $data = $this->cms_model->getVideoList($page, $perpage, $searchKey);
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->aview('cms/videogallery_data.php', $data, false);
    }

    public function video_form() {
        $data['menu'] = "";
        $this->viewer->aview('modal/videogallery_add.php', $data, false);
    }

    public function savegallery() {

        $title = $this->input->post("title");
        $video_link = $this->input->post("youtube_link");
        $img = $this->saveYoutubeImage($video_link);
        $save = array(
            "file_name" => $img,
            "title" => $title,
            "fk_user_id" => $this->session->userdata("admin_id"),
            "type" => "Y",
            "status" => "1"
        );
        $this->db->insert("home_gallery", $save);
        echo json_encode(array("status" => "1", "msg" => "Video saved"));
    }

    public function gallery_status($status = '') {
        $this->db->update('home_gallery', array("status" => $status), "pk_gallery_id =" . $this->input->post("id"));
        if ($status == 1) {
            $res = array("title" => "Gallery", "text" => "Video has been Activated");
        } else {
            $res = array("title" => "Gallery", "text" => "Video has been Deactivated");
        }
        echo json_encode($res);
    }

    public function gallery_delete($status = '') {

        $child = $this->db->get_where('home_gallery', "pk_gallery_id =" . $this->input->post("id"))->result_array();
        @unlink(IMG_GALLERY . $child[0]['file_name']);
        $this->db->delete('home_gallery', "pk_gallery_id =" . $this->input->post("id"));
        $res = array('status' => "1", "title" => "Gallery", "text" => "Video has been deleted");
        echo json_encode($res);
    }

    public function saveYoutubeImage($vid) {
        $url = "http://img.youtube.com/vi/$vid/hqdefault.jpg";

        $content = file_get_contents($url);
//Store in the filesystem.
        $img = $vid . ".jpg";
        @unlink(IMG_GALLERY . $img);
        @unlink(IMG_GALLERY . "thumb/" . $img);
        $fp = fopen(IMG_GALLERY . $img, "w");
        fwrite($fp, $content);
        fclose($fp);
        setImageRatio(IMG_GALLERY . $img);
        @copy(IMG_GALLERY . $img, IMG_GALLERY . "thumb/" . $img);
        resizeFile(IMG_GALLERY . "thumb/" . $img, 120);
        return $img;
    }

}
