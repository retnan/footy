<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Master extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model('master_model');
    }

    public function index($param = '') {
        
    }

    public function category($param = '') {
        $this->viewer->aview('master/category.php', array('menu' => "3-1", 'js' => array("master.js")));
    }

    public function club($param = '') {
        $data = array('menu' => "3-1", 'js' => array("master.js"));
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        $this->viewer->aview('master/club.php', $data);
    }

    public function clubloader() {
        $data['league'] = $this->input->post("league");
        $this->viewer->aview('master/club_loader.php', $data, false);
    }

    public function club_list() {
        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        $searchKey = isset($_GET['sk']) ? $_GET['sk'] : "";
        $data = $this->master_model->getClubList($page, $perpage, $searchKey, $_GET['league']);
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;

        $this->viewer->aview('master/club_data.php', $data, false);
    }

    public function club_form($id = '') {
        $leagues = $this->master_model->getLeagueKV("1");
        if ($id == '') {
            $this->viewer->aview('modal/club_add.php', array('leagues' => $leagues), false);
        } else {
            $data['club_data'] = $this->master_model->getClubByID($id);
            $data['id'] = $id;
            $data['leagues'] = $leagues;
            $this->viewer->aview('modal/club_edit.php', $data, false);
        }
    }

    public function club_status($status = '') {
        $this->db->update('master_club', array("status" => $status), "pk_club_id =" . $this->input->post("id"));
        if ($status == 1) {
            $res = array("title" => "Club", "text" => "Club has been Activated");
        } else {
            $res = array("title" => "Club", "text" => "Club has been Deactivated");
        }
        echo json_encode($res);
    }

    public function club_delete($status = '') {
        $child = $this->db->get_where('user_profile', "fk_club_id =" . $this->input->post("id"));
        if ($child->num_rows > 0) {
            $res = array("title" => "Club can not delete", "text" => $child->num_rows . " users has been Registered");
            $res['status'] = 'ERROR';
        } else {
            $this->db->update('master_club', array('is_deleted' => '1'), "pk_club_id =" . $this->input->post("id"));
            $res = array('status' => "1", "title" => "Club", "text" => "Club has been deleted");
        }
        echo json_encode($res);
    }

    public function club_save($param = '') {
        $chk_exist = $this->master_model->checkClubExist($this->input->post("title"), $this->input->post("league"), $this->input->post("club_id"));
//        print_r($_POST);
//        exit;
        if (!$chk_exist) {
            $data = array('title' => $this->input->post("title"), 'team_manager' => $this->input->post("team_manager"), "captain" => $this->input->post("captain"), "stadium" => $this->input->post("stadium"), "fk_league_id" => $this->input->post("league"), 'description' => $this->input->post("description"), 'trophies' => $this->input->post("trophies"));
            $logo = $this->uploadImage("logo", CLUB_IMG_DIR, "club", '150');

            if ($this->input->post("club_id") > 0) {
                if ($logo) {
                    $data['logo'] = $logo;
//                    $club = $this->db->get_where("master_club", array('pk_club_id' => $this->input->post("club_id")))->result_array();
                    if ($this->input->post("current_logo") != "") {
                        @unlink(CLUB_IMG_DIR . $this->input->post("current_logo"));
                    }
                }
                $this->db->update('master_club', $data, "pk_club_id =" . $this->input->post("club_id"));
                $id = $this->input->post("club_id");
            } else {
                if ($logo) {
                    $data['logo'] = $logo;
                }
                $data['status'] = '1';
                $this->db->insert('master_club', $data);
                $id = $this->db->insert_id();
            }
            //Maintaine Slug Code

            $this->load->library('slug');
            $this->slug->set_config(array('table' => 'master_club', 'field' => 'slug', 'title' => 'title', 'id' => 'pk_club_id'));
            $rslug = $this->slug->create_uri($data['title'], $id);
            $this->db->update("master_club", array('slug' => $rslug), array('pk_club_id' => $id));

            //End of Slug Code


            $res = array("status" => 'OK', "msg" => '');
        } else {
            $res['status'] = 'ERROR';
            $res['msg'] = "Club <b>" . $this->input->post("title") . "</b> is already exists in this Club List";
        }
        echo json_encode($res);
    }

    public function league($param = '') {
        $data = array('menu' => "3-2", 'js' => array("master.js"));
        $this->viewer->aview('master/league.php', $data);
    }

    public function league_list() {
        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        $searchKey = isset($_GET['sk']) ? $_GET['sk'] : "";
        $data = $this->master_model->getLeagueList($page, $perpage, $searchKey);
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;

        $this->viewer->aview('master/league_data.php', $data, false);
    }

    public function league_form($id = '') {
        if ($id == '') {
            $this->viewer->aview('modal/league_add.php', array(), false);
        } else {
            $data['league_data'] = $this->master_model->getLeagueByID($id);
            $data['id'] = $id;
            $this->viewer->aview('modal/league_edit.php', $data, false);
        }
    }

    public function league_status($status = '') {
        $this->db->update('master_league', array("status" => $status), "pk_league_id =" . $this->input->post("id"));
        if ($status == 1) {
            $res = array("title" => "League", "text" => "League has been Activated");
        } else {
            $res = array("title" => "League", "text" => "League has been Deactivated");
        }
        echo json_encode($res);
    }

    public function league_delete($status = '') {
        $child = $this->db->get_where('master_club', "fk_league_id =" . $this->input->post("id"));
        if ($child->num_rows > 0) {
            $res = array("title" => "League", "text" => "League has has been used in Club Master");
            $res['status'] = 'ERROR';
        } else {
            $this->db->update('master_league', array('is_deleted' => '1'), "pk_league_id =" . $this->input->post("id"));
            $res = array('status' => "1", "title" => "League", "text" => "League has been deleted");
        }
        echo json_encode($res);
    }

    public function league_save($param = '') {
        $chk_exist = $this->master_model->checkLeagueExist($this->input->post("title"), $this->input->post("league_id"));
//        print_r($_POST);
//        exit;
        if (!$chk_exist) {
            $data = array('title' => $this->input->post("title"), 'description' => $this->input->post("description"));
            $logo = $this->uploadImage("logo", LEAGUE_IMG_DIR, "league", '150');

            if ($this->input->post("league_id") > 0) {
                if ($logo) {
                    $data['logo'] = $logo;
                    if ($this->input->post("current_logo") != "") {
                        @unlink(LEAGUE_IMG_DIR . $this->input->post("current_logo"));
                    }
                }
                $this->db->update('master_league', $data, "pk_league_id =" . $this->input->post("league_id"));
            } else {
                if ($logo) {
                    $data['logo'] = $logo;
                }
                $data['status'] = '1';
                $this->db->insert('master_league', $data);
                $id = $this->db->insert_id();
            }
//            $this->load->library('slug');
//            $this->slug->set_config(array('table' => 'categories', 'field' => 'slug', 'title' => 'category', 'id' => 'pk_category_id'));
//            $rslug = $this->slug->create_uri($data['category'], $id);
//            $this->db->update("categories", array('slug' => $rslug), array('pk_category_id' => $id));


            $res = array("status" => 'OK', "msg" => '');
        } else {
            $res['status'] = 'ERROR';
            $res['msg'] = "League <b>" . $this->input->post("title") . "</b> is already exists in this League List";
        }
        echo json_encode($res);
    }

    public function media($param = '') {
        $this->viewer->aview('master/media.php', array('menu' => "3-3", 'js' => array("master.js", "jquery.form.js")));
    }

    public function uploadmedia() {

        if (isset($_FILES['logo']) and $_FILES['logo']['error'] == 0) {
            $name = $_FILES['logo']['name'];
            $pathinfo = pathinfo($name);
            $realname = $pathinfo['basename'];
            if (strtolower($pathinfo['extension']) == "php") {
                echo json_encode(array("status" => "0", "msg" => "Invalid file selected"));
                exit();
            }

            $CI = & get_instance();
            $config['allowed_types'] = "*";
            $config['upload_path'] = MEDIA_DIR;
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';
            $config['file_name'] = "media_" . strtotime("now") . rand("10", "99");
            $CI->load->library('upload', $config);
            $CI->upload->initialize($config);
            if (!$CI->upload->do_upload("logo")) {
                echo json_encode(array("status" => "0", "msg" => $CI->upload->display_errors()));
            } else {
                $data = $CI->upload->data();
                $filename = $data['file_name'];
                $data = array('name' => $realname, "media" => $filename);
                $this->db->insert("medias", $data);
                echo json_encode(array("status" => "1", "msg" => "File uploaded successfully"));
            }
        } else {
            echo json_encode(array("status" => "0", "msg" => "No file selected"));
        }
    }

    public function media_list() {
        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        $searchKey = isset($_GET['sk']) ? $_GET['sk'] : "";
        $data = $this->master_model->getMediaList($page, $perpage, $searchKey);
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->aview('master/media_data.php', $data, false);
    }

    public function media_delete() {
        $media = $this->db->get_where('medias', "pk_media_id = " . $this->input->post("id"))->result_array();
        @unlink(MEDIA_DIR . $media[0]['media']);
        $this->db->delete('medias', "pk_media_id =" . $this->input->post("id"));
        $res = array("title" => "Media", "text" => "File has been deleted");
        echo json_encode($res);
    }

    private function uploadImage($name, $path, $pref = "", $size = "200") {
        if (isset($_FILES[$name]) and $_FILES[$name]['error'] == 0) {
            $config['path'] = $path;
            $config['type'] = 'gif|jpg|png|jpeg';
            $config['width'] = $size;
            $config['prefix'] = $pref;
            $config['file_name'] = $name;
            $file = uploadFile($config);
            return $file;
            if ($file) {

//                @copy(IMG_ARTICLE . $file, CLUB_IMG_DIR . "thumb/" . $file);
//                resizeFile(CLUB_IMG_DIR . "thumb/" . $file, 120);
//                $art = $this->db->get_where("articles", array('pk_article_id' => $id))->result_array();
//                if ($art[0]['image']) {
//                    @unlink(IMG_ARTICLE . $art[0]['image']);
//                    @unlink(IMG_ARTICLE . "thumb/" . $art[0]['image']);
//                }
//                $this->db->update("articles", array('image' => $file), array('pk_article_id' => $id));
            }
        }
    }

}
