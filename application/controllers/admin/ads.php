<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ads extends CI_Controller {

    public $typemenu = array("square" => "1", "small" => "2", "medium" => "3", "large" => "4");

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model("auth_model");
        $this->load->model("master_model");
        $this->load->model("ads_model");
        $this->load->model("cms_model");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
        }
    }

    public function index($param = '') {
        $this->viewer->aview('account/profile.php');
    }

    public function manager($type = "") {
        $data['menu'] = "7-" . $this->typemenu[$type];
        $data['type'] = $type;
        $this->viewer->aview('ads/index.php', $data);
    }

    public function manage($type, $id) {
        $data['sub_types'] = array('1' => "Home", '3' => "Newsfeed", '4' => 'Articles', '5' => 'Clubs', '6' => 'League');
        $data['menu'] = "7-" . $this->typemenu[$type];
        $data['type'] = $type;
        $data['ad_data'] = $this->db->get_where("ads", array('pk_ad_id' => $id))->result_array();
        $data['location'] = $this->ads_model->getAdLocation($id);
        $data['current_club_league'] = "0";
        if (isset($data['location'][5])) {

            if (is_array($data['location'][5])) {
                $ldata = $this->db->get_where("master_club", array('pk_club_id' => $data['location'][5][0]))->result_array();
                if (count($ldata) > 0) {
                    $data['current_club_league'] = $ldata[0]['fk_league_id'];
                }
            }
        }
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        $this->viewer->aview('ads/manage_ads.php', $data);
    }

    public function ads_list() {
        $page = $this->input->post('page');
        $type = $this->input->get('type');
        $perpage = PAGING_MED;
        if (isset($_GET['sk'])) {
            $searchKey = $_GET['sk'];
        } else {
            $searchKey = "";
        }
        $data = $this->ads_model->getAdsList($page, $perpage, $searchKey, $type);
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->aview('ads/ads_list.php', $data, false);
    }

    public function ads_form($type = '', $id = '') {
        $tcat = $this->db->get_where("ads", array('ad_type' => $type, 'pk_ad_id' => $id));
        $cates = $tcat->result_array();
        if (count($cates) > 0) {
            $this->viewer->aview('modal/ads_edit.php', array('ads' => $cates[0]), false);
        } else {
            $data['type'] = $type;
            $this->viewer->aview('modal/ads_add.php', $data, false);
        }
    }

    public function ads_save($user_id = '') {
        $start_date = dmyToYmd($this->input->post("start_date"));
        $end_date = dmyToYmd($this->input->post("end_date"));
        $link = $this->input->post("ad_url");
        $type = $this->input->post("type");
        $this->db->insert("ads", array("ad_url" => $link, "start_date" => $start_date, 'end_date' => $end_date, "ad_type" => $type));
        $id = $this->db->insert_id();

        if (isset($_FILES['logo']) and $_FILES['logo']['error'] == 0) {
            $config['path'] = ADS_DIR . "/";
            $config['type'] = 'gif|jpg|png|jpeg';
            $config['width'] = "600";
            if ($type == "small") {
                $config['width'] = "600";
            }
            if ($type == "square") {
                $config['width'] = "350";
            }
            if ($type == "medium") {
                $config['width'] = "400";
            }
            if ($type == "large") {
                $config['width'] = "1200";
            }
            if ($type == "top") {
                $config['width'] = "1200";
            }

            $config['prefix'] = "logo";
            $config['file_name'] = "logo";
            $file = uploadFile($config);
            if ($file) {
                $tagboard = $this->db->get_where("ads", array("pk_ad_id" => $id))->result_array();
                if (count($tagboard)) {
                    @unlink(ADS_DIR . $tagboard[0]['image']);
                    $this->db->update("ads", array("image" => $file), array("pk_ad_id" => $id));
                }
            }
        }
        echo json_encode(array("status" => "1", "msg" => "Ads saved successfully."));
    }

    public function ads_update($type = '', $id = '') {
        $start_date = dmyToYmd($this->input->post("start_date"));
        $end_date = dmyToYmd($this->input->post("end_date"));
        $link = $this->input->post("ad_url");
        $type = $this->input->post("type");

        $this->db->update("ads", array("ad_url" => $link, "start_date" => $start_date, 'end_date' => $end_date), array("pk_ad_id" => $id));

        if (isset($_FILES['logo']) and $_FILES['logo']['error'] == 0) {
            $config['path'] = ADS_DIR . "/";
            $config['type'] = 'gif|jpg|png|jpeg';
            $config['width'] = "600";
            if ($type == "small") {
                $config['width'] = "300";
            }
            if ($type == "medium") {
                $config['width'] = "400";
            }
            if ($type == "large") {
                $config['width'] = "500";
            }
            if ($type == "top") {
                $config['width'] = "1200";
            }

            $config['prefix'] = "logo";
            $config['file_name'] = "logo";
            $file = uploadFile($config);
            if ($file) {
                $tagboard = $this->db->get_where("ads", array("pk_ad_id" => $id))->result_array();
                if (count($tagboard)) {
                    @unlink(ADS_DIR . $tagboard[0]['image']);
                    $this->db->update("ads", array("image" => $file), array("pk_ad_id" => $id));
                }
            }
        }
        echo json_encode(array("status" => "1", "msg" => "Ads updated successfully."));
    }

    public function get_clubs() {
        $league_id = $this->input->post("league_id");
        $data['curr_clubs'] = explode(",", $this->input->post('curr_clubs'));
        $data['clubs'] = $this->master_model->getClubKVByLeague($league_id, '1');
        $data['type'] = 'check_selected';
        $this->viewer->aview('utility/clubs.php', $data, false);
    }

    public function update_location() {
        $post = $this->input->post();
        $save_data = array();
        $sub_types = $post['sub_type'];
        $this->db->delete("ad_location", array("fk_ad_id" => $post['ad_id']));
        if (is_array($sub_types) and count($sub_types) > 0) {

            foreach ($sub_types as $type) {
                if ($type < 5) {
                    $save_data[] = array("fk_ad_id" => $post['ad_id'], "type" => $type, "ref_id" => 0);
                } else if ($type == 5) {
                    if ($post['club_league'] == 0) {
                        $save_data[] = array("fk_ad_id" => $post['ad_id'], "type" => $type, "ref_id" => 0);
                    } else {
                        if (isset($post['club']) and count($post['club']) > 0) {
                            $clubs = $post['club'];
                        } else {
                            $clubs_by_league = $this->master_model->getClubKVByLeague($post['club_league'], '1');
                            $clubs = array_keys($clubs_by_league);
                        }
                        if (is_array($clubs) and count($clubs) > 0) {
                            foreach ($clubs as $club) {
                                $save_data[] = array("fk_ad_id" => $post['ad_id'], "type" => $type, "ref_id" => $club);
                            }
                        }
                    }
                } else if ($type == 6) {
                    if (isset($post['leagues']) and count($post['leagues']) > 0) {
                        foreach ($post['leagues'] as $league) {
                            $save_data[] = array("fk_ad_id" => $post['ad_id'], "type" => $type, "ref_id" => $league);
                        }
                    }
                }
            }
            $this->db->insert_batch("ad_location", $save_data);
//            print_r($save_data);
        }
    }

    public function actions() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->update("ads", array('status' => $status), array("pk_ad_id" => $id));
        if ($status == '1') {
            echo json_encode(array('status' => '1', 'title' => "Ads status", 'text' => "Ads has been activated"));
        } else {
            echo json_encode(array('status' => '1', 'title' => "Ads status", 'text' => "Ads has been deactivated"));
        }
    }

    public function deleteaction() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->delete("ads", array("pk_ad_id" => $id));
        @unlink(ADS_DIR . $this->input->post('image'));
        echo json_encode(array('status' => '1', 'title' => "Ads status", 'text' => "Ads has been deleted"));
    }

    public function google_update() {
        $ad_ggl_small = $this->input->post('ad_ggl_small');
        $ad_ggl_mid = $this->input->post('ad_ggl_mid');
        $ad_ggl_large = $this->input->post('ad_ggl_large');
        $ad_ggl_top = $this->input->post('ad_ggl_top');
        $ad_ggl_square = $this->input->post('ad_ggl_square');
        $this->cms_model->setContent("ad_ggl_small", $ad_ggl_small);
        $this->cms_model->setContent("ad_ggl_mid", $ad_ggl_mid);
        $this->cms_model->setContent("ad_ggl_large", $ad_ggl_large);
        $this->cms_model->setContent("ad_ggl_top", $ad_ggl_top);
        $this->cms_model->setContent("ad_ggl_square", $ad_ggl_square);
        echo json_encode(array('status' => '1', 'title' => "Google Ads", 'msg' => "Ads has been updated"));
    }

    public function google($type = "") {
        $data['menu'] = "7-5";
        $data['type'] = $type;
        $this->viewer->aview('ads/google.php', $data);
    }

}
