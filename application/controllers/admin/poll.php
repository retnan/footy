<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Poll extends CI_Controller {

    public $typemenu = array("square" => "1", "small" => "2", "medium" => "3", "large" => "4");

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model("auth_model");
        $this->load->model("master_model");
        $this->load->model("poll_model");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
        }
    }

    public function index($param = '') {
        $data['menu'] = "9-1";
        $this->viewer->aview('poll/index.php', $data);
    }

    public function create_poll($param = '') {
        $data['menu'] = "9-1";
        $this->viewer->aview('poll/create_poll.php', $data);
    }

    public function edit_poll($id) {
        $data['menu'] = "9-1";
        $data['poll'] = $this->db->get_where("polls", array("pk_poll_id" => $id))->row_array();
        $this->viewer->aview('poll/edit_poll.php', $data);
    }

    public function poll_list() {
        $page = $this->input->post('page');

        $perpage = PAGING_MED;
        if (isset($_GET['sk'])) {
            $searchKey = $_GET['sk'];
        } else {
            $searchKey = "";
        }
        $data = $this->poll_model->getPollList($page, $perpage, $searchKey);
        foreach ($data['data'] as $key => $value) {
            $data['data'][$key]['placement'] = $this->poll_model->getPlacement("poll", $value['pk_poll_id']);
        }
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->aview('poll/poll_list.php', $data, false);
    }

    public function save() {
        $id = $this->input->post("poll_id");
        $type = $this->input->post("type");
        $start_date = dmyToYmd($this->input->post("start_date"));
        $end_date = dmyToYmd($this->input->post("end_date"));
        $post_data = array(
            "title" => $this->input->post("title"),
            "type" => $this->input->post("type"),
            "option1" => $this->input->post("option1"),
            "option2" => $this->input->post("option2"),
            "option3" => $this->input->post("option3"),
            "option4" => $this->input->post("option4"),
            "option5" => $this->input->post("option5"),
            "start_date" => $start_date,
            "end_date" => $end_date,
            "status" => "1"
        );
        if ($id > 0) {
            $this->db->update("polls", $post_data, array('pk_poll_id' => $id));
        } else {
            $post_data['status'] = '0';
            $this->db->insert("polls", $post_data);
            $id = $this->db->insert_id();
        }

        if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
            $config['path'] = POLL_DIR . "/";
            $config['type'] = 'gif|jpg|png|jpeg';
            $config['width'] = "600";
            if ($type == "V") {
                $config['width'] = "300";
            }
            if ($type == "H") {
                $config['width'] = "800";
            }

            $config['prefix'] = "img";
            $config['file_name'] = "image";
            $file = uploadFile($config);
            if ($file) {
                $tagboard = $this->db->get_where("polls", array("pk_poll_id" => $id))->result_array();
                if (count($tagboard) > 0) {
                    @unlink(POLL_DIR . $tagboard[0]['image']);
                }
                $this->db->update("polls", array("image" => $file), array("pk_poll_id" => $id));
            }
        }
        echo json_encode(array("status" => "1", "msg" => "Poll Created successfully."));
    }

    public function get_clubs() {
        $league_id = $this->input->post("league_id");
        $data['curr_clubs'] = explode(",", $this->input->post('curr_clubs'));
        $data['clubs'] = $this->master_model->getClubKVByLeague($league_id, '1');
        $data['type'] = 'check_selected';
        $this->viewer->aview('utility/clubs.php', $data, false);
    }

    public function actions() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->update("polls", array('status' => $status), array("pk_poll_id" => $id));
        if ($status == '1') {
            echo json_encode(array('status' => '1', 'title' => "Poll status", 'text' => "Poll has been activated"));
        } else {
            echo json_encode(array('status' => '1', 'title' => "Poll status", 'text' => "Poll has been deactivated"));
        }
    }

    public function deleteaction() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->delete("polls", array("pk_poll_id" => $id));
        @unlink(ADS_DIR . $this->input->post('image'));
        echo json_encode(array('status' => '1', 'title' => "Polls status", 'text' => "Poll has been deleted"));
    }

}
