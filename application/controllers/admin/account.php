<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model("auth_model");
        $this->load->model("email_model");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
        }
    }

    public function index($param = '') {
        $this->viewer->aview('account/profile.php');
    }

    public function profile($param = '') {
        $data['menu'] = "2-1";
        $data['data'] = $this->auth_model->getAdminUserById($this->session->userdata('admin_id'));
        $this->viewer->aview('account/profile.php', $data);
    }

    public function profile_save($param = '') {
        $data = array(
            'firstname' => $this->input->post("firstname"),
            'lastname' => $this->input->post("lastname"),
            'email' => $this->input->post("email")
        );
        $name = "profile_image";
        if (isset($_FILES[$name]) and $_FILES[$name]['error'] == 0) {
            $config['path'] = IMG_PROFILE;
            $config['type'] = 'gif|jpg|png|jpeg';
            $config['width'] = "400";
            $config['prefix'] = "admin";
            $config['file_name'] = $name;
            $file = uploadFile($config);
            if ($file) {
                $profile = $this->auth_model->getAdminUserById($this->session->userdata('admin_id'));
                @unlink(IMG_PROFILE . $profile['profile_pic']);
                $data2['profile_pic'] = $file;
                $this->db->update('user_profile', $data2, "fk_user_id =" . $this->session->userdata('admin_id'));
            }
        }

        $this->db->update('users', $data, "pk_user_id =" . $this->session->userdata('admin_id'));
        $newdata = array('admin_name' => $data['firstname'] . ' ' . $data['firstname'], 'admin_email' => $data['email']);
        $this->session->set_userdata($newdata);
        $res['status'] = 'OK';
        $profile = $this->auth_model->getAdminUserById($this->session->userdata('admin_id'));
        $res['image'] = $profile['thumb'];
        echo json_encode($res);
    }

    public function password_save($param = '') {
        $old_password = $this->auth_model->verifyAdminOldPassword($this->input->post("old_password"), $this->session->userdata('admin_id'));
        if ($old_password == true) {
            $data = array('password' => ($this->input->post("new_password")));
            $this->db->update('users', $data, "pk_user_id =" . $this->session->userdata('admin_id'));
            $res['status'] = 'OK';
        } else {
            $res['status'] = 'INVALID';
            $res['msg'] = '<strong>Old Password</strong> did not matched';
        }
        echo json_encode($res);
    }

    public function password($param = '') {
        $this->viewer->aview('account/change_password.php', array('menu' => "2-2"));
    }

    public function emails($param = '') {
        $data['menu'] = "2-3";
        $data['js'] = array("../plugins/ckeditor/ckeditor.js");
        $data['emails'] = $this->email_model->getOptions("paypal_id");
        $this->viewer->aview('account/emails.php', $data);
    }

    public function getemail() {
        $id = $this->input->post('id');
        $data['email'] = $this->email_model->getEmail($id);
        $this->viewer->aview('account/email_view.php', $data, false);
    }

    public function saveemail() {
        $id = $this->input->post('id');
        $content = $this->input->post('content');
        $save = array(
            'content' => $content
        );
        $this->db->update("emails", $save, array('pk_email_id' => $id));
    }

}
