<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model("cms_model");
        $this->load->model("master_model");
        $this->load->model("user_model");
        $this->load->model("article_model");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
            exit();
        }
    }

    public function index($param = '') {
        $data['menu'] = "5-2";
        $data['clubs'] = $this->master_model->getClubKV("1");
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        $this->viewer->aview('news/index.php', $data);
    }

    public function article_loader() {
        $data = $this->input->post();
        $this->viewer->aview('news/article_loader.php', $data, false);
    }

    public function article_list() {
        $page = $this->input->post('page');
        $ftype = $this->input->get();

        $perpage = PAGING_MED;
        if (isset($_GET['sk'])) {
            $searchKey = $_GET['sk'];
        } else {
            $searchKey = "";
        }
        $data = $this->article_model->getArticleList($page, $perpage, $searchKey, $ftype);
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['clubs'] = $this->master_model->getClubKV();
        $data['search'] = $searchKey;
        $this->viewer->aview('news/article_list.php', $data, false);
    }

    public function actions() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->update("post", array('status' => $status), array("pk_post_id" => $id));
        if ($status == '1') {
            echo json_encode(array('status' => '1', 'title' => "News status", 'text' => "News has been activated"));
        } else {
            echo json_encode(array('status' => '1', 'title' => "News status", 'text' => "News has been deactivated"));
        }
    }

    public function get_clubs() {
        $league_id = $this->input->post("league_id");
        $data = $this->master_model->getClubKVByLeague($league_id, '1');
        echo "<option value=''>Select Club</option>";
        foreach ($data as $key => $dt) {
            echo '<option value="' . $key . '">' . $dt . "</option>";
        }
    }

    public function comment_status() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->update("post", array('comment_allow' => $status), array("pk_post_id" => $id));
        if ($status == '1') {
            echo json_encode(array('status' => '1', 'title' => "Allow Comments", 'text' => "Comments has been allowed for News"));
        } else {
            echo json_encode(array('status' => '1', 'title' => "Deny Comments", 'text' => "Comments has been denied for News"));
        }
    }

    public function featured() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->update("post", array('featured_status' => $status), array("pk_post_id" => $id));
        if ($status == '1') {
            echo json_encode(array('status' => '1', 'title' => "News status", 'text' => "News has been Marked Featured"));
        } else {
            echo json_encode(array('status' => '1', 'title' => "News status", 'text' => "News has been removed from Featured"));
        }
    }

    public function sponsered() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->update("post", array('sponser_status' => $status), array("pk_post_id" => $id));
        if ($status == '1') {
            echo json_encode(array('status' => '1', 'title' => "News status", 'text' => "News has been Marked Sponsered"));
        } else {
            echo json_encode(array('status' => '1', 'title' => "News status", 'text' => "News has been removed from Sponsered"));
        }
    }

    public function deleteaction() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->update("post", array('is_deleted' => '1'), array("pk_post_id" => $id));
        $this->db->delete("post_tags", array('fk_post_id' => $id));
        $this->db->delete("comments", array('fk_ref_id' => $id, 'type' => 'A'));
        $this->db->delete("comments", array('fk_ref_id' => $id, 'type' => 'N'));

        $feeds = $this->db->get_where("news_feed", array('feed_ref_id' => $id))->result_array();
        foreach ($feeds as $fd) {
            $this->user_model->deleteAllFeedData($fd["pk_feed_id"]);
        }

//// public $ptype = array('ART_COM' => '1', 'NF_TEXT' => '3', 'NF_IMG' => '4', 'NF_VID' => '5', 'ART_SHARE' => '2', 'W_ART_WRT' => '7', 'W_ART_READS' => '1', 'ART_SHARE' => '2', 'W_ART_SHARE' => '3', 'W_ART_COM' => '1', 'NF_COM' => '1', 'NF_SHARE' => '2', 'W_NF_COM' => '1', 'NF_LIKE' => '1', 'W_NF_LIKE' => '1');
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "W_ART_WRT"));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "ART_COM"));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "ART_SHARE"));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "W_ART_READS"));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "W_ART_SHARE"));
        $this->db->delete("user_points", array('fk_ref_id' => $id, "point_type" => "W_ART_COM"));
        echo json_encode(array('status' => '1', 'title' => "News status", 'text' => "News has been deleted"));
    }

    public function post_status() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->update("post", array('post_type' => $status), array("pk_post_id" => $id));
        if ($status == 'A') {
            echo json_encode(array('status' => '1', 'title' => "Article", 'text' => "Article has been converted as Article"));
        } else {
            echo json_encode(array('status' => '1', 'title' => "Article status", 'text' => "Article has been converted as News"));
        }
    }

    public function add($type = "") {
        $data['menu'] = "5-1";
        $data['js'] = array("../plugins/ckeditor/ckeditor.js", "../plugins/select2/select2.js");
        $data['css'] = array("plugins/select2/select2.css");
        $data['clubs'] = $this->master_model->getClubKV("1");
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        $this->viewer->aview('news/add.php', $data);
    }

    public function edit($id = "") {
        $data['menu'] = "5-2";
        $data['js'] = array("../plugins/ckeditor/ckeditor.js", "../plugins/select2/select2.js");
        $data['css'] = array("plugins/select2/select2.css");
        $data['clubs'] = $this->master_model->getClubKV();
        $data['article'] = $this->article_model->getArticleById($id);
        $this->viewer->aview('news/edit.php', $data);
    }

    public function saveeditor() {
        $type = $this->input->post("type");
        $content = $this->input->post("content");
        $this->cms_model->setContent($type, $content);
    }

    public function savearticle() {
        $video_link = $this->input->post('youtube_link');
        $insert = array(
            'fk_club_id' => $this->input->post('club'),
            'fk_league_id' => $this->input->post('league'),
            'title' => $this->input->post('title'),
            'content' => $this->input->post('content'),
            'featured_status' => $this->input->post('featured_status'),
            'sponser_status' => $this->input->post('sponser_status'),
            'comment_allow' => $this->input->post('comment_allow'),
            'file_type' => $this->input->post('file_type'),
            'fk_user_id' => $this->session->userdata('admin_id'),
            'post_type' => $this->input->post('post_type')
        );
        if ($insert['file_type'] == 'V') {
            $update['file_name'] = $video_link . ".jpg";
        }
        $this->db->insert('post', $insert);
        $id = $this->db->insert_id();
        //Maintaine Slug Code

        $this->load->library('slug');
        $this->slug->set_config(array('table' => 'post', 'field' => 'slug', 'title' => 'title', 'id' => 'pk_post_id'));
        $rslug = $this->slug->create_uri($insert['title'], $id);
        $this->db->update("post", array('slug' => $rslug), array('pk_post_id' => $id));

        //End of Slug Code
        $this->article_model->updateArticleImage($id, "file_name");
        if ($this->input->post('file_type') == "V") {
            $this->article_model->saveYoutubeImage($video_link);
        }
        $this->article_model->saveTags($id, $this->input->post('tags'));
    }

    public function updatearticle() {
        $id = $this->input->post('pk_post_id');
        $video_link = $this->input->post('youtube_link');
        $update = array(
            'fk_club_id' => $this->input->post('club'),
            'title' => $this->input->post('title'),
            'content' => $this->input->post('content'),
            'featured_status' => $this->input->post('featured_status'),
            'sponser_status' => $this->input->post('sponser_status'),
            'comment_allow' => $this->input->post('comment_allow'),
            'file_type' => $this->input->post('file_type')
        );

        if ($update['file_type'] == 'V') {
            $update['file_name'] = $video_link . ".jpg";
        }
        $this->db->update('post', $update, array('pk_post_id' => $id));
//Maintaine Slug Code

        $this->load->library('slug');
        $this->slug->set_config(array('table' => 'post', 'field' => 'slug', 'title' => 'title', 'id' => 'pk_post_id'));
        $rslug = $this->slug->create_uri($update['title'], $id);
        $this->db->update("post", array('slug' => $rslug), array('pk_post_id' => $id));

        //End of Slug Code
        $this->article_model->updateArticleImage($id, "file_name");
        if ($this->input->post('file_type') == "V") {
            $this->article_model->saveYoutubeImage($video_link);
        }
        $this->article_model->saveTags($id, $this->input->post('tags'));
    }

    public function comments($article_id = "") {
        $data['menu'] = "5-2";
        $data['post_id'] = $article_id;
        $data['article'] = $this->article_model->getArticleDetails($article_id);
        $this->viewer->aview('news/comments.php', $data);
    }

    public function comment_list() {

        $post_id = $this->input->get('post_id');

        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        if (isset($_GET['sk'])) {
            $searchKey = $_GET['sk'];
        } else {
            $searchKey = "";
        }

        $data = $this->article_model->getCommentList($post_id, $page, $perpage, $searchKey);
        $data['clubs'] = $this->master_model->getClubKV();
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->aview('news/comment_list.php', $data, false);
    }

    public function commentactions() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->update("comments", array('status' => $status), array("pk_comment_id" => $id));
        if ($status == '1') {
//            $this->sendCommentEmail($id);
            echo json_encode(array('status' => '1', 'title' => "News status", 'text' => "News has been Activated"));
        } else if ($status == '0') {
            echo json_encode(array('status' => '1', 'title' => "News status", 'text' => "News has been Dectivated"));
        }
    }

    public function sendCommentEmail($id) {
        $comment = $this->db->get_where("comments", array("pk_comment_id" => $id))->result_array();
        $article = $this->db->get_where("articles", array("pk_article_id" => $comment[0]['fk_article_id']))->result_array();
        $category = $this->db->get_where("categories", array("pk_category_id" => $article[0]['fk_category_id']))->result_array();
        //$users_other = $this->db->query("SELECT * FROM users WHERE  pk_user_id IN (SELECT DISTINCT fk_user_id FROM comments WHERE fk_article_id='" . $comment[0]['fk_article_id'] . "' AND fk_user_id !='" . $comment[0]['fk_user_id'] . "' AND status='1')")->result_array();
        $users_other = $this->db->query("SELECT * FROM users WHERE  pk_user_id IN (SELECT DISTINCT fk_user_id FROM comments WHERE fk_article_id='" . $comment[0]['fk_article_id'] . "'  AND status='1')")->result_array();
        $user = $this->db->get_where("users", array("pk_user_id" => $comment[0]['fk_user_id']))->result_array();
        $data = array(
            "article_title" => $article[0]['title'],
            "article_link" => site_url() . ARTICLE_TAG . "/" . $category[0]["slug"] . "/" . $article[0]['slug'],
            "comment_by" => $user[0]['name'],
            'comment' => $comment[0]['comment']
        );
        $emails = array();
        foreach ($users_other as $user) {
            $emails[] = $user['email'];
        }
        if (count($emails) > 0) {
            $data_eamil = $this->viewer->emailview("comment_reply.php", array('data' => $data));

            $this->load->library('email');
            $config = getEmailConfig();
            $this->email->initialize($config);
            $this->email->from(MAIL_USER, SITE_NAME);
            $this->email->subject($data['comment_by'] . " replied on your comment");
            $this->email->message($data_eamil);
            $this->email->to($emails);
            $this->email->send();
        }
    }

}
