<?php
class Adminbanter extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->library('viewer');
    $this->load->model('Banter');
    $this->load->helper('url');
    if ($this->session->userdata('admin_id') == "")
    {
        redirect("admin/login");
  }
}
  public function index()
  {
    $data = array(
      'all_clubs'=> $this->Banter->get_all_clubs(),
      'all_leagues'=> $this->Banter->get_all_leagues()
    );
    $this->viewer->aview('banter/banterview',$data);
  }
  public function persist()
  {
    $league = $this->input->post('league');
    $club_one = $this->input->post('club_one');
    $club_two = $this->input->post('club_two');
    $status = $this->input->post('status');
    $result = $this->Banter->create_match_room($club_one,$club_two,$league,$status);
    if ($result == 1)
    {
      redirect('admin/Adminbanter');
    }
  }
}
?>
