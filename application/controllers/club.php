<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Club extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('auth_model');
        $this->load->model('master_model');
        $this->load->model('article_model');
        $this->load->model('gallery_model');
    }

    public function home($slug = '') {

        $data['club'] = $this->master_model->getClubBySlug($slug);

        if ($data['club']) {
            $post_filter = "(fk_club_id='" . $data['club']['pk_club_id'] . "' OR fk_league_id='" . $data['club']['fk_league_id'] . "') AND ";
            $data['hp_news'] = $this->article_model->getHomePageArticle(" $post_filter post_type='N' ORDER BY pk_post_id DESC,fk_club_id DESC LIMIT 0,4");
            $data['hp_videos'] = $this->gallery_model->getGalleryVideos(50);
            $wrt_data = $this->article_model->getWritersByPoints($data['club']['pk_club_id'], 11);
            $data['hp_writers'] = $wrt_data['wdata'];
            $data['wr_count'] = $wrt_data['wr_count'];
            $fan_data = $this->article_model->getTopFanz($data['club']['pk_club_id'], 13);
            $data['hp_fanz'] = $fan_data['fan_data'];
            $data['fan_count'] = $fan_data['fan_count'];
            $data['art_count'] = $this->article_model->getClubArticles($data['club']['pk_club_id']);
            $data['art_reads'] = $this->article_model->getClubArticleRead($data['club']['pk_club_id']);



            $ffplus = $this->db->query("SELECT users.*,user_profile.profile_pic,(SELECT SUM(user_points.points) as pnt FROM user_points WHERE user_points.fk_user_id = users.pk_user_id) as points FROM users LEFT OUTER JOIN user_profile ON user_profile.fk_user_id=users.pk_user_id WHERE users.fk_league_id='" . $data['club']['fk_league_id'] . "' AND ff_plus='1' ORDER BY points LIMIT 20")->result_array();
            foreach ($ffplus as $key => $user) {
                $ffplus[$key]['profile_pic'] = (is_file(IMG_PROFILE . $user['profile_pic'])) ? site_url() . IMG_PROFILE . $user['profile_pic'] : site_url() . "public/fassets/images/user.jpg";
                $ffplus[$key]['name'] = $user['firstname'] . ' ' . $user['lastname'];
                $ffplus[$key]['art_title'] = "";
                $f_article = $this->article_model->getArticleIdByUserId($user['pk_user_id']);
                if ($f_article) {
                    $ffplus[$key]['profile_pic'] = $f_article['image'];
                    $ffplus[$key]['art_title'] = $f_article['title'];
                } else {
                    unset($ffplus[$key]);
                }
            }
            $data['ffplus'] = array_slice($ffplus, 0, 3);

            $this->viewer->fview('home/club.php', $data);
        } else {
            $this->viewer->fview('general/page_404.php', array());
        }

//        echo $slug;
    }

}
