<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Youtube extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('master_model');
    }

    public function findvideo() {
        $search = $this->input->post("search");
        if (FALSE !== filter_var($search, FILTER_VALIDATE_URL)) {

            $vid = $this->getVidById($search);
            if ($vid != "") {
                $result = $this->get_video($vid);
            } else {
                $result = $this->search_video($search);
            }
        } else {
            $result = $this->search_video($search);
        }
        $vids = array();
        foreach ($result as $res) {
            $vids[] = $res['id'];
        }
        $exist = $this->checkExists($vids);
        $this->viewer->fview('upload/video_result.php', array('result' => $result, 'exist' => $exist), false);
    }

    function checkExists($vids) {
        if (count($vids) > 0) {
            $videos = $this->db->query("SELECT * FROM ko_post WHERE url IN ('" . implode("','", $vids) . "')");
            $res = $videos->result_array();
            $ret = array();
            foreach ($res as $rs) {
                $ret[] = $rs['url'];
            }
            return $ret;
        } else {
            return array();
        }
    }

    public function view($id) {
        $id = _decode($id);
        echo $id;
    }

    public function search_video($search) {
        $data = file_get_contents("https://gdata.youtube.com/feeds/api/videos?q=" . urlencode($search) . "&orderby=published&start-index=1&max-results=10&v=2");
        $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
        //print_r($vals);
        $ids = array();
        $title = array();
        $duration = array();
        $desc = array();
        $i = 0;
        foreach ($vals as $vl) {
            if (isset($vl['tag']) and trim($vl['tag']) == "YT:VIDEOID") {
                $ids[] = $vl['value'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "MEDIA:TITLE") {
                $title[] = $vl['value'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "YT:DURATION") {
                $duration[] = $vl['attributes']['SECONDS'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "MEDIA:DESCRIPTION") {
                if (isset($vl['value'])) {
                    $desc[] = $vl['value'];
                } else {
                    $desc[] = "";
                }
            }
        }
        $final = array();
        foreach ($ids as $key => $id) {
            $final[] = array(
                'id' => $id,
                'title' => $title[$key],
                'dur' => $duration[$key],
                'desc' => $desc[$key],
                'image' => "http://i.ytimg.com/vi/" . $id . "/mqdefault.jpg"
            );
        }
        return $final;
    }

    function get_video($id = "") {
        $data = file_get_contents("http://gdata.youtube.com/feeds/api/videos/" . $id . "?v=2");
        $p = xml_parser_create();
        $pdata = xml_parse_into_struct($p, $data, $vals, $index);

        $ids = array();
        $title = array();
        $duration = array();
        $desc = array();
        $i = 0;
        foreach ($vals as $vl) {
            if (isset($vl['tag']) and trim($vl['tag']) == "YT:VIDEOID") {
                $ids[] = $vl['value'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "MEDIA:TITLE") {
                $title[] = $vl['value'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "YT:DURATION") {
                $duration[] = $vl['attributes']['SECONDS'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "MEDIA:DESCRIPTION") {
                if (isset($vl['value'])) {
                    $desc[] = $vl['value'];
                } else {
                    $desc[] = "";
                }
            }
        }
        $final = array();
        foreach ($ids as $key => $id) {
            $final[] = array(
                'id' => $id,
                'title' => $title[$key],
                'dur' => $duration[$key],
                'desc' => $desc[$key],
                'image' => "http://i.ytimg.com/vi/" . $id . "/mqdefault.jpg"
            );
        }
        return $final;
    }

    function validateVID($vid) {
        if (verifyURL("http://gdata.youtube.com/feeds/api/videos/$vid")) {
            $str = @fopen("http://gdata.youtube.com/feeds/api/videos/$vid", 1);
            if (trim($str) == "Invalid id") {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    function verifyURL($url) {
        $file = $url;
        $file_headers = @get_headers($file);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        } else {
            $exists = true;
        }
        return $exists;
    }

    function linkValid($link) {
      
        if (preg_match('/((http:\/\/)?(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/v\/)([\w-]{11}).*|http:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11}).*)/i', $link, $match)) {
            return true;
        } else {
            if (preg_match('/((https:\/\/)?(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/v\/)([\w-]{11}).*|http:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11}).*)/i', $link, $match)) {
                return true;
            } else {
                return false;
            }
        }
    }

    function getVidById($link) {
        $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
        if (empty($video_id[1]))
            $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

        $video_id = explode("&", $video_id[1]); // Deleting any other params
        $video_id = $video_id[0];
        $video_id = explode("?", $video_id);
        $video_id = $video_id[0];
        return $video_id;
    }
    
    function getEmbeddFromUrl(){
        $url=  $this->input->post("url");
        $ret=array("status"=>"0","msg"=>"","vid"=>"");
        $validity=$this->linkValid($url);
        if($validity){
            $vid=  $this->getVidById($url);
            $ret=array("status"=>"1","msg"=>"","vid"=>$vid);
        }
        echo json_encode($ret);
    }

}
