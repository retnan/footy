<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Message extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model("master_model");
        $this->load->model("article_model");
        $this->load->model("user_model");
//        if (!($this->session->userdata("user_id") > 0)) {
//            redirect("login");
//        }
    }

    public function index($param = '') {
        $data = array('css' => "fassets/css/jquery-confirm.min.css", 'js' => 'fassets/js/jquery-confirm.min.js');
        $this->viewer->fview('message/index.php', $data);
    }

    public function loadmessage() {
        $page = $this->input->post('page');
        $data = array();
        $data['messages'] = $this->user_model->getMessages($this->session->userdata("user_id"), $page, 2, true);
        $this->viewer->fview('message/messages.php', $data, false);
    }

    public function delete_message() {
        $msg_id = $this->input->post('msg_id');
        $this->db->delete("messages", array("pk_message_id" => $msg_id));
    }

}
