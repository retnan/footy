<?php

class ads_model extends CI_Model {

    function getAdsList($page, $per_page, $searchKey, $type = "") {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($searchKey != "") {
            $arr = getSearchKeys($this->db->escape_str($searchKey), "articles.ad_url");
            $filter = implode(" AND ", $arr);
            $filter = " AND " . $filter;
        }


        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS ads.*"
                . " FROM ads"
                . " WHERE  ad_type='" . $type . "' $filter  ORDER BY created_at DESC  LIMIT $start,$per_page");
        $arr = $data->result_array();
        $count = getFoundRows();
        foreach ($arr as $key => $ar) {
            $image = ADS_DIR . $ar['image'];
            if (is_file($image)) {
                $arr[$key]['thumb'] = base_url() . $image;
            } else {
                $arr[$key]['thumb'] = base_url() . NO_IMAGE;
            }
        }

        return array('data' => $arr, 'count' => $count);
    }

    function getAdLocation($id) {
        $arr = $this->db->query("SELECT * FROM ad_location WHERE  fk_ad_id='$id' ORDER BY type")->result_array();
        $loc = array();
        foreach ($arr as $key => $ar) {
            $loc[$ar['type']][] = $ar['ref_id'];
        }
        return $loc;
    }

}

?>