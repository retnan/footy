<?php

class cms_model extends CI_Model {

    function getContent($key) {
        $res = $this->db->get_where("cms", array("cmskey" => $key));
        $data = $res->result_array();
        if (count($data) > 0) {
            return $data[0]['cmsval'];
        } else {
            return "";
        }
    }

    function setContent($key, $value) {
        $res = $this->db->get_where("cms", array("cmskey" => $key));
        $data = $res->result_array();
        if (count($data) > 0) {
            $this->db->update("cms", array("cmsval" => $value), array("cmskey" => $key));
        } else {
            $this->db->insert("cms", array("cmsval" => $value, "cmskey" => $key));
        }
    }

    function getVideoList($page, $per_page, $search = "") {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($search != "") {
            $filter = " AND (title LIKE '%" . $this->db->escape_str($search) . "%')";
        }
        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM home_gallery"
                . " WHERE 1=1 $filter LIMIT $start,$per_page");
        $arr = $data->result_array();
        return array('data' => $arr, 'count' => getFoundRows(), 'start' => ($start + 1));
    }

}

?>
