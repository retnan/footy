<?php

class cms_model extends CI_Model {

    function getContent($key) {
        $res = $this->db->get_where("cms", array("cmskey" => $key));
        $data = $res->result_array();
        if (count($data) > 0) {
            return $data[0]['cmsval'];
        } else {
            return "";
        }
    }

    function setContent($key, $value) {
        $res = $this->db->get_where("cms", array("cmskey" => $key));
        $data = $res->result_array();
        if (count($data) > 0) {
            $this->db->update("cms", array("cmsval" => $value), array("cmskey" => $key));
        } else {
            $this->db->insert("cms", array("cmsval" => $value, "cmskey" => $key));
        }
    }

}

?>
