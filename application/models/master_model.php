<?php

class master_model extends CI_Model {

    function getLeagueKV($type = 'all') {
        $filter = ($type == 'all') ? "1=1" : "status='$type' AND is_deleted='0'";
        $data = $this->db->query("SELECT * FROM master_league WHERE $filter ORDER BY title");
        $return = array();
        $arr = $data->result_array();
        foreach ($arr as $ar) {
            $return[$ar['pk_league_id']] = $ar['title'];
        }
        return $return;
    }

    function getLeagueKVWithClub($type = 'all') {
        $filter = ($type == 'all') ? "1=1" : "status='$type' AND is_deleted='0'";
        $data = $this->db->query("SELECT * FROM master_league WHERE $filter ORDER BY title");
        $return = array();
        $arr = $data->result_array();
        foreach ($arr as $ar) {
            $return[$ar['pk_league_id']] = array("title" => $ar['title'], "clubs" => $this->getClubKVByLeague($ar['pk_league_id'], "1"));
        }
        return $return;
    }

    function getClubKV($type = 'all') {
        $filter = ($type == 'all') ? "1=1" : "status='$type' AND is_deleted='0'";
        $data = $this->db->query("SELECT * FROM master_club WHERE $filter ORDER BY title");
        $return = array();
        $arr = $data->result_array();
        foreach ($arr as $ar) {
            $return[$ar['pk_club_id']] = $ar['title'];
        }
        return $return;
    }

    function getClubKVM($type = 'all') {
        $filter = ($type == 'all') ? "1=1" : "status='$type' AND is_deleted='0'";
        $data = $this->db->query("SELECT * FROM master_club WHERE $filter ORDER BY title");
        $return = array();
        $arr = $data->result_array();
        foreach ($arr as $ar) {
            $return[] = array('club_logo'=>$ar['logo'], 'club_name'=>$ar['title'], 'club_slug'=>$ar['slug']);
        }
        return $return;
    }

    function getClubKVByLeague($league_id, $type = "all") {
        $filter = ($type == 'all') ? "1=1" : "status='$type' AND is_deleted='0'";
        $filter.=" AND fk_league_id='$league_id'";
        $data = $this->db->query("SELECT * FROM master_club WHERE $filter ORDER BY title");
        $return = array();
        $arr = $data->result_array();
        foreach ($arr as $ar) {
            $return[$ar['pk_club_id']] = $ar['title'];
        }
        return $return;
    }

    function getHeaderLeagueClub($id = "") {
        $filter = "status='1' AND is_deleted='0'";
        if ($id != "") {
            $filter.="AND pk_league_id='$id'";
        }
        $data = $this->db->query("SELECT * FROM master_league WHERE $filter ORDER BY title")->result_array();
        $return = array();
        foreach ($data as $ar) {
            $image = LEAGUE_IMG_DIR . $ar['logo'];
            if (!is_file($image)) {
                $image = site_url() . NO_IMAGE;
            } else {
                $image = site_url() . $image;
            }
            $clubs = $this->db->get_where("master_club", array("fk_league_id" => $ar['pk_league_id'], "status" => "1", "is_deleted" => "0"))->result_array();
            $cd = array();

            foreach ($clubs as $clb) {
                $cimage = CLUB_IMG_DIR . $clb['logo'];
                if (!is_file($cimage)) {
                    $cimage = site_url() . NO_IMAGE;
                } else {
                    $cimage = site_url() . $cimage;
                }
                $cd[] = array("id" => $clb['pk_club_id'], "slug" => $clb['slug'], "name" => $clb['title'], "image" => $cimage);
            }
            $return[$ar['pk_league_id']] = array(
                "id" => $ar['pk_league_id'],
                "name" => $ar['title'],
                "image" => $image,
                "clubs" => $cd
            );
        }
        return $return;
    }

    function getClubIDBySlug($slug) {
        $data = $this->db->get_where("master_club", "slug = '" . $slug . "' AND status='1' AND is_deleted='0'");
        $arr = $data->result_array();
        if (count($arr) > 0) {
            return $arr[0]['pk_club_id'];
        }
        return 0;
    }

    function getClubBySlug($slug) {
        $data = $this->db->get_where("master_club", "slug = '" . $slug . "' AND status='1' AND is_deleted='0'");
        $arr = $data->result_array();
        if (count($arr) > 0) {
            $cimage = CLUB_IMG_DIR . $arr[0]['logo'];
            if (!is_file($cimage)) {
                $cimage = NO_IMAGE;
            } else {
                $cimage = site_url() . $cimage;
            }
            $arr[0]['logo'] = $cimage;
            $league = $this->getLeagueByID($arr[0]['fk_league_id']);
            $limage = LEAGUE_IMG_DIR . $league['logo'];
            if (!is_file($cimage)) {
                $limage = NO_IMAGE;
            } else {
                $limage = site_url() . $limage;
            }
            $league['logo'] = $limage;
            $arr[0]['league'] = $league;
            return $arr[0];
        } else {
            return false;
        }
    }

    function getClubByID($id) {
        $data = $this->db->get_where("master_club", "pk_club_id = '" . $id . "'");
        $arr = $data->result_array();
        if (count($arr) > 0) {
            return $arr[0];
        } else {
            return false;
        }
    }

    function getLeagueByClubID($club_id) {
        $data = $this->db->get_where("master_club", "pk_club_id = '" . $club_id . "'");
        $arr = $data->result_array();
        if (count($arr) > 0) {
            return $arr[0]['fk_league_id'];
        } else {
            return false;
        }
    }

    function getLeagueByID($id) {
        $data = $this->db->get_where("master_league", "pk_league_id = '" . $id . "'");
        $arr = $data->result_array();
        if (count($arr) > 0) {
            return $arr[0];
        } else {
            return false;
        }
    }

    function getClubList($page, $per_page, $search = "", $league = "0") {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($league > 0) {
            $filter = " AND fk_league_id='$league' ";
        }
        if ($search != "") {
            $filter.= " AND (title LIKE '%" . $this->db->escape_str($search) . "%' OR team_manager LIKE '%" . $this->db->escape_str($search) . "%'  OR captain LIKE '%" . $this->db->escape_str($search) . "%' OR stadium LIKE '%" . $this->db->escape_str($search) . "%')";
        }
        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM master_club"
                . " WHERE is_deleted='0' $filter LIMIT $start,$per_page");
        $arr = $data->result_array();
        return array('data' => $arr, 'count' => getFoundRows(), 'start' => ($start + 1));
    }

    function getLeagueList($page, $per_page, $search = "") {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($search != "") {
            $filter = " AND (title LIKE '%" . $this->db->escape_str($search) . "%' OR description LIKE '%" . $this->db->escape_str($search) . "%')";
        }
        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM master_league"
                . " WHERE is_deleted='0' $filter LIMIT $start,$per_page");
        $arr = $data->result_array();
        return array('data' => $arr, 'count' => getFoundRows(), 'start' => ($start + 1));
    }

    function getMediaList($page, $per_page, $search = "") {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($search != "") {
            $filter = " AND (name LIKE '%" . $this->db->escape_str($search) . "%')";
        }
        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM medias "
                . " WHERE 1=1 $filter ORDER BY created_at DESC LIMIT $start,$per_page");
        $arr = $data->result_array();
        return array('data' => $arr, 'count' => getFoundRows(), 'start' => ($start + 1));
    }

    function checkClubExist($val, $league_id, $ignore = '') {
        $data = $this->db->query("SELECT count(*) as row_count FROM master_club WHERE title='" . $this->db->escape_str($val) . "' AND fk_league_id='$league_id'  AND pk_club_id !='$ignore'");
        $arr = $data->result_array();
        return ($arr[0]['row_count'] == 0) ? false : true;
    }

    function checkLeagueExist($val, $ignore = '') {
        $data = $this->db->query("SELECT count(*) as row_count FROM master_league WHERE title='" . $this->db->escape_str($val) . "'  AND pk_league_id !='$ignore'");
        $arr = $data->result_array();
        return ($arr[0]['row_count'] == 0) ? false : true;
    }

    function getTrendingSearches($limit = 5) {
        $trendingSQL = "SELECT SUM(view_count) as cnt,fk_article_id FROM article_views WHERE  created_at >= '" . date("Y-m-d", strtotime("-30 days")) . "' GROUP BY fk_article_id ORDER BY cnt DESC LIMIT $limit";
        $data = $this->db->query($trendingSQL)->result_array();
        $ids = array();
        foreach ($data as $dt) {
            $ids[] = $dt['fk_article_id'];
        }
        $query = "SELECT  articles.title,articles.slug,articles.image,articles.content,categories.slug as cslug FROM articles LEFT OUTER JOIN categories ON categories.pk_category_id=articles.fk_category_id  WHERE articles.status='1' AND pk_article_id IN ('" . implode("','", $ids) . "')";
        $return = $this->db->query($query)->result_array();
        foreach ($return as $key => $dt) {
            $return[$key]['image'] = site_url() . IMG_ARTICLE . $dt['image'];
            $content = strip_tags($dt['content']);
            $return[$key]['content'] = wordLimiter($content, 100);
        }
        return $return;
    }

}

?>