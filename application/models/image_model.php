<?php

class image_model extends CI_Model {

    function getImageList($page, $per_page, $searchKey, $type) {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        $filter = " AND image_type='$type'";
        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS images.*"
                . " FROM images"
                . " WHERE 1=1  $filter  ORDER BY created_at DESC  LIMIT $start,$per_page");
        $arr = $data->result_array();
        $count = getFoundRows();
        foreach ($arr as $key => $ar) {
            $image = MEDIA_DIR . $ar['path'];
            if (is_file($image)) {
                $arr[$key]['thumb'] = base_url() . $image;
            } else {
                $arr[$key]['thumb'] = base_url() . NO_IMAGE;
            }
        }

        return array('data' => $arr, 'count' => $count, "start" => $start);
    }

}

?>