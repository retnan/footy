<?php

class auth_model extends CI_Model {

    function verifyUser($email, $password) {

        $query = $this->db->get_where('ko_user', array('email' => $email, 'pass' => ($password)));
        if ($query->num_rows > 0) {
            $arr = $query->result_array();
            return $arr[0];
        } else {
            return false;
        }
    }

    function updateResetPassword($email, $code) {
        $data = array('forgot_password_code' => $code);
        $where = "email = '" . $email . "'";
        $str = $this->db->update('ko_user', $data, $where);
    }

    function getUserDataByEmail($email) {
        $data = $this->db->get_where("ko_user", "email = '" . $email . "'");
        $arr = $data->result_array();
        if (count($arr) > 0) {
            return $arr[0];
        } else {
            return false;
        }
    }

    function getUserDataByCode($code) {
        $data = $this->db->get_where("ko_user", "forgot_password_code = '" . $code . "'");
        $arr = $data->result_array();
        if (count($arr) > 0) {
            return $arr[0];
        } else {
            return false;
        }
    }

    function resetPasswordByCode($code, $password) {
        $data = array('password' => md5($password), 'forgot_password_code' => '');
        $where = "forgot_password_code = '" . $code . "'";
        $str = $this->db->update('ko_user', $data, $where);
    }

    function updateAccountActivation($email, $code) {
        $data = array('active' => '1', 'activation_code' => '');
        $where = "email = '" . $email . "' AND activation_code='" . $code . "'";
        $str = $this->db->update('ko_user', $data, $where);
        return $this->db->affected_rows();
    }

    function check_email($email) {
        $this->db->select('count(*) as row_count');
        $query = $this->db->get_where('ko_user', array('email' => $email));
        $arr = $query->result_array();
        if ($arr[0]['row_count'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    function verifyAdmin($username, $password) {
        $query = $this->db->get_where('users', array('username' => $username, 'password' => sha1($password), 'user_role' => 'A'));
        if ($query->num_rows > 0) {
            $arr = $query->result_array();
            return $arr[0];
        } else {
            return false;
        }
    }

    function getAdminUserById($id) {
//        $query = $this->db->get_where('users', array('pk_user_id' => $id, 'user_role' => 'A'));
        $query = $this->db->query("SELECT u.*,up.* FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id WHERE pk_user_id='$id' AND user_role='A'");
        if ($query->num_rows > 0) {
            $arr = $query->result_array();
            $arr[0]['thumb'] = base_url() . NO_USER;
            if (is_file(IMG_PROFILE . $arr[0]['profile_pic'])) {
                $arr[0]['thumb'] = site_url() . IMG_PROFILE . $arr[0]['profile_pic'];
            }
            $arr[0]['name'] = $arr[0]['firstname'] . ' ' . $arr[0]['lastname'];
            return $arr[0];
        } else {
            return false;
        }
    }

    function verifyAdminOldPassword($password, $id) {
        $query = $this->db->get_where('users', array('password' => sha1($password), 'pk_user_id' => $id));
        if ($query->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function verifyCollege($username, $password) {
        $query = $this->db->get_where('ko_coll_users', array('username' => $username, 'password' => $password));
        if ($query->num_rows > 0) {
            $arr = $query->result_array();
            return $arr[0];
        } else {
            return false;
        }
    }

    function verifyUserOldPassword($password, $id) {
        $query = $this->db->get_where('ko_coll_users', array('password' => ($password), 'id' => $id));
        if ($query->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function verifyOldPassword($password, $id) {
        $query = $this->db->query("SELECT * FROM ko_user WHERE id='" . $id . "' AND (pass='" . sha1($password) . "' OR pass='')");
        if ($query->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function getPassword($id) {
        $query = $this->db->query("SELECT * FROM ko_user WHERE id='" . $id . "'");
        if ($query->num_rows > 0) {
            $data = $query->result_array();
            return $data[0]['pass'];
        } else {
            return false;
        }
    }

}

?>
