<?php

class poll_model extends CI_Model {

    function getPollList($page, $per_page, $searchKey) {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($searchKey != "") {
            $arr = getSearchKeys($this->db->escape_str($searchKey), "polls.title");
            $filter = implode(" AND ", $arr);
            $filter = " AND " . $filter;
        }
        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS polls.*"
                . " FROM polls"
                . " WHERE 1=1  $filter  ORDER BY created DESC  LIMIT $start,$per_page");
        $arr = $data->result_array();
        $count = getFoundRows();
        foreach ($arr as $key => $ar) {
            $image = POLL_DIR . $ar['image'];
            if (is_file($image)) {
                $arr[$key]['thumb'] = base_url() . $image;
            } else {
                $arr[$key]['thumb'] = base_url() . NO_IMAGE;
            }
            $arr[$key]['voting'] = $this->countVoting($ar['pk_poll_id']);
        }

        return array('data' => $arr, 'count' => $count);
    }

    public function countVoting($poll_id) {
//SELECT SUM(CASE WHEN voting_option=1 THEN 1 ELSE 0 END) as ans a, SUM(CASE WHEN voting_option=2 THEN 1 ELSE 0 END) as ans b FROM poll_voting GROUP BY poll_voting
//        SELECT SUM(CASE voting_option WHEN '1' THEN 1 ELSE 0 END) as a, SUM(CASE voting_option WHEN '2' THEN 1 ELSE 0 END) as b FROM poll_voting 
        $data = $this->db->query("SELECT SUM(CASE voting_option WHEN '1' THEN 1 ELSE 0 END) as countA, SUM(CASE voting_option WHEN '2' THEN 1 ELSE 0 END) as countB, SUM(CASE voting_option WHEN '3' THEN 1 ELSE 0 END) as countC, SUM(CASE voting_option WHEN '4' THEN 1 ELSE 0 END) as countD, SUM(CASE voting_option WHEN '5' THEN 1 ELSE 0 END) as countE FROM poll_voting WHERE fk_poll_id =  '$poll_id'")->row_array();

        $pollData = $this->db->get_where("polls", array("pk_poll_id" => $poll_id))->row_array();

        $total = $data['countA'] + $data['countB'];
        if (trim($pollData['option3']) != "") {
            $total = $total + $data['countC'];
        }
        if (trim($pollData['option4']) != "") {
            $total = $total + $data['countD'];
        }
        if (trim($pollData['option5']) != "") {
            $total = $total + $data['countE'];
        }
        if ($total > 0) {
            $data['perA'] = round($data['countA'] * 100 / $total);
            $data['perB'] = round($data['countB'] * 100 / $total);
            $data['perC'] = round($data['countC'] * 100 / $total);
            $data['perD'] = round($data['countD'] * 100 / $total);
            $data['perE'] = round($data['countE'] * 100 / $total);
        } else {
            $data['perA'] = 0;
            $data['perB'] = 0;
            $data['perC'] = 0;
            $data['perD'] = 0;
            $data['perE'] = 0;
        }
        return $data;
    }

    public function getPoll($filter3 = "", $type_id, $ref_id, $refall = FALSE) {
        $filter = "SELECT fk_ad_id FROM content_placement WHERE type='$type_id' AND content_type='poll'";
        $filter2 = "";
        if ($refall) {
            $filter2 = "(type='$type_id' AND ref_id='0') OR";
        }
        if ($type_id == 5) {
            $filter = "SELECT fk_ad_id FROM content_placement WHERE  ($filter2 (type='$type_id' AND ref_id='$ref_id'))  AND content_type='poll'";
        }
        if ($type_id == 6) {
            $filter = "SELECT fk_ad_id FROM content_placement WHERE  type='$type_id' AND ref_id='$ref_id'  AND content_type='poll'";
        }

        if ($filter3 != "") {
            $filter3 = " AND " . $filter3;
        }
        $data = $this->db->query("SELECT * FROM polls WHERE status='1' AND  start_date <= '" . date("Y-m-d") . "' AND end_date >= '" . date("Y-m-d") . "' $filter3 and pk_poll_id IN($filter)  ORDER BY pk_poll_id DESC")->row_array();
        if (is_array($data) and count($data) > 0) {
            $image = POLL_DIR . $data['image'];
            if (is_file($image)) {
                $data['image'] = base_url() . $image;
            } else {
                $data['image'] = base_url() . NO_IMAGE;
            }
            $data['votes'] = $this->countVoting($data['pk_poll_id']);
            return $data;
        }
        return false;
    }

    public function saveVoting($poll_id, $ans) {
        $filter = " AND ip_address='" . $_SERVER['REMOTE_ADDR'] . "' ";
        $user_id = 0;
        if ($this->session->userdata("user_id") > 0) {
            $filter = " AND fk_user_id='" . $this->session->userdata("user_id") . "'";
            $user_id = $this->session->userdata("user_id");
        }
        $data_views = $this->db->query("SELECT count(*) as vcount FROM poll_voting WHERE fk_poll_id =  '$poll_id' $filter")->result_array();
        $data = array('fk_poll_id' => $poll_id, 'voting_option' => $ans, 'fk_user_id' => $user_id, 'ip_address' => $_SERVER['REMOTE_ADDR']);

        if ($data_views[0]['vcount'] == 0) {
            $this->db->insert("poll_voting", $data);
        } else {
            $this->db->query("UPDATE poll_voting SET voting_option='$ans' WHERE fk_poll_id =  '$poll_id' $filter");
        }
    }

    public function getPlacement($type, $id) {
        $data['sub_types'] = array('2' => "All", '1' => "Home", '3' => "Newsfeed", '4' => 'Articles', '5' => 'Clubs', '6' => 'League', '7' => "Footyfanz+");
        $data['type'] = $type;
        $data['id'] = $id;
        if ($type == "poll") {
            $data['data'] = $this->db->get_where("polls", array('pk_poll_id' => $id))->row_array();
        }
        if ($type == "bg" or $type == "head") {
            $data['data'] = $this->db->get_where("images", array('pk_image_id' => $id))->row_array();
        }


        $data['location'] = $this->getLocation($type, $id);
        $data['current_club_league'] = "0";
        $data['current_club_names'] = array();
        if (isset($data['location'][5])) {
            if (is_array($data['location'][5])) {
                $ldata = $this->db->get_where("master_club", array('pk_club_id' => $data['location'][5][0]))->result_array();
                if (count($ldata) > 0) {
                    $data['current_club_league'] = $ldata[0]['fk_league_id'];
                }
                foreach ($data['location'][5] as $clubid) {
                    $ldata = $this->db->get_where("master_club", array('pk_club_id' => $clubid))->result_array();
                    if (count($ldata) > 0) {
                        $data['current_club_names'][] = $ldata[0]['title'];
                    }
                }
            }
        }
        $this->load->model("master_model");
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        return $data;
    }

    function getLocation($type, $id) {

        $arr = $this->db->query("SELECT * FROM content_placement WHERE content_type='$type' AND fk_ad_id='$id' ORDER BY type")->result_array();
        $loc = array();
        foreach ($arr as $key => $ar) {
            $loc[$ar['type']][] = $ar['ref_id'];
        }

        return $loc;
    }

}

?>