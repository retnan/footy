<?php
class Banter extends CI_Model
{
	// public $date = date("Y m d h:i:sa");
	// public __construct()
	// {
	// 	parent::__contruct();
	// }
	//create match room
	public function create_match_room($club_one, $club_two,$league_id, $status)
	{
		$data = array(
			'league_id' => $league_id,
	    'club_one' => $club_one,
	    'club_two' => $club_two,
	    'status' => $status,
			// 'status' => $status,//0 for upcoming, 1 for live and 2 for ended,
			'date'=>date("Y m d h:i:sa"),
		);
		$query = $this->db->insert('match_room', $data);
		return $query;
	}

	//create fixture
	public function create_fixture($club_one_id,$club_two_id,$league_id)
	{
		$data = array(
			'club_one_id'=>$club_one_id,
			'club_two_id'=> $club_two_id,
			'league_id' =>$league_id,
			'date'=> self::$date);
		$query = $this->db->insert('fixture', $data);
		return $query;
	}
	//Retrieve all fixtures
	public function get_all_fixtures()
	{
		$query = $this->db->query("SELECT club_one_id,club_two_id,date FROM fixture");
		$result = $query->result_array();
		return $result;
	}
	public function get_single_fixture($fixture_id)
	{
		$query = $this->db->query("SELECT club_one_id,club_two_id FROM fixture WHERE fixture_id = $fixture_id");
		$result = $query->result_array();
		return $result;
	}
	//Retrieve all club details: id, name, manager
	public function club_image($club_name)
	{
		$query = $this->db->query("SELECT logo FROM master_club WHERE title LIKE '%$club_name%' ");
		// $data = array(
		// 	'pk_club_id'=>$club_id);
		// $query = $this->db->get_where('master_club', $data);
		$result = $query->row_array();
		return $result;
	}
	//Retrieve all active match room
	public function get_all_live_matches()
	{
		$all_upcoming_fixtures = array();
		$query = $this->db->query("SELECT match_room_id,club_one, club_two FROM match_room WHERE status = 1");
		$result = $query->result_array();
		$result = $this->process_club_info($result);
		return $result;
	}
	//Retrieve all upcoming matches
	public function get_all_upcoming_matches()
		{
		$all_upcoming_fixtures = array();
		$query = $this->db->query("SELECT match_room_id,club_one, club_two FROM match_room WHERE status = 0");
		$result = $query->result_array();
		$result = $this->process_club_info($result);
		return $result;
		}
		//Retrieve all ended matches
	public function get_all_ended_matches()
			{
			$all_upcoming_fixtures = array();
			$query = $this->db->query("SELECT match_room_id,club_one, club_two FROM match_room WHERE status = 2");
			$result = $query->result_array();
		$result = $this->process_club_info($result);
			return $result;
			}

	private function process_club_info($result)
	{
		foreach ($result as $key => $match) {
			$logo_one = site_url() . PUBLIC_DIR. "images/club/" . $this->club_image($match['club_one'])['logo'];
			$logo_two = site_url() . PUBLIC_DIR. "images/club/" . $this->club_image($match['club_two'])['logo'];
			$result[$key]['club_one'] = array("name"=>$match['club_one'], "logo"=>$logo_one);
			$result[$key]['club_two'] = array("name"=>$match['club_two'], "logo"=>$logo_two);
		}

		return $result;
	}
		//Code section to create Match
	public function create_match($club_one,$club_two,$league_id,$status)
			{
				$data = array(
					'club_one'=>$club_one,
					'club_two'=>$club_two,
					'league_id'=>$league_id,
					'status'=>$status
				);
				if($query = $this->db->insert('match_room', $data))
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			//Code snippet to for a user to add banter
			public function add_banter($user_id,$match_room_id, $banter_post)
			{
				$data = array(
					'user_id'=> $user_id,
					'match_room_id'=>$match_room_id,
					'banter_post'=>$banter_post,
					// 'banter_img'=>$banter_img,
					'status'=>1,
					'date'=>date('Y-m-d h:m:sa')
				);
				$query = $this->db->insert('banter',$data);
					return $query;
			}
			//code snippet to retrieve all banters per Matchroom
			public function retrieve_all_banter($id)
			{
				$match_room = $id;
				// $data = array('banter_id'=>$id);
				$query = $this->db->query("SELECT * FROM banter WHERE match_room_id = $id");
				$result = $query->result_array();
				
				//added 3:26pm 12/08/2017
				$this->load->model('user_model');
				foreach ($result as $key => $banter) {
					$user_data = $this->user_model->getUserDetails($banter['user_id']);
					if (is_file(PUBLIC_DIR . PROFILE_IMG_DIR . $user_data['thumb'])) {
						$user_photo = site_url() . PUBLIC_DIR . PROFILE_IMG_DIR . $user_data['thumb'];
					} else {
						$user_photo = site_url() . "public/fassets/images/user.jpg";
					}
		
					// $user_photo = site_url() . DEFAULT_USER;
					//$result[$key]['user_id'] = 0;// block all user IDs from showing
					$result[$key]['user'] = array("name"=>$user_data['name'], "slug"=>$user_data['slug'], "photo"=>$user_photo);
				}

				return $result;
			}
			public function get_all_clubs()
			{
				$query = $this->db->query("SELECT pk_club_id, title FROM master_club");
				$result = $query->result_array();
				return $result;
			}
			public function get_all_leagues()
			{
				$query = $this->db->query("SELECT pk_league_id, title FROM master_league");
				$result = $query->result_array();
				return $result;
			}
			//Retrieve club banter details
			public function banter_details($id)
			{
				$query = $this->db->query("SELECT club_one,club_two FROM match_room WHERE match_room_id = $id")->result_array();
				return $query;
			}
			//Banter like_status
			public function banter_like($banter_id)
			{
				$query_one = $this->db->query("SELECT * FROM banter_likes WHERE banter_id = $banter_id")->row_array();
				$result_array = count($query_one);
				if ($result_array > 0)
				{
					$query_two = $this->db->query("SELECT like_count FROM banter_likes WHERE banter_id = $banter_id")->row_array();
					$new_count = $query_two['like_count']+1;
					$query_three = $this->db->query("UPDATE banter_likes SET like_count = $new_count WHERE banter_id = $banter_id");
					if($query_three)
					{
						return 1;
					}
					else
					{
						return 0;
					}
				}
				else
				{
					$data = array(
						'banter_id'=> $banter_id,
						'like_count'=> 1
					);
					$query_four = $this->db->insert('banter_likes', $data);
					if($query_four)
					{
						return 1;
					}
					else
					{
						return 0;
					}
				}
			}
			//Retrieve all banter like count
			public function count_likes($banter_id)
			{
				$query = $this->db->query("SELECT like_count FROM banter_likes WHERE banter_id = $banter_id")->row_array();
				return $query;
			}
			//Retrieve user details per banter details
			public function banter_user($user_id)
			{
				$query = $this->db->query("SELECT * FROM users WHERE pk_user_id =$user_id")->row_array();
				return $query;
			}
}
?>