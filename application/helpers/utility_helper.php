<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function varify_session() {
    $CI = &get_instance();
    if (!$CI->session->userdata('logged_in')) {
        redirect(base_url());
    }
}

function getEmailConfig() {
    $config['protocol'] = 'smtp';
    $config['smtp_host'] = MAIL_HOST;
    $config['smtp_user'] = MAIL_USER;
    $config['smtp_pass'] = MAIL_PASSWORD;
    $config['smtp_port'] = 25;
    $config['useragent'] = MAIL_AGENT;
    $config['mailtype'] = "html";

    return $config;
}

function setEmailTemplete($content, $link1 = '', $link2 = '', $link3 = '', $content2 = '') {
    $links = "";
    if ($link1 != '' or $link2 != '' or $link3 != '') {
        $links = '<table cellpadding="5px" cellspacing="5px" width="100%">
						<tr><td width="30%">
							' . $link1 . '
						</td>
						<td width="30%">
							' . $link2 . '
						</td>
						<td width="30%">
							' . $link3 . '
						</td></tr>
					</table>
		';
    }
    $msg = '
<table style="width:100%"><tr><td>
<html>  
    <body style="margin:0; padding:0; font-family:Arial" bgcolor="#595959" >
    	<table cellpadding="0" cellspacing="0" style="width:100%; height:150px;  border:#ccc 1px solid; background:#FFF;  ">
		<tr>
		 <td style="position:relative" valign="top">
        	<table width="100%" cellpadding="0" cellspacing="0" style="margin-top:0px; font-family:Arial">
			<tr>
				<td height="25px"  style="text-indent:7px; font-weight:bold; font-size:15px; position:relative; background:#f5f5f5; ">
				<div style="float:left; width:200px; margin:5px; font-size: 20px; margin-left: 0px;">					
                        <img src="' . base_url() . 'public/assets/img/logo/logo_small.png" style="width:140px; height:29px " />
                    
				</div>
				
				</td>
			</tr>
            <tr>
				<td><div style="margin-top:5px; font-size:13px; color:#333;  padding:7px; text-align:justify" valign="top">' . $content . '</div></td>
			</tr>
			<tr>
				<td>
					' . $links . '<br><div style="margin-top:5px; font-size:13px; color:#244D27;  padding:7px; text-align:justify" valign="top">' . $content2 . '</div>                                            
               <div style="margin-top:5px; font-size:11px; color:#444;background:#f5f5f5; padding:7px; text-align:justify" valign="top"">									
					Warm Regards<br>
					' . SITE_NAME . ' 
		</div>  
				</td>
			</tr>
		</table> 		   
        </td>		
		</tr>
	</table>
	<br>		
    </body>
    </html>
</td></tr></table>
';
    return $msg;
}

function parseAddress($street, $city, $state, $country, $zip = "") {
    $address = "";
    if (trim($street) != "") {
        $address.=trim($street);
    }
    if (trim($city) != "") {
        if (trim($street) != "") {
            $address.=', ';
        } $address.=trim($city);
    }
    if (trim($state) != "") {
        if (trim($city) != "") {
            $address.=', ';
        } $address.=trim($state);
    }
    if (trim($country) != "") {
        if (trim($state) != "") {
            $address.=', ';
        } $address.=trim($country);
    }
    if (trim($zip) != "") {
        if (trim($country) != "") {
            $address.='- ';
        } $address.=trim($zip);
    }
    return $address;
}

function getPaginationFooter($cur_page, $per_page, $count) {
    $previous_btn = true;
    $next_btn = true;
    $first_btn = false;
    $last_btn = false;
    $msg = "";
    $no_of_paginations = ceil($count / $per_page);

    if ($cur_page >= 7) {
        $start_loop = $cur_page - 3;
        if ($no_of_paginations > $cur_page + 3)
            $end_loop = $cur_page + 3;
        else if ($cur_page <= $no_of_paginations && $cur_page >
                $no_of_paginations - 6) {
            $start_loop = $no_of_paginations - 6;
            $end_loop = $no_of_paginations;
        } else {
            $end_loop = $no_of_paginations;
        }
    } else {
        $start_loop = 1;
        if ($no_of_paginations > 7)
            $end_loop = 7;
        else
            $end_loop = $no_of_paginations;
    }
    if ($count >
            0) {
        $total_string = " <span class='span4 total' a='$no_of_paginations'><div style='margin-top:5px;'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b>&nbsp;&nbsp;&nbsp;&nbsp; Total Records " . $count . "</div></span>";
        $msg .=
                "<div class=' row-fluid'>"
                . $total_string . "<div class='pagination span8' style='margin-top:0px; margin-bottom:5px;'>"
                . "<ul class='pull-right'>";
        // FOR ENABLING THE FIRST BUTTON
        if ($first_btn && $cur_page > 1) {
            $msg .= "<li p='1' class='inactive'><span>First</span></li>";
        } else if ($first_btn) {
            $msg .= "<li p='1' class='active'><span>First</span></li>";
        }
        // FOR ENABLING THE PREVIOUS BUTTON
        if ($previous_btn && $cur_page > 1) {
            $pre = $cur_page - 1;
            $msg .= "<li p='$pre' class='inactive'><span>Prev</span></li>";
        } else if ($previous_btn) {
            $msg .= "<li class='active'><span>Prev</span></li>";
        }
        for ($i = $start_loop; $i <= $end_loop; $i++) {
            if ($cur_page == $i) {
                $msg .= "<li p='$i' style='co lor:#fff;backgrou nd-color:#46A70C;' class='active cpageval'><span> 
    $i</span></li>";
            } else {
                $msg .= "<li p='$i' class='inactive'><span>$i</span></li>";
            }
        }
        // TO ENABLE THE NEXT BUTTON
        if ($next_btn && $cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $msg .= "<li p='$nex' class='inactive'><span>Next</span></li>";
        } else if ($next_btn) {
            $msg .= "<li class='active'><span>Next</span></li>";
        }
// TO ENABLE THE END BUTTON
        if ($last_btn && $cur_page < $no_of_paginations) {
            $msg .= "<li p='$no_of_paginations' class='inactive'><span>Last</span></li>";
        } else if ($last_btn) {
            $msg .= "<li p='$no_of_paginations' class='active'><span>Last</span></li>";
        }
        $msg = $msg . "</ul></div></div>";
        return $msg;
    }
}

function paginationScript($pagename, $did = "1", $xdata = "", $focus = true) {
    ?>
    <script type="text/javascript">
        var el<?php echo $did; ?> = $("#<?php echo $did; ?>");
        var sort_type = "";
        function loadData<?php echo $did; ?>(page) {
            App.blockUI(el<?php echo $did; ?>.parent());
            $.post("<?php echo $pagename; ?>?<?php echo $xdata; ?>", {page: page}, function(msg) {
                        el<?php echo $did; ?>.html(msg);
                        App.unblockUI(el<?php echo $did; ?>.parent());
                    });
                }

                function loadDataSort<?php echo $did; ?>(page, searchdata, sort_by) {
                    App.blockUI(el<?php echo $did; ?>.parent());
                    $.post("<?php echo $pagename; ?>?sk=" + searchdata + "&<?php echo $xdata; ?>", {page: page, sort_by: sort_by}, function(msg) {
                        el<?php echo $did; ?>.html(msg);
                        $(".data .searchbox", el<?php echo $did; ?>)<?php echo ($focus) ? '.focus()' : ''; ?>.val($(".searchbox", el<?php echo $did; ?>).val());
                        App.unblockUI(el<?php echo $did; ?>.parent());
                    });
                }

                $(document).ready(function() {
                    loadData<?php echo $did; ?>(1);
                    sort_type = "a2z";  // For first time page load default results
                    $('#<?php echo $did; ?> .pagination li.inactive,#<?php echo $did; ?> .sort-menu li,#<?php echo $did; ?> .data .searchBtn ').live('click', function() {
                        if ($(this).hasClass("sort"))
                        {
                            sort_type = $(this).attr('sort');
                            loadDataSort<?php echo $did; ?>(1, $(".data .searchbox", el<?php echo $did; ?>).val(), sort_type);

                        }
                        else
                        {
                            var page = ($(this).hasClass("searchBtn") == true) ? 1 : $(this).attr('p');
                            loadDataSort<?php echo $did; ?>(page, $(".data .searchbox", el<?php echo $did; ?>).val(), sort_type);
                        }
                    });

                    $(".data .searchbox", el<?php echo $did; ?>).live('keypress', function(e) {
                        if (e.which == 13)
                            $('.data .searchBtn', el<?php echo $did; ?>).click();

                    });
                });
                //*******************************************************

    </script>
    <?php
}

function getPaginationFooterFront($cur_page, $per_page, $count, $countview = "") {
    if ($countview == "") {
        $countview = $count;
    }
    $previous_btn = true;
    $next_btn = true;
    $first_btn = false;
    $last_btn = false;
    $msg = "";
    $no_of_paginations = ceil($count / $per_page);
    if ($cur_page >= 7) {
        $start_loop = $cur_page - 3;
        if ($no_of_paginations > $cur_page + 3)
            $end_loop = $cur_page + 3;
        else if ($cur_page <= $no_of_paginations && $cur_page >
                $no_of_paginations - 6) {
            $start_loop = $no_of_paginations - 6;
            $end_loop = $no_of_paginations;
        } else {
            $end_loop = $no_of_paginations;
        }
    } else {
        $start_loop = 1;
        if ($no_of_paginations > 7)
            $end_loop = 7;
        else
            $end_loop = $no_of_paginations;
    }
    if ($count >
            0) {
        $total_string = "<div class='row'><div class='col-md-5' a='$no_of_paginations'><div style='margin-top:10px;'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b> Pages &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Records <strong>" . $countview . "</strong></div></div>";
        $msg .=
                $total_string . "<div class='col-md-7' style='margin-top:-5px;'>"
                . "<ul class='pagination pull-right'>";
        // FOR ENABLING THE FIRST BUTTON
        if ($first_btn && $cur_page > 1) {
            $msg .= "<li p='1' class='inactive'><span>First</span></li>";
        } else if ($first_btn) {
            $msg .= "<li p='1' class='active'><span>First</span></li>";
        }
        // FOR ENABLING THE PREVIOUS BUTTON
        if ($previous_btn && $cur_page > 1) {
            $pre = $cur_page - 1;
            $msg .= "<li p='$pre' class='inactive'><span>Prev</span></li>";
        } else if ($previous_btn) {
            $msg .= "<li class='active'><span>Prev</span></li>";
        }
        for ($i = $start_loop; $i <= $end_loop; $i++) {
            if ($cur_page == $i) {
                $msg .= "<li p='$i' class='active cpageval'><span> 
    $i</span></li>";
            } else {
                $msg .= "<li p='$i' class='inactive'><span>$i</span></li>";
            }
        }
        // TO ENABLE THE NEXT BUTTON
        if ($next_btn && $cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $msg .= "<li p='$nex' class='inactive'><span>Next</span></li>";
        } else if ($next_btn) {
            $msg .= "<li class='active'><span>Next</span></li>";
        }
        // TO ENABLE THE END BUTTON
        if ($last_btn && $cur_page < $no_of_paginations) {
            $msg .= "<li p='$no_of_paginations' class='inactive'><span>Last</span></li>";
        } else if ($last_btn) {
            $msg .= "<li p='$no_of_paginations' class='active'><span>Last</span></li>";
        }
        $msg = $msg . "</ul></div></div><div class='clearfix'></div>";
        return $msg;
    }
}

function paginationScriptFront($pagename, $did = "1", $xdata = "") {
    ?>
    <script type="text/javascript">
        var el<?php echo $did; ?> = $("#<?php echo $did; ?>");
        var sort_type = "";
        function loadData<?php echo $did; ?>(page) {
            //App.blockUI({target: el<?php echo $did; ?>.parent()});
            $.post("<?php echo $pagename; ?>?<?php echo $xdata; ?>", {page: page}, function(msg) {
                        //App.unblockUI(el<?php echo $did; ?>.parent());
                        el<?php echo $did; ?>.html(msg);
                    });
                }

                function loadDataSort<?php echo $did; ?>(page, searchdata, sort_by) {
                    //App.blockUI({target: el<?php echo $did; ?>.parent()});
                    $.post("<?php echo $pagename; ?>?sk=" + searchdata + "&<?php echo $xdata; ?>", {page: page, sort_by: sort_by}, function(msg) {
                        el<?php echo $did; ?>.html(msg);
                        $(".data .txt_searchbox", el<?php echo $did; ?>).focus().val($(".txt_searchbox", el<?php echo $did; ?>).val());
                        // App.unblockUI(el<?php echo $did; ?>.parent());
                    });
                }

                $(document).ready(function() {
                    loadData<?php echo $did; ?>(1);
                    sort_type = "a2z";  // For first time page load default results
                    $(document).on('click', '#<?php echo $did; ?> .pagination li.inactive,#<?php echo $did; ?> .sort-menu li,#<?php echo $did; ?> .data .searchBtn', function() {

                        if ($(this).hasClass("sort"))
                        {
                            sort_type = $(this).attr('sort');
                            loadDataSort<?php echo $did; ?>(1, $(".data .txt_searchbox", el<?php echo $did; ?>).val(), sort_type);

                        }
                        else
                        {
                            var page = ($(this).hasClass("searchBtn") == true) ? 1 : $(this).attr('p');
                            loadDataSort<?php echo $did; ?>(page, $(".data .txt_searchbox", el<?php echo $did; ?>).val(), sort_type);
                        }
                    });

                    $(document).on('keypress', "#<?php echo $did; ?> .data .txt_searchbox", function(e) {
                        if (e.which == 13)
                            $('.data .searchBtn', el<?php echo $did; ?>).click();


                    });
                });
                //*******************************************************
    </script>
    <?php
}

function _encode($data) {
    return base64_encode($data);
}

function _decode($data) {
    return base64_decode($data);
}

/**
 * 
 * @param type $uconfig
 * file_name,path,type,prefix,width
 * @return boolean
 */
function uploadFile($uconfig) {
    $CI = & get_instance();
    $config['upload_path'] = $uconfig['path'];
    $config['allowed_types'] = $uconfig['type'];
    $config['max_size'] = '0';
    $config['max_width'] = '0';
    $config['max_height'] = '0';
    $config['file_name'] = $uconfig['prefix'] . "_" . strtotime("now") . rand("10", "99");
    $CI->load->library('upload', $config);
    $CI->upload->initialize($config);
    if (!$CI->upload->do_upload($uconfig['file_name'])) {
        echo $CI->upload->display_errors('<p>', '</p>');
        return false;
    } else {
        $data = $CI->upload->data();
        $org_wid = $data['image_width'];
        $org_ht = $data['image_height'];
        $file_name = explode(".", $data['file_name']);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $data['full_path'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $uconfig['width'];
        $height = intval(($config['width'] * $org_ht) / $org_wid);
        $config['height'] = $height;
        $CI->load->library('image_lib', $config);
        $CI->image_lib->initialize($config);
        $CI->image_lib->resize();
        $CI->image_lib->clear();
        return $data['file_name'];
    }
}

function uploadRawFile($uconfig) {
    $CI = & get_instance();
    $config['upload_path'] = $uconfig['path'];
    $config['allowed_types'] = $uconfig['type'];
    $config['max_size'] = '0';
    $config['file_name'] = $uconfig['prefix'] . "_" . strtotime("now") . rand("10", "99");
    $CI->load->library('upload', $config);
    $CI->upload->initialize($config);
    if (!$CI->upload->do_upload($uconfig['file_name'])) {
        echo $CI->upload->display_errors('<p>', '</p>');
        $data = $CI->upload->data();
        print_r($data);
        return false;
    } else {
        $data = $CI->upload->data();
        return $data['file_name'];
    }
}

function getSearchKeys($data, $field) {
    $CI = & get_instance();
    $keys = explode(' ', $data);
    $return = array();
    foreach ($keys as $key) {
        $key = trim($key);
        if ($key != '') {
            $return[] = " $field LIKE '%" . $CI->db->escape_str($key) . "%' ";
        }
    }
    return $return;
}

function getSearchKeys2($data, $field) {
    $CI = & get_instance();
    $keys = explode(' ', $data);
    $return = array();
    foreach ($keys as $key) {
        $key = trim($key);
        if ($key != '') {
            $return[] = " $field LIKE '" . $CI->db->escape_str($key) . "%' ";
        }
    }
    return $return;
}

function getFoundRows() {
    $CI = &get_instance();
    $c = $CI->db->query("SELECT FOUND_ROWS() as cnt");
    $fnd = $c->result_array($c);
    return $fnd[0]['cnt'];
}

function getTimestamp($date = false) {
    //$timezone = "Asia/Calcutta";
    //if (function_exists('date_default_timezone_set'))
    //   date_default_timezone_set($timezone);
    if ($date) {
        return date('Y-m-d');
    }
    return date('Y-m-d H:i:s');
}

function dmyToYmd($old_date) {
    list($d, $m, $y) = explode('/', $old_date);
    $new_date = "$y-$m-$d";
    return $new_date;
}

function dmyToYmdTime($old_date) {
    list($d, $m, $y) = explode('/', $old_date);
    $yp = explode(" ", $y);
    $new_date = $yp[0] . "-$m-$d " . $yp[1] . " " . $yp[2];
    return date("Y-m-d H:i:s", strtotime($new_date));
}

function YmdToDmy($old_date) {
    list($y, $m, $d) = explode('-', $old_date);
    $new_date = "$d/$m/$y";
    return $new_date;
}

function wordLimiter($text, $limit = 100, $dots = true) {
    $explode = explode(' ', $text);
    $string = '';
    $i = 0;
    if (strlen($text) <= $limit) {
        return $text;
    }
    while (strlen($string) < $limit) {
        $string .= $explode[$i++] . " ";
    }
    $dots = (strlen($text) <= $limit) ? '' : (($dots) ? '...' : '');
    return $string . $dots;
}

function timeFormat($duration) {
    $tm = gmdate("H:i:s", (int) $duration);
    $ntm = substr($tm, 0, 2);
    if ($ntm == '00') {
        $tm = substr($tm, 3);
    }
    $ntm1 = substr($tm, 0, 1);
    if ($ntm1 == '0') {
        $tm = substr($tm, 1);
    }
    return $tm;
}

/**
 * Calculates time ago and returns a string
 * 
 * @param {integer} $timestamp
 * @return {string}
 */
function timeAgo($timestamp)
{
    if (!is_numeric($timestamp)) {
        $timestamp = strtotime($timestamp);
    }
    $cur_time   = time();
    $time_elapsed   = $cur_time - $timestamp;
    $seconds    = $time_elapsed ;
    $minutes    = round($time_elapsed / 60 );
    $hours      = round($time_elapsed / 3600);
    $days       = round($time_elapsed / 86400 );
    $weeks      = round($time_elapsed / 604800);
    $months     = round($time_elapsed / 2600640 );
    $years      = round($time_elapsed / 31207680 );
    // Seconds
    if($seconds <= 60){
        return "just now";
    }
    //Minutes
    else if($minutes <=60){
        if($minutes==1){
            return "one minute ago";
        }
        else{
            return "$minutes minutes ago";
        }
    }
    //Hours
    else if($hours <=24){
        if($hours==1){
            return "an hour ago";
        }else{
            return "$hours hrs ago";
        }
    }
    //Days
    else if($days <= 7){
        if($days==1){
            return "yesterday";
        }else{
            return "$days days ago";
        }
    }
    //Weeks
    else if($weeks <= 4.3){
        if($weeks==1){
            return "a week ago";
        }else{
            return "$weeks weeks ago";
        }
    }
    //Months
    else if($months <=12){
        if ($months==1){
            return "a month ago";
        } else {
            return "$months months ago";
        }
    }
    //Years
    else{
        if($years==1){
            return "one year ago";
        }else{
            return "$years years ago";
        }
    }
}

function timeAgoHindi($timestamp) {
    if ($timestamp == '') {
        $timestamp = "1407873516";
    }
    $timestamp = (int) $timestamp;
    $current_time = time();
    $diff = $current_time - $timestamp;
    $intervals = array(
        'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute' => 60
    );
    if ($diff == 0) {
        return 'अभी';
    }
    if ($diff < 60) {
        return $diff == 1 ? $diff . ' सेकंड पूर्व' : $diff . ' सेकंड पूर्व';
    }
    if ($diff >= 60 && $diff < $intervals['hour']) {
        $diff = floor($diff / $intervals['minute']);
        return $diff == 1 ? $diff . ' मिनट पूर्व' : $diff . ' मिनट पूर्व';
    }
    if ($diff >= $intervals['hour'] && $diff < $intervals['day']) {
        $diff = floor($diff / $intervals['hour']);
        return $diff == 1 ? $diff . ' घंटा पूर्व' : $diff . ' घंटा पूर्व';
    }
    if ($diff >= $intervals['day'] && $diff < $intervals['week']) {
        $diff = floor($diff / $intervals['day']);
        return $diff == 1 ? $diff . ' दिन पूर्व' : $diff . ' दिन पूर्व';
    }
    if ($diff >= $intervals['week'] && $diff < $intervals['month']) {
        $diff = floor($diff / $intervals['week']);
        return $diff == 1 ? $diff . ' सप्ताह पूर्व' : $diff . ' सप्ताह पूर्व';
    }
    if ($diff >= $intervals['month'] && $diff < $intervals['year']) {
        $diff = floor($diff / $intervals['month']);
        return $diff == 1 ? $diff . ' माह पूर्व' : $diff . ' माह पूर्व';
    }
    if ($diff >= $intervals['year']) {
        $diff = floor($diff / $intervals['year']);
        return $diff == 1 ? $diff . ' साल पूर्व' : $diff . ' साल पूर्व';
    }
}

function parseUserImage($fpath, $sname, $gender = "", $type = "small") {
    $CI = &get_instance();
    $image = "";
    if ($gender == "") {
        $gender = "Male";
    }
    if ($sname != '') {
        if (is_file("public/images/profile/$type/" . $sname)) {
            $image = base_url() . "public/images/profile/$type/" . $sname;
        } else {
            if ($gender == "Female") {
                $image = base_url() . "public/assets/image/female_img.jpg";
            } else {
                $image = base_url() . "public/assets/image/male_img.jpg";
            }
        }
    } elseif ($fpath != '') {
        $image = "https://graph.facebook.com/" . $fpath . "/picture?type=$type";
    } else {
        if ($gender == "Female") {
            $image = base_url() . "public/assets/image/female_img.jpg";
        } else {
            $image = base_url() . "public/assets/image/male_img.jpg";
        }
    }
    return $image;
}

function dmy_to_ymd($date) {
    $day = explode("-", $date);
    return $day[2] . "-" . $day[1] . "-" . $day[0];
}

function ymd_to_dmy($date) {
    $day = explode("-", $date);
    return $day[2] . "-" . $day[1] . "-" . $day[0];
}

function getage($bdate) {

    list($Y, $m, $d) = explode("-", $bdate);
    $years = date("Y") - $Y;
    if (date("md") < $m . $d) {
        $years--;
    }
    return $years;
}

function get_date_diff($start, $end) {
    $todate = strtotime($end);
    $fromdate = strtotime($start);
    $calculate_seconds = $todate - $fromdate; // Number of seconds between the two dates
    $days = floor($calculate_seconds / (24 * 60 * 60 )); // convert to days
    return $days;
}

function sub_days($date, $days) {
    return date('Y-m-d', strtotime($date . " - $days days"));
}

function getCMSContents($key) {
    $CI = &get_instance();
    $res = $CI->db->get_where("cms", array("cmskey" => $key));
    $data = $res->result_array();
    $return = array();
    foreach ($data as $dt) {
        $return[$dt['pk_cms_id']] = $dt['cmsval'];
    }
    return $return;
}

function getImplode($data) {
    if (is_array($data) and count($data) > 0) {
        return "'" . implode("', '", $data) . "'";
    } else {
        return "''";
    }
}

function getTopAds() {
    $CI = &get_instance();
    $res = $CI->db->query("SELECT * FROM ads WHERE ad_type='top' AND status='1' AND start_date <= '" . date("Y-m-d") . "' AND end_date >= '" . date("Y-m-d") . "'");
    $data = $res->result_array();
    $return = array();
    foreach ($data as $dt) {
        $return[$dt['pk_ad_id']] = array(
            'image' => site_url() . ADS_DIR . $dt['image'],
            'link' => $dt['ad_url']
        );
    }
    return $return;
}

function getClubName($club_id) {
    $CI = &get_instance();
    $res = $CI->db->query("SELECT title FROM master_club WHERE pk_club_id='$club_id'")->result_array();
    return $res[0]['title'];
}

function getClubNews($club_id = "0") {
    $CI = &get_instance();
    $filter = "";
    if ($club_id > 0) {
        $filter = " AND fk_club_id='$club_id'";
    }
    $res = $CI->db->query("SELECT * FROM post WHERE post_type='N' AND status='1' AND is_deleted='0' $filter ORDER BY pk_post_id LIMIT 0,5");
    $data = $res->result_array();
    if (count($data) > 0) {
        foreach ($data as $key => $ar) {
            $image = IMG_ARTICLE . $ar['file_name'];
            if (is_file($image)) {
                $data[$key]['thumb'] = base_url() . $image;
            } else {
                $data[$key]['thumb'] = base_url() . NO_IMAGE;
            }
        }
        return $data;
    }
    return false;
}

function getLatestClubNews() {
    $CI = &get_instance();
    $league_id = $CI->session->userdata('site_league');
    if ($league_id == "" or $league_id == "0") {
        $league_id = "1";
    }
    $clubs = $CI->db->get_where("master_club", array("fk_league_id" => $league_id))->result_array();
    $club_ids = array();
    foreach ($clubs as $clb) {
        $club_ids[] = $clb['pk_club_id'];
    }
    $clubs = $CI->db->query("SELECT count(*) AS cnt,fk_club_id FROM post WHERE fk_club_id IN ('" . implode("','", $club_ids) . "') AND post_type='N' GROUP BY fk_club_id ORDER by cnt DESC LIMIT 0,4")->result_array();

    $data = array();
    if (count($clubs) > 0) {
        foreach ($clubs as $key => $ar) {
            $data[$key]['club_name'] = getClubName($ar['fk_club_id']);
            $data[$key]['club_news'] = getClubNews($ar['fk_club_id']);
        }
        return $data;
    }
    return false;
}

function getUserById($id) {
    $CI = &get_instance();
    $data = $CI->db->query("SELECT u.firstname,u.lastname,up.profile_pic "
            . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id"
            . " WHERE u.pk_user_id='$id'");
    $arr = $data->result_array();
    $user = $arr[0];
    $user['profile_pic'] = (is_file(IMG_PROFILE . $user['profile_pic'])) ? site_url() . IMG_PROFILE . $user['profile_pic'] : site_url() . "public/fassets/images/user.jpg";
    $user['name'] = $user['firstname'] . ' ' . $user['lastname'];

    return $user;
}

function getTopAticles($type, $club_id, $user_id, $records) {
    $CI = &get_instance();
    $filter = "";
    if ($club_id > 0) {
        $filter.=" AND post.fk_club_id='$club_id' ";
    }
    if ($user_id > 0) {
        $filter.=" AND post.fk_user_id='$user_id' ";
    }

    $res = $CI->db->query("SELECT post.*,count(pv.fk_post_id) as ncount FROM post LEFT JOIN post_views pv ON post.pk_post_id=pv.fk_post_id WHERE post.fk_user_id>0 AND post.post_type='$type' AND post.status='1' AND post.is_deleted='0' $filter GROUP BY post.pk_post_id ORDER BY ncount DESC LIMIT 0,$records");
    $data = $res->result_array();
    $ids = array();

    if (count($data) > 0) {
        foreach ($data as $key => $ar) {
            $ids[] = $ar['pk_post_id'];
            $image = IMG_ARTICLE . $ar['file_name'];
            if (is_file($image)) {
                $data[$key]['thumb'] = base_url() . IMG_ARTICLE . "thumb/" . $ar['file_name'];
                $data[$key]['large'] = base_url() . $image;
            } else {
                $data[$key]['thumb'] = base_url() . NO_IMAGE;
                $data[$key]['image'] = base_url() . NO_IMAGE;
            }

            $data[$key]['user'] = getUserById($ar['fk_user_id']);
        }
    }
    if (count($data) > 0) {
        return $data;
    } else {
        return false;
    }
}

function getAticles($type, $sponser, $featured, $club_id, $user_id, $records, $extra = true) {
    $CI = &get_instance();
    $filter = "";
    if ($club_id > 0) {
        $filter.=" AND fk_club_id='$club_id' ";
    }
    if ($user_id > 0) {
        $filter.=" AND fk_user_id='$user_id' ";
    }
    if ($sponser > 0) {
        $filter.=" AND sponser_status='$sponser' ";
    }
    if ($featured > 0) {
        $filter.=" AND featured_status='$featured' ";
    }
    $res = $CI->db->query("SELECT * FROM post WHERE post.fk_user_id>0 AND post_type='$type' AND status='1' AND is_deleted='0' $filter ORDER BY pk_post_id DESC LIMIT 0,$records");

    $data = $res->result_array();
    $ids = array();
    $data2 = array();
    if (count($data) > 0) {
        foreach ($data as $key => $ar) {
            $ids[] = $ar['pk_post_id'];
            $image = IMG_ARTICLE . $ar['file_name'];
            if (is_file($image)) {
                $data[$key]['thumb'] = base_url() . IMG_ARTICLE . "thumb/" . $ar['file_name'];
                $data[$key]['large'] = base_url() . $image;
            } else {
                $data[$key]['thumb'] = base_url() . NO_IMAGE;
                $data[$key]['image'] = base_url() . NO_IMAGE;
                $data[$key]['large'] = base_url() . NO_IMAGE;
            }
            $data[$key]['user'] = getUserById($ar['fk_user_id']);
        }
    }

    if (count($data) < $records and $extra == true) {
        $rec = $records - count($data);

        $res2 = $CI->db->query("SELECT * FROM post WHERE post.fk_user_id>0 AND pk_post_id NOT IN ('" . implode("','", $ids) . "') AND  post_type='$type' AND status='1' AND is_deleted='0' ORDER BY pk_post_id DESC LIMIT 0,$rec");
        $data2 = $res2->result_array();
        if (count($data2) > 0) {
            foreach ($data2 as $key => $ar) {
                $image = IMG_ARTICLE . $ar['file_name'];
                if (is_file($image)) {
                    $data2[$key]['thumb'] = base_url() . IMG_ARTICLE . "thumb/" . $ar['file_name'];
                    $data2[$key]['large'] = base_url() . $image;
                } else {
                    $data2[$key]['thumb'] = base_url() . NO_IMAGE;
                    $data2[$key]['image'] = base_url() . NO_IMAGE;
                    $data2[$key]['large'] = base_url() . NO_IMAGE;
                }
                $data2[$key]['user'] = getUserById($ar['fk_user_id']);
            }
        }
    }
    $final_data = array_merge($data, $data2);
    if (count($final_data) > 0) {
        return $final_data;
    } else {
        return false;
    }
}

function getHomePageNews($type, $sponser, $featured, $club_id, $user_id, $records, $extra = true) {
    $CI = &get_instance();
    $filter = "";
    if ($club_id > 0) {
        $filter.=" AND fk_club_id='$club_id' ";
    }
    if ($user_id > 0) {
        $filter.=" AND fk_user_id='$user_id' ";
    }
    if ($sponser > 0) {
        $filter.=" AND sponser_status='$sponser' ";
    }
    if ($featured > 0) {
        $filter.=" AND featured_status='$featured' ";
    }
    $hp_news = $CI->db->query("SELECT pk_post_id FROM post WHERE post_type='N' ORDER BY pk_post_id DESC LIMIT 0,4")->result_array();
    $post_ids = array();
    foreach ($hp_news as $row) {
        $post_ids[] = $row['pk_post_id'];
    }
    $post_ids_string = implode("','", $post_ids);
    $filter.=" AND pk_post_id NOT IN('" . $post_ids_string . "') ";
    $date = date('Y-m-d', strtotime("-7 days"));
    $res = $CI->db->query("SELECT post.*,(SELECT count(*) FROM post_views WHERE post_views.fk_post_id=post.pk_post_id AND post_views.created_at>='$date') as pcount FROM post WHERE post_type='$type' AND status='1' AND post.fk_user_id>0 AND is_deleted='0' $filter ORDER BY pcount DESC LIMIT 0,$records");
    $data = $res->result_array();
    $ids = array();
    $data2 = array();
    if (count($data) > 0) {
        foreach ($data as $key => $ar) {
            if ($ar['fk_user_id'] > 0) {
                
            } else {
                return;
            }
            $ids[] = $ar['pk_post_id'];
            $image = IMG_ARTICLE . $ar['file_name'];
            if (is_file($image)) {
                $data[$key]['thumb'] = base_url() . IMG_ARTICLE . "thumb/" . $ar['file_name'];
                $data[$key]['large'] = base_url() . $image;
            } else {
                $data[$key]['thumb'] = base_url() . NO_IMAGE;
                $data[$key]['image'] = base_url() . NO_IMAGE;
                $data[$key]['large'] = base_url() . NO_IMAGE;
            }// echo "id:" . $ar['fk_user_id'] . "<br>";

            $data[$key]['user'] = getUserById($ar['fk_user_id']);
        }
    }

    if (count($data) < $records and $extra == true) {
        $rec = $records - count($data);
        $ids = array_merge($ids, $post_ids);
        $res2 = $CI->db->query("SELECT * FROM post WHERE post.fk_user_id>0 AND pk_post_id NOT IN ('" . implode("','", $ids) . "') AND  post_type='$type' AND status='1' AND is_deleted='0' ORDER BY pk_post_id DESC LIMIT 0,$rec");
        $data2 = $res2->result_array();
        if (count($data2) > 0) {
            foreach ($data2 as $key => $ar) {
                $image = IMG_ARTICLE . $ar['file_name'];
                if (is_file($image)) {
                    $data2[$key]['thumb'] = base_url() . IMG_ARTICLE . "thumb/" . $ar['file_name'];
                    $data2[$key]['large'] = base_url() . $image;
                } else {
                    $data2[$key]['thumb'] = base_url() . NO_IMAGE;
                    $data2[$key]['image'] = base_url() . NO_IMAGE;
                    $data2[$key]['large'] = base_url() . NO_IMAGE;
                }
                $data2[$key]['user'] = getUserById($ar['fk_user_id']);
            }
        }
    }
    $final_data = array_merge($data, $data2);
    if (count($final_data) > 0) {
        return $final_data;
    } else {
        return false;
    }
}

function getAds($type = "small", $page = 1) {
    $page -= 1;
    $start = $page * 1;
    $CI = &get_instance();
    $res = $CI->db->query("SELECT * FROM ads WHERE ad_type='$type' AND status='1' AND start_date <= '" . date("Y-m-d") . "' AND end_date >= '" . date("Y-m-d") . "'");
    $data = $res->result_array();
    $return = array();
    foreach ($data as $dt) {
        $return[$dt['pk_ad_id']] = array(
            'image' => site_url() . ADS_DIR . $dt['image'],
            'link' => $dt['ad_url']
        );
    }
    return $return;
}

function getSiteAds($type_id, $ref_id, $ad_size, $page = 1) {
    $page -= 1;
    $start = $page * 1;
    $filter = "SELECT fk_ad_id FROM ad_location WHERE type='$type_id'";
    if ($type_id == 5) {
        $filter = "SELECT fk_ad_id FROM ad_location WHERE  (type='$type_id' AND ref_id='0') OR (type='$type_id' AND ref_id='$ref_id')";
        //echo $filter;
    }
    if ($type_id == 6) {
        $filter = "SELECT fk_ad_id FROM ad_location WHERE  type='$type_id' AND ref_id='$ref_id'";
    }
    $CI = &get_instance();
    $res = $CI->db->query("SELECT * FROM ads WHERE ad_type='$ad_size' AND status='1' AND start_date <= '" . date("Y-m-d") . "' AND end_date >= '" . date("Y-m-d") . "' and pk_ad_id IN($filter)");
    $data = $res->result_array();
    $return = array();
    foreach ($data as $dt) {
        $return[$dt['pk_ad_id']] = array(
            'image' => site_url() . ADS_DIR . $dt['image'],
            'link' => $dt['ad_url']
        );
    }
    return $return;
}

function getMashHead($type_id, $ref_id, $refall = FALSE) {
    $filter = "SELECT fk_ad_id FROM content_placement WHERE type='$type_id' AND content_type='head'";
    $filter2 = "";
    if ($refall) {
        $filter2 = "(type='$type_id' AND ref_id='0') OR";
    }
    if ($type_id == 5) {
        $filter = "SELECT fk_ad_id FROM content_placement WHERE  ($filter2 (type='$type_id' AND ref_id='$ref_id'))  AND content_type='head'";
    }
    if ($type_id == 6) {
        $filter = "SELECT fk_ad_id FROM content_placement WHERE  type='$type_id' AND ref_id='$ref_id'  AND content_type='head'";
    }
    $CI = &get_instance();
    $res = $CI->db->query("SELECT * FROM images WHERE image_type='head' and pk_image_id IN($filter)");
    $data = $res->result_array();
    $return = array();
    foreach ($data as $dt) {
        $return[] = array(
            'image' => site_url() . MEDIA_DIR . $dt['path']
        );
    }
    return $return;
}

function getBackground($type_id, $ref_id, $refall = FALSE) {
    $filter = "SELECT fk_ad_id FROM content_placement WHERE type='$type_id' AND content_type='bg'";
    $filter2 = "";
    if ($refall) {
        $filter2 = "(type='$type_id' AND ref_id='0') OR";
    }
    if ($type_id == 5) {
        $filter = "SELECT fk_ad_id FROM content_placement WHERE  ($filter2 (type='$type_id' AND ref_id='$ref_id'))  AND content_type='bg'";
    }
    if ($type_id == 6) {
        $filter = "SELECT fk_ad_id FROM content_placement WHERE  type='$type_id' AND ref_id='$ref_id'  AND content_type='bg'";
    }
    $CI = &get_instance();
    $res = $CI->db->query("SELECT * FROM images WHERE image_type='bg' and pk_image_id IN($filter)");
    $data = $res->result_array();
    $return = array();
    foreach ($data as $dt) {
        $return[] = array(
            'image' => site_url() . MEDIA_DIR . $dt['path'],
            'color' => $dt['color']
        );
    }
    return $return;
}

function getCMSContent($key) {
    $CI = &get_instance();
    $res = $CI->db->get_where("cms", array("cmskey" => $key));
    $data = $res->result_array();
    if (count($data) > 0) {
        return $data[0]['cmsval'];
    } else {
        return "";
    }
}

function getMenus() {
    $CI = &get_instance();
    $res = $CI->db->query("SELECT pk_page_id,slug,title,menu_order FROM pages WHERE status='1' ORDER BY menu_order");
    $data = $res->result_array();
    return $data;
}

function getEmailFormat($key) {
    $CI = &get_instance();
    $res = $CI->db->get_where("emails", array("pk_email_id" => $key))->result_array();
    return $res[0]['content'];
}

function get_time_ago($time_stamp) {
    $time_difference = strtotime('now') - $time_stamp;


    if ($time_difference >= 60 * 60 * 24 * 365.242199) {
        /*
         * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 365.242199 days/year
         * This means that the time difference is 1 year or more
         */
        return get_time_ago_string($time_stamp, 60 * 60 * 24 * 365.242199, 'year');
    } elseif ($time_difference >= 60 * 60 * 24 * 30.4368499) {
        /*
         * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 30.4368499 days/month
         * This means that the time difference is 1 month or more
         */
        return get_time_ago_string($time_stamp, 60 * 60 * 24 * 30.4368499, 'month');
    } elseif ($time_difference >= 60 * 60 * 24 * 7) {
        /*
         * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 7 days/week
         * This means that the time difference is 1 week or more
         */
        return get_time_ago_string($time_stamp, 60 * 60 * 24 * 7, 'week');
    } elseif ($time_difference >= 60 * 60 * 24) {
        /*
         * 60 seconds/minute * 60 minutes/hour * 24 hours/day
         * This means that the time difference is 1 day or more
         */
        return get_time_ago_string($time_stamp, 60 * 60 * 24, 'day');
    } elseif ($time_difference >= 60 * 60) {
        /*
         * 60 seconds/minute * 60 minutes/hour
         * This means that the time difference is 1 hour or more
         */
        return get_time_ago_string($time_stamp, 60 * 60, 'hour');
    } else {
        /*
         * 60 seconds/minute
         * This means that the time difference is a matter of minutes
         */
        return get_time_ago_string($time_stamp, 60, 'min');
    }
}

function get_time_ago_string($time_stamp, $divisor, $time_unit) {
    $time_difference = strtotime("now") - $time_stamp;
    $time_units = floor($time_difference / $divisor);

    settype($time_units, 'string');

    if ($time_units === '0') {
        if ($time_unit == 'min') {
            return 'just now';
        }
        return 'less than 1 ' . $time_unit . '';
    } elseif ($time_units === '1') {
        return '1 ' . $time_unit . '';
    } else {
        /*
         * More than "1" $time_unit. This is the "plural" message.
         */
        // TODO: This pluralizes the time unit, which is done by adding "s" at the end; this will not work for i18n!
        return $time_units . ' ' . $time_unit . 's';
    }
}

function resizeFile($path, $width) {
    $CI = & get_instance();
    //$data = $CI->upload->data();
    $img_info = getimagesize($path);
    $org_wid = $img_info[0];
    $org_ht = $img_info[1];
    $config['file_name'] = basename($path);
    $config['image_library'] = 'gd2';
    $config['source_image'] = $path;
    $config['create_thumb'] = FALSE;
    $config['maintain_ratio'] = TRUE;
    $config['width'] = $width;
    $height = intval(($width * $org_ht) / $org_wid);
    $config['height'] = $height;
    $CI->load->library('image_lib', $config);
    $CI->image_lib->initialize($config);
    $CI->image_lib->resize();
    return basename($path);
}

function hindiMonthName($id = "") {
    $month = array(
        '1' => "जनवरी",
        '2' => "फ़रवरी",
        '3' => "मार्च",
        '4' => "अप्रैल",
        '5' => "मई",
        '6' => "जून",
        '7' => "जुलाई",
        '8' => "अगस्त",
        '9' => "सितम्बर",
        '10' => "अक्टूबर",
        '11' => "नवम्बर",
        '12' => "दिसम्बर",
    );
    return $month[$id];
}

function makeClickableLinks($s) {
    return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $s);
}

function setImageRatio($file, $ratio = "") {
    $CI = & get_instance();
    // $file = IMG_ARTICLE ."81_147046061210.png";
    $size = getimagesize($file);
    $height = $size[1];
    $width = $size[0];
    if ($ratio == "") {
        $ratio = 800 / 500;
    }

    $new_height = $width / $ratio;
    if ($height > $new_height) {
        $extra = $height - $new_height;
        $topOffset = floor($extra / 2);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $file;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['x_axis'] = '0';
        $config['y_axis'] = $topOffset;
        $config['width'] = $width;
        $config['height'] = $new_height;
        $CI->load->library('image_lib', $config);
        $CI->image_lib->initialize($config);
        if (!$CI->image_lib->crop()) {
            echo $CI->image_lib->display_errors();
        }
    } elseif ($height < $new_height) {
        $new_width = $ratio * $height;
        $extra = $width - $new_width;
        $leftOffset = floor($extra / 2);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $file;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['x_axis'] = $leftOffset;
        $config['y_axis'] = '0';
        $config['width'] = $new_width;
        $config['height'] = $height;
        $CI->load->library('image_lib', $config);
        $CI->image_lib->initialize($config);
        if (!$CI->image_lib->crop()) {
            echo $CI->image_lib->display_errors();
        }
    }
}

function arrangeClubs($clubs_raw) {
    $clubs = array();
    foreach ($clubs_raw as $key => $club) {
        $clubs[$key]['name'] = $club['club_name'];
        $clubs[$key]['slug'] = $club['club_slug'];
        $clubs[$key]['logo'] = site_url().'public/images/club/'.$club['club_logo'];
    }

    return $clubs;
}